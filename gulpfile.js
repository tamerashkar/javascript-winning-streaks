var gulp   = require("gulp");
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var umd    = require("gulp-umd");
var Server = require('karma').Server;
var header = require('gulp-header');
var pkg    = require('./package.json');
var banner = ['/**',
  ' * <%= pkg.name %>',
  ' * <%= pkg.description %>',
  ' * @version v<%= pkg.version %>',
  ' */',
  ''].join('\n');

gulp.task('test', function (done) {
  return new Server({
    configFile:  __dirname + '/karma.conf.js',
    singleRun:   true
  }, done).start();
});

gulp.task("bundle", function() {
  return gulp.src('src/*.js')
    .pipe(concat('streaker.js'))
    .pipe(umd(
      {
        dependencies: function(file) {
          return [
            {
              name:    'lodash',
              amd:     'lodash',
              cjs:     'lodash',
              global:  '_',
              param:   '_'
            }
          ];
        }
      }
    ))
    .pipe(header(banner, { pkg : pkg } ))
    .pipe(gulp.dest('build'))
    .pipe(uglify())
    .pipe(header(banner, { pkg : pkg } ))
    .pipe(concat('streaker.min.js'))
    .pipe(gulp.dest('build'));
});
