describe('Streaks calculator', function() {
  it('should vomit if you throw invalid data at it', function() {
    expect(function() { Streaker(); }).toThrow(new TypeError('The `data` parameter must be an array.'));
  });

  it('should calculate streaks', function() {
    var streaker = Streaker(mockStockData.data, mockStockData.options);
    expect(streaker.get()).toEqual(mockStockData.result);
  });

  it('should calculate streaks with personal streak `start` and `end` methods', function() {
    var data = [{
      date: "2006-11-14",
      score: 14
    }, {
      date: "2006-11-25",
      score: 20
    }];
    var options = {
      streaks: {
        start: function(streak, today) {
          streak.won = true;
          streak.start = today.date;
          return streak;
        },
        end: function(streak, today, pending) {
          streak.end = today.date;
          return streak;
        }
      }
    };
    var streaker = Streaker(data, options);
    expect(streaker.get()).toEqual([{
      streakLength: 1,
      won: true,
      start: '2006-11-14',
      positive: true,
      end: '2006-11-25'
    }]);
  });

  it('should be able to calculate streaks with personal `compare` method', function() {

    var data = [
      {score: 120, date: "2006-03-27T00:00:00-05:00"},
      {score: 125, date: "2006-03-28T00:00:00-05:00"},
      {score: 130, date: "2006-03-29T00:00:00-05:00"},
      {score: 125, date: "2006-03-30T00:00:00-05:00"},
      {score: 140, date: "2006-03-31T00:00:00-05:00"}
    ];

    function compare(today, key) {
      var tomorrow = data[key+1];
      var change = tomorrow.score - today.score;
      if (Math.abs(change) >= 5) {
        return change;
      }
      return 0;
    }
    var result = [{
      change: 10,
      endDate: "2006-03-29T00:00:00-05:00",
      endPrice: 130,
      positive: true,
      pending: false,
      startDate: "2006-03-27T00:00:00-05:00",
      startPrice: 120,
      streakLength: 2
    },
    {
      change: -5,
      endDate: "2006-03-30T00:00:00-05:00",
      endPrice: 125,
      positive: false,
      pending: false,
      startDate: "2006-03-29T00:00:00-05:00",
      startPrice: 130,
      streakLength: -1
    },
    {
      change: 140-125,
      endDate: "2006-03-31T00:00:00-05:00",
      endPrice: 140,
      positive: true,
      pending: true,
      startDate: "2006-03-30T00:00:00-05:00",
      startPrice: 125,
      streakLength: 1
    }];

    expect(Streaker(data, { compare: compare }).get()).toEqual(result);
  });
});
