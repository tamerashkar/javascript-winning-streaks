var streaker;
describe('Streaker', function() {
  beforeEach(function() {
    streaker = Streaker(mockStockData.data, mockStockData.options);
  });

  it('calculates the winning/losing streaks and return a streaks collection object', function() {
    expect(streaker.streaks).toEqual(mockStockData.result);
  });

  describe('Streaker Collection', function() {
     it('averages the value of an object key from the collection', function() {
       var count = 0;
       for (var i = 0; i < mockStockData.result.length; i++) {
         count += mockStockData.result[i].change;
       }
       var result = count/mockStockData.result.length;
       var avg = streaker.avg('change');
       expect(avg).toEqual(result);
     });

    it('counts the number of streaks', function() {
      expect(streaker.count()).toEqual(mockStockData.result.length);
    });

    it('counts the number of streaks that pass the predicate', function() {
      var positiveCount = 0, negativeCount = 0;
      for (var i = 0; i < mockStockData.result.length; i++) {
        if (mockStockData.result[i].positive) { positiveCount++; }
        else { negativeCount++; }
      }

      expect(streaker.countWhere(function(s) { return s.positive; })).toEqual(positiveCount);
      expect(streaker.countWhere(function(s) { return !s.positive; })).toEqual(negativeCount);
      expect(streaker.winning().count()).toEqual(positiveCount);
      expect(streaker.losing().count()).toEqual(negativeCount);
    });

    it('returns the longest streak in the collection', function() {
      var longestStreak = mockStockData.result[0];
      for (var i = 1; i < mockStockData.result.length; i++) {
        if (mockStockData.result[i].streakLength > longestStreak.streakLength) {
          longestStreak = mockStockData.result[i];
        }
      }

      expect(streaker.longest()).toEqual(longestStreak);

    });

    it('groups the streaks by an object key from the collection', function() {
      var s, d, streak, grouped = {};
      for (var i = 0; i < mockStockData.result.length; i++) {
        d = mockStockData.result[i];
        s = parseInt(d.streakLength);
        if (! grouped[s]) { grouped[s] = []; }
        grouped[s].push(d);
      }

      var groupedBy = streaker.groupBy('streakLength').get();
      expect(groupedBy).toEqual(grouped);
    });

    it('return the array of the streak objects', function() {
      expect(streaker.get()).toEqual(mockStockData.result);
    });

   it('returns an array of the values of an object key from the collection', function() {
     var map = [];
     for (var i = 0; i < mockStockData.result.length; i++) {
       map.push(mockStockData.result[i].streakLength);
     }
     var arr = streaker.lists('streakLength');
     expect(arr).toEqual(map);
   });

  });
});

