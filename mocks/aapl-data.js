var aaplData = [{
  "symbol":        "AAPL",
  "timestamp":     "2006-02-24T00:  00:  00-05:  00",
  "tradingDay":    "2006-02-24",
  "open":          9.544009,
  "high":          9.643233,
  "low":           9.419648,
  "close":         9.454046,
  "volume":        144366032,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-02-27T00:  00:  00-05:  00",
  "tradingDay":    "2006-02-27",
  "open":          9.524164,
  "high":          9.541364,
  "low":           9.346885,
  "close":         9.391866,
  "volume":        213633664,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-02-28T00:  00:  00-05:  00",
  "tradingDay":    "2006-02-28",
  "open":          9.469923,
  "high":          9.578407,
  "low":           9.009523,
  "close":         9.06112,
  "volume":        342162752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-01T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-01",
  "open":          9.107424,
  "high":          9.193419,
  "low":           8.99894,
  "close":         9.141822,
  "volume":        206011504,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-02T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-02",
  "open":          9.127269,
  "high":          9.259567,
  "low":           9.084933,
  "close":         9.209294,
  "volume":        168878032,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-03T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-03",
  "open":          9.181512,
  "high":          9.248984,
  "low":           8.934114,
  "close":         8.95925,
  "volume":        199174688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-06T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-06",
  "open":          8.955281,
  "high":          8.95925,
  "low":           8.591461,
  "close":         8.662902,
  "volume":        246548640,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-07T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-07",
  "open":          8.699945,
  "high":          8.850766,
  "low":           8.609982,
  "close":         8.772709,
  "volume":        235670208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-08T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-08",
  "open":          8.770063,
  "high":          8.890454,
  "low":           8.645702,
  "close":         8.686716,
  "volume":        176400416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-09T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-09",
  "open":          8.729052,
  "high":          8.793878,
  "low":           8.441963,
  "close":         8.457839,
  "volume":        216033536,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-10T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-10",
  "open":          8.473716,
  "high":          8.531926,
  "low":           8.262037,
  "close":         8.359939,
  "volume":        281666976,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-13T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-13",
  "open":          8.606014,
  "high":          8.768741,
  "low":           8.571616,
  "close":         8.689362,
  "volume":        232540912,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-14T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-14",
  "open":          8.701268,
  "high":          8.906331,
  "low":           8.665547,
  "close":         8.906331,
  "volume":        173423808,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-15T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-15",
  "open":          8.957928,
  "high":          9.001586,
  "low":           8.668193,
  "close":         8.762126,
  "volume":        240878880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-16T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-16",
  "open":          8.844151,
  "high":          8.850766,
  "low":           8.50679,
  "close":         8.508112,
  "volume":        202455152,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-17T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-17",
  "open":          8.566324,
  "high":          8.67084,
  "low":           8.481653,
  "close":         8.554418,
  "volume":        219567216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-20T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-20",
  "open":          8.628505,
  "high":          8.660255,
  "low":           8.449902,
  "close":         8.465777,
  "volume":        163476608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-21T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-21",
  "open":          8.505466,
  "high":          8.512081,
  "low":           8.121801,
  "close":         8.177367,
  "volume":        363184192,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-22T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-22",
  "open":          8.223671,
  "high":          8.367876,
  "low":           8.105926,
  "close":         8.158845,
  "volume":        363474432,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-23T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-23",
  "open":          8.17869,
  "high":          8.189274,
  "low":           7.88631,
  "close":         7.959074,
  "volume":        385906304,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-24T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-24",
  "open":          7.970981,
  "high":          8.062266,
  "low":           7.809577,
  "close":         7.932614,
  "volume":        289448608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-27T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-27",
  "open":          7.98421,
  "high":          8.120479,
  "low":           7.858528,
  "close":         7.87308,
  "volume":        299335360,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-28T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-28",
  "open":          7.888956,
  "high":          7.956428,
  "low":           7.706384,
  "close":         7.767241,
  "volume":        369952224,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-29T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-29",
  "open":          7.822807,
  "high":          8.271298,
  "low":           7.629651,
  "close":         8.246162,
  "volume":        633711168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-30T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-30",
  "open":          8.310987,
  "high":          8.374491,
  "low":           8.140323,
  "close":         8.301727,
  "volume":        375506304,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-03-31T00:  00:  00-05:  00",
  "tradingDay":    "2006-03-31",
  "open":          8.367876,
  "high":          8.415504,
  "low":           8.234255,
  "close":         8.297758,
  "volume":        220295872,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-04-03T00:  00:  00-04:  00",
  "tradingDay":    "2006-04-03",
  "open":          8.423442,
  "high":          8.482976,
  "low":           8.283205,
  "close":         8.288498,
  "volume":        220335936,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-04-04T00:  00:  00-04:  00",
  "tradingDay":    "2006-04-04",
  "open":          8.215733,
  "high":          8.231609,
  "low":           8.076819,
  "close":         8.092695,
  "volume":        251662832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-04-05T00:  00:  00-04:  00",
  "tradingDay":    "2006-04-05",
  "open":          8.561032,
  "high":          8.891778,
  "low":           8.486945,
  "close":         8.891778,
  "volume":        603281472,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-04-06T00:  00:  00-04:  00",
  "tradingDay":    "2006-04-06",
  "open":          9.035984,
  "high":          9.532103,
  "low":           9.022753,
  "close":         9.42494,
  "volume":        719176320,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-04-07T00:  00:  00-04:  00",
  "tradingDay":    "2006-04-07",
  "open":          9.383928,
  "high":          9.420972,
  "low":           9.058475,
  "close":         9.233109,
  "volume":        417179520,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-04-10T00:  00:  00-04:  00",
  "tradingDay":    "2006-04-10",
  "open":          9.299257,
  "high":          9.383928,
  "low":           9.055828,
  "close":         9.084933,
  "volume":        244002880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-04-11T00:  00:  00-04:  00",
  "tradingDay":    "2006-04-11",
  "open":          9.127269,
  "high":          9.168283,
  "low":           8.873257,
  "close":         8.99497,
  "volume":        253619776,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-04-12T00:  00:  00-04:  00",
  "tradingDay":    "2006-04-12",
  "open":          8.997617,
  "high":          9.018785,
  "low":           8.771387,
  "close":         8.825629,
  "volume":        199881424,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-04-13T00:  00:  00-04:  00",
  "tradingDay":    "2006-04-13",
  "open":          8.776678,
  "high":          8.922207,
  "low":           8.70656,
  "close":         8.793878,
  "volume":        198389344,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-04-17T00:  00:  00-04:  00",
  "tradingDay":    "2006-04-17",
  "open":          8.79917,
  "high":          8.842827,
  "low":           8.513405,
  "close":         8.574262,
  "volume":        194990960,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-04-18T00:  00:  00-04:  00",
  "tradingDay":    "2006-04-18",
  "open":          8.606014,
  "high":          8.793878,
  "low":           8.571616,
  "close":         8.760803,
  "volume":        214624608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-04-19T00:  00:  00-04:  00",
  "tradingDay":    "2006-04-19",
  "open":          8.840181,
  "high":          8.863996,
  "low":           8.661579,
  "close":         8.685392,
  "volume":        294797888,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-04-20T00:  00:  00-04:  00",
  "tradingDay":    "2006-04-20",
  "open":          9.196065,
  "high":          9.260891,
  "low":           8.758157,
  "close":         8.947343,
  "volume":        450117184,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-04-21T00:  00:  00-04:  00",
  "tradingDay":    "2006-04-21",
  "open":          9.021431,
  "high":          9.080965,
  "low":           8.793878,
  "close":         8.869287,
  "volume":        213051648,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-04-24T00:  00:  00-04:  00",
  "tradingDay":    "2006-04-24",
  "open":          8.844151,
  "high":          8.853412,
  "low":           8.665547,
  "close":         8.698623,
  "volume":        190895680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-04-25T00:  00:  00-04:  00",
  "tradingDay":    "2006-04-25",
  "open":          8.726405,
  "high":          8.809752,
  "low":           8.673486,
  "close":         8.754188,
  "volume":        142830864,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-04-26T00:  00:  00-04:  00",
  "tradingDay":    "2006-04-26",
  "open":          8.817691,
  "high":          9.033338,
  "low":           8.784616,
  "close":         9.016139,
  "volume":        191925168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-04-27T00:  00:  00-04:  00",
  "tradingDay":    "2006-04-27",
  "open":          8.960574,
  "high":          9.242369,
  "low":           8.910299,
  "close":         9.17622,
  "volume":        228384400,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-04-28T00:  00:  00-04:  00",
  "tradingDay":    "2006-04-28",
  "open":          9.178865,
  "high":          9.432879,
  "low":           9.155051,
  "close":         9.312487,
  "volume":        205205744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-01T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-01",
  "open":          9.362761,
  "high":          9.46463,
  "low":           9.14976,
  "close":         9.207972,
  "volume":        202613872,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-02T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-02",
  "open":          9.280736,
  "high":          9.522842,
  "low":           9.275444,
  "close":         9.475215,
  "volume":        208390976,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-03T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-03",
  "open":          9.502997,
  "high":          9.518872,
  "low":           9.284704,
  "close":         9.411711,
  "volume":        185536576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-04T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-04",
  "open":          9.422295,
  "high":          9.643233,
  "low":           9.321748,
  "close":         9.410388,
  "volume":        232289216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-05T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-05",
  "open":          9.506966,
  "high":          9.558562,
  "low":           9.413034,
  "close":         9.510935,
  "volume":        152243664,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-08T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-08",
  "open":          9.656463,
  "high":          9.763625,
  "low":           9.488444,
  "close":         9.510935,
  "volume":        160603552,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-09T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-09",
  "open":          9.502997,
  "high":          9.599574,
  "low":           9.342916,
  "close":         9.397158,
  "volume":        143538352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-10T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-10",
  "open":          9.431556,
  "high":          9.436848,
  "low":           9.209294,
  "close":         9.34027,
  "volume":        126198768,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-11T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-11",
  "open":          9.36673,
  "high":          9.372021,
  "low":           8.93676,
  "close":         9.016139,
  "volume":        219444768,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-12T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-12",
  "open":          8.976449,
  "high":          9.08758,
  "low":           8.845473,
  "close":         8.956604,
  "volume":        173296080,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-15T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-15",
  "open":          8.912947,
  "high":          9.046567,
  "low":           8.879871,
  "close":         8.968512,
  "volume":        143017552,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-16T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-16",
  "open":          9.009523,
  "high":          9.029368,
  "low":           8.566324,
  "close":         8.596753,
  "volume":        252994672,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-17T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-17",
  "open":          8.561032,
  "high":          8.692007,
  "low":           8.476361,
  "close":         8.633797,
  "volume":        203638832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-18T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-18",
  "open":          8.689362,
  "high":          8.766095,
  "low":           8.350677,
  "close":         8.358616,
  "volume":        177816160,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-19T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-19",
  "open":          8.369199,
  "high":          8.583523,
  "low":           8.310987,
  "close":         8.534573,
  "volume":        266227632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-22T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-22",
  "open":          8.449902,
  "high":          8.465777,
  "low":           8.304373,
  "close":         8.385076,
  "volume":        194112640,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-23T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-23",
  "open":          8.580877,
  "high":          8.624536,
  "low":           8.334802,
  "close":         8.354647,
  "volume":        187560784,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-24T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-24",
  "open":          8.333479,
  "high":          8.420795,
  "low":           8.144292,
  "close":         8.379784,
  "volume":        247341536,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-25T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-25",
  "open":          8.501498,
  "high":          8.526634,
  "low":           8.373168,
  "close":         8.510758,
  "volume":        125196488,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-26T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-26",
  "open":          8.508112,
  "high":          8.541187,
  "low":           8.353323,
  "close":         8.407566,
  "volume":        116893288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-30T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-30",
  "open":          8.373168,
  "high":          8.374491,
  "low":           8.099311,
  "close":         8.099311,
  "volume":        152120464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-05-31T00:  00:  00-04:  00",
  "tradingDay":    "2006-05-31",
  "open":          8.170752,
  "high":          8.174721,
  "low":           7.764595,
  "close":         7.907478,
  "volume":        345849120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-01T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-01",
  "open":          7.918061,
  "high":          8.239547,
  "low":           7.874403,
  "close":         8.224994,
  "volume":        254492800,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-02T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-02",
  "open":          8.333479,
  "high":          8.348031,
  "low":           8.054329,
  "close":         8.157522,
  "volume":        185162432,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-05T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-05",
  "open":          8.09005,
  "high":          8.09005,
  "low":           7.933938,
  "close":         7.937906,
  "volume":        163539344,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-06T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-06",
  "open":          7.967012,
  "high":          8.021255,
  "low":           7.793701,
  "close":         7.900863,
  "volume":        196021216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-07T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-07",
  "open":          7.951136,
  "high":          7.990826,
  "low":           7.719614,
  "close":         7.74872,
  "volume":        202677376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-08T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-08",
  "open":          7.731521,
  "high":          8.060944,
  "low":           7.560856,
  "close":         8.038453,
  "volume":        377262944,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-09T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-09",
  "open":          8.094019,
  "high":          8.144292,
  "low":           7.818838,
  "close":         7.83736,
  "volume":        209471856,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-12T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-12",
  "open":          7.858528,
  "high":          7.902186,
  "low":           7.535719,
  "close":         7.541011,
  "volume":        193820880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-13T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-13",
  "open":          7.621713,
  "high":          7.818838,
  "low":           7.588639,
  "close":         7.716968,
  "volume":        291802368,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-14T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-14",
  "open":          7.710353,
  "high":          7.776502,
  "low":           7.499999,
  "close":         7.621713,
  "volume":        237126752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-15T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-15",
  "open":          7.5807,
  "high":          7.903509,
  "low":           7.507936,
  "close":         7.855882,
  "volume":        321388512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-16T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-16",
  "open":          7.800316,
  "high":          7.830744,
  "low":           7.609807,
  "close":         7.615098,
  "volume":        226300480,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-19T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-19",
  "open":          7.650819,
  "high":          7.697123,
  "low":           7.541011,
  "close":         7.567471,
  "volume":        194816352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-20T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-20",
  "open":          7.621713,
  "high":          7.719614,
  "low":           7.579378,
  "close":         7.603191,
  "volume":        181683920,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-21T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-21",
  "open":          7.638912,
  "high":          7.767241,
  "low":           7.5807,
  "close":         7.654788,
  "volume":        233230256,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-22T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-22",
  "open":          7.699769,
  "high":          7.904832,
  "low":           7.68257,
  "close":         7.882341,
  "volume":        261161808,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-23T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-23",
  "open":          7.900863,
  "high":          7.960397,
  "low":           7.769887,
  "close":         7.783117,
  "volume":        178222816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-26T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-26",
  "open":          7.828098,
  "high":          7.832068,
  "low":           7.72226,
  "close":         7.804285,
  "volume":        125941776,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-27T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-27",
  "open":          7.817515,
  "high":          7.834714,
  "low":           7.593931,
  "close":         7.597899,
  "volume":        148644240,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-28T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-28",
  "open":          7.579378,
  "high":          7.5807,
  "low":           7.330657,
  "close":         7.411359,
  "volume":        229747232,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-29T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-29",
  "open":          7.509259,
  "high":          7.817515,
  "low":           7.460309,
  "close":         7.801639,
  "volume":        236275648,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-06-30T00:  00:  00-04:  00",
  "tradingDay":    "2006-06-30",
  "open":          7.619067,
  "high":          7.640235,
  "low":           7.474862,
  "close":         7.576732,
  "volume":        199935088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-07-03T00:  00:  00-04:  00",
  "tradingDay":    "2006-07-03",
  "open":          7.609807,
  "high":          7.697123,
  "low":           7.585992,
  "close":         7.666695,
  "volume":        52578852,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-07-05T00:  00:  00-04:  00",
  "tradingDay":    "2006-07-05",
  "open":          7.560856,
  "high":          7.62039,
  "low":           7.4828,
  "close":         7.541011,
  "volume":        140063632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-07-06T00:  00:  00-04:  00",
  "tradingDay":    "2006-07-06",
  "open":          7.552918,
  "high":          7.593931,
  "low":           7.357116,
  "close":         7.378284,
  "volume":        170977824,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-07-07T00:  00:  00-04:  00",
  "tradingDay":    "2006-07-07",
  "open":          7.339917,
  "high":          7.481477,
  "low":           7.232756,
  "close":         7.329334,
  "volume":        215796192,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-07-10T00:  00:  00-04:  00",
  "tradingDay":    "2006-07-10",
  "open":          7.369023,
  "high":          7.473539,
  "low":           7.210265,
  "close":         7.276414,
  "volume":        143398512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-07-11T00:  00:  00-04:  00",
  "tradingDay":    "2006-07-11",
  "open":          7.290967,
  "high":          7.40739,
  "low":           7.214234,
  "close":         7.362408,
  "volume":        222806864,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-07-12T00:  00:  00-04:  00",
  "tradingDay":    "2006-07-12",
  "open":          7.298905,
  "high":          7.308166,
  "low":           7.001233,
  "close":         7.006525,
  "volume":        250334016,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-07-13T00:  00:  00-04:  00",
  "tradingDay":    "2006-07-13",
  "open":          6.883488,
  "high":          7.159991,
  "low":           6.801463,
  "close":         6.912593,
  "volume":        337414400,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-07-14T00:  00:  00-04:  00",
  "tradingDay":    "2006-07-14",
  "open":          6.945668,
  "high":          6.997264,
  "low":           6.63609,
  "close":         6.703562,
  "volume":        268071952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-07-17T00:  00:  00-04:  00",
  "tradingDay":    "2006-07-17",
  "open":          6.833215,
  "high":          7.02637,
  "low":           6.833215,
  "close":         6.928469,
  "volume":        276607200,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-07-18T00:  00:  00-04:  00",
  "tradingDay":    "2006-07-18",
  "open":          7.032985,
  "high":          7.124271,
  "low":           6.859674,
  "close":         6.998588,
  "volume":        270265472,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-07-19T00:  00:  00-04:  00",
  "tradingDay":    "2006-07-19",
  "open":          7.006525,
  "high":          7.286998,
  "low":           6.927146,
  "close":         7.157345,
  "volume":        378944768,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-07-20T00:  00:  00-04:  00",
  "tradingDay":    "2006-07-20",
  "open":          8.064913,
  "high":          8.148261,
  "low":           7.900863,
  "close":         8.004056,
  "volume":        532483968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-07-21T00:  00:  00-04:  00",
  "tradingDay":    "2006-07-21",
  "open":          7.914093,
  "high":          8.09005,
  "low":           7.890279,
  "close":         8.033161,
  "volume":        240819168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-07-24T00:  00:  00-04:  00",
  "tradingDay":    "2006-07-24",
  "open":          8.104602,
  "high":          8.215733,
  "low":           7.994795,
  "close":         8.12577,
  "volume":        195172368,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-07-25T00:  00:  00-04:  00",
  "tradingDay":    "2006-07-25",
  "open":          8.173397,
  "high":          8.21441,
  "low":           8.041099,
  "close":         8.193242,
  "volume":        159078224,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-07-26T00:  00:  00-04:  00",
  "tradingDay":    "2006-07-26",
  "open":          8.202503,
  "high":          8.551771,
  "low":           8.160168,
  "close":         8.449902,
  "volume":        242587136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-07-27T00:  00:  00-04:  00",
  "tradingDay":    "2006-07-27",
  "open":          8.534573,
  "high":          8.602044,
  "low":           8.31628,
  "close":         8.387721,
  "volume":        198439232,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-07-28T00:  00:  00-04:  00",
  "tradingDay":    "2006-07-28",
  "open":          8.459162,
  "high":          8.689362,
  "low":           8.400951,
  "close":         8.677454,
  "volume":        186708928,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-07-31T00:  00:  00-04:  00",
  "tradingDay":    "2006-07-31",
  "open":          8.841505,
  "high":          9.079641,
  "low":           8.768741,
  "close":         8.991002,
  "volume":        241095808,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-01T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-01",
  "open":          8.893102,
  "high":          8.987033,
  "low":           8.72376,
  "close":         8.887809,
  "volume":        192158736,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-02T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-02",
  "open":          8.949989,
  "high":          9.086257,
  "low":           8.931468,
  "close":         9.017462,
  "volume":        148728128,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-03T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-03",
  "open":          8.984387,
  "high":          9.260891,
  "low":           8.971157,
  "close":         9.206648,
  "volume":        227137984,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-04T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-04",
  "open":          8.870611,
  "high":          9.076996,
  "low":           8.594107,
  "close":         9.035984,
  "volume":        500274240,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-07T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-07",
  "open":          8.95925,
  "high":          9.207972,
  "low":           8.772709,
  "close":         8.891778,
  "volume":        339086400,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-08T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-08",
  "open":          8.875902,
  "high":          8.878549,
  "low":           8.534573,
  "close":         8.570292,
  "volume":        269387904,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-09T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-09",
  "open":          8.656287,
  "high":          8.678778,
  "low":           8.387721,
  "close":         8.412858,
  "volume":        258083920,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-10T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-10",
  "open":          8.367876,
  "high":          8.574262,
  "low":           8.295113,
  "close":         8.476361,
  "volume":        188380912,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-11T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-11",
  "open":          8.365231,
  "high":          8.484299,
  "low":           8.279237,
  "close":         8.420795,
  "volume":        209896656,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-14T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-14",
  "open":          8.473716,
  "high":          8.628505,
  "low":           8.414181,
  "close":         8.459162,
  "volume":        193734704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-15T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-15",
  "open":          8.64438,
  "high":          8.797846,
  "low":           8.572939,
  "close":         8.791231,
  "volume":        232626320,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-16T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-16",
  "open":          8.877225,
  "high":          9.005555,
  "low":           8.775355,
  "close":         8.993649,
  "volume":        210966208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-17T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-17",
  "open":          8.996294,
  "high":          9.083611,
  "low":           8.887809,
  "close":         8.942051,
  "volume":        156883184,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-18T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-18",
  "open":          8.957928,
  "high":          9.049213,
  "low":           8.898394,
  "close":         8.984387,
  "volume":        144791584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-21T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-21",
  "open":          8.797846,
  "high":          8.905007,
  "low":           8.751542,
  "close":         8.805784,
  "volume":        142101456,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-22T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-22",
  "open":          8.82166,
  "high":          9.03863,
  "low":           8.797846,
  "close":         8.946021,
  "volume":        155819680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-23T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-23",
  "open":          8.996294,
  "high":          9.082288,
  "low":           8.856058,
  "close":         8.905007,
  "volume":        144821808,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-24T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-24",
  "open":          8.981741,
  "high":          9.021431,
  "low":           8.767417,
  "close":         8.971157,
  "volume":        176882656,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-25T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-25",
  "open":          8.908977,
  "high":          9.135208,
  "low":           8.905007,
  "close":         9.095518,
  "volume":        146957888,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-28T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-28",
  "open":          9.062443,
  "high":          9.076996,
  "low":           8.82166,
  "close":         8.86135,
  "volume":        199286560,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-29T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-29",
  "open":          8.862672,
  "high":          8.898394,
  "low":           8.615274,
  "close":         8.7952,
  "volume":        255783824,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-30T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-30",
  "open":          8.908977,
  "high":          8.97248,
  "low":           8.82166,
  "close":         8.858704,
  "volume":        183640112,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-08-31T00:  00:  00-04:  00",
  "tradingDay":    "2006-08-31",
  "open":          8.901039,
  "high":          9.035984,
  "low":           8.819015,
  "close":         8.976449,
  "volume":        155169632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-09-01T00:  00:  00-04:  00",
  "tradingDay":    "2006-09-01",
  "open":          9.059797,
  "high":          9.082288,
  "low":           8.97248,
  "close":         9.046567,
  "volume":        110308936,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-09-05T00:  00:  00-04:  00",
  "tradingDay":    "2006-09-05",
  "open":          9.124623,
  "high":          9.459338,
  "low":           9.069058,
  "close":         9.456693,
  "volume":        273512672,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-09-06T00:  00:  00-04:  00",
  "tradingDay":    "2006-09-06",
  "open":          9.403773,
  "high":          9.484475,
  "low":           9.221201,
  "close":         9.264859,
  "volume":        262992512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-09-07T00:  00:  00-04:  00",
  "tradingDay":    "2006-09-07",
  "open":          9.34027,
  "high":          9.72129,
  "low":           9.293965,
  "close":         9.631327,
  "volume":        342305632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-09-08T00:  00:  00-04:  00",
  "tradingDay":    "2006-09-08",
  "open":          9.706737,
  "high":          9.733196,
  "low":           9.513581,
  "close":         9.594282,
  "volume":        241903840,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-09-11T00:  00:  00-04:  00",
  "tradingDay":    "2006-09-11",
  "open":          9.582376,
  "high":          9.754364,
  "low":           9.448754,
  "close":         9.591637,
  "volume":        256238096,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-09-12T00:  00:  00-04:  00",
  "tradingDay":    "2006-09-12",
  "open":          9.632649,
  "high":          9.71732,
  "low":           9.452724,
  "close":         9.608835,
  "volume":        454993280,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-09-13T00:  00:  00-04:  00",
  "tradingDay":    "2006-09-13",
  "open":          9.637941,
  "high":          9.83242,
  "low":           9.565178,
  "close":         9.816544,
  "volume":        309448864,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-09-14T00:  00:  00-04:  00",
  "tradingDay":    "2006-09-14",
  "open":          9.753041,
  "high":          9.878724,
  "low":           9.718643,
  "close":         9.812575,
  "volume":        216596656,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-09-15T00:  00:  00-04:  00",
  "tradingDay":    "2006-09-15",
  "open":          9.869463,
  "high":          9.919738,
  "low":           9.696153,
  "close":         9.803314,
  "volume":        265451360,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-09-18T00:  00:  00-04:  00",
  "tradingDay":    "2006-09-18",
  "open":          9.763625,
  "high":          9.903861,
  "low":           9.697476,
  "close":         9.775532,
  "volume":        190405120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-09-19T00:  00:  00-04:  00",
  "tradingDay":    "2006-09-19",
  "open":          9.803314,
  "high":          9.837712,
  "low":           9.631327,
  "close":         9.759655,
  "volume":        191672704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-09-20T00:  00:  00-04:  00",
  "tradingDay":    "2006-09-20",
  "open":          9.840358,
  "high":          10.01235,
  "low":           9.81919,
  "close":         9.95678,
  "volume":        222057792,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-09-21T00:  00:  00-04:  00",
  "tradingDay":    "2006-09-21",
  "open":          9.955458,
  "high":          10.06262,
  "low":           9.79273,
  "close":         9.876079,
  "volume":        214394816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-09-22T00:  00:  00-04:  00",
  "tradingDay":    "2006-09-22",
  "open":          9.829775,
  "high":          9.835066,
  "low":           9.602221,
  "close":         9.657786,
  "volume":        179673328,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-09-25T00:  00:  00-04:  00",
  "tradingDay":    "2006-09-25",
  "open":          9.764948,
  "high":          10.03616,
  "low":           9.753041,
  "close":         10.02161,
  "volume":        231981568,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-09-26T00:  00:  00-04:  00",
  "tradingDay":    "2006-09-26",
  "open":          10.0785,
  "high":          10.29017,
  "low":           10.06791,
  "close":         10.26768,
  "volume":        297804736,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-09-27T00:  00:  00-04:  00",
  "tradingDay":    "2006-09-27",
  "open":          10.20947,
  "high":          10.24916,
  "low":           10.03087,
  "close":         10.10892,
  "volume":        218533184,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-09-28T00:  00:  00-04:  00",
  "tradingDay":    "2006-09-28",
  "open":          10.18963,
  "high":          10.25048,
  "low":           10.04807,
  "close":         10.1883,
  "volume":        195459600,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-09-29T00:  00:  00-04:  00",
  "tradingDay":    "2006-09-29",
  "open":          10.20153,
  "high":          10.25577,
  "low":           10.14464,
  "close":         10.18433,
  "volume":        109565920,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-02T00:  00:  00-04:  00",
  "tradingDay":    "2006-10-02",
  "open":          9.935613,
  "high":          10.03748,
  "low":           9.829775,
  "close":         9.903861,
  "volume":        192468640,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-03T00:  00:  00-04:  00",
  "tradingDay":    "2006-10-03",
  "open":          9.849619,
  "high":          9.915768,
  "low":           9.682923,
  "close":         9.800669,
  "volume":        213501392,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-04T00:  00:  00-04:  00",
  "tradingDay":    "2006-10-04",
  "open":          9.803314,
  "high":          9.98324,
  "low":           9.678954,
  "close":         9.972656,
  "volume":        226468272,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-05T00:  00:  00-04:  00",
  "tradingDay":    "2006-10-05",
  "open":          9.860203,
  "high":          10.07585,
  "low":           9.807283,
  "close":         9.899893,
  "volume":        184594016,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-06T00:  00:  00-04:  00",
  "tradingDay":    "2006-10-06",
  "open":          9.84565,
  "high":          9.927675,
  "low":           9.764948,
  "close":         9.81919,
  "volume":        126063464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-09T00:  00:  00-04:  00",
  "tradingDay":    "2006-10-09",
  "open":          9.763625,
  "high":          9.932967,
  "low":           9.727904,
  "close":         9.873432,
  "volume":        118325656,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-10T00:  00:  00-04:  00",
  "tradingDay":    "2006-10-10",
  "open":          9.861526,
  "high":          9.866817,
  "low":           9.66837,
  "close":         9.764948,
  "volume":        143529280,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-11T00:  00:  00-04:  00",
  "tradingDay":    "2006-10-11",
  "open":          9.713351,
  "high":          9.787439,
  "low":           9.604867,
  "close":         9.688215,
  "volume":        154364640,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-12T00:  00:  00-04:  00",
  "tradingDay":    "2006-10-12",
  "open":          9.738488,
  "high":          9.973979,
  "low":           9.737164,
  "close":         9.95678,
  "volume":        160075200,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-13T00:  00:  00-04:  00",
  "tradingDay":    "2006-10-13",
  "open":          10.00573,
  "high":          10.1711,
  "low":           9.887985,
  "close":         9.925029,
  "volume":        184702864,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-16T00:  00:  00-04:  00",
  "tradingDay":    "2006-10-16",
  "open":          9.94752,
  "high":          10.03881,
  "low":           9.894601,
  "close":         9.975303,
  "volume":        137394672,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-17T00:  00:  00-04:  00",
  "tradingDay":    "2006-10-17",
  "open":          9.927675,
  "high":          9.958103,
  "low":           9.795377,
  "close":         9.828451,
  "volume":        129871528,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-18T00:  00:  00-04:  00",
  "tradingDay":    "2006-10-18",
  "open":          9.889308,
  "high":          10.39733,
  "low":           9.778178,
  "close":         9.860203,
  "volume":        309226624,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-19T00:  00:  00-04:  00",
  "tradingDay":    "2006-10-19",
  "open":          10.48597,
  "high":          10.57726,
  "low":           10.34045,
  "close":         10.45025,
  "volume":        408857440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-20T00:  00:  00-04:  00",
  "tradingDay":    "2006-10-20",
  "open":          10.44761,
  "high":          10.58255,
  "low":           10.40792,
  "close":         10.57726,
  "volume":        172693648,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-23T00:  00:  00-04:  00",
  "tradingDay":    "2006-10-23",
  "open":          10.58255,
  "high":          10.83524,
  "low":           10.5508,
  "close":         10.77703,
  "volume":        224614144,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-24T00:  00:  00-04:  00",
  "tradingDay":    "2006-10-24",
  "open":          10.74396,
  "high":          10.80614,
  "low":           10.61033,
  "close":         10.72279,
  "volume":        125009792,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-25T00:  00:  00-04:  00",
  "tradingDay":    "2006-10-25",
  "open":          10.76248,
  "high":          10.84847,
  "low":           10.7175,
  "close":         10.80614,
  "volume":        131004568,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-26T00:  00:  00-04:  00",
  "tradingDay":    "2006-10-26",
  "open":          10.83524,
  "high":          10.92785,
  "low":           10.73337,
  "close":         10.87361,
  "volume":        116825264,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-27T00:  00:  00-04:  00",
  "tradingDay":    "2006-10-27",
  "open":          10.8154,
  "high":          10.90801,
  "low":           10.5852,
  "close":         10.63812,
  "volume":        160561984,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-30T00:  00:  00-05:  00",
  "tradingDay":    "2006-10-30",
  "open":          10.58255,
  "high":          10.70294,
  "low":           10.51773,
  "close":         10.63944,
  "volume":        134961536,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-10-31T00:  00:  00-05:  00",
  "tradingDay":    "2006-10-31",
  "open":          10.77571,
  "high":          10.80614,
  "low":           10.6143,
  "close":         10.72676,
  "volume":        135280512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-01T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-01",
  "open":          10.7294,
  "high":          10.76645,
  "low":           10.36691,
  "close":         10.47274,
  "volume":        165022352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-02T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-02",
  "open":          10.44099,
  "high":          10.49391,
  "low":           10.38543,
  "close":         10.44893,
  "volume":        125672680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-03T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-03",
  "open":          10.4992,
  "high":          10.5217,
  "low":           10.2915,
  "close":         10.35765,
  "volume":        116589432,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-06T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-06",
  "open":          10.44496,
  "high":          10.59181,
  "low":           10.37617,
  "close":         10.54551,
  "volume":        116114752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-07T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-07",
  "open":          10.64341,
  "high":          10.71617,
  "low":           10.60107,
  "close":         10.65135,
  "volume":        141988064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-08T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-08",
  "open":          10.58652,
  "high":          10.93976,
  "low":           10.56932,
  "close":         10.90801,
  "volume":        186400528,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-09T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-09",
  "open":          10.96754,
  "high":          11.20436,
  "low":           10.86435,
  "close":         11.02575,
  "volume":        249224400,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-10T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-10",
  "open":          11.05354,
  "high":          11.06015,
  "low":           10.91462,
  "close":         10.99665,
  "volume":        100961128,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-13T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-13",
  "open":          11.00988,
  "high":          11.1726,
  "low":           10.93314,
  "close":         11.15937,
  "volume":        121711944,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-14T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-14",
  "open":          11.21891,
  "high":          11.24537,
  "low":           11.09984,
  "close":         11.24537,
  "volume":        159067632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-15T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-15",
  "open":          11.25198,
  "high":          11.36444,
  "low":           11.11307,
  "close":         11.11968,
  "volume":        177360368,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-16T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-16",
  "open":          11.22817,
  "high":          11.41736,
  "low":           11.19509,
  "close":         11.32607,
  "volume":        187399792,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-17T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-17",
  "open":          11.26521,
  "high":          11.36973,
  "low":           11.24537,
  "close":         11.35782,
  "volume":        125604656,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-20T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-20",
  "open":          11.29829,
  "high":          11.50996,
  "low":           11.27183,
  "close":         11.43985,
  "volume":        153261072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-21T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-21",
  "open":          11.56553,
  "high":          11.72164,
  "low":           11.52452,
  "close":         11.72164,
  "volume":        168284672,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-22T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-22",
  "open":          11.77324,
  "high":          12.00608,
  "low":           11.62242,
  "close":         11.94787,
  "volume":        181488160,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-24T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-24",
  "open":          11.84468,
  "high":          12.31434,
  "low":           11.84071,
  "close":         12.12251,
  "volume":        140123344,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-27T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-27",
  "open":          12.23893,
  "high":          12.32492,
  "low":           11.84071,
  "close":         11.846,
  "volume":        290090336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-28T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-28",
  "open":          11.95449,
  "high":          12.16749,
  "low":           11.89495,
  "close":         12.14632,
  "volume":        279786368,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-29T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-29",
  "open":          12.30375,
  "high":          12.3236,
  "low":           11.93993,
  "close":         12.145,
  "volume":        312418656,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-11-30T00:  00:  00-05:  00",
  "tradingDay":    "2006-11-30",
  "open":          12.20056,
  "high":          12.26142,
  "low":           12.0471,
  "close":         12.12648,
  "volume":        234974048,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-12-01T00:  00:  00-05:  00",
  "tradingDay":    "2006-12-01",
  "open":          12.145,
  "high":          12.21512,
  "low":           11.92009,
  "close":         12.08149,
  "volume":        214703968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-12-04T00:  00:  00-05:  00",
  "tradingDay":    "2006-12-04",
  "open":          12.15558,
  "high":          12.17807,
  "low":           11.97301,
  "close":         12.05503,
  "volume":        191653056,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-12-05T00:  00:  00-05:  00",
  "tradingDay":    "2006-12-05",
  "open":          12.12648,
  "high":          12.21512,
  "low":           12.02196,
  "close":         12.07488,
  "volume":        178996816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-12-06T00:  00:  00-05:  00",
  "tradingDay":    "2006-12-06",
  "open":          11.99285,
  "high":          12.09075,
  "low":           11.8632,
  "close":         11.88437,
  "volume":        172331584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-12-07T00:  00:  00-05:  00",
  "tradingDay":    "2006-12-07",
  "open":          11.91083,
  "high":          11.97301,
  "low":           11.49673,
  "close":         11.51526,
  "volume":        271344832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-12-08T00:  00:  00-05:  00",
  "tradingDay":    "2006-12-08",
  "open":          11.54039,
  "high":          11.82616,
  "low":           11.50996,
  "close":         11.67666,
  "volume":        211812784,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-12-11T00:  00:  00-05:  00",
  "tradingDay":    "2006-12-11",
  "open":          11.76133,
  "high":          11.81425,
  "low":           11.64888,
  "close":         11.74149,
  "volume":        134037864,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-12-12T00:  00:  00-05:  00",
  "tradingDay":    "2006-12-12",
  "open":          11.72297,
  "high":          11.75339,
  "low":           11.31549,
  "close":         11.39619,
  "volume":        279337376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-12-13T00:  00:  00-05:  00",
  "tradingDay":    "2006-12-13",
  "open":          11.63565,
  "high":          11.78382,
  "low":           11.52981,
  "close":         11.78118,
  "volume":        231191696,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-12-14T00:  00:  00-05:  00",
  "tradingDay":    "2006-12-14",
  "open":          11.78118,
  "high":          11.90686,
  "low":           11.67666,
  "close":         11.71503,
  "volume":        224818976,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-12-15T00:  00:  00-05:  00",
  "tradingDay":    "2006-12-15",
  "open":          11.77721,
  "high":          11.80367,
  "low":           11.55362,
  "close":         11.60522,
  "volume":        199582848,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-12-18T00:  00:  00-05:  00",
  "tradingDay":    "2006-12-18",
  "open":          11.59331,
  "high":          11.64226,
  "low":           11.19112,
  "close":         11.30755,
  "volume":        194942592,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-12-19T00:  00:  00-05:  00",
  "tradingDay":    "2006-12-19",
  "open":          11.20965,
  "high":          11.46763,
  "low":           11.0628,
  "close":         11.41868,
  "volume":        245860048,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-12-20T00:  00:  00-05:  00",
  "tradingDay":    "2006-12-20",
  "open":          11.43985,
  "high":          11.46631,
  "low":           11.21097,
  "close":         11.21362,
  "volume":        153416016,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-12-21T00:  00:  00-05:  00",
  "tradingDay":    "2006-12-21",
  "open":          11.20568,
  "high":          11.30887,
  "low":           10.87493,
  "close":         10.96754,
  "volume":        245504784,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-12-22T00:  00:  00-05:  00",
  "tradingDay":    "2006-12-22",
  "open":          11.04163,
  "high":          11.11836,
  "low":           10.79555,
  "close":         10.87493,
  "volume":        165624784,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-12-26T00:  00:  00-05:  00",
  "tradingDay":    "2006-12-26",
  "open":          10.86832,
  "high":          10.92388,
  "low":           10.70162,
  "close":         10.78365,
  "volume":        132514032,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-12-27T00:  00:  00-05:  00",
  "tradingDay":    "2006-12-27",
  "open":          10.33912,
  "high":          10.84847,
  "low":           10.15655,
  "close":         10.78497,
  "volume":        522367456,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-12-28T00:  00:  00-05:  00",
  "tradingDay":    "2006-12-28",
  "open":          10.61298,
  "high":          10.74925,
  "low":           10.53757,
  "close":         10.69898,
  "volume":        304664224,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2006-12-29T00:  00:  00-05:  00",
  "tradingDay":    "2006-12-29",
  "open":          11.10645,
  "high":          11.29829,
  "low":           11.0284,
  "close":         11.2242,
  "volume":        290376832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-01-03T00:  00:  00-05:  00",
  "tradingDay":    "2007-01-03",
  "open":          11.41603,
  "high":          11.4544,
  "low":           10.83524,
  "close":         11.08661,
  "volume":        333763584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-01-04T00:  00:  00-05:  00",
  "tradingDay":    "2007-01-04",
  "open":          11.11968,
  "high":          11.37105,
  "low":           11.08926,
  "close":         11.33268,
  "volume":        231063952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-01-05T00:  00:  00-05:  00",
  "tradingDay":    "2007-01-05",
  "open":          11.34724,
  "high":          11.40413,
  "low":           11.16599,
  "close":         11.25198,
  "volume":        225426688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-01-08T00:  00:  00-05:  00",
  "tradingDay":    "2007-01-08",
  "open":          11.37237,
  "high":          11.44778,
  "low":           11.28241,
  "close":         11.30755,
  "volume":        215216448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-01-09T00:  00:  00-05:  00",
  "tradingDay":    "2007-01-09",
  "open":          11.4372,
  "high":          12.30111,
  "low":           11.26521,
  "close":         12.24687,
  "volume":        904794496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-01-10T00:  00:  00-05:  00",
  "tradingDay":    "2007-01-10",
  "open":          12.53528,
  "high":          12.93879,
  "low":           12.36329,
  "close":         12.83295,
  "volume":        797770560,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-01-11T00:  00:  00-05:  00",
  "tradingDay":    "2007-01-11",
  "open":          12.69271,
  "high":          12.80384,
  "low":           12.58158,
  "close":         12.67419,
  "volume":        389066560,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-01-12T00:  00:  00-05:  00",
  "tradingDay":    "2007-01-12",
  "open":          12.51411,
  "high":          12.57629,
  "low":           12.33418,
  "close":         12.51808,
  "volume":        354421408,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-01-16T00:  00:  00-05:  00",
  "tradingDay":    "2007-01-16",
  "open":          12.65831,
  "high":          12.86602,
  "low":           12.62789,
  "close":         12.84618,
  "volume":        335925344,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-01-17T00:  00:  00-05:  00",
  "tradingDay":    "2007-01-17",
  "open":          12.90704,
  "high":          12.91233,
  "low":           12.54454,
  "close":         12.56174,
  "volume":        452427104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-01-18T00:  00:  00-05:  00",
  "tradingDay":    "2007-01-18",
  "open":          12.18469,
  "high":          12.18601,
  "low":           11.78118,
  "close":         11.78382,
  "volume":        638776256,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-01-19T00:  00:  00-05:  00",
  "tradingDay":    "2007-01-19",
  "open":          11.72561,
  "high":          11.86056,
  "low":           11.65814,
  "close":         11.70841,
  "volume":        367664960,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-01-22T00:  00:  00-05:  00",
  "tradingDay":    "2007-01-22",
  "open":          11.79308,
  "high":          11.79573,
  "low":           11.33136,
  "close":         11.48218,
  "volume":        389764992,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-01-23T00:  00:  00-05:  00",
  "tradingDay":    "2007-01-23",
  "open":          11.34195,
  "high":          11.57744,
  "low":           11.31284,
  "close":         11.33798,
  "volume":        326241184,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-01-24T00:  00:  00-05:  00",
  "tradingDay":    "2007-01-24",
  "open":          11.46763,
  "high":          11.52981,
  "low":           11.38825,
  "close":         11.47027,
  "volume":        250646192,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-01-25T00:  00:  00-05:  00",
  "tradingDay":    "2007-01-25",
  "open":          11.52452,
  "high":          11.70841,
  "low":           11.38163,
  "close":         11.41074,
  "volume":        244699024,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-01-26T00:  00:  00-05:  00",
  "tradingDay":    "2007-01-26",
  "open":          11.52584,
  "high":          11.55892,
  "low":           11.24404,
  "close":         11.29564,
  "volume":        266417360,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-01-29T00:  00:  00-05:  00",
  "tradingDay":    "2007-01-29",
  "open":          11.41736,
  "high":          11.46366,
  "low":           11.31549,
  "close":         11.36973,
  "volume":        243531216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-01-30T00:  00:  00-05:  00",
  "tradingDay":    "2007-01-30",
  "open":          11.43455,
  "high":          11.44249,
  "low":           11.27844,
  "close":         11.31813,
  "volume":        155977648,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-01-31T00:  00:  00-05:  00",
  "tradingDay":    "2007-01-31",
  "open":          11.22685,
  "high":          11.37767,
  "low":           11.15937,
  "close":         11.34195,
  "volume":        230633104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-02-01T00:  00:  00-05:  00",
  "tradingDay":    "2007-02-01",
  "open":          11.4081,
  "high":          11.41339,
  "low":           11.21097,
  "close":         11.21097,
  "volume":        179084496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-02-02T00:  00:  00-05:  00",
  "tradingDay":    "2007-02-02",
  "open":          11.12895,
  "high":          11.27844,
  "low":           11.07338,
  "close":         11.21229,
  "volume":        167835696,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-02-05T00:  00:  00-05:  00",
  "tradingDay":    "2007-02-05",
  "open":          11.15276,
  "high":          11.2758,
  "low":           11.10513,
  "close":         11.10513,
  "volume":        156446288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-02-06T00:  00:  00-05:  00",
  "tradingDay":    "2007-02-06",
  "open":          11.1726,
  "high":          11.17525,
  "low":           10.96225,
  "close":         11.13291,
  "volume":        233435104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-02-07T00:  00:  00-05:  00",
  "tradingDay":    "2007-02-07",
  "open":          11.17657,
  "high":          11.42794,
  "low":           11.05354,
  "close":         11.39751,
  "volume":        288416096,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-02-08T00:  00:  00-05:  00",
  "tradingDay":    "2007-02-08",
  "open":          11.30226,
  "high":          11.44514,
  "low":           11.29961,
  "close":         11.40148,
  "volume":        183312816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-02-09T00:  00:  00-05:  00",
  "tradingDay":    "2007-02-09",
  "open":          11.36179,
  "high":          11.40413,
  "low":           11.00855,
  "close":         11.01649,
  "volume":        232320208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-02-12T00:  00:  00-05:  00",
  "tradingDay":    "2007-02-12",
  "open":          11.16996,
  "high":          11.26918,
  "low":           11.06412,
  "close":         11.22949,
  "volume":        195365872,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-02-13T00:  00:  00-05:  00",
  "tradingDay":    "2007-02-13",
  "open":          11.26654,
  "high":          11.28373,
  "low":           11.15276,
  "close":         11.20568,
  "volume":        155753920,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-02-14T00:  00:  00-05:  00",
  "tradingDay":    "2007-02-14",
  "open":          11.19642,
  "high":          11.33004,
  "low":           11.18848,
  "close":         11.28506,
  "volume":        137129360,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-02-15T00:  00:  00-05:  00",
  "tradingDay":    "2007-02-15",
  "open":          11.30358,
  "high":          11.32739,
  "low":           11.21626,
  "close":         11.27315,
  "volume":        98196168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-02-16T00:  00:  00-05:  00",
  "tradingDay":    "2007-02-16",
  "open":          11.27844,
  "high":          11.29961,
  "low":           11.20039,
  "close":         11.22288,
  "volume":        107993712,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-02-20T00:  00:  00-05:  00",
  "tradingDay":    "2007-02-20",
  "open":          11.19906,
  "high":          11.39883,
  "low":           11.13424,
  "close":         11.36444,
  "volume":        166796368,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-02-21T00:  00:  00-05:  00",
  "tradingDay":    "2007-02-21",
  "open":          11.37502,
  "high":          11.83939,
  "low":           11.37237,
  "close":         11.80102,
  "volume":        311689984,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-02-22T00:  00:  00-05:  00",
  "tradingDay":    "2007-02-22",
  "open":          12.0127,
  "high":          12.01402,
  "low":           11.71238,
  "close":         11.84203,
  "volume":        226346592,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-02-23T00:  00:  00-05:  00",
  "tradingDay":    "2007-02-23",
  "open":          11.79573,
  "high":          11.95184,
  "low":           11.75472,
  "close":         11.78382,
  "volume":        139857280,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-02-26T00:  00:  00-05:  00",
  "tradingDay":    "2007-02-26",
  "open":          11.88569,
  "high":          11.90686,
  "low":           11.59067,
  "close":         11.72826,
  "volume":        166554496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-02-27T00:  00:  00-05:  00",
  "tradingDay":    "2007-02-27",
  "open":          11.41736,
  "high":          11.52055,
  "low":           11.03501,
  "close":         11.10381,
  "volume":        309600768,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-02-28T00:  00:  00-05:  00",
  "tradingDay":    "2007-02-28",
  "open":          10.98077,
  "high":          11.32475,
  "low":           10.98077,
  "close":         11.19377,
  "volume":        248346096,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-01T00:  00:  00-05:  00",
  "tradingDay":    "2007-03-01",
  "open":          11.11704,
  "high":          11.68328,
  "low":           11.07999,
  "close":         11.5179,
  "volume":        382185920,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-02T00:  00:  00-05:  00",
  "tradingDay":    "2007-03-02",
  "open":          11.47954,
  "high":          11.58141,
  "low":           11.27315,
  "close":         11.29961,
  "volume":        232191696,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-05T00:  00:  00-05:  00",
  "tradingDay":    "2007-03-05",
  "open":          11.36311,
  "high":          11.72826,
  "low":           11.34591,
  "close":         11.42,
  "volume":        226681440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-06T00:  00:  00-05:  00",
  "tradingDay":    "2007-03-06",
  "open":          11.6158,
  "high":          11.68328,
  "low":           11.56288,
  "close":         11.6674,
  "volume":        195130048,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-07T00:  00:  00-05:  00",
  "tradingDay":    "2007-03-07",
  "open":          11.64888,
  "high":          11.77059,
  "low":           11.5695,
  "close":         11.60522,
  "volume":        167789584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-08T00:  00:  00-05:  00",
  "tradingDay":    "2007-03-08",
  "open":          11.72032,
  "high":          11.73752,
  "low":           11.57082,
  "close":         11.64226,
  "volume":        137997088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-09T00:  00:  00-05:  00",
  "tradingDay":    "2007-03-09",
  "open":          11.7481,
  "high":          11.75472,
  "low":           11.56288,
  "close":         11.63829,
  "volume":        121835904,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-12T00:  00:  00-04:  00",
  "tradingDay":    "2007-03-12",
  "open":          11.64226,
  "high":          11.90554,
  "low":           11.64094,
  "close":         11.88966,
  "volume":        196920688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-13T00:  00:  00-04:  00",
  "tradingDay":    "2007-03-13",
  "open":          11.82748,
  "high":          11.98624,
  "low":           11.69518,
  "close":         11.69518,
  "volume":        234240864,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-14T00:  00:  00-04:  00",
  "tradingDay":    "2007-03-14",
  "open":          11.72164,
  "high":          11.90686,
  "low":           11.63168,
  "close":         11.90686,
  "volume":        214993472,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-15T00:  00:  00-04:  00",
  "tradingDay":    "2007-03-15",
  "open":          11.90157,
  "high":          11.95449,
  "low":           11.81557,
  "close":         11.84997,
  "volume":        150949632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-16T00:  00:  00-04:  00",
  "tradingDay":    "2007-03-16",
  "open":          11.846,
  "high":          11.90554,
  "low":           11.8169,
  "close":         11.85262,
  "volume":        154162816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-19T00:  00:  00-04:  00",
  "tradingDay":    "2007-03-19",
  "open":          11.93861,
  "high":          12.11192,
  "low":           11.85262,
  "close":         12.05636,
  "volume":        192455024,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-20T00:  00:  00-04:  00",
  "tradingDay":    "2007-03-20",
  "open":          12.08811,
  "high":          12.15029,
  "low":           12.0471,
  "close":         12.10266,
  "volume":        132036328,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-21T00:  00:  00-04:  00",
  "tradingDay":    "2007-03-21",
  "open":          12.17013,
  "high":          12.43605,
  "low":           12.12515,
  "close":         12.41885,
  "volume":        185519952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-22T00:  00:  00-04:  00",
  "tradingDay":    "2007-03-22",
  "open":          12.40033,
  "high":          12.48368,
  "low":           12.30375,
  "close":         12.43076,
  "volume":        152585328,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-23T00:  00:  00-04:  00",
  "tradingDay":    "2007-03-23",
  "open":          12.35006,
  "high":          12.44531,
  "low":           12.34344,
  "close":         12.37255,
  "volume":        121828344,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-26T00:  00:  00-04:  00",
  "tradingDay":    "2007-03-26",
  "open":          12.4387,
  "high":          12.68742,
  "low":           12.34344,
  "close":         12.68081,
  "volume":        233540176,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-27T00:  00:  00-04:  00",
  "tradingDay":    "2007-03-27",
  "open":          12.66228,
  "high":          12.81046,
  "low":           12.56835,
  "close":         12.62921,
  "volume":        251461008,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-28T00:  00:  00-04:  00",
  "tradingDay":    "2007-03-28",
  "open":          12.54851,
  "high":          12.62127,
  "low":           12.3236,
  "close":         12.33551,
  "volume":        256098256,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-29T00:  00:  00-04:  00",
  "tradingDay":    "2007-03-29",
  "open":          12.46119,
  "high":          12.46119,
  "low":           12.20189,
  "close":         12.40298,
  "volume":        195949408,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-03-30T00:  00:  00-04:  00",
  "tradingDay":    "2007-03-30",
  "open":          12.4731,
  "high":          12.52602,
  "low":           12.27068,
  "close":         12.29185,
  "volume":        162190880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-04-02T00:  00:  00-04:  00",
  "tradingDay":    "2007-04-02",
  "open":          12.45457,
  "high":          12.46913,
  "low":           12.3064,
  "close":         12.38975,
  "volume":        135443024,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-04-03T00:  00:  00-04:  00",
  "tradingDay":    "2007-04-03",
  "open":          12.45457,
  "high":          12.59878,
  "low":           12.4043,
  "close":         12.5022,
  "volume":        157648112,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-04-04T00:  00:  00-04:  00",
  "tradingDay":    "2007-04-04",
  "open":          12.56041,
  "high":          12.58687,
  "low":           12.45325,
  "close":         12.47177,
  "volume":        128634168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-04-05T00:  00:  00-04:  00",
  "tradingDay":    "2007-04-05",
  "open":          12.45193,
  "high":          12.52602,
  "low":           12.37255,
  "close":         12.52602,
  "volume":        95992064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-04-09T00:  00:  00-04:  00",
  "tradingDay":    "2007-04-09",
  "open":          12.59613,
  "high":          12.60804,
  "low":           12.30905,
  "close":         12.38975,
  "volume":        111614320,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-04-10T00:  00:  00-04:  00",
  "tradingDay":    "2007-04-10",
  "open":          12.39636,
  "high":          12.47045,
  "low":           12.358,
  "close":         12.46913,
  "volume":        95210496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-04-11T00:  00:  00-04:  00",
  "tradingDay":    "2007-04-11",
  "open":          12.42282,
  "high":          12.42944,
  "low":           12.21512,
  "close":         12.24951,
  "volume":        148364560,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-04-12T00:  00:  00-04:  00",
  "tradingDay":    "2007-04-12",
  "open":          12.17675,
  "high":          12.21247,
  "low":           12.00211,
  "close":         12.19659,
  "volume":        179105664,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-04-13T00:  00:  00-04:  00",
  "tradingDay":    "2007-04-13",
  "open":          12.02593,
  "high":          12.09208,
  "low":           11.9148,
  "close":         11.93861,
  "volume":        194327312,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-04-16T00:  00:  00-04:  00",
  "tradingDay":    "2007-04-16",
  "open":          11.98756,
  "high":          12.10531,
  "low":           11.93993,
  "close":         12.09605,
  "volume":        161245280,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-04-17T00:  00:  00-04:  00",
  "tradingDay":    "2007-04-17",
  "open":          12.17146,
  "high":          12.21115,
  "low":           11.86717,
  "close":         11.95316,
  "volume":        203101408,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-04-18T00:  00:  00-04:  00",
  "tradingDay":    "2007-04-18",
  "open":          11.9267,
  "high":          12.01931,
  "low":           11.85394,
  "close":         11.95978,
  "volume":        125306848,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-04-19T00:  00:  00-04:  00",
  "tradingDay":    "2007-04-19",
  "open":          11.92274,
  "high":          12.07223,
  "low":           11.88437,
  "close":         11.94258,
  "volume":        115108688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-04-20T00:  00:  00-04:  00",
  "tradingDay":    "2007-04-20",
  "open":          12.0246,
  "high":          12.06297,
  "low":           11.97962,
  "close":         12.03519,
  "volume":        141551936,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-04-23T00:  00:  00-04:  00",
  "tradingDay":    "2007-04-23",
  "open":          12.11721,
  "high":          12.40959,
  "low":           12.09472,
  "close":         12.37123,
  "volume":        210797648,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-04-24T00:  00:  00-04:  00",
  "tradingDay":    "2007-04-24",
  "open":          12.43076,
  "high":          12.75225,
  "low":           12.07885,
  "close":         12.33551,
  "volume":        284958016,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-04-25T00:  00:  00-04:  00",
  "tradingDay":    "2007-04-25",
  "open":          12.46913,
  "high":          12.62127,
  "low":           12.40959,
  "close":         12.61466,
  "volume":        329174720,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-04-26T00:  00:  00-04:  00",
  "tradingDay":    "2007-04-26",
  "open":          13.44152,
  "high":          13.56059,
  "low":           13.00494,
  "close":         13.07638,
  "volume":        469714528,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-04-27T00:  00:  00-04:  00",
  "tradingDay":    "2007-04-27",
  "open":          12.98906,
  "high":          13.22323,
  "low":           12.92424,
  "close":         13.21926,
  "volume":        188961408,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-04-30T00:  00:  00-04:  00",
  "tradingDay":    "2007-04-30",
  "open":          13.23514,
  "high":          13.36214,
  "low":           13.18618,
  "close":         13.20338,
  "volume":        166673168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-01T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-01",
  "open":          13.1756,
  "high":          13.27615,
  "low":           13.03801,
  "close":         13.15973,
  "volume":        143790048,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-02T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-02",
  "open":          13.18354,
  "high":          13.30128,
  "low":           13.15973,
  "close":         13.28144,
  "volume":        136539776,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-03T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-03",
  "open":          13.32642,
  "high":          13.42168,
  "low":           13.23117,
  "close":         13.28276,
  "volume":        155706288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-04T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-04",
  "open":          13.33568,
  "high":          13.44152,
  "low":           13.29599,
  "close":         13.33701,
  "volume":        103172040,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-07T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-07",
  "open":          13.38728,
  "high":          13.80534,
  "low":           13.36347,
  "close":         13.74845,
  "volume":        232654288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-08T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-08",
  "open":          13.68892,
  "high":          13.91118,
  "low":           13.6823,
  "close":         13.89927,
  "volume":        211770448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-09T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-09",
  "open":          13.87943,
  "high":          14.15064,
  "low":           13.87678,
  "close":         14.14006,
  "volume":        193911584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-10T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-10",
  "open":          14.10698,
  "high":          14.39936,
  "low":           14.01305,
  "close":         14.20091,
  "volume":        323324288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-11T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-11",
  "open":          14.25383,
  "high":          14.43773,
  "low":           14.12683,
  "close":         14.38613,
  "volume":        176138880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-14T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-14",
  "open":          14.50255,
  "high":          14.55283,
  "low":           14.32131,
  "close":         14.46816,
  "volume":        176078416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-15T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-15",
  "open":          14.49594,
  "high":          14.57929,
  "low":           14.08714,
  "close":         14.22473,
  "volume":        257823904,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-16T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-16",
  "open":          14.35835,
  "high":          14.39804,
  "low":           13.6823,
  "close":         14.20091,
  "volume":        304306688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-17T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-17",
  "open":          14.17578,
  "high":          14.53563,
  "low":           14.17578,
  "close":         14.47874,
  "volume":        199076416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-18T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-18",
  "open":          14.58326,
  "high":          14.6375,
  "low":           14.5224,
  "close":         14.55547,
  "volume":        167633120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-21T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-21",
  "open":          14.59384,
  "high":          14.87696,
  "low":           14.55944,
  "close":         14.81478,
  "volume":        172809296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-22T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-22",
  "open":          14.88225,
  "high":          15.04895,
  "low":           14.81875,
  "close":         15.02116,
  "volume":        154578544,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-23T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-23",
  "open":          15.08467,
  "high":          15.21432,
  "low":           14.89548,
  "close":         14.93517,
  "volume":        246106448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-24T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-24",
  "open":          14.92459,
  "high":          15.14288,
  "low":           14.60178,
  "close":         14.64411,
  "volume":        239838800,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-25T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-25",
  "open":          14.81742,
  "high":          15.05292,
  "low":           14.75128,
  "close":         15.03175,
  "volume":        170869744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-29T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-29",
  "open":          15.14156,
  "high":          15.1958,
  "low":           14.90871,
  "close":         15.12833,
  "volume":        174217472,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-30T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-30",
  "open":          15.12171,
  "high":          15.72764,
  "low":           15.01984,
  "close":         15.71309,
  "volume":        399749248,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-05-31T00:  00:  00-04:  00",
  "tradingDay":    "2007-05-31",
  "open":          15.88507,
  "high":          16.1629,
  "low":           15.81496,
  "close":         16.03325,
  "volume":        350326880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-01T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-01",
  "open":          15.88243,
  "high":          16.03325,
  "low":           15.64958,
  "close":         15.66414,
  "volume":        238765472,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-04T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-04",
  "open":          15.69456,
  "high":          16.10469,
  "low":           15.59799,
  "close":         16.05177,
  "volume":        239673280,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-05T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-05",
  "open":          16.06235,
  "high":          16.2317,
  "low":           15.94196,
  "close":         16.22905,
  "volume":        248417888,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-06T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-06",
  "open":          16.1801,
  "high":          16.41162,
  "low":           16.13379,
  "close":         16.35738,
  "volume":        300622592,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-07T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-07",
  "open":          16.53598,
  "high":          16.8826,
  "low":           16.29785,
  "close":         16.41427,
  "volume":        517501184,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-08T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-08",
  "open":          16.6405,
  "high":          16.64711,
  "low":           16.17878,
  "close":         16.46983,
  "volume":        335537600,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-11T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-11",
  "open":          16.6696,
  "high":          16.68945,
  "low":           15.81496,
  "close":         15.90095,
  "volume":        507067200,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-12T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-12",
  "open":          15.78982,
  "high":          16.10204,
  "low":           15.65223,
  "close":         15.92609,
  "volume":        385166304,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-13T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-13",
  "open":          16.02928,
  "high":          16.03325,
  "low":           15.26724,
  "close":         15.54507,
  "volume":        464991872,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-14T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-14",
  "open":          15.50538,
  "high":          15.80305,
  "low":           15.40218,
  "close":         15.71044,
  "volume":        262816400,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-15T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-15",
  "open":          15.95784,
  "high":          15.96445,
  "low":           15.85729,
  "close":         15.94196,
  "volume":        219060032,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-18T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-18",
  "open":          16.30975,
  "high":          16.56112,
  "low":           16.21185,
  "close":         16.54921,
  "volume":        246080000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-19T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-19",
  "open":          16.49629,
  "high":          16.53863,
  "low":           16.2608,
  "close":         16.36003,
  "volume":        252383168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-20T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-20",
  "open":          16.38781,
  "high":          16.49232,
  "low":           16.07426,
  "close":         16.08088,
  "volume":        242391376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-21T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-21",
  "open":          16.10072,
  "high":          16.44337,
  "low":           15.97107,
  "close":         16.39178,
  "volume":        234156960,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-22T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-22",
  "open":          16.38516,
  "high":          16.46454,
  "low":           16.19068,
  "close":         16.27271,
  "volume":        170664896,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-25T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-25",
  "open":          16.43014,
  "high":          16.54921,
  "low":           16.01605,
  "close":         16.18539,
  "volume":        260744576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-26T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-26",
  "open":          16.40236,
  "high":          16.40501,
  "low":           15.70647,
  "close":         15.82951,
  "volume":        362764672,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-27T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-27",
  "open":          15.96313,
  "high":          16.1457,
  "low":           15.77791,
  "close":         16.12586,
  "volume":        263296384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-28T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-28",
  "open":          16.18804,
  "high":          16.20524,
  "low":           15.87581,
  "close":         15.9499,
  "volume":        227099424,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-06-29T00:  00:  00-04:  00",
  "tradingDay":    "2007-06-29",
  "open":          16.13644,
  "high":          16.40501,
  "low":           16.02002,
  "close":         16.1457,
  "volume":        306773088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-02T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-02",
  "open":          16.01473,
  "high":          16.15232,
  "low":           15.7832,
  "close":         16.04251,
  "volume":        268839136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-03T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-03",
  "open":          16.14041,
  "high":          16.85482,
  "low":           16.07426,
  "close":         16.82439,
  "volume":        313813984,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-05T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-05",
  "open":          17.04004,
  "high":          17.59172,
  "low":           17.02549,
  "close":         17.56262,
  "volume":        392114208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-06T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-06",
  "open":          17.61289,
  "high":          17.64067,
  "low":           17.25172,
  "close":         17.50308,
  "volume":        234490288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-09T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-09",
  "open":          17.51367,
  "high":          17.58246,
  "low":           17.09031,
  "close":         17.24246,
  "volume":        268788512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-10T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-10",
  "open":          17.05062,
  "high":          17.79414,
  "low":           17.04136,
  "close":         17.5097,
  "volume":        338758336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-11T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-11",
  "open":          17.47266,
  "high":          17.6883,
  "low":           17.37211,
  "close":         17.51499,
  "volume":        221636784,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-12T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-12",
  "open":          17.70815,
  "high":          17.75974,
  "low":           17.51499,
  "close":         17.73725,
  "volume":        190193472,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-13T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-13",
  "open":          17.86426,
  "high":          18.23734,
  "low":           17.79679,
  "close":         18.22146,
  "volume":        244894800,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-16T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-16",
  "open":          18.30878,
  "high":          18.51913,
  "low":           18.19104,
  "close":         18.27042,
  "volume":        252666624,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-17T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-17",
  "open":          18.29688,
  "high":          18.46886,
  "low":           18.19104,
  "close":         18.37758,
  "volume":        191597120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-18T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-18",
  "open":          18.28232,
  "high":          18.3154,
  "low":           17.99788,
  "close":         18.27306,
  "volume":        204406800,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-19T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-19",
  "open":          18.56147,
  "high":          18.62894,
  "low":           18.47548,
  "close":         18.52178,
  "volume":        197812608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-20T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-20",
  "open":          18.74007,
  "high":          19.07479,
  "low":           18.52178,
  "close":         19.0179,
  "volume":        314935680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-23T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-23",
  "open":          18.95969,
  "high":          19.21238,
  "low":           18.64482,
  "close":         19.01129,
  "volume":        279474208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-24T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-24",
  "open":          18.37361,
  "high":          18.65408,
  "low":           17.74784,
  "close":         17.84574,
  "volume":        487178080,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-25T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-25",
  "open":          18.17119,
  "high":          18.30481,
  "low":           17.86029,
  "close":         18.15928,
  "volume":        437710368,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-26T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-26",
  "open":          19.30367,
  "high":          19.64632,
  "low":           18.11959,
  "close":         19.31557,
  "volume":        590623744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-27T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-27",
  "open":          19.34071,
  "high":          19.70188,
  "low":           19.02187,
  "close":         19.03113,
  "volume":        312858560,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-30T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-30",
  "open":          19.09596,
  "high":          19.24281,
  "low":           18.46489,
  "close":         18.71097,
  "volume":        299234816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-07-31T00:  00:  00-04:  00",
  "tradingDay":    "2007-07-31",
  "open":          18.91471,
  "high":          18.98218,
  "low":           17.39989,
  "close":         17.43164,
  "volume":        477679104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-01T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-01",
  "open":          17.68036,
  "high":          17.91056,
  "low":           16.90377,
  "close":         17.86029,
  "volume":        472759168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-02T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-02",
  "open":          18.07858,
  "high":          18.11959,
  "low":           17.74784,
  "close":         18.05742,
  "volume":        230435824,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-03T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-03",
  "open":          17.89469,
  "high":          17.98597,
  "low":           17.39725,
  "close":         17.44355,
  "volume":        183173744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-06T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-06",
  "open":          17.58246,
  "high":          17.89601,
  "low":           16.97389,
  "close":         17.89336,
  "volume":        249711936,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-07T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-07",
  "open":          17.85235,
  "high":          18.15664,
  "low":           17.54674,
  "close":         17.86426,
  "volume":        256592592,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-08T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-08",
  "open":          18.09313,
  "high":          18.10637,
  "low":           17.46339,
  "close":         17.72931,
  "volume":        218227056,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-09T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-09",
  "open":          17.34565,
  "high":          17.59569,
  "low":           16.54921,
  "close":         16.7212,
  "volume":        303719392,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-10T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-10",
  "open":          16.28858,
  "high":          16.90112,
  "low":           15.9155,
  "close":         16.53731,
  "volume":        380842752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-13T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-13",
  "open":          16.97654,
  "high":          17.1128,
  "low":           16.73575,
  "close":         16.90642,
  "volume":        203536784,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-14T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-14",
  "open":          16.97257,
  "high":          16.97389,
  "low":           16.36664,
  "close":         16.40898,
  "volume":        199539008,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-15T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-15",
  "open":          16.23831,
  "high":          16.51878,
  "low":           15.82951,
  "close":         15.86258,
  "volume":        268022064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-16T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-16",
  "open":          15.48024,
  "high":          15.67737,
  "low":           14.76715,
  "close":         15.48553,
  "volume":        502891040,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-17T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-17",
  "open":          16.15232,
  "high":          16.33886,
  "low":           15.852,
  "close":         16.14835,
  "volume":        322575232,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-20T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-20",
  "open":          16.39971,
  "high":          16.47116,
  "low":           15.94196,
  "close":         16.16952,
  "volume":        216883136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-21T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-21",
  "open":          16.16819,
  "high":          17.06121,
  "low":           16.00811,
  "close":         16.87731,
  "volume":        351520384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-22T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-22",
  "open":          17.36417,
  "high":          17.56262,
  "low":           17.24246,
  "close":         17.53086,
  "volume":        286248288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-23T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-23",
  "open":          17.6076,
  "high":          17.64067,
  "low":           17.16705,
  "close":         17.34036,
  "volume":        233924912,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-24T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-24",
  "open":          17.55336,
  "high":          17.90924,
  "low":           17.17366,
  "close":         17.89998,
  "volume":        246146512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-27T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-27",
  "open":          17.64729,
  "high":          17.81531,
  "low":           17.47663,
  "close":         17.49647,
  "volume":        190961440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-28T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-28",
  "open":          17.32977,
  "high":          17.51764,
  "low":           16.75295,
  "close":         16.77809,
  "volume":        318369600,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-29T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-29",
  "open":          17.18292,
  "high":          17.7518,
  "low":           17.13794,
  "close":         17.73857,
  "volume":        314956096,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-30T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-30",
  "open":          17.55203,
  "high":          18.29026,
  "low":           17.50308,
  "close":         18.02566,
  "volume":        387354528,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-08-31T00:  00:  00-04:  00",
  "tradingDay":    "2007-08-31",
  "open":          18.45431,
  "high":          18.47548,
  "low":           18.17913,
  "close":         18.32069,
  "volume":        236647536,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-09-04T00:  00:  00-04:  00",
  "tradingDay":    "2007-09-04",
  "open":          18.51384,
  "high":          19.27985,
  "low":           18.50061,
  "close":         19.07214,
  "volume":        355347360,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-09-05T00:  00:  00-04:  00",
  "tradingDay":    "2007-09-05",
  "open":          19.1793,
  "high":          19.2944,
  "low":           18.00582,
  "close":         18.09313,
  "volume":        628517632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-09-06T00:  00:  00-04:  00",
  "tradingDay":    "2007-09-06",
  "open":          17.93438,
  "high":          18.2003,
  "low":           17.55733,
  "close":         17.86161,
  "volume":        513250208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-09-07T00:  00:  00-04:  00",
  "tradingDay":    "2007-09-07",
  "open":          17.46472,
  "high":          17.50308,
  "low":           17.1988,
  "close":         17.43297,
  "volume":        386186720,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-09-10T00:  00:  00-04:  00",
  "tradingDay":    "2007-09-10",
  "open":          18.12489,
  "high":          18.26248,
  "low":           17.72138,
  "close":         18.08652,
  "volume":        401647200,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-09-11T00:  00:  00-04:  00",
  "tradingDay":    "2007-09-11",
  "open":          18.24395,
  "high":          18.29688,
  "low":           17.69492,
  "close":         17.92512,
  "volume":        262362128,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-09-12T00:  00:  00-04:  00",
  "tradingDay":    "2007-09-12",
  "open":          17.99127,
  "high":          18.4424,
  "low":           17.95951,
  "close":         18.10504,
  "volume":        276254208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-09-13T00:  00:  00-04:  00",
  "tradingDay":    "2007-09-13",
  "open":          18.36699,
  "high":          18.38948,
  "low":           18.07858,
  "close":         18.15135,
  "volume":        177132096,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-09-14T00:  00:  00-04:  00",
  "tradingDay":    "2007-09-14",
  "open":          18.068,
  "high":          18.38684,
  "low":           18.01905,
  "close":         18.36435,
  "volume":        163946752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-09-17T00:  00:  00-04:  00",
  "tradingDay":    "2007-09-17",
  "open":          18.38816,
  "high":          18.59984,
  "low":           18.20427,
  "close":         18.31143,
  "volume":        214171840,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-09-18T00:  00:  00-04:  00",
  "tradingDay":    "2007-09-18",
  "open":          18.39742,
  "high":          18.89883,
  "low":           18.23469,
  "close":         18.6435,
  "volume":        287252832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-09-19T00:  00:  00-04:  00",
  "tradingDay":    "2007-09-19",
  "open":          18.92265,
  "high":          18.93985,
  "low":           18.4424,
  "close":         18.62365,
  "volume":        277208096,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-09-20T00:  00:  00-04:  00",
  "tradingDay":    "2007-09-20",
  "open":          18.54163,
  "high":          18.75859,
  "low":           18.43182,
  "close":         18.56279,
  "volume":        186763344,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-09-21T00:  00:  00-04:  00",
  "tradingDay":    "2007-09-21",
  "open":          18.6726,
  "high":          19.13697,
  "low":           18.56279,
  "close":         19.07082,
  "volume":        307442784,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-09-24T00:  00:  00-04:  00",
  "tradingDay":    "2007-09-24",
  "open":          19.41215,
  "high":          19.82492,
  "low":           19.40157,
  "close":         19.61721,
  "volume":        284032832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-09-25T00:  00:  00-04:  00",
  "tradingDay":    "2007-09-25",
  "open":          19.4267,
  "high":          20.27077,
  "low":           19.42406,
  "close":         20.26547,
  "volume":        321774016,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-09-26T00:  00:  00-04:  00",
  "tradingDay":    "2007-09-26",
  "open":          20.43614,
  "high":          20.50626,
  "low":           20.01014,
  "close":         20.21123,
  "volume":        263275216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-09-27T00:  00:  00-04:  00",
  "tradingDay":    "2007-09-27",
  "open":          20.34353,
  "high":          20.44275,
  "low":           20.1517,
  "close":         20.44011,
  "volume":        177681616,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-09-28T00:  00:  00-04:  00",
  "tradingDay":    "2007-09-28",
  "open":          20.29987,
  "high":          20.45334,
  "low":           20.20859,
  "close":         20.30384,
  "volume":        166047312,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-01T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-01",
  "open":          20.45731,
  "high":          20.8251,
  "low":           20.2324,
  "close":         20.68354,
  "volume":        225967888,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-02T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-02",
  "open":          20.71132,
  "high":          20.98121,
  "low":           20.624,
  "close":         20.96269,
  "volume":        213820352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-03T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-03",
  "open":          20.87405,
  "high":          21.05927,
  "low":           20.77218,
  "close":         20.89257,
  "volume":        186946272,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-04T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-04",
  "open":          20.90315,
  "high":          20.91374,
  "low":           20.30781,
  "close":         20.67031,
  "volume":        177346768,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-05T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-05",
  "open":          20.9521,
  "high":          21.37678,
  "low":           20.86346,
  "close":         21.35958,
  "volume":        254691584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-08T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-08",
  "open":          21.62947,
  "high":          22.21423,
  "low":           21.56068,
  "close":         22.21423,
  "volume":        225660256,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-09T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-09",
  "open":          22.51719,
  "high":          22.63759,
  "low":           22.0515,
  "close":         22.20762,
  "volume":        298525056,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-10T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-10",
  "open":          22.1666,
  "high":          22.21026,
  "low":           21.90862,
  "close":         22.06606,
  "volume":        180216784,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-11T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-11",
  "open":          22.42326,
  "high":          22.73946,
  "low":           20.26945,
  "close":         21.46278,
  "volume":        443798880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-12T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-12",
  "open":          21.56597,
  "high":          22.13088,
  "low":           21.40589,
  "close":         22.12691,
  "volume":        266759760,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-15T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-15",
  "open":          22.22349,
  "high":          22.43385,
  "low":           21.63079,
  "close":         22.09119,
  "volume":        290989056,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-16T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-16",
  "open":          21.90068,
  "high":          22.51455,
  "low":           21.84909,
  "close":         22.43517,
  "volume":        288262656,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-17T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-17",
  "open":          22.84662,
  "high":          22.89292,
  "low":           22.38225,
  "close":         22.85456,
  "volume":        304401184,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-18T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-18",
  "open":          22.68918,
  "high":          23.04506,
  "low":           22.62965,
  "close":         22.95378,
  "volume":        222534000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-19T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-19",
  "open":          23.0411,
  "high":          23.10328,
  "low":           22.49073,
  "close":         22.5463,
  "volume":        348718400,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-22T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-22",
  "open":          22.51058,
  "high":          23.139,
  "low":           22.48544,
  "close":         23.06756,
  "volume":        445285664,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-23T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-23",
  "open":          24.94619,
  "high":          24.95149,
  "low":           24.17886,
  "close":         24.62868,
  "volume":        484608128,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-24T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-24",
  "open":          24.58237,
  "high":          24.76759,
  "low":           23.71317,
  "close":         24.59825,
  "volume":        347828000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-25T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-25",
  "open":          24.45801,
  "high":          24.59428,
  "low":           24.03333,
  "close":         24.18151,
  "volume":        262829248,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-26T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-26",
  "open":          24.47786,
  "high":          24.52416,
  "low":           24.19474,
  "close":         24.43552,
  "volume":        190822352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-29T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-29",
  "open":          24.53475,
  "high":          24.68557,
  "low":           24.43552,
  "close":         24.48712,
  "volume":        145923104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-30T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-30",
  "open":          24.63132,
  "high":          25.05335,
  "low":           24.43949,
  "close":         24.73981,
  "volume":        253596336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-10-31T00:  00:  00-04:  00",
  "tradingDay":    "2007-10-31",
  "open":          24.82316,
  "high":          25.15258,
  "low":           24.4686,
  "close":         25.13009,
  "volume":        224953520,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-01T00:  00:  00-04:  00",
  "tradingDay":    "2007-11-01",
  "open":          24.93826,
  "high":          25.14993,
  "low":           23.81372,
  "close":         24.79802,
  "volume":        217320784,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-02T00:  00:  00-04:  00",
  "tradingDay":    "2007-11-02",
  "open":          25.03219,
  "high":          25.06262,
  "low":           24.27544,
  "close":         24.85491,
  "volume":        270522464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-05T00:  00:  00-05:  00",
  "tradingDay":    "2007-11-05",
  "open":          24.51358,
  "high":          24.99911,
  "low":           24.37467,
  "close":         24.63132,
  "volume":        217088736,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-06T00:  00:  00-05:  00",
  "tradingDay":    "2007-11-06",
  "open":          24.74642,
  "high":          25.4013,
  "low":           24.51093,
  "close":         25.37352,
  "volume":        257730176,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-07T00:  00:  00-05:  00",
  "tradingDay":    "2007-11-07",
  "open":          25.21741,
  "high":          25.49126,
  "low":           24.62471,
  "close":         24.6472,
  "volume":        268420400,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-08T00:  00:  00-05:  00",
  "tradingDay":    "2007-11-08",
  "open":          24.69615,
  "high":          24.72658,
  "low":           22.19571,
  "close":         23.21441,
  "volume":        510311392,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-09T00:  00:  00-05:  00",
  "tradingDay":    "2007-11-09",
  "open":          22.64288,
  "high":          23.1681,
  "low":           21.85703,
  "close":         21.87819,
  "volume":        412049440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-12T00:  00:  00-05:  00",
  "tradingDay":    "2007-11-12",
  "open":          21.86629,
  "high":          22.18645,
  "low":           19.92811,
  "close":         20.34221,
  "volume":        477562688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-13T00:  00:  00-05:  00",
  "tradingDay":    "2007-11-13",
  "open":          21.2802,
  "high":          22.62039,
  "low":           20.34221,
  "close":         22.48544,
  "volume":        469567136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-14T00:  00:  00-05:  00",
  "tradingDay":    "2007-11-14",
  "open":          23.43799,
  "high":          23.49224,
  "low":           21.66255,
  "close":         21.97609,
  "volume":        391205664,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-15T00:  00:  00-05:  00",
  "tradingDay":    "2007-11-15",
  "open":          22.01314,
  "high":          22.43649,
  "low":           21.20744,
  "close":         21.73663,
  "volume":        401529312,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-16T00:  00:  00-05:  00",
  "tradingDay":    "2007-11-16",
  "open":          21.86893,
  "high":          22.09649,
  "low":           21.07911,
  "close":         22.01314,
  "volume":        373476800,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-19T00:  00:  00-05:  00",
  "tradingDay":    "2007-11-19",
  "open":          21.97477,
  "high":          22.2526,
  "low":           21.44558,
  "close":         21.69033,
  "volume":        311403520,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-20T00:  00:  00-05:  00",
  "tradingDay":    "2007-11-20",
  "open":          21.91788,
  "high":          22.72755,
  "low":           21.63476,
  "close":         22.33859,
  "volume":        416709376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-21T00:  00:  00-05:  00",
  "tradingDay":    "2007-11-21",
  "open":          21.94037,
  "high":          22.80164,
  "low":           21.78558,
  "close":         22.287,
  "volume":        328960032,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-23T00:  00:  00-05:  00",
  "tradingDay":    "2007-11-23",
  "open":          22.75533,
  "high":          22.76195,
  "low":           22.45766,
  "close":         22.69447,
  "volume":        125731640,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-26T00:  00:  00-05:  00",
  "tradingDay":    "2007-11-26",
  "open":          22.96568,
  "high":          23.45255,
  "low":           22.80164,
  "close":         22.82677,
  "volume":        352490944,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-27T00:  00:  00-05:  00",
  "tradingDay":    "2007-11-27",
  "open":          23.18133,
  "high":          23.25674,
  "low":           22.49206,
  "close":         23.12709,
  "volume":        355534816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-28T00:  00:  00-05:  00",
  "tradingDay":    "2007-11-28",
  "open":          23.39301,
  "high":          23.8931,
  "low":           23.19853,
  "close":         23.84282,
  "volume":        310690752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-29T00:  00:  00-05:  00",
  "tradingDay":    "2007-11-29",
  "open":          23.80181,
  "high":          24.4977,
  "low":           23.70127,
  "close":         24.38128,
  "volume":        283699488,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-11-30T00:  00:  00-05:  00",
  "tradingDay":    "2007-11-30",
  "open":          24.78479,
  "high":          24.83242,
  "low":           23.77403,
  "close":         24.10742,
  "volume":        320649280,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-12-03T00:  00:  00-05:  00",
  "tradingDay":    "2007-12-03",
  "open":          24.05979,
  "high":          24.36143,
  "low":           23.50943,
  "close":         23.6629,
  "volume":        259550304,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-12-04T00:  00:  00-05:  00",
  "tradingDay":    "2007-12-04",
  "open":          23.43667,
  "high":          23.93279,
  "low":           23.4155,
  "close":         23.78858,
  "volume":        208888336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-12-05T00:  00:  00-05:  00",
  "tradingDay":    "2007-12-05",
  "open":          24.19606,
  "high":          24.60751,
  "low":           24.13256,
  "close":         24.54136,
  "volume":        240905328,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-12-06T00:  00:  00-05:  00",
  "tradingDay":    "2007-12-06",
  "open":          24.63265,
  "high":          25.14993,
  "low":           24.62338,
  "close":         25.13009,
  "volume":        243082992,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-12-07T00:  00:  00-05:  00",
  "tradingDay":    "2007-12-07",
  "open":          25.20814,
  "high":          25.79687,
  "low":           24.8774,
  "close":         25.70559,
  "volume":        287786464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-12-10T00:  00:  00-05:  00",
  "tradingDay":    "2007-12-10",
  "open":          25.61165,
  "high":          25.88551,
  "low":           25.49259,
  "close":         25.69368,
  "volume":        195006832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-12-11T00:  00:  00-05:  00",
  "tradingDay":    "2007-12-11",
  "open":          25.76512,
  "high":          26.0403,
  "low":           24.7914,
  "close":         24.94355,
  "volume":        299896192,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-12-12T00:  00:  00-05:  00",
  "tradingDay":    "2007-12-12",
  "open":          25.59181,
  "high":          25.7294,
  "low":           24.57576,
  "close":         25.25048,
  "volume":        330869344,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-12-13T00:  00:  00-05:  00",
  "tradingDay":    "2007-12-13",
  "open":          25.16184,
  "high":          25.41718,
  "low":           24.84829,
  "close":         25.37881,
  "volume":        233756352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-12-14T00:  00:  00-05:  00",
  "tradingDay":    "2007-12-14",
  "open":          25.18565,
  "high":          26.45969,
  "low":           25.07585,
  "close":         25.1883,
  "volume":        182105696,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-12-17T00:  00:  00-05:  00",
  "tradingDay":    "2007-12-17",
  "open":          25.23196,
  "high":          25.48729,
  "low":           24.20797,
  "close":         24.39583,
  "volume":        276617760,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-12-18T00:  00:  00-05:  00",
  "tradingDay":    "2007-12-18",
  "open":          24.67631,
  "high":          24.78347,
  "low":           23.6285,
  "close":         24.20797,
  "volume":        330043968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-12-19T00:  00:  00-05:  00",
  "tradingDay":    "2007-12-19",
  "open":          24.20797,
  "high":          24.42758,
  "low":           23.93279,
  "close":         24.22649,
  "volume":        223379056,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-12-20T00:  00:  00-05:  00",
  "tradingDay":    "2007-12-20",
  "open":          24.5321,
  "high":          24.84962,
  "low":           24.25427,
  "close":         24.76759,
  "volume":        208957872,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-12-21T00:  00:  00-05:  00",
  "tradingDay":    "2007-12-21",
  "open":          25.15258,
  "high":          25.65399,
  "low":           25.12215,
  "close":         25.65399,
  "volume":        268671360,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-12-24T00:  00:  00-05:  00",
  "tradingDay":    "2007-12-24",
  "open":          25.80216,
  "high":          26.37105,
  "low":           25.77041,
  "close":         26.30093,
  "volume":        129649296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-12-26T00:  00:  00-05:  00",
  "tradingDay":    "2007-12-26",
  "open":          26.32871,
  "high":          26.58669,
  "low":           26.03898,
  "close":         26.32077,
  "volume":        189973520,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-12-27T00:  00:  00-05:  00",
  "tradingDay":    "2007-12-27",
  "open":          26.32077,
  "high":          26.85129,
  "low":           26.16863,
  "close":         26.2705,
  "volume":        214753856,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-12-28T00:  00:  00-05:  00",
  "tradingDay":    "2007-12-28",
  "open":          26.53774,
  "high":          26.66607,
  "low":           26.04692,
  "close":         26.4372,
  "volume":        188870704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2007-12-31T00:  00:  00-05:  00",
  "tradingDay":    "2007-12-31",
  "open":          26.39354,
  "high":          26.52584,
  "low":           26.16202,
  "close":         26.20568,
  "volume":        145593552,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-02T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-02",
  "open":          26.35914,
  "high":          26.49409,
  "low":           25.47407,
  "close":         25.77703,
  "volume":        291326176,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-03T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-03",
  "open":          25.85244,
  "high":          26.11439,
  "low":           25.49259,
  "close":         25.78893,
  "volume":        227317120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-04T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-04",
  "open":          25.32854,
  "high":          25.5336,
  "low":           23.66687,
  "close":         23.82034,
  "volume":        393004640,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-07T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-07",
  "open":          23.97909,
  "high":          24.29,
  "low":           22.52116,
  "close":         23.5015,
  "volume":        559392832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-08T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-08",
  "open":          23.83224,
  "high":          24.13917,
  "low":           22.59657,
  "close":         22.65611,
  "volume":        411357088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-09T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-09",
  "open":          22.66272,
  "high":          23.74757,
  "low":           22.26583,
  "close":         23.73434,
  "volume":        490108576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-10T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-10",
  "open":          23.49356,
  "high":          23.94602,
  "low":           23.20647,
  "close":         23.55177,
  "volume":        400332000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-11T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-11",
  "open":          23.28452,
  "high":          23.52928,
  "low":           22.49073,
  "close":         22.84662,
  "volume":        332657728,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-14T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-14",
  "open":          23.48562,
  "high":          23.73699,
  "low":           23.17472,
  "close":         23.65232,
  "volume":        297068512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-15T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-15",
  "open":          23.51208,
  "high":          23.71053,
  "low":           21.78426,
  "close":         22.36373,
  "volume":        634458752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-16T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-16",
  "open":          21.85967,
  "high":          22.35976,
  "low":           20.73116,
  "close":         21.12012,
  "volume":        598585344,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-17T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-17",
  "open":          21.36752,
  "high":          21.87687,
  "low":           20.95872,
  "close":         21.2855,
  "volume":        475088000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-18T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-18",
  "open":          21.39398,
  "high":          21.92847,
  "low":           21.11615,
  "close":         21.34768,
  "volume":        465489984,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-22T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-22",
  "open":          19.58811,
  "high":          21.1651,
  "low":           19.31557,
  "close":         20.59093,
  "volume":        657327488,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-23T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-23",
  "open":          18.01772,
  "high":          18.52178,
  "low":           16.68813,
  "close":         18.39874,
  "volume":        910564032,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-24T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-24",
  "open":          18.52046,
  "high":          18.61439,
  "low":           17.46472,
  "close":         17.93967,
  "volume":        541487872,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-25T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-25",
  "open":          18.38816,
  "high":          18.40139,
  "low":           17.1472,
  "close":         17.20012,
  "volume":        419704864,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-28T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-28",
  "open":          16.95537,
  "high":          17.62215,
  "low":           16.72914,
  "close":         17.20012,
  "volume":        398136960,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-29T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-29",
  "open":          17.35094,
  "high":          17.56791,
  "low":           17.07311,
  "close":         17.40254,
  "volume":        296942272,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-30T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-30",
  "open":          17.38004,
  "high":          17.91982,
  "low":           17.1988,
  "close":         17.48721,
  "volume":        335564064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-01-31T00:  00:  00-05:  00",
  "tradingDay":    "2008-01-31",
  "open":          17.12603,
  "high":          18.07858,
  "low":           17.11942,
  "close":         17.90792,
  "volume":        363267328,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-02-01T00:  00:  00-05:  00",
  "tradingDay":    "2008-02-01",
  "open":          18.02434,
  "high":          18.07064,
  "low":           17.48721,
  "close":         17.69492,
  "volume":        272852032,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-02-04T00:  00:  00-05:  00",
  "tradingDay":    "2008-02-04",
  "open":          17.75577,
  "high":          17.97936,
  "low":           17.38666,
  "close":         17.41709,
  "volume":        242749648,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-02-05T00:  00:  00-05:  00",
  "tradingDay":    "2008-02-05",
  "open":          17.25568,
  "high":          17.72799,
  "low":           17.05327,
  "close":         17.11413,
  "volume":        308026304,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-02-06T00:  00:  00-05:  00",
  "tradingDay":    "2008-02-06",
  "open":          17.30861,
  "high":          17.45281,
  "low":           16.10998,
  "close":         16.14041,
  "volume":        424707968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-02-07T00:  00:  00-05:  00",
  "tradingDay":    "2008-02-07",
  "open":          15.87184,
  "high":          16.5082,
  "low":           15.51464,
  "close":         16.03986,
  "volume":        562744384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-02-08T00:  00:  00-05:  00",
  "tradingDay":    "2008-02-08",
  "open":          16.15099,
  "high":          16.62991,
  "low":           16.08749,
  "close":         16.60081,
  "volume":        366047392,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-02-11T00:  00:  00-05:  00",
  "tradingDay":    "2008-02-11",
  "open":          16.93552,
  "high":          17.19615,
  "low":           16.82836,
  "close":         17.12603,
  "volume":        324328832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-02-12T00:  00:  00-05:  00",
  "tradingDay":    "2008-02-12",
  "open":          17.2914,
  "high":          17.33109,
  "low":           16.35473,
  "close":         16.51878,
  "volume":        330955520,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-02-13T00:  00:  00-05:  00",
  "tradingDay":    "2008-02-13",
  "open":          16.76221,
  "high":          17.16969,
  "low":           16.62065,
  "close":         17.11942,
  "volume":        261457360,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-02-14T00:  00:  00-05:  00",
  "tradingDay":    "2008-02-14",
  "open":          17.11942,
  "high":          17.30464,
  "low":           16.80322,
  "close":         16.86276,
  "volume":        257604704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-02-15T00:  00:  00-05:  00",
  "tradingDay":    "2008-02-15",
  "open":          16.70532,
  "high":          16.81249,
  "low":           16.41294,
  "close":         16.48835,
  "volume":        243307488,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-02-19T00:  00:  00-05:  00",
  "tradingDay":    "2008-02-19",
  "open":          16.66828,
  "high":          16.76883,
  "low":           16.06632,
  "close":         16.16422,
  "volume":        271313856,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-02-20T00:  00:  00-05:  00",
  "tradingDay":    "2008-02-20",
  "open":          16.16687,
  "high":          16.48438,
  "low":           16.09807,
  "close":         16.38119,
  "volume":        261161808,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-02-21T00:  00:  00-05:  00",
  "tradingDay":    "2008-02-21",
  "open":          16.67622,
  "high":          16.73178,
  "low":           15.98959,
  "close":         16.07955,
  "volume":        253245616,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-02-22T00:  00:  00-05:  00",
  "tradingDay":    "2008-02-22",
  "open":          16.20524,
  "high":          16.20788,
  "low":           15.32942,
  "close":         15.80437,
  "volume":        412993536,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-02-25T00:  00:  00-05:  00",
  "tradingDay":    "2008-02-25",
  "open":          15.68927,
  "high":          15.8983,
  "low":           15.43394,
  "close":         15.84141,
  "volume":        339268544,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-02-26T00:  00:  00-05:  00",
  "tradingDay":    "2008-02-26",
  "open":          15.56359,
  "high":          16.02002,
  "low":           15.27253,
  "close":         15.76336,
  "volume":        406247424,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-02-27T00:  00:  00-05:  00",
  "tradingDay":    "2008-02-27",
  "open":          15.64165,
  "high":          16.27932,
  "low":           15.62312,
  "close":         16.26742,
  "volume":        398216352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-02-28T00:  00:  00-05:  00",
  "tradingDay":    "2008-02-28",
  "open":          16.82836,
  "high":          17.48985,
  "low":           16.63917,
  "close":         17.18689,
  "volume":        436850944,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-02-29T00:  00:  00-05:  00",
  "tradingDay":    "2008-02-29",
  "open":          17.10486,
  "high":          17.22658,
  "low":           16.51085,
  "close":         16.53995,
  "volume":        338919328,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-03-03T00:  00:  00-05:  00",
  "tradingDay":    "2008-03-03",
  "open":          16.46322,
  "high":          16.66696,
  "low":           15.61122,
  "close":         16.10469,
  "volume":        430242400,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-03-04T00:  00:  00-05:  00",
  "tradingDay":    "2008-03-04",
  "open":          16.13909,
  "high":          16.52143,
  "low":           15.92873,
  "close":         16.48703,
  "volume":        481967904,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-03-05T00:  00:  00-05:  00",
  "tradingDay":    "2008-03-05",
  "open":          16.34944,
  "high":          16.55583,
  "low":           16.17348,
  "close":         16.46983,
  "volume":        329836832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-03-06T00:  00:  00-05:  00",
  "tradingDay":    "2008-03-06",
  "open":          16.48703,
  "high":          16.86805,
  "low":           15.98297,
  "close":         15.99885,
  "volume":        397827808,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-03-07T00:  00:  00-05:  00",
  "tradingDay":    "2008-03-07",
  "open":          15.93006,
  "high":          16.27006,
  "low":           15.75013,
  "close":         16.17348,
  "volume":        332230688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-03-10T00:  00:  00-04:  00",
  "tradingDay":    "2008-03-10",
  "open":          16.13776,
  "high":          16.33356,
  "low":           15.79247,
  "close":         15.8348,
  "volume":        270139232,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-03-11T00:  00:  00-04:  00",
  "tradingDay":    "2008-03-11",
  "open":          16.41824,
  "high":          16.86541,
  "low":           16.14041,
  "close":         16.84821,
  "volume":        314608384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-03-12T00:  00:  00-04:  00",
  "tradingDay":    "2008-03-12",
  "open":          16.80719,
  "high":          17.02416,
  "low":           16.5598,
  "close":         16.67357,
  "volume":        286048736,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-03-13T00:  00:  00-04:  00",
  "tradingDay":    "2008-03-13",
  "open":          16.38384,
  "high":          17.13265,
  "low":           16.27271,
  "close":         16.92626,
  "volume":        341282944,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-03-14T00:  00:  00-04:  00",
  "tradingDay":    "2008-03-14",
  "open":          17.18292,
  "high":          17.23849,
  "low":           16.43147,
  "close":         16.75031,
  "volume":        312295456,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-03-17T00:  00:  00-04:  00",
  "tradingDay":    "2008-03-17",
  "open":          16.21317,
  "high":          17.01226,
  "low":           16.21317,
  "close":         16.76618,
  "volume":        289549888,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-03-18T00:  00:  00-04:  00",
  "tradingDay":    "2008-03-18",
  "open":          17.09031,
  "high":          17.59569,
  "low":           17.02284,
  "close":         17.57188,
  "volume":        325324320,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-03-19T00:  00:  00-04:  00",
  "tradingDay":    "2008-03-19",
  "open":          17.60363,
  "high":          17.76636,
  "low":           17.15514,
  "close":         17.15514,
  "volume":        272796864,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-03-20T00:  00:  00-04:  00",
  "tradingDay":    "2008-03-20",
  "open":          17.34697,
  "high":          17.63406,
  "low":           17.09031,
  "close":         17.63141,
  "volume":        245328672,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-03-24T00:  00:  00-04:  00",
  "tradingDay":    "2008-03-24",
  "open":          17.72931,
  "high":          18.63424,
  "low":           17.68036,
  "close":         18.4596,
  "volume":        288132640,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-03-25T00:  00:  00-04:  00",
  "tradingDay":    "2008-03-25",
  "open":          18.51649,
  "high":          18.93191,
  "low":           18.16854,
  "close":         18.65143,
  "volume":        284176448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-03-26T00:  00:  00-04:  00",
  "tradingDay":    "2008-03-26",
  "open":          18.63688,
  "high":          19.28118,
  "low":           18.60645,
  "close":         19.19121,
  "volume":        319105792,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-03-27T00:  00:  00-04:  00",
  "tradingDay":    "2008-03-27",
  "open":          19.17666,
  "high":          19.22429,
  "low":           18.52046,
  "close":         18.55486,
  "volume":        269905664,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-03-28T00:  00:  00-04:  00",
  "tradingDay":    "2008-03-28",
  "open":          18.76257,
  "high":          19.13697,
  "low":           18.73346,
  "close":         18.92,
  "volume":        193101296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-03-31T00:  00:  00-04:  00",
  "tradingDay":    "2008-03-31",
  "open":          18.9544,
  "high":          19.27721,
  "low":           18.85518,
  "close":         18.98483,
  "volume":        207340320,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-01T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-01",
  "open":          19.35526,
  "high":          19.79979,
  "low":           18.99938,
  "close":         19.78259,
  "volume":        278743264,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-02T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-02",
  "open":          19.68336,
  "high":          20.00352,
  "low":           19.29573,
  "close":         19.5127,
  "volume":        282091008,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-03T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-03",
  "open":          19.45581,
  "high":          20.32501,
  "low":           19.44787,
  "close":         20.05777,
  "volume":        283926240,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-04T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-04",
  "open":          20.1345,
  "high":          20.46922,
  "low":           19.94399,
  "close":         20.25224,
  "volume":        230750272,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-07T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-07",
  "open":          20.65576,
  "high":          21.12674,
  "low":           20.52081,
  "close":         20.624,
  "volume":        312897856,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-08T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-08",
  "open":          20.31443,
  "high":          20.69809,
  "low":           20.1517,
  "close":         20.22049,
  "volume":        274066720,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-09T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-09",
  "open":          20.28267,
  "high":          20.35941,
  "low":           19.90562,
  "close":         20.03528,
  "volume":        235914352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-10T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-10",
  "open":          19.99426,
  "high":          20.56182,
  "low":           19.92415,
  "close":         20.44672,
  "volume":        258206368,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-11T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-11",
  "open":          20.20462,
  "high":          20.28135,
  "low":           19.36849,
  "close":         19.46639,
  "volume":        326662208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-14T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-14",
  "open":          19.41744,
  "high":          19.74554,
  "low":           19.12242,
  "close":         19.55106,
  "volume":        228190896,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-15T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-15",
  "open":          19.76539,
  "high":          19.80772,
  "low":           19.27853,
  "close":         19.63044,
  "volume":        188436080,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-16T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-16",
  "open":          20.07232,
  "high":          20.38719,
  "low":           19.92679,
  "close":         20.33427,
  "volume":        214820368,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-17T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-17",
  "open":          20.39248,
  "high":          20.63856,
  "low":           20.28797,
  "close":         20.43879,
  "volume":        190396800,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-18T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-18",
  "open":          21.05133,
  "high":          21.46674,
  "low":           20.95343,
  "close":         21.30534,
  "volume":        277177120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-21T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-21",
  "open":          21.46013,
  "high":          22.29229,
  "low":           21.40059,
  "close":         22.24731,
  "volume":        280521056,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-22T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-22",
  "open":          22.14676,
  "high":          22.22614,
  "low":           20.91506,
  "close":         21.19421,
  "volume":        388615328,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-23T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-23",
  "open":          21.70356,
  "high":          21.80807,
  "low":           21.31063,
  "close":         21.55009,
  "volume":        406059968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-24T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-24",
  "open":          21.8729,
  "high":          22.48809,
  "low":           21.06059,
  "close":         22.3505,
  "volume":        458177728,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-25T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-25",
  "open":          22.58334,
  "high":          22.63626,
  "low":           22.01711,
  "close":         22.45501,
  "volume":        267920016,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-28T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-28",
  "open":          22.45766,
  "high":          22.98685,
  "low":           22.37564,
  "close":         22.78708,
  "volume":        212509680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-29T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-29",
  "open":          22.63759,
  "high":          23.23954,
  "low":           22.52381,
  "close":         23.15884,
  "volume":        249293952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-04-30T00:  00:  00-04:  00",
  "tradingDay":    "2008-04-30",
  "open":          23.30966,
  "high":          23.81372,
  "low":           22.87705,
  "close":         23.01331,
  "volume":        307616640,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-01T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-01",
  "open":          23.14694,
  "high":          23.81372,
  "low":           23.13371,
  "close":         23.81372,
  "volume":        243922000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-02T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-02",
  "open":          23.83886,
  "high":          24.06773,
  "low":           23.62189,
  "close":         23.93808,
  "volume":        271593536,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-05T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-05",
  "open":          24.06773,
  "high":          24.51622,
  "low":           23.95263,
  "close":         24.43949,
  "volume":        230689040,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-06T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-06",
  "open":          24.43023,
  "high":          24.75568,
  "low":           24.10213,
  "close":         24.69483,
  "volume":        248050544,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-07T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-07",
  "open":          24.61412,
  "high":          24.89857,
  "low":           23.88516,
  "close":         24.15637,
  "volume":        312370272,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-08T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-08",
  "open":          24.31248,
  "high":          24.67366,
  "low":           24.21988,
  "close":         24.48315,
  "volume":        242709584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-09T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-09",
  "open":          24.23443,
  "high":          24.37599,
  "low":           23.99497,
  "close":         24.27015,
  "volume":        181696784,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-12T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-12",
  "open":          24.50299,
  "high":          24.98721,
  "low":           24.19077,
  "close":         24.89327,
  "volume":        221189312,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-13T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-13",
  "open":          24.95281,
  "high":          25.32854,
  "low":           24.85358,
  "close":         25.13141,
  "volume":        222233920,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-14T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-14",
  "open":          25.29943,
  "high":          25.43305,
  "low":           24.55062,
  "close":         24.64191,
  "volume":        247498000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-15T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-15",
  "open":          24.71467,
  "high":          25.12347,
  "low":           24.36937,
  "close":         25.10098,
  "volume":        235720848,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-16T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-16",
  "open":          25.15126,
  "high":          25.17639,
  "low":           24.73981,
  "close":         24.82183,
  "volume":        206720496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-19T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-19",
  "open":          24.85358,
  "high":          24.96339,
  "low":           23.98571,
  "close":         24.29,
  "volume":        255455776,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-20T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-20",
  "open":          24.0545,
  "high":          24.62868,
  "low":           23.82959,
  "close":         24.59428,
  "volume":        261812608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-21T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-21",
  "open":          24.56385,
  "high":          24.87211,
  "low":           23.3176,
  "close":         23.57426,
  "volume":        312511616,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-22T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-22",
  "open":          23.71582,
  "high":          23.98968,
  "low":           22.75533,
  "close":         23.42344,
  "volume":        325823200,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-23T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-23",
  "open":          23.91559,
  "high":          24.07699,
  "low":           23.52266,
  "close":         23.96851,
  "volume":        245204704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-27T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-27",
  "open":          24.17754,
  "high":          24.6644,
  "low":           24.05715,
  "close":         24.6644,
  "volume":        213236080,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-28T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-28",
  "open":          24.79405,
  "high":          24.86549,
  "low":           24.30587,
  "close":         24.74113,
  "volume":        200838352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-29T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-29",
  "open":          24.70673,
  "high":          24.89857,
  "low":           24.54136,
  "close":         24.6988,
  "volume":        174708784,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-05-30T00:  00:  00-04:  00",
  "tradingDay":    "2008-05-30",
  "open":          24.79934,
  "high":          25.07585,
  "low":           24.79008,
  "close":         24.97133,
  "volume":        164720000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-02T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-02",
  "open":          24.95149,
  "high":          25.0904,
  "low":           24.41303,
  "close":         24.62074,
  "volume":        183524464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-03T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-03",
  "open":          24.72129,
  "high":          24.89857,
  "low":           24.1233,
  "close":         24.52416,
  "volume":        204458192,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-04T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-04",
  "open":          24.34556,
  "high":          24.75171,
  "low":           24.24104,
  "close":         24.50035,
  "volume":        196382512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-05T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-05",
  "open":          24.65249,
  "high":          25.11554,
  "low":           24.56782,
  "close":         25.06129,
  "volume":        203933616,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-06T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-06",
  "open":          24.87211,
  "high":          25.13009,
  "low":           24.54798,
  "close":         24.55988,
  "volume":        260887424,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-09T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-09",
  "open":          24.44478,
  "high":          24.46727,
  "low":           23.25145,
  "close":         24.02672,
  "volume":        509775456,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-10T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-10",
  "open":          23.89707,
  "high":          24.7107,
  "low":           23.68407,
  "close":         24.55988,
  "volume":        307999104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-11T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-11",
  "open":          24.38789,
  "high":          24.60751,
  "low":           23.75948,
  "close":         23.92088,
  "volume":        259572224,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-12T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-12",
  "open":          24.01085,
  "high":          24.1577,
  "low":           22.64949,
  "close":         22.92203,
  "volume":        353187072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-13T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-13",
  "open":          22.7077,
  "high":          23.0411,
  "low":           21.87025,
  "close":         22.80428,
  "volume":        363343680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-16T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-16",
  "open":          22.92732,
  "high":          23.53589,
  "low":           22.3677,
  "close":         23.39565,
  "volume":        283916416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-17T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-17",
  "open":          23.56367,
  "high":          24.07699,
  "low":           23.47107,
  "close":         24.0029,
  "volume":        242913680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-18T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-18",
  "open":          23.96189,
  "high":          24.10477,
  "low":           23.46313,
  "close":         23.64835,
  "volume":        218985952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-19T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-19",
  "open":          23.61263,
  "high":          24.1233,
  "low":           23.39036,
  "close":         23.93279,
  "volume":        213995728,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-20T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-20",
  "open":          23.72773,
  "high":          23.94602,
  "low":           23.15223,
  "close":         23.18795,
  "volume":        239816128,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-23T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-23",
  "open":          23.05962,
  "high":          23.26865,
  "low":           22.69712,
  "close":         22.9088,
  "volume":        174393584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-24T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-24",
  "open":          22.80428,
  "high":          23.25542,
  "low":           22.70638,
  "close":         22.9207,
  "volume":        167930176,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-25T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-25",
  "open":          23.10063,
  "high":          23.65893,
  "low":           23.00405,
  "close":         23.46842,
  "volume":        173970304,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-26T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-26",
  "open":          23.02919,
  "high":          23.13106,
  "low":           22.22746,
  "close":         22.26053,
  "volume":        234752576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-27T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-27",
  "open":          22.02901,
  "high":          22.56614,
  "low":           21.71679,
  "close":         22.50264,
  "volume":        281357056,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-06-30T00:  00:  00-04:  00",
  "tradingDay":    "2008-06-30",
  "open":          22.51323,
  "high":          22.75533,
  "low":           22.04357,
  "close":         22.15205,
  "volume":        184738384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-01T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-01",
  "open":          21.71017,
  "high":          23.11518,
  "low":           21.69694,
  "close":         23.10989,
  "volume":        299992192,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-02T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-02",
  "open":          23.16943,
  "high":          23.47636,
  "low":           22.24995,
  "close":         22.24995,
  "volume":        226394960,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-03T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-03",
  "open":          22.44972,
  "high":          22.77782,
  "low":           21.92847,
  "close":         22.50661,
  "volume":        141302496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-07T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-07",
  "open":          22.88234,
  "high":          23.43402,
  "low":           22.7421,
  "close":         23.1734,
  "volume":        221465952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-08T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-08",
  "open":          23.20514,
  "high":          23.77403,
  "low":           22.85323,
  "close":         23.75418,
  "volume":        239903808,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-09T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-09",
  "open":          23.84018,
  "high":          23.93411,
  "low":           23.03845,
  "close":         23.053,
  "volume":        241903840,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-10T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-10",
  "open":          23.14164,
  "high":          23.46181,
  "low":           22.67198,
  "close":         23.36787,
  "volume":        226945232,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-11T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-11",
  "open":          23.21441,
  "high":          23.43138,
  "low":           22.62303,
  "close":         22.83207,
  "volume":        251058144,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-14T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-14",
  "open":          23.71317,
  "high":          23.72111,
  "low":           22.89821,
  "close":         23.00405,
  "volume":        239191792,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-15T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-15",
  "open":          22.81883,
  "high":          22.98553,
  "low":           22.01314,
  "close":         22.44311,
  "volume":        280761440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-16T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-16",
  "open":          22.51719,
  "high":          22.87837,
  "low":           22.30552,
  "close":         22.86249,
  "volume":        201941152,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-17T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-17",
  "open":          23.03316,
  "high":          23.14958,
  "low":           22.67463,
  "close":         22.73019,
  "volume":        204495232,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-18T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-18",
  "open":          22.31213,
  "high":          22.44443,
  "low":           21.82924,
  "close":         21.84909,
  "volume":        234429824,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-21T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-21",
  "open":          22.08061,
  "high":          22.15999,
  "low":           21.31592,
  "close":         21.99991,
  "volume":        367261312,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-22T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-22",
  "open":          19.71247,
  "high":          21.53289,
  "low":           19.38569,
  "close":         21.43499,
  "volume":        507399776,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-23T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-23",
  "open":          21.82792,
  "high":          22.27509,
  "low":           21.37414,
  "close":         21.99594,
  "volume":        286626208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-24T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-24",
  "open":          21.73928,
  "high":          21.86364,
  "low":           20.96269,
  "close":         21.03942,
  "volume":        226656496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-25T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-25",
  "open":          21.22067,
  "high":          21.56465,
  "low":           20.98915,
  "close":         21.44822,
  "volume":        171051152,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-28T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-28",
  "open":          21.47733,
  "high":          21.49453,
  "low":           20.37661,
  "close":         20.42688,
  "volume":        210820320,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-29T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-29",
  "open":          20.5605,
  "high":          21.09499,
  "low":           20.32765,
  "close":         20.78144,
  "volume":        184712688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-30T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-30",
  "open":          20.87405,
  "high":          21.23258,
  "low":           20.64914,
  "close":         21.15187,
  "volume":        195786128,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-07-31T00:  00:  00-04:  00",
  "tradingDay":    "2008-07-31",
  "open":          20.84229,
  "high":          21.45881,
  "low":           20.76821,
  "close":         21.02884,
  "volume":        172099536,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-01T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-01",
  "open":          21.15452,
  "high":          21.16643,
  "low":           20.60548,
  "close":         20.72587,
  "volume":        147100000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-04T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-04",
  "open":          20.71794,
  "high":          20.88992,
  "low":           20.22976,
  "close":         20.27209,
  "volume":        159998864,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-05T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-05",
  "open":          20.56579,
  "high":          21.27359,
  "low":           20.48244,
  "close":         21.25242,
  "volume":        185902416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-06T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-06",
  "open":          21.16378,
  "high":          22.14676,
  "low":           20.90315,
  "close":         21.72208,
  "volume":        213656336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-07T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-07",
  "open":          21.51834,
  "high":          21.98138,
  "low":           21.3662,
  "close":         21.64006,
  "volume":        181542576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-08T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-08",
  "open":          21.67842,
  "high":          22.44443,
  "low":           21.66387,
  "close":         22.4312,
  "volume":        192806512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-11T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-11",
  "open":          22.5,
  "high":          23.35067,
  "low":           22.44708,
  "close":         22.96172,
  "volume":        240609040,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-12T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-12",
  "open":          22.97495,
  "high":          23.71979,
  "low":           22.9551,
  "close":         23.3811,
  "volume":        225793296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-13T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-13",
  "open":          23.54648,
  "high":          23.81372,
  "low":           23.2713,
  "close":         23.72111,
  "volume":        227521200,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-14T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-14",
  "open":          23.59278,
  "high":          23.87325,
  "low":           23.52795,
  "close":         23.72376,
  "volume":        191978080,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-15T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-15",
  "open":          23.68671,
  "high":          23.78065,
  "low":           23.15884,
  "close":         23.25013,
  "volume":        191212384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-18T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-18",
  "open":          23.22764,
  "high":          23.52398,
  "low":           22.99612,
  "close":         23.20382,
  "volume":        149016880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-19T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-19",
  "open":          23.09137,
  "high":          23.42609,
  "low":           22.73019,
  "close":         22.95775,
  "volume":        166345120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-20T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-20",
  "open":          23.1218,
  "high":          23.40889,
  "low":           22.96833,
  "close":         23.26336,
  "volume":        136900336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-21T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-21",
  "open":          23.08211,
  "high":          23.21176,
  "low":           22.74078,
  "close":         23.05829,
  "volume":        145778736,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-22T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-22",
  "open":          23.26071,
  "high":          23.48297,
  "low":           23.22764,
  "close":         23.38904,
  "volume":        118713416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-25T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-25",
  "open":          23.30437,
  "high":          23.31495,
  "low":           22.71035,
  "close":         22.8281,
  "volume":        130790656,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-26T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-26",
  "open":          22.85588,
  "high":          23.13635,
  "low":           22.83603,
  "close":         22.9723,
  "volume":        120338528,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-27T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-27",
  "open":          22.92864,
  "high":          23.25277,
  "low":           22.78047,
  "close":         23.10857,
  "volume":        128977336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-28T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-28",
  "open":          23.19192,
  "high":          23.3176,
  "low":           22.85456,
  "close":         22.98553,
  "volume":        116452624,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-08-29T00:  00:  00-04:  00",
  "tradingDay":    "2008-08-29",
  "open":          22.88234,
  "high":          22.95378,
  "low":           22.36373,
  "close":         22.42855,
  "volume":        161795552,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-02T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-02",
  "open":          22.79634,
  "high":          22.95378,
  "low":           21.82924,
  "close":         21.98668,
  "volume":        210948064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-03T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-03",
  "open":          22.07267,
  "high":          22.3161,
  "low":           21.69694,
  "close":         22.08855,
  "volume":        198400672,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-04T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-04",
  "open":          21.94302,
  "high":          22.21423,
  "low":           21.27491,
  "close":         21.32915,
  "volume":        200901088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-05T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-05",
  "open":          20.97327,
  "high":          21.48527,
  "low":           20.85685,
  "close":         21.19156,
  "volume":        212420496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-08T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-08",
  "open":          21.77632,
  "high":          21.81469,
  "low":           20.03792,
  "close":         20.89257,
  "volume":        282432672,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-09T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-09",
  "open":          20.75233,
  "high":          21.16246,
  "low":           19.81698,
  "close":         20.06703,
  "volume":        336096928,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-10T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-10",
  "open":          20.16096,
  "high":          20.50494,
  "low":           19.68601,
  "close":         20.05777,
  "volume":        262750640,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-11T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-11",
  "open":          19.60398,
  "high":          20.24034,
  "low":           19.31557,
  "close":         20.19536,
  "volume":        262159552,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-12T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-12",
  "open":          19.96516,
  "high":          19.96516,
  "low":           19.38172,
  "close":         19.70453,
  "volume":        214154448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-15T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-15",
  "open":          18.79828,
  "high":          19.53916,
  "low":           18.56941,
  "close":         18.56941,
  "volume":        248526736,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-16T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-16",
  "open":          17.70947,
  "high":          18.85253,
  "low":           17.48324,
  "close":         18.50591,
  "volume":        323897984,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-17T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-17",
  "open":          18.32201,
  "high":          18.32466,
  "low":           16.91171,
  "close":         16.91171,
  "volume":        324065056,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-18T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-18",
  "open":          17.27421,
  "high":          17.91718,
  "low":           15.96578,
  "close":         17.7399,
  "volume":        452154240,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-19T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-19",
  "open":          18.86576,
  "high":          19.07743,
  "low":           18.0336,
  "close":         18.64217,
  "volume":        386268352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-22T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-22",
  "open":          18.51384,
  "high":          18.55486,
  "low":           17.28611,
  "close":         17.33771,
  "volume":        231271056,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-23T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-23",
  "open":          17.44355,
  "high":          17.96613,
  "low":           16.75692,
  "close":         16.78073,
  "volume":        346391840,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-24T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-24",
  "open":          16.83762,
  "high":          17.32448,
  "low":           16.55715,
  "close":         17.02813,
  "volume":        282738784,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-25T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-25",
  "open":          17.17101,
  "high":          17.83251,
  "low":           17.003,
  "close":         17.45413,
  "volume":        271583712,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-26T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-26",
  "open":          16.5254,
  "high":          17.17234,
  "low":           16.27271,
  "close":         16.96595,
  "volume":        304087488,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-29T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-29",
  "open":          15.82554,
  "high":          15.83348,
  "low":           13.3079,
  "close":         13.92573,
  "volume":        707829952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-09-30T00:  00:  00-04:  00",
  "tradingDay":    "2008-09-30",
  "open":          14.32131,
  "high":          15.21432,
  "low":           14.06332,
  "close":         15.03704,
  "volume":        439413344,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-01T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-01",
  "open":          14.7989,
  "high":          14.86505,
  "low":           14.20753,
  "close":         14.43641,
  "volume":        350233920,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-02T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-02",
  "open":          14.28955,
  "high":          14.39275,
  "low":           13.22984,
  "close":         13.24307,
  "volume":        434763264,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-03T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-03",
  "open":          13.75904,
  "high":          14.08978,
  "low":           12.52205,
  "close":         12.84221,
  "volume":        620870528,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-06T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-06",
  "open":          12.18204,
  "high":          13.06844,
  "low":           11.58141,
  "close":         12.98377,
  "volume":        569013504,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-07T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-07",
  "open":          13.28276,
  "high":          13.42829,
  "low":           11.76795,
  "close":         11.79573,
  "volume":        507987136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-08T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-08",
  "open":          11.36576,
  "high":          12.74431,
  "low":           11.33533,
  "close":         11.87908,
  "volume":        596401600,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-09T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-09",
  "open":          12.36064,
  "high":          12.67419,
  "low":           11.45704,
  "close":         11.74016,
  "volume":        436834336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-10T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-10",
  "open":          11.33798,
  "high":          13.22984,
  "low":           11.24537,
  "close":         12.80649,
  "volume":        599375168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-13T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-13",
  "open":          13.85165,
  "high":          14.62295,
  "low":           13.36479,
  "close":         14.58723,
  "volume":        415856000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-14T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-14",
  "open":          15.38102,
  "high":          15.39954,
  "low":           13.64526,
  "close":         13.76962,
  "volume":        535149952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-15T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-15",
  "open":          13.73919,
  "high":          14.15593,
  "low":           12.95069,
  "close":         12.95863,
  "volume":        427651296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-16T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-16",
  "open":          13.21926,
  "high":          13.68363,
  "low":           12.13706,
  "close":         13.47989,
  "volume":        534645760,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-17T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-17",
  "open":          13.17692,
  "high":          13.49973,
  "low":           11.36311,
  "close":         12.88587,
  "volume":        475967072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-20T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-20",
  "open":          13.20074,
  "high":          13.23381,
  "low":           12.38843,
  "close":         13.02346,
  "volume":        418201440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-21T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-21",
  "open":          12.81972,
  "high":          12.95202,
  "low":           12.06033,
  "close":         12.10398,
  "volume":        590340352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-22T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-22",
  "open":          12.8819,
  "high":          13.39522,
  "low":           12.29449,
  "close":         12.81575,
  "volume":        607070592,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-23T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-23",
  "open":          12.76812,
  "high":          13.13062,
  "low":           12.15823,
  "close":         12.99568,
  "volume":        452285760,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-24T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-24",
  "open":          11.93993,
  "high":          12.95202,
  "low":           11.92141,
  "close":         12.75092,
  "volume":        429239392,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-27T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-27",
  "open":          12.57761,
  "high":          12.9163,
  "low":           12.15294,
  "close":         12.18336,
  "volume":        326477024,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-28T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-28",
  "open":          12.62392,
  "high":          13.29599,
  "low":           12.22041,
  "close":         13.21794,
  "volume":        441563776,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-29T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-29",
  "open":          13.33039,
  "high":          14.49197,
  "low":           13.22191,
  "close":         13.8318,
  "volume":        527302496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-30T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-30",
  "open":          14.31866,
  "high":          14.84256,
  "low":           14.23664,
  "close":         14.69042,
  "volume":        442205504,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-10-31T00:  00:  00-04:  00",
  "tradingDay":    "2008-10-31",
  "open":          14.20885,
  "high":          14.65602,
  "low":           13.90986,
  "close":         14.23399,
  "volume":        448167040,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-11-03T00:  00:  00-05:  00",
  "tradingDay":    "2008-11-03",
  "open":          14.01702,
  "high":          14.43376,
  "low":           13.87281,
  "close":         14.15064,
  "volume":        285727488,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-11-04T00:  00:  00-05:  00",
  "tradingDay":    "2008-11-04",
  "open":          14.54754,
  "high":          14.78964,
  "low":           14.11227,
  "close":         14.6838,
  "volume":        377923584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-11-05T00:  00:  00-05:  00",
  "tradingDay":    "2008-11-05",
  "open":          14.40862,
  "high":          14.51579,
  "low":           13.62542,
  "close":         13.66643,
  "volume":        339498336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-11-06T00:  00:  00-05:  00",
  "tradingDay":    "2008-11-06",
  "open":          13.3714,
  "high":          13.59763,
  "low":           12.96525,
  "close":         13.11077,
  "volume":        356239264,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-11-07T00:  00:  00-05:  00",
  "tradingDay":    "2008-11-07",
  "open":          13.1293,
  "high":          13.21,
  "low":           12.66361,
  "close":         12.997,
  "volume":        295765408,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-11-10T00:  00:  00-05:  00",
  "tradingDay":    "2008-11-10",
  "open":          13.26953,
  "high":          13.28276,
  "low":           12.5022,
  "close":         12.68477,
  "volume":        303935552,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-11-11T00:  00:  00-05:  00",
  "tradingDay":    "2008-11-11",
  "open":          12.54718,
  "high":          12.85544,
  "low":           12.20585,
  "close":         12.53792,
  "volume":        330690208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-11-12T00:  00:  00-05:  00",
  "tradingDay":    "2008-11-12",
  "open":          12.23761,
  "high":          12.33551,
  "low":           11.90818,
  "close":         11.92274,
  "volume":        318253952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-11-13T00:  00:  00-05:  00",
  "tradingDay":    "2008-11-13",
  "open":          11.88966,
  "high":          12.75886,
  "low":           11.38031,
  "close":         12.75886,
  "volume":        500859264,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-11-14T00:  00:  00-05:  00",
  "tradingDay":    "2008-11-14",
  "open":          12.40562,
  "high":          12.43473,
  "low":           11.90686,
  "close":         11.93861,
  "volume":        379358208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-11-17T00:  00:  00-05:  00",
  "tradingDay":    "2008-11-17",
  "open":          11.70577,
  "high":          11.97962,
  "low":           11.54436,
  "close":         11.66078,
  "volume":        313933408,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-11-18T00:  00:  00-05:  00",
  "tradingDay":    "2008-11-18",
  "open":          11.86982,
  "high":          12.03784,
  "low":           11.49144,
  "close":         11.89495,
  "volume":        326737792,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-11-19T00:  00:  00-05:  00",
  "tradingDay":    "2008-11-19",
  "open":          11.83277,
  "high":          12.11589,
  "low":           11.40545,
  "close":         11.41603,
  "volume":        316458016,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-11-20T00:  00:  00-05:  00",
  "tradingDay":    "2008-11-20",
  "open":          11.27712,
  "high":          11.4372,
  "low":           10.58388,
  "close":         10.6487,
  "volume":        463437824,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-11-21T00:  00:  00-05:  00",
  "tradingDay":    "2008-11-21",
  "open":          10.84318,
  "high":          11.12895,
  "low":           10.4701,
  "close":         10.92521,
  "volume":        423708704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-11-24T00:  00:  00-05:  00",
  "tradingDay":    "2008-11-24",
  "open":          11.27315,
  "high":          12.54057,
  "low":           11.2242,
  "close":         12.29714,
  "volume":        389458112,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-11-25T00:  00:  00-05:  00",
  "tradingDay":    "2008-11-25",
  "open":          12.5194,
  "high":          12.52999,
  "low":           11.66343,
  "close":         12.0127,
  "volume":        333541344,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-11-26T00:  00:  00-05:  00",
  "tradingDay":    "2008-11-26",
  "open":          11.90289,
  "high":          12.60143,
  "low":           11.88701,
  "close":         12.56835,
  "volume":        243092816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-11-28T00:  00:  00-05:  00",
  "tradingDay":    "2008-11-28",
  "open":          12.52602,
  "high":          12.5366,
  "low":           12.15294,
  "close":         12.2601,
  "volume":        81310864,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-01T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-01",
  "open":          12.07223,
  "high":          12.20718,
  "low":           11.76398,
  "close":         11.7653,
  "volume":        249479136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-02T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-02",
  "open":          11.91083,
  "high":          12.25745,
  "low":           11.44382,
  "close":         12.23364,
  "volume":        310287104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-03T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-03",
  "open":          11.82748,
  "high":          12.73108,
  "low":           11.7481,
  "close":         12.68742,
  "volume":        361794880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-04T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-04",
  "open":          12.49294,
  "high":          12.59613,
  "low":           11.7825,
  "close":         12.0934,
  "volume":        294702656,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-05T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-05",
  "open":          11.95316,
  "high":          12.50088,
  "low":           11.75604,
  "close":         12.43605,
  "volume":        281939072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-08T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-08",
  "open":          12.86999,
  "high":          13.33568,
  "low":           12.67419,
  "close":         13.1928,
  "volume":        320080128,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-09T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-09",
  "open":          12.97715,
  "high":          13.70612,
  "low":           12.86073,
  "close":         13.23778,
  "volume":        325054464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-10T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-10",
  "open":          12.94805,
  "high":          13.16237,
  "low":           12.7668,
  "close":         12.99303,
  "volume":        253344640,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-11T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-11",
  "open":          12.87925,
  "high":          13.39389,
  "low":           12.54586,
  "close":         12.56835,
  "volume":        281635968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-12T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-12",
  "open":          12.2773,
  "high":          13.09755,
  "low":           12.24157,
  "close":         13.00097,
  "volume":        281509760,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-15T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-15",
  "open":          12.69933,
  "high":          12.72843,
  "low":           12.30375,
  "close":         12.53528,
  "volume":        240810848,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-16T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-16",
  "open":          12.44664,
  "high":          12.76415,
  "low":           12.27068,
  "close":         12.62524,
  "volume":        303553856,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-17T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-17",
  "open":          12.04313,
  "high":          12.05239,
  "low":           11.64491,
  "close":         11.79573,
  "volume":        350969376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-18T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-18",
  "open":          11.81557,
  "high":          12.01667,
  "low":           11.70047,
  "close":         11.83145,
  "volume":        231549216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-19T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-19",
  "open":          11.89892,
  "high":          12.03122,
  "low":           11.7481,
  "close":         11.90686,
  "volume":        220001088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-22T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-22",
  "open":          11.9095,
  "high":          11.91083,
  "low":           11.20436,
  "close":         11.34327,
  "volume":        228137232,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-23T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-23",
  "open":          11.49277,
  "high":          11.62506,
  "low":           11.36444,
  "close":         11.42794,
  "volume":        173222000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-24T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-24",
  "open":          11.39619,
  "high":          11.41074,
  "low":           11.18583,
  "close":         11.25066,
  "volume":        73280528,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-26T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-26",
  "open":          11.46234,
  "high":          11.56553,
  "low":           11.27712,
  "close":         11.35253,
  "volume":        83470376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-29T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-29",
  "open":          11.44646,
  "high":          11.59199,
  "low":           11.25463,
  "close":         11.45837,
  "volume":        185695312,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-30T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-30",
  "open":          11.56553,
  "high":          11.64888,
  "low":           11.20832,
  "close":         11.41603,
  "volume":        261375728,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2008-12-31T00:  00:  00-05:  00",
  "tradingDay":    "2008-12-31",
  "open":          11.3737,
  "high":          11.60786,
  "low":           11.29035,
  "close":         11.29167,
  "volume":        164141776,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-01-02T00:  00:  00-05:  00",
  "tradingDay":    "2009-01-02",
  "open":          11.36179,
  "high":          12.04445,
  "low":           11.26654,
  "close":         12.00608,
  "volume":        203813440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-01-05T00:  00:  00-05:  00",
  "tradingDay":    "2009-01-05",
  "open":          12.32625,
  "high":          12.72446,
  "low":           12.26539,
  "close":         12.51279,
  "volume":        320931232,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-01-06T00:  00:  00-05:  00",
  "tradingDay":    "2009-01-06",
  "open":          12.69403,
  "high":          12.85544,
  "low":           12.22305,
  "close":         12.3064,
  "volume":        348825728,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-01-07T00:  00:  00-05:  00",
  "tradingDay":    "2009-01-07",
  "open":          12.14632,
  "high":          12.23761,
  "low":           11.94126,
  "close":         12.04048,
  "volume":        204408304,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-01-08T00:  00:  00-05:  00",
  "tradingDay":    "2009-01-08",
  "open":          11.96375,
  "high":          12.3236,
  "low":           11.91215,
  "close":         12.26406,
  "volume":        181802592,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-01-09T00:  00:  00-05:  00",
  "tradingDay":    "2009-01-09",
  "open":          12.33154,
  "high":          12.35403,
  "low":           11.92538,
  "close":         11.98359,
  "volume":        148415200,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-01-12T00:  00:  00-05:  00",
  "tradingDay":    "2009-01-12",
  "open":          11.96772,
  "high":          12.03784,
  "low":           11.58273,
  "close":         11.72958,
  "volume":        166862896,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-01-13T00:  00:  00-05:  00",
  "tradingDay":    "2009-01-13",
  "open":          11.67401,
  "high":          11.87246,
  "low":           11.42397,
  "close":         11.6039,
  "volume":        215668448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-01-14T00:  00:  00-05:  00",
  "tradingDay":    "2009-01-14",
  "open":          11.40942,
  "high":          11.54304,
  "low":           11.20832,
  "close":         11.28903,
  "volume":        289385120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-01-15T00:  00:  00-05:  00",
  "tradingDay":    "2009-01-15",
  "open":          10.65929,
  "high":          11.12895,
  "low":           10.59049,
  "close":         11.03104,
  "volume":        494666464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-01-16T00:  00:  00-05:  00",
  "tradingDay":    "2009-01-16",
  "open":          11.15276,
  "high":          11.16334,
  "low":           10.6368,
  "close":         10.89213,
  "volume":        283039616,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-01-20T00:  00:  00-05:  00",
  "tradingDay":    "2009-01-20",
  "open":          10.83921,
  "high":          10.84847,
  "low":           10.34574,
  "close":         10.34574,
  "volume":        248580400,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-01-21T00:  00:  00-05:  00",
  "tradingDay":    "2009-01-21",
  "open":          10.50317,
  "high":          10.96489,
  "low":           10.49259,
  "close":         10.95828,
  "volume":        296961184,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-01-22T00:  00:  00-05:  00",
  "tradingDay":    "2009-01-22",
  "open":          11.64755,
  "high":          11.90686,
  "low":           11.35385,
  "close":         11.68989,
  "volume":        380504864,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-01-23T00:  00:  00-05:  00",
  "tradingDay":    "2009-01-23",
  "open":          11.48615,
  "high":          11.88966,
  "low":           11.44382,
  "close":         11.68989,
  "volume":        206180816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-01-26T00:  00:  00-05:  00",
  "tradingDay":    "2009-01-26",
  "open":          11.75604,
  "high":          12.03519,
  "low":           11.68195,
  "close":         11.85923,
  "volume":        188808720,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-01-27T00:  00:  00-05:  00",
  "tradingDay":    "2009-01-27",
  "open":          11.932,
  "high":          12.11192,
  "low":           11.87246,
  "close":         12.00344,
  "volume":        166972496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-01-28T00:  00:  00-05:  00",
  "tradingDay":    "2009-01-28",
  "open":          12.18733,
  "high":          12.56835,
  "low":           12.10531,
  "close":         12.46251,
  "volume":        237235600,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-01-29T00:  00:  00-05:  00",
  "tradingDay":    "2009-01-29",
  "open":          12.31566,
  "high":          12.48103,
  "low":           12.25084,
  "close":         12.30375,
  "volume":        160135680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-01-30T00:  00:  00-05:  00",
  "tradingDay":    "2009-01-30",
  "open":          12.25084,
  "high":          12.38578,
  "low":           11.90818,
  "close":         11.92406,
  "volume":        175960496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-02-02T00:  00:  00-05:  00",
  "tradingDay":    "2009-02-02",
  "open":          11.78779,
  "high":          12.17146,
  "low":           11.76133,
  "close":         12.10663,
  "volume":        150744784,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-02-03T00:  00:  00-05:  00",
  "tradingDay":    "2009-02-03",
  "open":          12.16087,
  "high":          12.35403,
  "low":           11.9439,
  "close":         12.30111,
  "volume":        161820496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-02-04T00:  00:  00-05:  00",
  "tradingDay":    "2009-02-04",
  "open":          12.33286,
  "high":          12.73372,
  "low":           12.31698,
  "close":         12.37652,
  "volume":        218232352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-02-05T00:  00:  00-05:  00",
  "tradingDay":    "2009-02-05",
  "open":          12.27333,
  "high":          12.86602,
  "low":           12.25348,
  "close":         12.76151,
  "volume":        202305488,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-02-06T00:  00:  00-05:  00",
  "tradingDay":    "2009-02-06",
  "open":          12.83559,
  "high":          13.22984,
  "low":           12.83295,
  "close":         13.1928,
  "volume":        185494256,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-02-09T00:  00:  00-05:  00",
  "tradingDay":    "2009-02-09",
  "open":          13.22984,
  "high":          13.62674,
  "low":           13.16369,
  "close":         13.56191,
  "volume":        193015888,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-02-10T00:  00:  00-05:  00",
  "tradingDay":    "2009-02-10",
  "open":          13.4058,
  "high":          13.56191,
  "low":           12.84089,
  "close":         12.94276,
  "volume":        229275568,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-02-11T00:  00:  00-05:  00",
  "tradingDay":    "2009-02-11",
  "open":          12.7496,
  "high":          13.00626,
  "low":           12.67022,
  "close":         12.80913,
  "volume":        182246288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-02-12T00:  00:  00-05:  00",
  "tradingDay":    "2009-02-12",
  "open":          12.67816,
  "high":          13.19677,
  "low":           12.67816,
  "close":         13.13327,
  "volume":        220643568,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-02-13T00:  00:  00-05:  00",
  "tradingDay":    "2009-02-13",
  "open":          13.09622,
  "high":          13.22191,
  "low":           12.98112,
  "close":         13.11871,
  "volume":        164418416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-02-17T00:  00:  00-05:  00",
  "tradingDay":    "2009-02-17",
  "open":          12.81575,
  "high":          12.83824,
  "low":           12.4731,
  "close":         12.50617,
  "volume":        183090592,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-02-18T00:  00:  00-05:  00",
  "tradingDay":    "2009-02-18",
  "open":          12.57497,
  "high":          12.68081,
  "low":           12.26671,
  "close":         12.485,
  "volume":        184800368,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-02-19T00:  00:  00-05:  00",
  "tradingDay":    "2009-02-19",
  "open":          12.35271,
  "high":          12.46913,
  "low":           11.92141,
  "close":         11.99153,
  "volume":        249112544,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-02-20T00:  00:  00-05:  00",
  "tradingDay":    "2009-02-20",
  "open":          11.82748,
  "high":          12.22438,
  "low":           11.77456,
  "close":         12.06562,
  "volume":        202545088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-02-23T00:  00:  00-05:  00",
  "tradingDay":    "2009-02-23",
  "open":          12.12515,
  "high":          12.17146,
  "low":           11.44514,
  "close":         11.50335,
  "volume":        212444688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-02-24T00:  00:  00-05:  00",
  "tradingDay":    "2009-02-24",
  "open":          11.5695,
  "high":          12.0246,
  "low":           11.50996,
  "close":         11.93993,
  "volume":        217876336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-02-25T00:  00:  00-05:  00",
  "tradingDay":    "2009-02-25",
  "open":          11.88834,
  "high":          12.29317,
  "low":           11.80764,
  "close":         12.06033,
  "volume":        224954272,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-02-26T00:  00:  00-05:  00",
  "tradingDay":    "2009-02-26",
  "open":          12.17146,
  "high":          12.29317,
  "low":           11.76927,
  "close":         11.7997,
  "volume":        170084400,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-02-27T00:  00:  00-05:  00",
  "tradingDay":    "2009-02-27",
  "open":          11.633,
  "high":          12.07885,
  "low":           11.5986,
  "close":         11.81557,
  "volume":        190786832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-02T00:  00:  00-05:  00",
  "tradingDay":    "2009-03-02",
  "open":          11.65814,
  "high":          12.06562,
  "low":           11.5986,
  "close":         11.63433,
  "volume":        208125664,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-03T00:  00:  00-05:  00",
  "tradingDay":    "2009-03-03",
  "open":          11.7653,
  "high":          12.00476,
  "low":           11.62639,
  "close":         11.69121,
  "volume":        195536704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-04T00:  00:  00-05:  00",
  "tradingDay":    "2009-03-04",
  "open":          11.93067,
  "high":          12.27333,
  "low":           11.8341,
  "close":         12.06165,
  "volume":        200176208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-05T00:  00:  00-05:  00",
  "tradingDay":    "2009-03-05",
  "open":          11.96772,
  "high":          12.15426,
  "low":           11.7018,
  "close":         11.75339,
  "volume":        190827648,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-06T00:  00:  00-05:  00",
  "tradingDay":    "2009-03-06",
  "open":          11.68724,
  "high":          11.69518,
  "low":           10.89213,
  "close":         11.28506,
  "volume":        272960896,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-09T00:  00:  00-04:  00",
  "tradingDay":    "2009-03-09",
  "open":          11.13688,
  "high":          11.58934,
  "low":           10.92388,
  "close":         10.99532,
  "volume":        188555504,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-10T00:  00:  00-04:  00",
  "tradingDay":    "2009-03-10",
  "open":          11.22817,
  "high":          11.79705,
  "low":           11.1607,
  "close":         11.72561,
  "volume":        227975472,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-11T00:  00:  00-04:  00",
  "tradingDay":    "2009-03-11",
  "open":          11.88172,
  "high":          12.44531,
  "low":           11.85129,
  "close":         12.26142,
  "volume":        228591504,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-12T00:  00:  00-04:  00",
  "tradingDay":    "2009-03-12",
  "open":          12.29053,
  "high":          12.77738,
  "low":           12.17146,
  "close":         12.74695,
  "volume":        207542880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-13T00:  00:  00-04:  00",
  "tradingDay":    "2009-03-13",
  "open":          12.74034,
  "high":          12.85941,
  "low":           12.56968,
  "close":         12.69139,
  "volume":        162316352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-16T00:  00:  00-04:  00",
  "tradingDay":    "2009-03-16",
  "open":          12.77077,
  "high":          12.88455,
  "low":           12.45987,
  "close":         12.62392,
  "volume":        215299584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-17T00:  00:  00-04:  00",
  "tradingDay":    "2009-03-17",
  "open":          12.6001,
  "high":          13.18883,
  "low":           12.57761,
  "close":         13.18486,
  "volume":        212727376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-18T00:  00:  00-04:  00",
  "tradingDay":    "2009-03-18",
  "open":          13.21661,
  "high":          13.69024,
  "low":           13.1928,
  "close":         13.43094,
  "volume":        214936016,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-19T00:  00:  00-04:  00",
  "tradingDay":    "2009-03-19",
  "open":          13.4746,
  "high":          13.6532,
  "low":           13.26292,
  "close":         13.44417,
  "volume":        135061312,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-20T00:  00:  00-04:  00",
  "tradingDay":    "2009-03-20",
  "open":          13.50635,
  "high":          13.64129,
  "low":           13.30525,
  "close":         13.4402,
  "volume":        187806448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-23T00:  00:  00-04:  00",
  "tradingDay":    "2009-03-23",
  "open":          13.58837,
  "high":          14.3094,
  "low":           13.46137,
  "close":         14.24325,
  "volume":        179940144,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-24T00:  00:  00-04:  00",
  "tradingDay":    "2009-03-24",
  "open":          14.07126,
  "high":          14.47874,
  "low":           13.94293,
  "close":         14.08978,
  "volume":        173694416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-25T00:  00:  00-04:  00",
  "tradingDay":    "2009-03-25",
  "open":          14.23267,
  "high":          14.33586,
  "low":           13.74052,
  "close":         14.08846,
  "volume":        174564416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-26T00:  00:  00-04:  00",
  "tradingDay":    "2009-03-26",
  "open":          14.26574,
  "high":          14.55018,
  "low":           14.23267,
  "close":         14.53563,
  "volume":        166357968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-27T00:  00:  00-04:  00",
  "tradingDay":    "2009-03-27",
  "open":          14.31998,
  "high":          14.35835,
  "low":           14.07655,
  "close":         14.13609,
  "volume":        133097560,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-30T00:  00:  00-04:  00",
  "tradingDay":    "2009-03-30",
  "open":          13.82651,
  "high":          13.89266,
  "low":           13.57514,
  "close":         13.82386,
  "volume":        135748384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-03-31T00:  00:  00-04:  00",
  "tradingDay":    "2009-03-31",
  "open":          13.95087,
  "high":          14.21547,
  "low":           13.89134,
  "close":         13.90721,
  "volume":        153893728,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-01T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-01",
  "open":          13.77094,
  "high":          14.42053,
  "low":           13.74448,
  "close":         14.37952,
  "volume":        159162112,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-02T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-02",
  "open":          14.574,
  "high":          15.18125,
  "low":           14.52372,
  "close":         14.91136,
  "volume":        219299632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-03T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-03",
  "open":          15.10716,
  "high":          15.36382,
  "low":           15.01852,
  "close":         15.3453,
  "volume":        171782832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-06T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-06",
  "open":          15.20638,
  "high":          15.71044,
  "low":           14.98677,
  "close":         15.67075,
  "volume":        177645328,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-07T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-07",
  "open":          15.41674,
  "high":          15.43526,
  "low":           15.10716,
  "close":         15.21432,
  "volume":        144906464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-08T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-08",
  "open":          15.27121,
  "high":          15.45113,
  "low":           15.15876,
  "close":         15.38895,
  "volume":        122997672,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-09T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-09",
  "open":          15.66678,
  "high":          15.87581,
  "low":           15.60592,
  "close":         15.81892,
  "volume":        143327472,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-13T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-13",
  "open":          15.87714,
  "high":          16.00547,
  "low":           15.74351,
  "close":         15.90492,
  "volume":        105137296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-14T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-14",
  "open":          15.81892,
  "high":          15.8983,
  "low":           15.51199,
  "close":         15.65223,
  "volume":        122761840,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-15T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-15",
  "open":          15.49082,
  "high":          15.64429,
  "low":           15.31487,
  "close":         15.56359,
  "volume":        111497160,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-16T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-16",
  "open":          15.76865,
  "high":          16.29255,
  "low":           15.71573,
  "close":         16.06765,
  "volume":        160238480,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-17T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-17",
  "open":          16.01605,
  "high":          16.43808,
  "low":           15.90889,
  "close":         16.32827,
  "volume":        134350032,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-20T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-20",
  "open":          16.10337,
  "high":          16.27139,
  "low":           15.76468,
  "close":         15.94196,
  "volume":        125974272,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-21T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-21",
  "open":          15.72896,
  "high":          16.15893,
  "low":           15.69059,
  "close":         16.10866,
  "volume":        127061968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-22T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-22",
  "open":          16.22376,
  "high":          16.58361,
  "low":           16.03457,
  "close":         16.07558,
  "volume":        253421728,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-23T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-23",
  "open":          16.75163,
  "high":          16.82836,
  "low":           16.34018,
  "close":         16.59023,
  "volume":        255146624,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-24T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-24",
  "open":          16.48968,
  "high":          16.55583,
  "low":           16.26874,
  "close":         16.39178,
  "volume":        146029696,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-27T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-27",
  "open":          16.25948,
  "high":          16.53731,
  "low":           16.22773,
  "close":         16.50159,
  "volume":        129796696,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-28T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-28",
  "open":          16.31901,
  "high":          16.69739,
  "low":           16.30711,
  "close":         16.39178,
  "volume":        123039240,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-29T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-29",
  "open":          16.51746,
  "high":          16.78206,
  "low":           16.38252,
  "close":         16.55583,
  "volume":        123707432,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-04-30T00:  00:  00-04:  00",
  "tradingDay":    "2009-04-30",
  "open":          16.69871,
  "high":          16.8019,
  "low":           16.52672,
  "close":         16.64711,
  "volume":        134567728,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-05-01T00:  00:  00-04:  00",
  "tradingDay":    "2009-05-01",
  "open":          16.64314,
  "high":          16.92759,
  "low":           16.64314,
  "close":         16.83365,
  "volume":        107330064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-05-04T00:  00:  00-04:  00",
  "tradingDay":    "2009-05-04",
  "open":          16.96595,
  "high":          17.49647,
  "low":           16.89186,
  "close":         17.47266,
  "volume":        164557504,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-05-05T00:  00:  00-04:  00",
  "tradingDay":    "2009-05-05",
  "open":          17.43164,
  "high":          17.57717,
  "low":           17.34697,
  "close":         17.55733,
  "volume":        107549264,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-05-06T00:  00:  00-04:  00",
  "tradingDay":    "2009-05-06",
  "open":          17.63935,
  "high":          17.66184,
  "low":           17.2279,
  "close":         17.52954,
  "volume":        127860160,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-05-07T00:  00:  00-04:  00",
  "tradingDay":    "2009-05-07",
  "open":          17.50705,
  "high":          17.51499,
  "low":           16.92097,
  "close":         17.07444,
  "volume":        143585968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-05-08T00:  00:  00-04:  00",
  "tradingDay":    "2009-05-08",
  "open":          17.07179,
  "high":          17.36152,
  "low":           16.704,
  "close":         17.09164,
  "volume":        126343896,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-05-11T00:  00:  00-04:  00",
  "tradingDay":    "2009-05-11",
  "open":          16.85085,
  "high":          17.3258,
  "low":           16.81778,
  "close":         17.14191,
  "volume":        109271888,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-05-12T00:  00:  00-04:  00",
  "tradingDay":    "2009-05-12",
  "open":          17.14058,
  "high":          17.16043,
  "low":           16.30578,
  "close":         16.46057,
  "volume":        164530288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-05-13T00:  00:  00-04:  00",
  "tradingDay":    "2009-05-13",
  "open":          16.30049,
  "high":          16.40765,
  "low":           15.79379,
  "close":         15.80834,
  "volume":        160883232,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-05-14T00:  00:  00-04:  00",
  "tradingDay":    "2009-05-14",
  "open":          15.84671,
  "high":          16.34283,
  "low":           15.83612,
  "close":         16.26609,
  "volume":        120913744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-05-15T00:  00:  00-04:  00",
  "tradingDay":    "2009-05-15",
  "open":          16.18274,
  "high":          16.48703,
  "low":           16.08881,
  "close":         16.19597,
  "volume":        99224904,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-05-18T00:  00:  00-04:  00",
  "tradingDay":    "2009-05-18",
  "open":          16.36929,
  "high":          16.76221,
  "low":           16.08352,
  "close":         16.7556,
  "volume":        123931168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-05-19T00:  00:  00-04:  00",
  "tradingDay":    "2009-05-19",
  "open":          16.77809,
  "high":          17.10751,
  "low":           16.63521,
  "close":         16.86143,
  "volume":        100535576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-05-20T00:  00:  00-04:  00",
  "tradingDay":    "2009-05-20",
  "open":          16.88525,
  "high":          17.09428,
  "low":           16.577,
  "close":         16.6524,
  "volume":        104949840,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-05-21T00:  00:  00-04:  00",
  "tradingDay":    "2009-05-21",
  "open":          16.55715,
  "high":          16.7728,
  "low":           16.25816,
  "close":         16.42882,
  "volume":        110188752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-05-22T00:  00:  00-04:  00",
  "tradingDay":    "2009-05-22",
  "open":          16.41162,
  "high":          16.42882,
  "low":           16.10733,
  "close":         16.20656,
  "volume":        80464288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-05-26T00:  00:  00-04:  00",
  "tradingDay":    "2009-05-26",
  "open":          16.50555,
  "high":          17.30861,
  "low":           16.47777,
  "close":         17.30199,
  "volume":        171939296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-05-27T00:  00:  00-04:  00",
  "tradingDay":    "2009-05-27",
  "open":          17.43429,
  "high":          17.85764,
  "low":           17.31919,
  "close":         17.60231,
  "volume":        174649072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-05-28T00:  00:  00-04:  00",
  "tradingDay":    "2009-05-28",
  "open":          17.65523,
  "high":          17.91189,
  "low":           17.46736,
  "close":         17.86955,
  "volume":        131615312,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-05-29T00:  00:  00-04:  00",
  "tradingDay":    "2009-05-29",
  "open":          17.91189,
  "high":          17.97936,
  "low":           17.70815,
  "close":         17.96745,
  "volume":        123274320,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-01T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-01",
  "open":          18.2466,
  "high":          18.52046,
  "low":           17.99259,
  "close":         18.43579,
  "volume":        122198720,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-02T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-02",
  "open":          18.38816,
  "high":          18.69906,
  "low":           18.30349,
  "close":         18.45431,
  "volume":        123157912,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-03T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-03",
  "open":          18.52178,
  "high":          18.66863,
  "low":           18.39874,
  "close":         18.64746,
  "volume":        152576256,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-04T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-04",
  "open":          18.53898,
  "high":          19.07479,
  "low":           18.52707,
  "close":         19.01658,
  "volume":        148644240,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-05T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-05",
  "open":          19.22429,
  "high":          19.36849,
  "low":           18.94646,
  "close":         19.13961,
  "volume":        170802464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-08T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-08",
  "open":          19.02716,
  "high":          19.0814,
  "low":           18.44637,
  "close":         19.03113,
  "volume":        251501072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-09T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-09",
  "open":          19.02584,
  "high":          19.12506,
  "low":           18.59455,
  "close":         18.88163,
  "volume":        182747424,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-10T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-10",
  "open":          18.82342,
  "high":          18.83268,
  "low":           18.29688,
  "close":         18.55486,
  "volume":        185942480,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-11T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-11",
  "open":          18.46225,
  "high":          18.72817,
  "low":           18.32995,
  "close":         18.51517,
  "volume":        141676656,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-12T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-12",
  "open":          18.36435,
  "high":          18.40271,
  "low":           17.99788,
  "close":         18.12092,
  "volume":        152005568,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-15T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-15",
  "open":          17.99391,
  "high":          18.11562,
  "low":           17.84574,
  "close":         18.00449,
  "volume":        145759840,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-16T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-16",
  "open":          18.0799,
  "high":          18.31936,
  "low":           18.00582,
  "close":         18.03889,
  "volume":        138972160,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-17T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-17",
  "open":          18.08123,
  "high":          18.18442,
  "low":           17.79811,
  "close":         17.93702,
  "volume":        154253520,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-18T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-18",
  "open":          18.01243,
  "high":          18.25718,
  "low":           17.93834,
  "close":         17.97671,
  "volume":        115452608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-19T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-19",
  "open":          18.26645,
  "high":          18.45563,
  "low":           18.11166,
  "close":         18.45299,
  "volume":        194866240,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-22T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-22",
  "open":          18.61042,
  "high":          18.72817,
  "low":           18.03625,
  "close":         18.17384,
  "volume":        171395824,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-23T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-23",
  "open":          18.04551,
  "high":          18.11827,
  "low":           17.57982,
  "close":         17.72931,
  "volume":        190729392,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-24T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-24",
  "open":          17.91586,
  "high":          18.19104,
  "low":           17.84177,
  "close":         18.02169,
  "volume":        131068064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-25T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-25",
  "open":          17.95951,
  "high":          18.54824,
  "low":           17.88807,
  "close":         18.50326,
  "volume":        159187824,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-26T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-26",
  "open":          18.494,
  "high":          18.99276,
  "low":           18.48738,
  "close":         18.84459,
  "volume":        118636320,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-29T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-29",
  "open":          18.97953,
  "high":          19.04436,
  "low":           18.72552,
  "close":         18.78241,
  "volume":        153241408,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-06-30T00:  00:  00-04:  00",
  "tradingDay":    "2009-06-30",
  "open":          18.86311,
  "high":          19.02452,
  "low":           18.75992,
  "close":         18.84327,
  "volume":        117226632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-01T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-01",
  "open":          18.98483,
  "high":          19.13829,
  "low":           18.85518,
  "close":         18.89619,
  "volume":        111833520,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-02T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-02",
  "open":          18.68715,
  "high":          18.89619,
  "low":           18.494,
  "close":         18.52443,
  "volume":        100048040,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-06T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-06",
  "open":          18.34979,
  "high":          18.38816,
  "low":           18.02566,
  "close":         18.33789,
  "volume":        134621392,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-07T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-07",
  "open":          18.32069,
  "high":          18.47944,
  "low":           17.8841,
  "close":         17.91321,
  "volume":        124658312,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-08T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-08",
  "open":          17.982,
  "high":          18.26248,
  "low":           17.78356,
  "close":         18.15399,
  "volume":        155472736,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-09T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-09",
  "open":          18.22543,
  "high":          18.25586,
  "low":           17.98333,
  "close":         18.04021,
  "volume":        92632992,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-10T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-10",
  "open":          18.03757,
  "high":          18.38552,
  "low":           18.03492,
  "close":         18.32598,
  "volume":        120202472,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-13T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-13",
  "open":          18.46092,
  "high":          18.83136,
  "low":           18.195,
  "close":         18.83136,
  "volume":        130548024,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-14T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-14",
  "open":          18.79035,
  "high":          18.94249,
  "low":           18.67525,
  "close":         18.8221,
  "volume":        93808360,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-15T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-15",
  "open":          19.18856,
  "high":          19.44787,
  "low":           19.09331,
  "close":         19.432,
  "volume":        131084688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-16T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-16",
  "open":          19.28382,
  "high":          19.58282,
  "low":           19.25868,
  "close":         19.51667,
  "volume":        106244640,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-17T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-17",
  "open":          19.72305,
  "high":          20.11201,
  "low":           19.66352,
  "close":         20.07629,
  "volume":        162552176,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-20T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-20",
  "open":          20.27738,
  "high":          20.51155,
  "low":           19.96251,
  "close":         20.22976,
  "volume":        198556384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-21T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-21",
  "open":          20.28003,
  "high":          20.29855,
  "low":           19.81169,
  "close":         20.04454,
  "volume":        236148672,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-22T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-22",
  "open":          20.87537,
  "high":          20.99973,
  "low":           20.65311,
  "close":         20.73646,
  "volume":        235965744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-23T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-23",
  "open":          20.7219,
  "high":          20.96136,
  "low":           20.58035,
  "close":         20.87934,
  "volume":        142254128,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-24T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-24",
  "open":          20.76424,
  "high":          21.16775,
  "low":           20.70471,
  "close":         21.16643,
  "volume":        118336240,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-27T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-27",
  "open":          21.19024,
  "high":          21.28417,
  "low":           20.80525,
  "close":         21.18098,
  "volume":        116973416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-28T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-28",
  "open":          21.01958,
  "high":          21.18098,
  "low":           20.85023,
  "close":         21.16775,
  "volume":        98141744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-29T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-29",
  "open":          21.02222,
  "high":          21.22728,
  "low":           20.93623,
  "close":         21.17172,
  "volume":        103237048,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-30T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-30",
  "open":          21.39266,
  "high":          21.7922,
  "low":           21.3662,
  "close":         21.53686,
  "volume":        126770200,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-07-31T00:  00:  00-04:  00",
  "tradingDay":    "2009-07-31",
  "open":          21.56332,
  "high":          21.82924,
  "low":           21.55274,
  "close":         21.61624,
  "volume":        114106408,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-03T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-03",
  "open":          21.85703,
  "high":          22.04621,
  "low":           21.81204,
  "close":         22.01843,
  "volume":        106425288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-04T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-04",
  "open":          21.81998,
  "high":          21.90465,
  "low":           21.72473,
  "close":         21.90201,
  "volume":        106849336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-05T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-05",
  "open":          21.92847,
  "high":          22.14544,
  "low":           21.72473,
  "close":         21.8438,
  "volume":        114238688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-06T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-06",
  "open":          21.90598,
  "high":          22.02901,
  "low":           21.57655,
  "close":         21.68504,
  "volume":        92262616,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-07T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-07",
  "open":          21.89407,
  "high":          22.04092,
  "low":           21.80278,
  "close":         21.89671,
  "volume":        104602144,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-10T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-10",
  "open":          21.91656,
  "high":          22.04092,
  "low":           21.65196,
  "close":         21.7922,
  "volume":        81103000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-11T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-11",
  "open":          21.65593,
  "high":          21.74722,
  "low":           21.41647,
  "close":         21.54215,
  "volume":        95964848,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-12T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-12",
  "open":          21.50511,
  "high":          22.05547,
  "low":           21.49321,
  "close":         21.87025,
  "volume":        120147296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-13T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-13",
  "open":          22.04753,
  "high":          22.31478,
  "low":           22.02769,
  "close":         22.2817,
  "volume":        118822264,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-14T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-14",
  "open":          22.2182,
  "high":          22.25657,
  "low":           21.89936,
  "close":         22.06473,
  "volume":        82618512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-17T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-17",
  "open":          21.63741,
  "high":          21.6427,
  "low":           21.09102,
  "close":         21.11351,
  "volume":        141557232,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-18T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-18",
  "open":          21.3834,
  "high":          21.7287,
  "low":           21.35429,
  "close":         21.69694,
  "volume":        116389880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-19T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-19",
  "open":          21.53157,
  "high":          21.86893,
  "low":           21.49188,
  "close":         21.77632,
  "volume":        111562920,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-20T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-20",
  "open":          21.8266,
  "high":          22.0568,
  "low":           21.77765,
  "close":         22.0052,
  "volume":        92376752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-21T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-21",
  "open":          22.17983,
  "high":          22.40739,
  "low":           22.06738,
  "close":         22.38754,
  "volume":        112319544,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-24T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-24",
  "open":          22.50661,
  "high":          22.58467,
  "low":           22.26186,
  "close":         22.36637,
  "volume":        109850880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-25T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-25",
  "open":          22.41929,
  "high":          22.6151,
  "low":           22.37564,
  "close":         22.41135,
  "volume":        87559608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-26T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-26",
  "open":          22.34785,
  "high":          22.4312,
  "low":           22.06209,
  "close":         22.14808,
  "volume":        82064456,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-27T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-27",
  "open":          22.32536,
  "high":          22.43385,
  "low":           21.80675,
  "close":         22.41797,
  "volume":        121256912,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-28T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-28",
  "open":          22.79105,
  "high":          22.82016,
  "low":           22.29626,
  "close":         22.49735,
  "volume":        122519960,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-08-31T00:  00:  00-04:  00",
  "tradingDay":    "2009-08-31",
  "open":          22.24731,
  "high":          22.33859,
  "low":           22.02769,
  "close":         22.25392,
  "volume":        84100768,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-01T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-01",
  "open":          22.22482,
  "high":          22.49073,
  "low":           21.8213,
  "close":         21.86893,
  "volume":        126614496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-02T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-02",
  "open":          21.77897,
  "high":          22.17454,
  "low":           21.7115,
  "close":         21.85306,
  "volume":        98370776,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-03T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-03",
  "open":          22.01975,
  "high":          22.10707,
  "low":           21.82924,
  "close":         22.03431,
  "volume":        79393224,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-04T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-04",
  "open":          22.13088,
  "high":          22.58334,
  "low":           22.10575,
  "close":         22.53175,
  "volume":        101131200,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-08T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-08",
  "open":          22.88498,
  "high":          22.90615,
  "low":           22.75533,
  "close":         22.87837,
  "volume":        85047112,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-09T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-09",
  "open":          22.85852,
  "high":          23.08211,
  "low":           22.45104,
  "close":         22.64156,
  "volume":        219009392,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-10T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-10",
  "open":          22.76327,
  "high":          22.9207,
  "low":           22.5979,
  "close":         22.82942,
  "volume":        132582064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-11T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-11",
  "open":          22.87572,
  "high":          22.91144,
  "low":           22.60583,
  "close":         22.7765,
  "volume":        94202168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-14T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-14",
  "open":          22.60054,
  "high":          23.0067,
  "low":           22.52381,
  "close":         22.98289,
  "volume":        86926952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-15T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-15",
  "open":          23.02522,
  "high":          23.23822,
  "low":           22.96568,
  "close":         23.1734,
  "volume":        115126072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-16T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-16",
  "open":          23.5478,
  "high":          24.17754,
  "low":           23.53325,
  "close":         24.06112,
  "volume":        203549648,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-17T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-17",
  "open":          24.07567,
  "high":          24.71202,
  "low":           24.07435,
  "close":         24.41568,
  "volume":        218815120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-18T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-18",
  "open":          24.58502,
  "high":          24.68027,
  "low":           24.44346,
  "close":         24.47786,
  "volume":        162442576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-21T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-21",
  "open":          24.38128,
  "high":          24.49638,
  "low":           24.02804,
  "close":         24.34556,
  "volume":        118162392,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-22T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-22",
  "open":          24.50035,
  "high":          24.52549,
  "low":           24.19077,
  "close":         24.40642,
  "volume":        96363192,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-23T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-23",
  "open":          24.52152,
  "high":          24.99117,
  "low":           24.47918,
  "close":         24.54136,
  "volume":        160362432,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-24T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-24",
  "open":          24.76627,
  "high":          24.83242,
  "low":           24.18019,
  "close":         24.3191,
  "volume":        148710752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-25T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-25",
  "open":          24.07964,
  "high":          24.54136,
  "low":           24.00423,
  "close":         24.12727,
  "volume":        120259920,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-28T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-28",
  "open":          24.32571,
  "high":          24.69747,
  "low":           24.25427,
  "close":         24.62735,
  "volume":        91148464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-29T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-29",
  "open":          24.70409,
  "high":          24.79273,
  "low":           24.38392,
  "close":         24.52549,
  "volume":        93236928,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-09-30T00:  00:  00-04:  00",
  "tradingDay":    "2009-09-30",
  "open":          24.62471,
  "high":          24.66704,
  "low":           24.15902,
  "close":         24.52152,
  "volume":        145661584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-01T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-01",
  "open":          24.52152,
  "high":          24.63662,
  "low":           23.90633,
  "close":         23.9275,
  "volume":        141646416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-02T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-02",
  "open":          23.99629,
  "high":          24.59957,
  "low":           23.99232,
  "close":         24.46198,
  "volume":        149400848,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-05T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-05",
  "open":          24.63397,
  "high":          24.72129,
  "low":           24.37863,
  "close":         24.61016,
  "volume":        114225080,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-06T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-06",
  "open":          24.83771,
  "high":          25.13803,
  "low":           24.7795,
  "close":         25.13803,
  "volume":        163343568,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-07T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-07",
  "open":          25.10495,
  "high":          25.20947,
  "low":           25.00837,
  "close":         25.16978,
  "volume":        125707456,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-08T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-08",
  "open":          25.22402,
  "high":          25.32854,
  "low":           24.98985,
  "close":         25.04013,
  "volume":        118295424,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-09T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-09",
  "open":          25.00044,
  "high":          25.22931,
  "low":           24.95413,
  "close":         25.19888,
  "volume":        79201992,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-12T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-12",
  "open":          25.27165,
  "high":          25.33647,
  "low":           25.08908,
  "close":         25.24386,
  "volume":        77787008,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-13T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-13",
  "open":          25.22005,
  "high":          25.29149,
  "low":           25.09701,
  "close":         25.13935,
  "volume":        93948200,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-14T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-14",
  "open":          25.43438,
  "high":          25.44364,
  "low":           25.16713,
  "close":         25.30737,
  "volume":        101439592,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-15T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-15",
  "open":          25.08775,
  "high":          25.25842,
  "low":           25.07452,
  "close":         25.21079,
  "volume":        100841704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-16T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-16",
  "open":          25.05071,
  "high":          25.18433,
  "low":           24.85094,
  "close":         24.87872,
  "volume":        116463960,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-19T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-19",
  "open":          24.85226,
  "high":          25.1367,
  "low":           24.54798,
  "close":         25.11818,
  "volume":        254356736,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-20T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-20",
  "open":          26.53907,
  "high":          26.69121,
  "low":           26.17525,
  "close":         26.29564,
  "volume":        308026304,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-21T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-21",
  "open":          26.39618,
  "high":          27.61201,
  "low":           26.35782,
  "close":         27.1106,
  "volume":        322248704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-22T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-22",
  "open":          27.08149,
  "high":          27.49823,
  "low":           26.79176,
  "close":         27.14764,
  "volume":        213637440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-23T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-23",
  "open":          27.21379,
  "high":          27.22702,
  "low":           26.88701,
  "close":         26.98094,
  "volume":        113591664,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-26T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-26",
  "open":          26.94522,
  "high":          27.3527,
  "low":           26.47292,
  "close":         26.78779,
  "volume":        130747576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-27T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-27",
  "open":          26.6793,
  "high":          26.83145,
  "low":           25.99003,
  "close":         26.11174,
  "volume":        204232192,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-28T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-28",
  "open":          26.15672,
  "high":          26.19774,
  "low":           25.28223,
  "close":         25.45422,
  "volume":        220924752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-29T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-29",
  "open":          25.79819,
  "high":          26.03765,
  "low":           25.41982,
  "close":         25.9768,
  "volume":        153982912,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-10-30T00:  00:  00-04:  00",
  "tradingDay":    "2009-10-30",
  "open":          25.93843,
  "high":          26.03633,
  "low":           24.61677,
  "close":         24.93826,
  "volume":        193696912,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-11-02T00:  00:  00-05:  00",
  "tradingDay":    "2009-11-02",
  "open":          25.09569,
  "high":          25.51772,
  "low":           24.55062,
  "close":         25.04542,
  "volume":        183456432,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-11-03T00:  00:  00-05:  00",
  "tradingDay":    "2009-11-03",
  "open":          24.85226,
  "high":          25.0732,
  "low":           24.59693,
  "close":         24.97133,
  "volume":        141061376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-11-04T00:  00:  00-05:  00",
  "tradingDay":    "2009-11-04",
  "open":          25.23328,
  "high":          25.64605,
  "low":           25.16713,
  "close":         25.24386,
  "volume":        131666704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-11-05T00:  00:  00-05:  00",
  "tradingDay":    "2009-11-05",
  "open":          25.45422,
  "high":          25.79819,
  "low":           25.37749,
  "close":         25.66987,
  "volume":        103910520,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-11-06T00:  00:  00-05:  00",
  "tradingDay":    "2009-11-06",
  "open":          25.46877,
  "high":          25.82333,
  "low":           25.45422,
  "close":         25.71088,
  "volume":        79697840,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-11-09T00:  00:  00-05:  00",
  "tradingDay":    "2009-11-09",
  "open":          26.05486,
  "high":          26.71105,
  "low":           25.96489,
  "close":         26.65285,
  "volume":        142765104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-11-10T00:  00:  00-05:  00",
  "tradingDay":    "2009-11-10",
  "open":          26.59463,
  "high":          27.11853,
  "low":           26.59331,
  "close":         26.85394,
  "volume":        108302864,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-11-11T00:  00:  00-05:  00",
  "tradingDay":    "2009-11-11",
  "open":          27.06297,
  "high":          27.12118,
  "low":           26.70179,
  "close":         26.88966,
  "volume":        119879720,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-11-12T00:  00:  00-05:  00",
  "tradingDay":    "2009-11-12",
  "open":          26.8751,
  "high":          27.10398,
  "low":           26.64887,
  "close":         26.72296,
  "volume":        98239256,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-11-13T00:  00:  00-05:  00",
  "tradingDay":    "2009-11-13",
  "open":          26.83938,
  "high":          27.09869,
  "low":           26.73355,
  "close":         27.04842,
  "volume":        92682880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-11-16T00:  00:  00-05:  00",
  "tradingDay":    "2009-11-16",
  "open":          27.18468,
  "high":          27.51808,
  "low":           27.1225,
  "close":         27.33683,
  "volume":        130981896,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-11-17T00:  00:  00-05:  00",
  "tradingDay":    "2009-11-17",
  "open":          27.26406,
  "high":          27.44399,
  "low":           27.12118,
  "close":         27.38578,
  "volume":        107039056,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-11-18T00:  00:  00-05:  00",
  "tradingDay":    "2009-11-18",
  "open":          27.32492,
  "high":          27.38578,
  "low":           26.98888,
  "close":         27.24819,
  "volume":        101048056,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-11-19T00:  00:  00-05:  00",
  "tradingDay":    "2009-11-19",
  "open":          27.06958,
  "high":          27.06958,
  "low":           26.43323,
  "close":         26.52716,
  "volume":        146401568,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-11-20T00:  00:  00-05:  00",
  "tradingDay":    "2009-11-20",
  "open":          26.34723,
  "high":          26.51128,
  "low":           26.16334,
  "close":         26.4491,
  "volume":        109840296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-11-23T00:  00:  00-05:  00",
  "tradingDay":    "2009-11-23",
  "open":          26.85658,
  "high":          27.25348,
  "low":           26.84997,
  "close":         27.2376,
  "volume":        128274376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-11-24T00:  00:  00-05:  00",
  "tradingDay":    "2009-11-24",
  "open":          27.16484,
  "high":          27.2376,
  "low":           26.84335,
  "close":         27.04709,
  "volume":        85962464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-11-25T00:  00:  00-05:  00",
  "tradingDay":    "2009-11-25",
  "open":          27.1741,
  "high":          27.20717,
  "low":           26.95713,
  "close":         27.01402,
  "volume":        77381112,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-11-27T00:  00:  00-05:  00",
  "tradingDay":    "2009-11-27",
  "open":          26.35649,
  "high":          26.85129,
  "low":           26.24404,
  "close":         26.53774,
  "volume":        79704640,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-11-30T00:  00:  00-05:  00",
  "tradingDay":    "2009-11-30",
  "open":          26.60654,
  "high":          26.68195,
  "low":           26.29696,
  "close":         26.44778,
  "volume":        114690696,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-01T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-01",
  "open":          26.75604,
  "high":          26.82615,
  "low":           26.0403,
  "close":         26.05882,
  "volume":        125733152,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-02T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-02",
  "open":          26.3221,
  "high":          26.64755,
  "low":           25.89742,
  "close":         25.96092,
  "volume":        193085424,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-03T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-03",
  "open":          26.11836,
  "high":          26.32474,
  "low":           25.96622,
  "close":         25.994,
  "volume":        121132192,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-04T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-04",
  "open":          26.42,
  "high":          26.44381,
  "low":           25.17375,
  "close":         25.57594,
  "volume":        223279280,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-07T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-07",
  "open":          25.57594,
  "high":          25.63547,
  "low":           24.96207,
  "close":         24.99779,
  "volume":        192950128,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-08T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-08",
  "open":          25.05203,
  "high":          25.44761,
  "low":           24.96472,
  "close":         25.1195,
  "volume":        186374080,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-09T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-09",
  "open":          25.30605,
  "high":          26.21626,
  "low":           25.17772,
  "close":         26.16863,
  "volume":        184857808,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-10T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-10",
  "open":          26.39354,
  "high":          26.42,
  "low":           25.94637,
  "close":         25.98738,
  "volume":        132186744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-11T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-11",
  "open":          26.16599,
  "high":          26.19509,
  "low":           25.59049,
  "close":         25.75454,
  "volume":        116018000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-14T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-14",
  "open":          25.84715,
  "high":          26.11968,
  "low":           25.47539,
  "close":         26.06015,
  "volume":        133839072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-15T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-15",
  "open":          25.908,
  "high":          26.13026,
  "low":           25.56932,
  "close":         25.68839,
  "volume":        113302168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-16T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-16",
  "open":          25.81143,
  "high":          25.99664,
  "low":           25.73866,
  "close":         25.80216,
  "volume":        95324632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-17T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-17",
  "open":          25.70029,
  "high":          25.79819,
  "low":           25.269,
  "close":         25.38278,
  "volume":        104967224,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-18T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-18",
  "open":          25.55609,
  "high":          25.86435,
  "low":           25.48068,
  "close":         25.85508,
  "volume":        164338288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-21T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-21",
  "open":          25.93711,
  "high":          26.42661,
  "low":           25.88684,
  "close":         26.22552,
  "volume":        165311088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-22T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-22",
  "open":          26.3856,
  "high":          26.57214,
  "low":           26.28241,
  "close":         26.50731,
  "volume":        94351832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-23T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-23",
  "open":          26.62506,
  "high":          26.77456,
  "low":           26.56685,
  "close":         26.73752,
  "volume":        93274720,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-24T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-24",
  "open":          26.92935,
  "high":          27.69668,
  "low":           26.90289,
  "close":         27.65566,
  "volume":        135215504,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-28T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-28",
  "open":          27.97583,
  "high":          28.30525,
  "low":           27.73108,
  "close":         27.99567,
  "volume":        174001296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-29T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-29",
  "open":          28.13062,
  "high":          28.14252,
  "low":           27.61465,
  "close":         27.6636,
  "volume":        120183576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-30T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-30",
  "open":          27.62788,
  "high":          28.04727,
  "low":           27.55909,
  "close":         27.99964,
  "volume":        111242432,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2009-12-31T00:  00:  00-05:  00",
  "tradingDay":    "2009-12-31",
  "open":          28.19677,
  "high":          28.22587,
  "low":           27.85676,
  "close":         27.87925,
  "volume":        95133400,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-01-04T00:  00:  00-05:  00",
  "tradingDay":    "2010-01-04",
  "open":          28.23646,
  "high":          28.37802,
  "low":           28.09754,
  "close":         28.31319,
  "volume":        133282752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-01-05T00:  00:  00-05:  00",
  "tradingDay":    "2010-01-05",
  "open":          28.39389,
  "high":          28.52222,
  "low":           28.21264,
  "close":         28.36214,
  "volume":        162484912,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-01-06T00:  00:  00-05:  00",
  "tradingDay":    "2010-01-06",
  "open":          28.36214,
  "high":          28.47459,
  "low":           27.8819,
  "close":         27.911,
  "volume":        149056176,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-01-07T00:  00:  00-05:  00",
  "tradingDay":    "2010-01-07",
  "open":          28.01419,
  "high":          28.04727,
  "low":           27.65699,
  "close":         27.85941,
  "volume":        128801976,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-01-08T00:  00:  00-05:  00",
  "tradingDay":    "2010-01-08",
  "open":          27.82236,
  "high":          28.04727,
  "low":           27.65831,
  "close":         28.04462,
  "volume":        120904680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-01-11T00:  00:  00-05:  00",
  "tradingDay":    "2010-01-11",
  "open":          28.15311,
  "high":          28.17957,
  "low":           27.57761,
  "close":         27.79723,
  "volume":        124779248,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-01-12T00:  00:  00-05:  00",
  "tradingDay":    "2010-01-12",
  "open":          27.67551,
  "high":          27.75224,
  "low":           27.30904,
  "close":         27.48103,
  "volume":        160475056,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-01-13T00:  00:  00-05:  00",
  "tradingDay":    "2010-01-13",
  "open":          27.50088,
  "high":          27.90571,
  "low":           27.00211,
  "close":         27.86867,
  "volume":        163561264,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-01-14T00:  00:  00-05:  00",
  "tradingDay":    "2010-01-14",
  "open":          27.79723,
  "high":          27.84353,
  "low":           27.65302,
  "close":         27.70726,
  "volume":        116930328,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-01-15T00:  00:  00-05:  00",
  "tradingDay":    "2010-01-15",
  "open":          27.90571,
  "high":          27.99435,
  "low":           27.23628,
  "close":         27.24422,
  "volume":        160407024,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-01-19T00:  00:  00-05:  00",
  "tradingDay":    "2010-01-19",
  "open":          27.56173,
  "high":          28.4693,
  "low":           27.41753,
  "close":         28.44946,
  "volume":        197066576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-01-20T00:  00:  00-05:  00",
  "tradingDay":    "2010-01-20",
  "open":          28.43226,
  "high":          28.51693,
  "low":           27.71652,
  "close":         28.01155,
  "volume":        165251376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-01-21T00:  00:  00-05:  00",
  "tradingDay":    "2010-01-21",
  "open":          28.05785,
  "high":          28.22058,
  "low":           27.41356,
  "close":         27.52734,
  "volume":        164172000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-01-22T00:  00:  00-05:  00",
  "tradingDay":    "2010-01-22",
  "open":          27.35667,
  "high":          27.45193,
  "low":           26.08396,
  "close":         26.16202,
  "volume":        238034560,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-01-25T00:  00:  00-05:  00",
  "tradingDay":    "2010-01-25",
  "open":          26.79176,
  "high":          27.08149,
  "low":           26.48483,
  "close":         26.86584,
  "volume":        287687456,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-01-26T00:  00:  00-05:  00",
  "tradingDay":    "2010-01-26",
  "open":          27.24686,
  "high":          28.2735,
  "low":           26.80102,
  "close":         27.24554,
  "volume":        504030880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-01-27T00:  00:  00-05:  00",
  "tradingDay":    "2010-01-27",
  "open":          27.36593,
  "high":          27.85941,
  "low":           26.39751,
  "close":         27.5022,
  "volume":        465010784,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-01-28T00:  00:  00-05:  00",
  "tradingDay":    "2010-01-28",
  "open":          27.11192,
  "high":          27.18733,
  "low":           26.2877,
  "close":         26.36576,
  "volume":        316789088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-01-29T00:  00:  00-05:  00",
  "tradingDay":    "2010-01-29",
  "open":          26.60257,
  "high":          26.75074,
  "low":           25.16978,
  "close":         25.40924,
  "volume":        336347136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-02-01T00:  00:  00-05:  00",
  "tradingDay":    "2010-02-01",
  "open":          25.45025,
  "high":          25.93049,
  "low":           25.30869,
  "close":         25.76247,
  "volume":        202430208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-02-02T00:  00:  00-05:  00",
  "tradingDay":    "2010-02-02",
  "open":          25.91859,
  "high":          25.97283,
  "low":           25.58387,
  "close":         25.91197,
  "volume":        188518480,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-02-03T00:  00:  00-05:  00",
  "tradingDay":    "2010-02-03",
  "open":          25.82069,
  "high":          26.48615,
  "low":           25.72146,
  "close":         26.35782,
  "volume":        166108528,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-02-04T00:  00:  00-05:  00",
  "tradingDay":    "2010-02-04",
  "open":          26.02707,
  "high":          26.24404,
  "low":           25.34441,
  "close":         25.40792,
  "volume":        204529248,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-02-05T00:  00:  00-05:  00",
  "tradingDay":    "2010-02-05",
  "open":          25.48465,
  "high":          25.93049,
  "low":           25.24916,
  "close":         25.85905,
  "volume":        229541632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-02-08T00:  00:  00-05:  00",
  "tradingDay":    "2010-02-08",
  "open":          25.88948,
  "high":          26.17922,
  "low":           25.6659,
  "close":         25.68177,
  "volume":        129109608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-02-09T00:  00:  00-05:  00",
  "tradingDay":    "2010-02-09",
  "open":          25.98606,
  "high":          26.12894,
  "low":           25.76512,
  "close":         25.95563,
  "volume":        170848576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-02-10T00:  00:  00-05:  00",
  "tradingDay":    "2010-02-10",
  "open":          25.91594,
  "high":          26.00987,
  "low":           25.70029,
  "close":         25.81407,
  "volume":        99979256,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-02-11T00:  00:  00-05:  00",
  "tradingDay":    "2010-02-11",
  "open":          25.78232,
  "high":          26.42661,
  "low":           25.67384,
  "close":         26.28373,
  "volume":        148566384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-02-12T00:  00:  00-05:  00",
  "tradingDay":    "2010-02-12",
  "open":          26.20964,
  "high":          26.67666,
  "low":           25.86435,
  "close":         26.50996,
  "volume":        176944640,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-02-16T00:  00:  00-05:  00",
  "tradingDay":    "2010-02-16",
  "open":          26.71635,
  "high":          26.94787,
  "low":           26.66078,
  "close":         26.9095,
  "volume":        146840736,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-02-17T00:  00:  00-05:  00",
  "tradingDay":    "2010-02-17",
  "open":          27.01402,
  "high":          27.02989,
  "low":           26.57347,
  "close":         26.79705,
  "volume":        117805624,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-02-18T00:  00:  00-05:  00",
  "tradingDay":    "2010-02-18",
  "open":          26.67533,
  "high":          26.97433,
  "low":           26.5814,
  "close":         26.84732,
  "volume":        114141936,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-02-19T00:  00:  00-05:  00",
  "tradingDay":    "2010-02-19",
  "open":          26.70576,
  "high":          26.88304,
  "low":           26.60654,
  "close":         26.68063,
  "volume":        112228080,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-02-22T00:  00:  00-05:  00",
  "tradingDay":    "2010-02-22",
  "open":          26.76927,
  "high":          26.79043,
  "low":           26.35253,
  "close":         26.51525,
  "volume":        105503888,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-02-23T00:  00:  00-05:  00",
  "tradingDay":    "2010-02-23",
  "open":          26.45969,
  "high":          26.63564,
  "low":           25.89213,
  "close":         26.07073,
  "volume":        155306448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-02-24T00:  00:  00-05:  00",
  "tradingDay":    "2010-02-24",
  "open":          26.22552,
  "high":          26.6502,
  "low":           26.17392,
  "close":         26.547,
  "volume":        124330264,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-02-25T00:  00:  00-05:  00",
  "tradingDay":    "2010-02-25",
  "open":          26.11307,
  "high":          26.83806,
  "low":           26.04824,
  "close":         26.72429,
  "volume":        179624944,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-02-26T00:  00:  00-05:  00",
  "tradingDay":    "2010-02-26",
  "open":          26.77456,
  "high":          27.14367,
  "low":           26.72429,
  "close":         27.07091,
  "volume":        136989520,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-01T00:  00:  00-05:  00",
  "tradingDay":    "2010-03-01",
  "open":          27.2204,
  "high":          27.71652,
  "low":           27.18071,
  "close":         27.64905,
  "volume":        148498352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-02T00:  00:  00-05:  00",
  "tradingDay":    "2010-03-02",
  "open":          27.77341,
  "high":          27.89248,
  "low":           27.48368,
  "close":         27.63053,
  "volume":        152939824,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-03T00:  00:  00-05:  00",
  "tradingDay":    "2010-03-03",
  "open":          27.64244,
  "high":          27.76547,
  "low":           27.51014,
  "close":         27.69403,
  "volume":        100486448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-04T00:  00:  00-05:  00",
  "tradingDay":    "2010-03-04",
  "open":          27.68742,
  "high":          27.90439,
  "low":           27.60142,
  "close":         27.87661,
  "volume":        98812960,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-05T00:  00:  00-05:  00",
  "tradingDay":    "2010-03-05",
  "open":          28.43623,
  "high":          29.06597,
  "low":           28.39521,
  "close":         28.96674,
  "volume":        242853968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-08T00:  00:  00-05:  00",
  "tradingDay":    "2010-03-08",
  "open":          29.10698,
  "high":          29.11756,
  "low":           28.87413,
  "close":         28.98394,
  "volume":        116048984,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-09T00:  00:  00-05:  00",
  "tradingDay":    "2010-03-09",
  "open":          28.88207,
  "high":          29.76715,
  "low":           28.82651,
  "close":         29.5052,
  "volume":        248425456,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-10T00:  00:  00-05:  00",
  "tradingDay":    "2010-03-10",
  "open":          29.61236,
  "high":          29.83065,
  "low":           29.52901,
  "close":         29.74598,
  "volume":        160949744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-11T00:  00:  00-05:  00",
  "tradingDay":    "2010-03-11",
  "open":          29.62294,
  "high":          29.8333,
  "low":           29.54489,
  "close":         29.8333,
  "volume":        109519056,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-12T00:  00:  00-05:  00",
  "tradingDay":    "2010-03-12",
  "open":          30.0807,
  "high":          30.12832,
  "low":           29.86637,
  "close":         29.97883,
  "volume":        112386816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-15T00:  00:  00-04:  00",
  "tradingDay":    "2010-03-15",
  "open":          29.81742,
  "high":          29.8333,
  "low":           29.13873,
  "close":         29.61368,
  "volume":        133221528,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-16T00:  00:  00-04:  00",
  "tradingDay":    "2010-03-16",
  "open":          29.65866,
  "high":          29.7645,
  "low":           29.43773,
  "close":         29.69438,
  "volume":        120643144,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-17T00:  00:  00-04:  00",
  "tradingDay":    "2010-03-17",
  "open":          29.75392,
  "high":          29.95898,
  "low":           29.53827,
  "close":         29.65073,
  "volume":        121736128,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-18T00:  00:  00-04:  00",
  "tradingDay":    "2010-03-18",
  "open":          29.64808,
  "high":          29.76715,
  "low":           29.45096,
  "close":         29.72084,
  "volume":        92352560,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-19T00:  00:  00-04:  00",
  "tradingDay":    "2010-03-19",
  "open":          29.73936,
  "high":          29.7989,
  "low":           29.26838,
  "close":         29.40333,
  "volume":        151022944,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-22T00:  00:  00-04:  00",
  "tradingDay":    "2010-03-22",
  "open":          29.16784,
  "high":          29.89945,
  "low":           29.1255,
  "close":         29.73407,
  "volume":        123210824,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-23T00:  00:  00-04:  00",
  "tradingDay":    "2010-03-23",
  "open":          29.85182,
  "high":          30.26724,
  "low":           29.64808,
  "close":         30.21167,
  "volume":        162627008,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-24T00:  00:  00-04:  00",
  "tradingDay":    "2010-03-24",
  "open":          30.11642,
  "high":          30.4551,
  "low":           30.09922,
  "close":         30.34529,
  "volume":        161506816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-25T00:  00:  00-04:  00",
  "tradingDay":    "2010-03-25",
  "open":          30.55036,
  "high":          30.55697,
  "low":           29.93252,
  "close":         29.98544,
  "volume":        146655552,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-26T00:  00:  00-04:  00",
  "tradingDay":    "2010-03-26",
  "open":          30.28973,
  "high":          30.68662,
  "low":           30.23681,
  "close":         30.54771,
  "volume":        173005056,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-29T00:  00:  00-04:  00",
  "tradingDay":    "2010-03-29",
  "open":          30.82554,
  "high":          30.94064,
  "low":           30.64296,
  "close":         30.74483,
  "volume":        145974512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-30T00:  00:  00-04:  00",
  "tradingDay":    "2010-03-30",
  "open":          31.30181,
  "high":          31.41823,
  "low":           30.99091,
  "close":         31.20259,
  "volume":        142347856,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-03-31T00:  00:  00-04:  00",
  "tradingDay":    "2010-03-31",
  "open":          31.15496,
  "high":          31.30313,
  "low":           31.01869,
  "close":         31.09013,
  "volume":        116256848,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-01T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-01",
  "open":          31.41956,
  "high":          31.58361,
  "low":           30.79246,
  "close":         31.21846,
  "volume":        162819760,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-05T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-05",
  "open":          31.08749,
  "high":          31.5545,
  "low":           31.05971,
  "close":         31.55186,
  "volume":        184783744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-06T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-06",
  "open":          31.51349,
  "high":          31.78338,
  "low":           31.35473,
  "close":         31.69077,
  "volume":        120672624,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-07T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-07",
  "open":          31.71458,
  "high":          32.00564,
  "low":           31.57435,
  "close":         31.83101,
  "volume":        169664896,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-08T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-08",
  "open":          31.80984,
  "high":          31.95536,
  "low":           31.49232,
  "close":         31.74501,
  "volume":        154679072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-09T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-09",
  "open":          31.94081,
  "high":          32.00167,
  "low":           31.81248,
  "close":         31.98844,
  "volume":        90212704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-12T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-12",
  "open":          32.04268,
  "high":          32.15778,
  "low":           31.99109,
  "close":         32.05459,
  "volume":        89951928,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-13T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-13",
  "open":          31.9977,
  "high":          32.12206,
  "low":           31.89848,
  "close":         32.07311,
  "volume":        82661592,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-14T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-14",
  "open":          32.45016,
  "high":          32.52028,
  "low":           32.29008,
  "close":         32.5044,
  "volume":        109080648,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-15T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-15",
  "open":          32.51631,
  "high":          32.94628,
  "low":           32.48059,
  "close":         32.93173,
  "volume":        101713216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-16T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-16",
  "open":          32.88543,
  "high":          33.22543,
  "low":           32.35358,
  "close":         32.73063,
  "volume":        202610848,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-19T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-19",
  "open":          32.68168,
  "high":          32.79546,
  "low":           31.98579,
  "close":         32.68698,
  "volume":        153041872,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-20T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-20",
  "open":          32.88145,
  "high":          32.97539,
  "low":           32.14323,
  "close":         32.35888,
  "volume":        199312256,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-21T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-21",
  "open":          34.24016,
  "high":          34.43067,
  "low":           33.83268,
  "close":         34.2944,
  "volume":        265198896,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-22T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-22",
  "open":          34.16475,
  "high":          35.29061,
  "low":           33.89486,
  "close":         35.25357,
  "volume":        214186192,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-23T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-23",
  "open":          35.45466,
  "high":          36.00899,
  "low":           35.32368,
  "close":         35.83038,
  "volume":        215139344,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-26T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-26",
  "open":          35.9693,
  "high":          36.04603,
  "low":           35.48112,
  "close":         35.65443,
  "volume":        129325032,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-27T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-27",
  "open":          35.3594,
  "high":          35.43481,
  "low":           34.46639,
  "close":         34.66748,
  "volume":        191488272,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-28T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-28",
  "open":          34.82756,
  "high":          34.92679,
  "low":           33.92264,
  "close":         34.60927,
  "volume":        204731824,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-29T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-29",
  "open":          34.79713,
  "high":          35.72058,
  "low":           34.66352,
  "close":         35.54065,
  "volume":        150859680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-04-30T00:  00:  00-04:  00",
  "tradingDay":    "2010-04-30",
  "open":          35.62929,
  "high":          35.79599,
  "low":           34.52989,
  "close":         34.5418,
  "volume":        146438608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-05-03T00:  00:  00-04:  00",
  "tradingDay":    "2010-05-03",
  "open":          34.90562,
  "high":          35.44011,
  "low":           34.77861,
  "close":         35.23769,
  "volume":        122649976,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-05-04T00:  00:  00-04:  00",
  "tradingDay":    "2010-05-04",
  "open":          34.77994,
  "high":          34.83286,
  "low":           33.96762,
  "close":         34.22296,
  "volume":        195396112,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-05-05T00:  00:  00-04:  00",
  "tradingDay":    "2010-05-05",
  "open":          33.47548,
  "high":          34.15152,
  "low":           32.90659,
  "close":         33.86708,
  "volume":        238395104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-05-06T00:  00:  00-04:  00",
  "tradingDay":    "2010-05-06",
  "open":          33.58131,
  "high":          34.16607,
  "low":           26.36046,
  "close":         32.57849,
  "volume":        347120480,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-05-07T00:  00:  00-04:  00",
  "tradingDay":    "2010-05-07",
  "open":          32.24245,
  "high":          32.62083,
  "low":           29.79493,
  "close":         31.20391,
  "volume":        452444480,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-05-10T00:  00:  00-04:  00",
  "tradingDay":    "2010-05-10",
  "open":          33.10769,
  "high":          33.6898,
  "low":           32.88013,
  "close":         33.60248,
  "volume":        265715152,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-05-11T00:  00:  00-04:  00",
  "tradingDay":    "2010-05-11",
  "open":          33.31804,
  "high":          34.38305,
  "low":           33.14076,
  "close":         33.93719,
  "volume":        229553728,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-05-12T00:  00:  00-04:  00",
  "tradingDay":    "2010-05-12",
  "open":          34.29837,
  "high":          34.81169,
  "low":           34.22561,
  "close":         34.6741,
  "volume":        176650608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-05-13T00:  00:  00-04:  00",
  "tradingDay":    "2010-05-13",
  "open":          34.8236,
  "high":          35.05909,
  "low":           33.92132,
  "close":         34.18062,
  "volume":        161893072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-05-14T00:  00:  00-04:  00",
  "tradingDay":    "2010-05-14",
  "open":          33.75859,
  "high":          33.9319,
  "low":           33.00846,
  "close":         33.57999,
  "volume":        204991072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-05-17T00:  00:  00-04:  00",
  "tradingDay":    "2010-05-17",
  "open":          33.69641,
  "high":          33.89221,
  "low":           32.77165,
  "close":         33.63291,
  "volume":        205928352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-05-18T00:  00:  00-04:  00",
  "tradingDay":    "2010-05-18",
  "open":          33.99805,
  "high":          34.20576,
  "low":           33.109,
  "close":         33.38683,
  "volume":        211285184,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-05-19T00:  00:  00-04:  00",
  "tradingDay":    "2010-05-19",
  "open":          33.00846,
  "high":          33.46092,
  "low":           32.39327,
  "close":         32.855,
  "volume":        276896704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-05-20T00:  00:  00-04:  00",
  "tradingDay":    "2010-05-20",
  "open":          32.00035,
  "high":          32.26097,
  "low":           31.25022,
  "close":         31.45528,
  "volume":        346325312,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-05-21T00:  00:  00-04:  00",
  "tradingDay":    "2010-05-21",
  "open":          30.80172,
  "high":          32.34697,
  "low":           30.60724,
  "close":         32.05856,
  "volume":        330396192,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-05-24T00:  00:  00-04:  00",
  "tradingDay":    "2010-05-24",
  "open":          32.71476,
  "high":          33.19368,
  "low":           32.57981,
  "close":         32.64596,
  "volume":        203610112,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-05-25T00:  00:  00-04:  00",
  "tradingDay":    "2010-05-25",
  "open":          31.66563,
  "high":          32.64596,
  "low":           31.3759,
  "close":         32.44222,
  "volume":        282913376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-05-26T00:  00:  00-04:  00",
  "tradingDay":    "2010-05-26",
  "open":          33.08519,
  "high":          33.35641,
  "low":           32.24775,
  "close":         32.29537,
  "volume":        229636112,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-05-27T00:  00:  00-04:  00",
  "tradingDay":    "2010-05-27",
  "open":          33.15399,
  "high":          33.58925,
  "low":           32.95686,
  "close":         33.51781,
  "volume":        179869088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-05-28T00:  00:  00-04:  00",
  "tradingDay":    "2010-05-28",
  "open":          34.31689,
  "high":          34.31821,
  "low":           33.51781,
  "close":         33.98483,
  "volume":        220176448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-01T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-01",
  "open":          34.35658,
  "high":          35.18345,
  "low":           34.26,
  "close":         34.5074,
  "volume":        236605968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-02T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-02",
  "open":          34.99823,
  "high":          35.03263,
  "low":           34.44125,
  "close":         34.92017,
  "volume":        185874448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-03T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-03",
  "open":          35.0829,
  "high":          35.13185,
  "low":           34.45184,
  "close":         34.81036,
  "volume":        175511520,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-04T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-04",
  "open":          34.16078,
  "high":          34.64896,
  "low":           33.68715,
  "close":         33.86311,
  "volume":        204743152,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-07T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-07",
  "open":          34.17136,
  "high":          34.28514,
  "low":           33.14737,
  "close":         33.19897,
  "volume":        239442736,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-08T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-08",
  "open":          33.50326,
  "high":          33.57734,
  "low":           32.49911,
  "close":         32.98597,
  "volume":        270159648,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-09T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-09",
  "open":          33.26909,
  "high":          33.32598,
  "low":           32.08105,
  "close":         32.17498,
  "volume":        230708688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-10T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-10",
  "open":          32.39195,
  "high":          33.20426,
  "low":           32.04268,
  "close":         33.14208,
  "volume":        209578432,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-11T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-11",
  "open":          32.84044,
  "high":          33.58528,
  "low":           32.72667,
  "close":         33.53898,
  "volume":        147329024,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-14T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-14",
  "open":          33.86311,
  "high":          34.28514,
  "low":           33.60513,
  "close":         33.64085,
  "volume":        162770624,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-15T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-15",
  "open":          33.82077,
  "high":          34.37775,
  "low":           33.80225,
  "close":         34.35658,
  "volume":        157941392,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-16T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-16",
  "open":          34.54313,
  "high":          35.42291,
  "low":           34.48094,
  "close":         35.35676,
  "volume":        211558800,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-17T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-17",
  "open":          35.79996,
  "high":          36.10424,
  "low":           35.65443,
  "close":         35.96798,
  "volume":        235792656,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-18T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-18",
  "open":          36.01825,
  "high":          36.38207,
  "low":           35.90844,
  "close":         36.25903,
  "volume":        211809760,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-21T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-21",
  "open":          36.73795,
  "high":          36.91259,
  "low":           35.55256,
  "close":         35.74307,
  "volume":        209614720,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-22T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-22",
  "open":          36.00634,
  "high":          36.5104,
  "low":           35.91903,
  "close":         36.22993,
  "volume":        193625872,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-23T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-23",
  "open":          36.3265,
  "high":          36.33709,
  "low":           35.44275,
  "close":         35.84891,
  "volume":        207446128,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-24T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-24",
  "open":          35.85288,
  "high":          36.14394,
  "low":           35.46921,
  "close":         35.58828,
  "volume":        193051408,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-25T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-25",
  "open":          35.72852,
  "high":          35.7563,
  "low":           35.16625,
  "close":         35.284,
  "volume":        148457536,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-28T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-28",
  "open":          35.31442,
  "high":          35.6875,
  "low":           34.99558,
  "close":         35.49567,
  "volume":        158038896,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-29T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-29",
  "open":          34.94267,
  "high":          34.97839,
  "low":           33.64349,
  "close":         33.89089,
  "volume":        305948416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-06-30T00:  00:  00-04:  00",
  "tradingDay":    "2010-06-30",
  "open":          33.96233,
  "high":          34.12903,
  "low":           33.07593,
  "close":         33.27703,
  "volume":        199620640,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-01T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-01",
  "open":          33.64349,
  "high":          33.70964,
  "low":           32.17763,
  "close":         32.87352,
  "volume":        276148384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-02T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-02",
  "open":          33.13944,
  "high":          33.19765,
  "low":           32.17498,
  "close":         32.66978,
  "volume":        187480672,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-06T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-06",
  "open":          33.20691,
  "high":          33.44505,
  "low":           32.56659,
  "close":         32.89336,
  "volume":        166083584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-07T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-07",
  "open":          33.13944,
  "high":          34.23487,
  "low":           33.04153,
  "close":         34.22164,
  "volume":        176699744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-08T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-08",
  "open":          34.7257,
  "high":          34.78126,
  "low":           33.72155,
  "close":         34.14491,
  "volume":        199265392,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-09T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-09",
  "open":          33.98615,
  "high":          34.38437,
  "low":           33.75727,
  "close":         34.34732,
  "volume":        117043712,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-12T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-12",
  "open":          34.20312,
  "high":          34.64235,
  "low":           33.71758,
  "close":         34.03907,
  "volume":        151949632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-13T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-13",
  "open":          33.91074,
  "high":          33.92132,
  "low":           32.6023,
  "close":         33.31275,
  "volume":        321500384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-14T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-14",
  "open":          32.99258,
  "high":          33.84194,
  "low":           32.94231,
  "close":         33.43578,
  "volume":        219247488,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-15T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-15",
  "open":          32.84044,
  "high":          33.99673,
  "low":           32.7174,
  "close":         33.26644,
  "volume":        222673824,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-16T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-16",
  "open":          33.49532,
  "high":          33.73213,
  "low":           32.86425,
  "close":         33.06138,
  "volume":        280711552,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-19T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-19",
  "open":          33.05873,
  "high":          33.05873,
  "low":           31.69871,
  "close":         32.48985,
  "volume":        276565632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-20T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-20",
  "open":          32.13529,
  "high":          33.45827,
  "low":           31.75295,
  "close":         33.32465,
  "volume":        290184832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-21T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-21",
  "open":          35.07099,
  "high":          35.07893,
  "low":           33.60381,
  "close":         33.63556,
  "volume":        320074080,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-22T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-22",
  "open":          34.09066,
  "high":          34.39759,
  "low":           33.77711,
  "close":         34.26794,
  "volume":        174204624,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-23T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-23",
  "open":          34.0126,
  "high":          34.44787,
  "low":           33.90545,
  "close":         34.38966,
  "volume":        143988848,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-26T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-26",
  "open":          34.39759,
  "high":          34.41082,
  "low":           34.09463,
  "close":         34.30234,
  "volume":        113528168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-27T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-27",
  "open":          34.51269,
  "high":          35.03263,
  "low":           34.43728,
  "close":         34.93737,
  "volume":        157859760,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-28T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-28",
  "open":          34.88313,
  "high":          35.19006,
  "low":           34.43067,
  "close":         34.5246,
  "volume":        140370512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-29T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-29",
  "open":          34.49152,
  "high":          34.74818,
  "low":           33.88163,
  "close":         34.14755,
  "volume":        173796464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-07-30T00:  00:  00-04:  00",
  "tradingDay":    "2010-07-30",
  "open":          33.85385,
  "high":          34.35791,
  "low":           33.72287,
  "close":         34.03378,
  "volume":        121061144,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-02T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-02",
  "open":          34.45581,
  "high":          34.74025,
  "low":           34.34732,
  "close":         34.64235,
  "volume":        115562208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-03T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-03",
  "open":          34.53122,
  "high":          34.82889,
  "low":           34.32086,
  "close":         34.65293,
  "volume":        112745848,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-04T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-04",
  "open":          34.77332,
  "high":          34.96383,
  "low":           34.43861,
  "close":         34.79184,
  "volume":        113529680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-05T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-05",
  "open":          34.62647,
  "high":          34.8183,
  "low":           34.47036,
  "close":         34.62251,
  "volume":        78113544,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-06T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-06",
  "open":          34.36849,
  "high":          34.59472,
  "low":           34.08405,
  "close":         34.4095,
  "volume":        120132184,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-09T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-09",
  "open":          34.5934,
  "high":          34.68203,
  "low":           34.34071,
  "close":         34.62912,
  "volume":        81829384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-10T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-10",
  "open":          34.37775,
  "high":          34.45713,
  "low":           34.07346,
  "close":         34.31954,
  "volume":        121996144,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-11T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-11",
  "open":          33.78902,
  "high":          33.82739,
  "low":           33.04947,
  "close":         33.09975,
  "volume":        167384432,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-12T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-12",
  "open":          32.6367,
  "high":          33.48474,
  "low":           32.56129,
  "close":         33.31142,
  "volume":        144402304,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-13T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-13",
  "open":          33.2929,
  "high":          33.32333,
  "low":           32.95422,
  "close":         32.95554,
  "volume":        95797048,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-16T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-16",
  "open":          32.75445,
  "high":          33.07593,
  "low":           32.62744,
  "close":         32.76239,
  "volume":        85960200,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-17T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-17",
  "open":          33.08519,
  "high":          33.68715,
  "low":           32.96877,
  "close":         33.33524,
  "volume":        114095072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-18T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-18",
  "open":          33.38683,
  "high":          33.69244,
  "low":           33.28364,
  "close":         33.48077,
  "volume":        91701008,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-19T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-19",
  "open":          33.45034,
  "high":          33.53501,
  "low":           32.89997,
  "close":         33.05873,
  "volume":        115189568,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-20T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-20",
  "open":          32.99391,
  "high":          33.59322,
  "low":           32.94231,
  "close":         33.02698,
  "volume":        103723064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-23T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-23",
  "open":          33.31142,
  "high":          33.33921,
  "low":           32.44619,
  "close":         32.51896,
  "volume":        111778344,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-24T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-24",
  "open":          32.10486,
  "high":          32.14852,
  "low":           31.57302,
  "close":         31.74236,
  "volume":        162660272,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-25T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-25",
  "open":          31.49232,
  "high":          32.2795,
  "low":           31.38119,
  "close":         32.13397,
  "volume":        161125104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-26T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-26",
  "open":          32.47265,
  "high":          32.51234,
  "low":           31.78867,
  "close":         31.78867,
  "volume":        125933456,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-27T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-27",
  "open":          31.98315,
  "high":          32.09692,
  "low":           31.16422,
  "close":         31.96595,
  "volume":        148091696,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-30T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-30",
  "open":          31.85217,
  "high":          32.51234,
  "low":           31.84159,
  "close":         32.08237,
  "volume":        103469096,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-08-31T00:  00:  00-04:  00",
  "tradingDay":    "2010-08-31",
  "open":          31.99638,
  "high":          32.35491,
  "low":           31.79793,
  "close":         32.16175,
  "volume":        113591664,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-01T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-01",
  "open":          32.73989,
  "high":          33.26777,
  "low":           32.58246,
  "close":         33.11827,
  "volume":        188166240,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-02T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-02",
  "open":          33.24131,
  "high":          33.3617,
  "low":           32.88543,
  "close":         33.3617,
  "volume":        112144936,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-03T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-03",
  "open":          33.74801,
  "high":          34.23619,
  "low":           33.66995,
  "close":         34.23487,
  "volume":        140587440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-07T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-07",
  "open":          33.95308,
  "high":          34.33541,
  "low":           33.90147,
  "close":         34.10786,
  "volume":        92546064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-08T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-08",
  "open":          34.36849,
  "high":          34.97839,
  "low":           34.27853,
  "close":         34.78391,
  "volume":        142143024,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-09T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-09",
  "open":          35.06438,
  "high":          35.26018,
  "low":           34.78391,
  "close":         34.80375,
  "volume":        118393688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-10T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-10",
  "open":          34.81963,
  "high":          34.99294,
  "low":           34.58281,
  "close":         34.84873,
  "volume":        104676968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-13T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-13",
  "open":          35.16757,
  "high":          35.49303,
  "low":           35.15963,
  "close":         35.32898,
  "volume":        104951352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-14T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-14",
  "open":          35.21917,
  "high":          35.61077,
  "low":           35.12788,
  "close":         35.46392,
  "volume":        110180440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-15T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-15",
  "open":          35.47847,
  "high":          35.77085,
  "low":           35.43481,
  "close":         35.74968,
  "volume":        115908400,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-16T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-16",
  "open":          35.75233,
  "high":          36.60301,
  "low":           35.65443,
  "close":         36.58978,
  "volume":        176036096,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-17T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-17",
  "open":          36.73795,
  "high":          36.77367,
  "low":           36.20744,
  "close":         36.43102,
  "volume":        171277904,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-20T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-20",
  "open":          36.52495,
  "high":          37.54365,
  "low":           36.49453,
  "close":         37.47089,
  "volume":        177977152,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-21T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-21",
  "open":          37.55423,
  "high":          38.01596,
  "low":           37.41268,
  "close":         37.54233,
  "volume":        180779904,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-22T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-22",
  "open":          37.40209,
  "high":          38.09931,
  "low":           37.3624,
  "close":         38.06887,
  "volume":        158033616,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-23T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-23",
  "open":          37.88101,
  "high":          38.73169,
  "low":           37.83735,
  "close":         38.22367,
  "volume":        212213392,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-24T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-24",
  "open":          38.64437,
  "high":          38.83356,
  "low":           38.43931,
  "close":         38.67348,
  "volume":        175330112,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-27T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-27",
  "open":          38.8931,
  "high":          38.99232,
  "low":           38.50017,
  "close":         38.52002,
  "volume":        130413480,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-28T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-28",
  "open":          38.60072,
  "high":          38.60072,
  "low":           36.38207,
  "close":         37.95113,
  "volume":        279411456,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-29T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-29",
  "open":          38.00008,
  "high":          38.34141,
  "low":           37.83735,
  "close":         38.0186,
  "volume":        126780784,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-09-30T00:  00:  00-04:  00",
  "tradingDay":    "2010-09-30",
  "open":          38.23425,
  "high":          38.36655,
  "low":           37.20893,
  "close":         37.53968,
  "volume":        181782944,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-01T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-01",
  "open":          37.8572,
  "high":          37.91409,
  "low":           37.22217,
  "close":         37.37695,
  "volume":        121047536,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-04T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-04",
  "open":          37.25524,
  "high":          37.42723,
  "low":           36.74854,
  "close":         36.86364,
  "volume":        117510080,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-05T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-05",
  "open":          37.30816,
  "high":          38.29379,
  "low":           37.28435,
  "close":         38.22631,
  "volume":        135506512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-06T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-06",
  "open":          38.31231,
  "high":          38.62982,
  "low":           37.73945,
  "close":         38.25939,
  "volume":        181101904,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-07T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-07",
  "open":          38.41153,
  "high":          38.43005,
  "low":           37.95774,
  "close":         38.26336,
  "volume":        110247712,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-08T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-08",
  "open":          38.59278,
  "high":          38.96189,
  "low":           38.36655,
  "close":         38.905,
  "volume":        177737552,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-11T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-11",
  "open":          38.99364,
  "high":          39.32439,
  "low":           38.97512,
  "close":         39.07566,
  "volume":        115472264,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-12T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-12",
  "open":          39.08228,
  "high":          39.62338,
  "low":           38.69597,
  "close":         39.49638,
  "volume":        150778800,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-13T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-13",
  "open":          39.71599,
  "high":          39.94884,
  "low":           39.66307,
  "close":         39.70806,
  "volume":        170094224,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-14T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-14",
  "open":          39.91312,
  "high":          40.01631,
  "low":           39.74245,
  "close":         39.99514,
  "volume":        117508568,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-15T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-15",
  "open":          40.67383,
  "high":          41.67401,
  "low":           40.33912,
  "close":         41.63961,
  "volume":        248947760,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-18T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-18",
  "open":          42.13308,
  "high":          42.2032,
  "low":           41.58008,
  "close":         42.0709,
  "volume":        298150912,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-19T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-19",
  "open":          40.13935,
  "high":          41.51128,
  "low":           39.69218,
  "close":         40.94504,
  "volume":        332786240,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-20T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-20",
  "open":          40.88022,
  "high":          41.57478,
  "low":           40.59842,
  "close":         41.08263,
  "volume":        194803504,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-21T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-21",
  "open":          41.32474,
  "high":          41.63961,
  "low":           40.58916,
  "close":         40.94901,
  "volume":        148867216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-22T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-22",
  "open":          40.88948,
  "high":          41.01781,
  "low":           40.52301,
  "close":         40.6778,
  "volume":        100631576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-25T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-25",
  "open":          40.89212,
  "high":          41.22419,
  "low":           40.80613,
  "close":         40.85905,
  "volume":        105937760,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-26T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-26",
  "open":          40.59842,
  "high":          40.97812,
  "low":           40.43702,
  "close":         40.75453,
  "volume":        106071544,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-27T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-27",
  "open":          40.70161,
  "high":          40.99929,
  "low":           40.4304,
  "close":         40.72543,
  "volume":        107711024,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-28T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-28",
  "open":          40.74131,
  "high":          40.74792,
  "low":           39.8086,
  "close":         40.38277,
  "volume":        148756096,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-10-29T00:  00:  00-04:  00",
  "tradingDay":    "2010-10-29",
  "open":          40.24916,
  "high":          40.46745,
  "low":           39.80463,
  "close":         39.81919,
  "volume":        116216792,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-01T00:  00:  00-04:  00",
  "tradingDay":    "2010-11-01",
  "open":          39.98323,
  "high":          40.4304,
  "low":           39.98059,
  "close":         40.24254,
  "volume":        114429168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-02T00:  00:  00-04:  00",
  "tradingDay":    "2010-11-02",
  "open":          40.61562,
  "high":          41.03765,
  "low":           40.61562,
  "close":         40.92785,
  "volume":        117139704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-03T00:  00:  00-04:  00",
  "tradingDay":    "2010-11-03",
  "open":          41.19376,
  "high":          41.39354,
  "low":           40.81804,
  "close":         41.38295,
  "volume":        137229136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-04T00:  00:  00-04:  00",
  "tradingDay":    "2010-11-04",
  "open":          41.73354,
  "high":          42.35931,
  "low":           41.67798,
  "close":         42.10662,
  "volume":        173440448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-05T00:  00:  00-04:  00",
  "tradingDay":    "2010-11-05",
  "open":          42.06958,
  "high":          42.27861,
  "low":           41.90553,
  "close":         41.95581,
  "volume":        97520424,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-08T00:  00:  00-05:  00",
  "tradingDay":    "2010-11-08",
  "open":          41.96507,
  "high":          42.30507,
  "low":           41.90686,
  "close":         42.15293,
  "volume":        76054560,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-09T00:  00:  00-05:  00",
  "tradingDay":    "2010-11-09",
  "open":          42.47441,
  "high":          42.50749,
  "low":           41.60786,
  "close":         41.81689,
  "volume":        103515960,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-10T00:  00:  00-05:  00",
  "tradingDay":    "2010-11-10",
  "open":          41.89098,
  "high":          42.17277,
  "low":           41.48217,
  "close":         42.07487,
  "volume":        103429792,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-11T00:  00:  00-05:  00",
  "tradingDay":    "2010-11-11",
  "open":          41.67401,
  "high":          42.12382,
  "low":           41.57478,
  "close":         41.8923,
  "volume":        97475072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-12T00:  00:  00-05:  00",
  "tradingDay":    "2010-11-12",
  "open":          41.80631,
  "high":          41.87246,
  "low":           40.16978,
  "close":         40.75189,
  "volume":        214768224,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-15T00:  00:  00-05:  00",
  "tradingDay":    "2010-11-15",
  "open":          40.80878,
  "high":          41.08396,
  "low":           40.51904,
  "close":         40.62091,
  "volume":        108921160,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-16T00:  00:  00-05:  00",
  "tradingDay":    "2010-11-16",
  "open":          40.44628,
  "high":          40.695,
  "low":           39.59957,
  "close":         39.89989,
  "volume":        177531952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-17T00:  00:  00-05:  00",
  "tradingDay":    "2010-11-17",
  "open":          39.84829,
  "high":          40.2174,
  "low":           39.39318,
  "close":         39.75568,
  "volume":        129337880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-18T00:  00:  00-05:  00",
  "tradingDay":    "2010-11-18",
  "open":          40.37749,
  "high":          40.96886,
  "low":           40.31001,
  "close":         40.80481,
  "volume":        133108144,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-19T00:  00:  00-05:  00",
  "tradingDay":    "2010-11-19",
  "open":          40.74395,
  "high":          40.80084,
  "low":           40.38277,
  "close":         40.5799,
  "volume":        103857608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-22T00:  00:  00-05:  00",
  "tradingDay":    "2010-11-22",
  "open":          40.57328,
  "high":          41.45704,
  "low":           40.46612,
  "close":         41.45704,
  "volume":        105985376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-23T00:  00:  00-05:  00",
  "tradingDay":    "2010-11-23",
  "open":          41.07205,
  "high":          41.24404,
  "low":           40.55741,
  "close":         40.8445,
  "volume":        140169456,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-24T00:  00:  00-05:  00",
  "tradingDay":    "2010-11-24",
  "open":          41.27711,
  "high":          41.72693,
  "low":           41.24404,
  "close":         41.64755,
  "volume":        111754912,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-26T00:  00:  00-05:  00",
  "tradingDay":    "2010-11-26",
  "open":          41.50731,
  "high":          42.03122,
  "low":           41.40147,
  "close":         41.67401,
  "volume":        64179140,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-29T00:  00:  00-05:  00",
  "tradingDay":    "2010-11-29",
  "open":          41.74016,
  "high":          42.00211,
  "low":           41.19509,
  "close":         41.92141,
  "volume":        120333240,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-11-30T00:  00:  00-05:  00",
  "tradingDay":    "2010-11-30",
  "open":          41.48085,
  "high":          41.58934,
  "low":           41.12762,
  "close":         41.16466,
  "volume":        135476272,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-01T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-01",
  "open":          41.70973,
  "high":          42.03783,
  "low":           41.67401,
  "close":         41.85923,
  "volume":        124650000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-02T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-02",
  "open":          42.00872,
  "high":          42.2032,
  "low":           41.65946,
  "close":         42.09075,
  "volume":        125163232,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-03T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-03",
  "open":          41.93993,
  "high":          42.1569,
  "low":           41.85129,
  "close":         41.99682,
  "volume":        92422856,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-06T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-06",
  "open":          42.15558,
  "high":          42.64375,
  "low":           42.12647,
  "close":         42.35534,
  "volume":        121067944,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-07T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-07",
  "open":          42.83823,
  "high":          42.86337,
  "low":           42.08678,
  "close":         42.09869,
  "volume":        105673200,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-08T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-08",
  "open":          42.28655,
  "high":          42.47044,
  "low":           41.95316,
  "close":         42.46912,
  "volume":        86906544,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-09T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-09",
  "open":          42.6173,
  "high":          42.66625,
  "low":           42.20585,
  "close":         42.30375,
  "volume":        79405320,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-10T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-10",
  "open":          42.2892,
  "high":          42.47441,
  "low":           42.15028,
  "close":         42.40959,
  "volume":        70882920,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-13T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-13",
  "open":          42.91364,
  "high":          43.00493,
  "low":           42.4678,
  "close":         42.55644,
  "volume":        118728536,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-14T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-14",
  "open":          42.56438,
  "high":          42.67154,
  "low":           42.2032,
  "close":         42.37387,
  "volume":        94905880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-15T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-15",
  "open":          42.38974,
  "high":          42.7324,
  "low":           42.22834,
  "close":         42.38313,
  "volume":        112653632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-16T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-16",
  "open":          42.47971,
  "high":          42.6808,
  "low":           42.34873,
  "close":         42.50087,
  "volume":        86932240,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-17T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-17",
  "open":          42.55115,
  "high":          42.57232,
  "low":           42.36593,
  "close":         42.4162,
  "volume":        104511440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-20T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-20",
  "open":          42.54718,
  "high":          42.76547,
  "low":           42.10133,
  "close":         42.62788,
  "volume":        104095712,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-21T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-21",
  "open":          42.7324,
  "high":          42.91629,
  "low":           42.60671,
  "close":         42.89116,
  "volume":        69202632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-22T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-22",
  "open":          42.91232,
  "high":          43.09225,
  "low":           42.80516,
  "close":         43.01816,
  "volume":        71845896,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-23T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-23",
  "open":          42.99699,
  "high":          43.01684,
  "low":           42.75489,
  "close":         42.81178,
  "volume":        60334044,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-27T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-27",
  "open":          42.71255,
  "high":          43.05521,
  "low":           42.53659,
  "close":         42.95465,
  "volume":        67437680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-28T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-28",
  "open":          43.11739,
  "high":          43.21661,
  "low":           43.00493,
  "close":         43.05917,
  "volume":        47490356,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-29T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-29",
  "open":          43.1584,
  "high":          43.18883,
  "low":           43.01022,
  "close":         43.03536,
  "volume":        44074592,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-30T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-30",
  "open":          43.0605,
  "high":          43.06446,
  "low":           42.73901,
  "close":         42.81971,
  "volume":        42560592,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2010-12-31T00:  00:  00-05:  00",
  "tradingDay":    "2010-12-31",
  "open":          42.59745,
  "high":          42.7959,
  "low":           42.50881,
  "close":         42.67418,
  "volume":        52277260,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-01-03T00:  00:  00-05:  00",
  "tradingDay":    "2011-01-03",
  "open":          43.08167,
  "high":          43.69288,
  "low":           42.97583,
  "close":         43.6016,
  "volume":        120161656,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-01-04T00:  00:  00-05:  00",
  "tradingDay":    "2011-01-04",
  "open":          43.98129,
  "high":          43.98923,
  "low":           43.41373,
  "close":         43.82915,
  "volume":        83508920,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-01-05T00:  00:  00-05:  00",
  "tradingDay":    "2011-01-05",
  "open":          43.59895,
  "high":          44.23266,
  "low":           43.59233,
  "close":         44.18768,
  "volume":        68976624,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-01-06T00:  00:  00-05:  00",
  "tradingDay":    "2011-01-06",
  "open":          44.28161,
  "high":          44.35305,
  "low":           44.04215,
  "close":         44.15196,
  "volume":        81100728,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-01-07T00:  00:  00-05:  00",
  "tradingDay":    "2011-01-07",
  "open":          44.18636,
  "high":          44.49858,
  "low":           43.90985,
  "close":         44.46815,
  "volume":        84205832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-01-10T00:  00:  00-05:  00",
  "tradingDay":    "2011-01-10",
  "open":          44.82006,
  "high":          45.40879,
  "low":           44.60707,
  "close":         45.3056,
  "volume":        121089104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-01-11T00:  00:  00-05:  00",
  "tradingDay":    "2011-01-11",
  "open":          45.62709,
  "high":          45.63767,
  "low":           44.91135,
  "close":         45.19844,
  "volume":        119879720,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-01-12T00:  00:  00-05:  00",
  "tradingDay":    "2011-01-12",
  "open":          45.41144,
  "high":          45.56755,
  "low":           45.24607,
  "close":         45.56623,
  "volume":        81681232,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-01-13T00:  00:  00-05:  00",
  "tradingDay":    "2011-01-13",
  "open":          45.66413,
  "high":          45.85993,
  "low":           45.49082,
  "close":         45.73293,
  "volume":        80484696,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-01-14T00:  00:  00-05:  00",
  "tradingDay":    "2011-01-14",
  "open":          45.76071,
  "high":          46.10336,
  "low":           45.56887,
  "close":         46.10336,
  "volume":        83371352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-01-18T00:  00:  00-05:  00",
  "tradingDay":    "2011-01-18",
  "open":          43.59498,
  "high":          45.61121,
  "low":           43.12929,
  "close":         45.06746,
  "volume":        507776928,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-01-19T00:  00:  00-05:  00",
  "tradingDay":    "2011-01-19",
  "open":          46.08881,
  "high":          46.11924,
  "low":           44.5687,
  "close":         44.828,
  "volume":        306559936,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-01-20T00:  00:  00-05:  00",
  "tradingDay":    "2011-01-20",
  "open":          44.50916,
  "high":          44.75656,
  "low":           43.67436,
  "close":         44.01304,
  "volume":        206456704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-01-21T00:  00:  00-05:  00",
  "tradingDay":    "2011-01-21",
  "open":          44.15725,
  "high":          44.3041,
  "low":           43.21264,
  "close":         43.22454,
  "volume":        203651680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-01-24T00:  00:  00-05:  00",
  "tradingDay":    "2011-01-24",
  "open":          43.24439,
  "high":          44.64411,
  "low":           43.22454,
  "close":         44.64411,
  "volume":        155136368,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-01-25T00:  00:  00-05:  00",
  "tradingDay":    "2011-01-25",
  "open":          44.49593,
  "high":          45.17198,
  "low":           44.26309,
  "close":         45.16669,
  "volume":        147627584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-01-26T00:  00:  00-05:  00",
  "tradingDay":    "2011-01-26",
  "open":          45.37307,
  "high":          45.72234,
  "low":           45.17992,
  "close":         45.49082,
  "volume":        136829280,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-01-27T00:  00:  00-05:  00",
  "tradingDay":    "2011-01-27",
  "open":          45.48156,
  "high":          45.60195,
  "low":           45.35587,
  "close":         45.40615,
  "volume":        76942704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-01-28T00:  00:  00-05:  00",
  "tradingDay":    "2011-01-28",
  "open":          45.53316,
  "high":          45.56358,
  "low":           44.1255,
  "close":         44.46551,
  "volume":        159826528,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-01-31T00:  00:  00-05:  00",
  "tradingDay":    "2011-01-31",
  "open":          44.42582,
  "high":          44.98676,
  "low":           44.22737,
  "close":         44.89151,
  "volume":        101837936,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-02-01T00:  00:  00-05:  00",
  "tradingDay":    "2011-02-01",
  "open":          45.15346,
  "high":          45.72895,
  "low":           45.11112,
  "close":         45.64693,
  "volume":        115169912,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-02-02T00:  00:  00-05:  00",
  "tradingDay":    "2011-02-02",
  "open":          45.5702,
  "high":          45.67604,
  "low":           45.45113,
  "close":         45.553,
  "volume":        69905584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-02-03T00:  00:  00-05:  00",
  "tradingDay":    "2011-02-03",
  "open":          45.4842,
  "high":          45.54241,
  "low":           44.78963,
  "close":         45.43658,
  "volume":        106305864,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-02-04T00:  00:  00-05:  00",
  "tradingDay":    "2011-02-04",
  "open":          45.45906,
  "high":          45.86787,
  "low":           45.44584,
  "close":         45.84141,
  "volume":        86880840,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-02-07T00:  00:  00-05:  00",
  "tradingDay":    "2011-02-07",
  "open":          46.02531,
  "high":          46.73442,
  "low":           45.99223,
  "close":         46.55318,
  "volume":        130932008,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-02-08T00:  00:  00-05:  00",
  "tradingDay":    "2011-02-08",
  "open":          46.79131,
  "high":          47.03474,
  "low":           46.58889,
  "close":         46.99241,
  "volume":        102862136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-02-09T00:  00:  00-05:  00",
  "tradingDay":    "2011-02-09",
  "open":          46.99108,
  "high":          47.49514,
  "low":           46.94875,
  "close":         47.38401,
  "volume":        130317488,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-02-10T00:  00:  00-05:  00",
  "tradingDay":    "2011-02-10",
  "open":          47.28214,
  "high":          47.62744,
  "low":           46.03986,
  "close":         46.90509,
  "volume":        250663568,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-02-11T00:  00:  00-05:  00",
  "tradingDay":    "2011-02-11",
  "open":          46.93287,
  "high":          47.33638,
  "low":           46.77279,
  "close":         47.2107,
  "volume":        99226416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-02-14T00:  00:  00-05:  00",
  "tradingDay":    "2011-02-14",
  "open":          47.20276,
  "high":          47.55864,
  "low":           47.19218,
  "close":         47.51895,
  "volume":        83796904,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-02-15T00:  00:  00-05:  00",
  "tradingDay":    "2011-02-15",
  "open":          47.52028,
  "high":          47.62347,
  "low":           47.30331,
  "close":         47.61421,
  "volume":        76712920,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-02-16T00:  00:  00-05:  00",
  "tradingDay":    "2011-02-16",
  "open":          47.73328,
  "high":          48.2757,
  "low":           47.69359,
  "close":         48.04153,
  "volume":        129888912,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-02-17T00:  00:  00-05:  00",
  "tradingDay":    "2011-02-17",
  "open":          47.24377,
  "high":          47.66316,
  "low":           47.16704,
  "close":         47.40253,
  "volume":        143229200,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-02-18T00:  00:  00-05:  00",
  "tradingDay":    "2011-02-18",
  "open":          47.39062,
  "high":          47.56129,
  "low":           46.24095,
  "close":         46.37854,
  "volume":        220293600,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-02-22T00:  00:  00-05:  00",
  "tradingDay":    "2011-02-22",
  "open":          45.25268,
  "high":          45.69588,
  "low":           44.67983,
  "close":         44.79757,
  "volume":        235544720,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-02-23T00:  00:  00-05:  00",
  "tradingDay":    "2011-02-23",
  "open":          44.81874,
  "high":          45.59534,
  "low":           44.79757,
  "close":         45.32809,
  "volume":        181367968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-02-24T00:  00:  00-05:  00",
  "tradingDay":    "2011-02-24",
  "open":          45.51331,
  "high":          45.66281,
  "low":           44.76582,
  "close":         45.36249,
  "volume":        134948688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-02-25T00:  00:  00-05:  00",
  "tradingDay":    "2011-02-25",
  "open":          45.68133,
  "high":          46.09674,
  "low":           45.6165,
  "close":         46.06102,
  "volume":        102586240,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-02-28T00:  00:  00-05:  00",
  "tradingDay":    "2011-02-28",
  "open":          46.46453,
  "high":          46.97256,
  "low":           46.45263,
  "close":         46.72913,
  "volume":        108810048,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-01T00:  00:  00-05:  00",
  "tradingDay":    "2011-03-01",
  "open":          47.02813,
  "high":          47.0612,
  "low":           45.99752,
  "close":         46.21317,
  "volume":        123132968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-02T00:  00:  00-05:  00",
  "tradingDay":    "2011-03-02",
  "open":          46.29916,
  "high":          46.87995,
  "low":           46.09278,
  "close":         46.58493,
  "volume":        162670096,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-03T00:  00:  00-05:  00",
  "tradingDay":    "2011-03-03",
  "open":          47.257,
  "high":          47.59966,
  "low":           47.08766,
  "close":         47.56923,
  "volume":        135188288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-04T00:  00:  00-05:  00",
  "tradingDay":    "2011-03-04",
  "open":          47.6367,
  "high":          47.66581,
  "low":           47.32977,
  "close":         47.62744,
  "volume":        122359720,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-07T00:  00:  00-05:  00",
  "tradingDay":    "2011-03-07",
  "open":          47.77429,
  "high":          47.84838,
  "low":           46.47776,
  "close":         47.01357,
  "volume":        147426528,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-08T00:  00:  00-05:  00",
  "tradingDay":    "2011-03-08",
  "open":          46.95404,
  "high":          47.28346,
  "low":           46.60213,
  "close":         47.06649,
  "volume":        96187832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-09T00:  00:  00-05:  00",
  "tradingDay":    "2011-03-09",
  "open":          46.92493,
  "high":          46.9342,
  "low":           46.38383,
  "close":         46.63123,
  "volume":        122395248,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-10T00:  00:  00-05:  00",
  "tradingDay":    "2011-03-10",
  "open":          46.26344,
  "high":          46.27402,
  "low":           45.62973,
  "close":         45.8639,
  "volume":        137105168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-11T00:  00:  00-05:  00",
  "tradingDay":    "2011-03-11",
  "open":          45.69588,
  "high":          46.61139,
  "low":           45.64296,
  "close":         46.56773,
  "volume":        127168544,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-14T00:  00:  00-04:  00",
  "tradingDay":    "2011-03-14",
  "open":          46.72516,
  "high":          47.16175,
  "low":           46.47776,
  "close":         46.77544,
  "volume":        117821496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-15T00:  00:  00-04:  00",
  "tradingDay":    "2011-03-15",
  "open":          45.28972,
  "high":          46.01869,
  "low":           44.9947,
  "close":         45.69985,
  "volume":        194648560,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-16T00:  00:  00-04:  00",
  "tradingDay":    "2011-03-16",
  "open":          45.24607,
  "high":          45.37836,
  "low":           43.16369,
  "close":         43.65981,
  "volume":        312267488,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-17T00:  00:  00-04:  00",
  "tradingDay":    "2011-03-17",
  "open":          44.56208,
  "high":          44.92987,
  "low":           43.7458,
  "close":         44.27235,
  "volume":        178011920,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-18T00:  00:  00-04:  00",
  "tradingDay":    "2011-03-18",
  "open":          44.60177,
  "high":          44.74334,
  "low":           43.65849,
  "close":         43.74713,
  "volume":        203330448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-21T00:  00:  00-04:  00",
  "tradingDay":    "2011-03-21",
  "open":          44.4708,
  "high":          44.94707,
  "low":           44.35438,
  "close":         44.88886,
  "volume":        110515288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-22T00:  00:  00-04:  00",
  "tradingDay":    "2011-03-22",
  "open":          45.32677,
  "high":          45.32809,
  "low":           44.86769,
  "close":         45.14023,
  "volume":        88066800,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-23T00:  00:  00-04:  00",
  "tradingDay":    "2011-03-23",
  "open":          44.88622,
  "high":          45.01057,
  "low":           44.44566,
  "close":         44.87431,
  "volume":        100690528,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-24T00:  00:  00-04:  00",
  "tradingDay":    "2011-03-24",
  "open":          45.22622,
  "high":          45.77526,
  "low":           44.83065,
  "close":         45.63899,
  "volume":        109252232,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-25T00:  00:  00-04:  00",
  "tradingDay":    "2011-03-25",
  "open":          46.04647,
  "high":          46.57699,
  "low":           45.9102,
  "close":         46.50819,
  "volume":        121254640,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-28T00:  00:  00-04:  00",
  "tradingDay":    "2011-03-28",
  "open":          46.72119,
  "high":          46.87598,
  "low":           46.36267,
  "close":         46.36267,
  "volume":        83509680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-29T00:  00:  00-04:  00",
  "tradingDay":    "2011-03-29",
  "open":          45.98694,
  "high":          46.43146,
  "low":           45.7832,
  "close":         46.43146,
  "volume":        95265672,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-30T00:  00:  00-04:  00",
  "tradingDay":    "2011-03-30",
  "open":          46.38913,
  "high":          46.42088,
  "low":           45.96577,
  "close":         46.12321,
  "volume":        88923192,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-03-31T00:  00:  00-04:  00",
  "tradingDay":    "2011-03-31",
  "open":          45.81495,
  "high":          46.27799,
  "low":           45.7832,
  "close":         46.10733,
  "volume":        74065880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-04-01T00:  00:  00-04:  00",
  "tradingDay":    "2011-04-01",
  "open":          46.44998,
  "high":          46.51481,
  "low":           45.41805,
  "close":         45.58475,
  "volume":        113018720,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-04-04T00:  00:  00-04:  00",
  "tradingDay":    "2011-04-04",
  "open":          45.54374,
  "high":          45.59004,
  "low":           44.76979,
  "close":         45.1389,
  "volume":        124200256,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-04-05T00:  00:  00-04:  00",
  "tradingDay":    "2011-04-05",
  "open":          44.58325,
  "high":          45.27914,
  "low":           44.45227,
  "close":         44.83462,
  "volume":        130376440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-04-06T00:  00:  00-04:  00",
  "tradingDay":    "2011-04-06",
  "open":          45.17331,
  "high":          45.49743,
  "low":           44.6031,
  "close":         44.72216,
  "volume":        108665680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-04-07T00:  00:  00-04:  00",
  "tradingDay":    "2011-04-07",
  "open":          44.7301,
  "high":          45.03836,
  "low":           44.45625,
  "close":         44.72746,
  "volume":        100812224,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-04-08T00:  00:  00-04:  00",
  "tradingDay":    "2011-04-08",
  "open":          44.97089,
  "high":          45.00131,
  "low":           44.18106,
  "close":         44.32792,
  "volume":        101915792,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-04-11T00:  00:  00-04:  00",
  "tradingDay":    "2011-04-11",
  "open":          44.19562,
  "high":          44.40862,
  "low":           43.66113,
  "close":         43.76432,
  "volume":        107751080,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-04-12T00:  00:  00-04:  00",
  "tradingDay":    "2011-04-12",
  "open":          43.72331,
  "high":          44.15196,
  "low":           43.68495,
  "close":         43.976,
  "volume":        115136656,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-04-13T00:  00:  00-04:  00",
  "tradingDay":    "2011-04-13",
  "open":          44.29351,
  "high":          44.4708,
  "low":           43.99187,
  "close":         44.46947,
  "volume":        93521136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-04-14T00:  00:  00-04:  00",
  "tradingDay":    "2011-04-14",
  "open":          44.29351,
  "high":          44.45227,
  "low":           43.93102,
  "close":         43.97865,
  "volume":        81522504,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-04-15T00:  00:  00-04:  00",
  "tradingDay":    "2011-04-15",
  "open":          44.1083,
  "high":          44.14005,
  "low":           43.23513,
  "close":         43.32244,
  "volume":        122513160,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-04-18T00:  00:  00-04:  00",
  "tradingDay":    "2011-04-18",
  "open":          43.14252,
  "high":          43.95351,
  "low":           42.35667,
  "close":         43.90324,
  "volume":        164642912,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-04-19T00:  00:  00-04:  00",
  "tradingDay":    "2011-04-19",
  "open":          44.06861,
  "high":          44.71423,
  "low":           43.88472,
  "close":         44.69835,
  "volume":        113211464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-04-20T00:  00:  00-04:  00",
  "tradingDay":    "2011-04-20",
  "open":          45.44584,
  "high":          45.74218,
  "low":           45.17992,
  "close":         45.30031,
  "volume":        189143584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-04-21T00:  00:  00-04:  00",
  "tradingDay":    "2011-04-21",
  "open":          46.89318,
  "high":          46.98315,
  "low":           46.10865,
  "close":         46.39706,
  "volume":        203489168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-04-25T00:  00:  00-04:  00",
  "tradingDay":    "2011-04-25",
  "open":          46.36796,
  "high":          46.80057,
  "low":           46.34414,
  "close":         46.70267,
  "volume":        71953984,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-04-26T00:  00:  00-04:  00",
  "tradingDay":    "2011-04-26",
  "open":          46.78337,
  "high":          46.96462,
  "low":           46.21846,
  "close":         46.36002,
  "volume":        91136376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-04-27T00:  00:  00-04:  00",
  "tradingDay":    "2011-04-27",
  "open":          46.6008,
  "high":          46.61536,
  "low":           45.92079,
  "close":         46.3243,
  "volume":        96159864,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-04-28T00:  00:  00-04:  00",
  "tradingDay":    "2011-04-28",
  "open":          45.8004,
  "high":          46.27138,
  "low":           45.71175,
  "close":         45.87449,
  "volume":        97503040,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-04-29T00:  00:  00-04:  00",
  "tradingDay":    "2011-04-29",
  "open":          45.91285,
  "high":          46.82703,
  "low":           45.8639,
  "close":         46.32165,
  "volume":        271664576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-02T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-02",
  "open":          46.26477,
  "high":          46.36663,
  "low":           45.70911,
  "close":         45.81231,
  "volume":        119510856,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-03T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-03",
  "open":          46.03853,
  "high":          46.2899,
  "low":           45.72499,
  "close":         46.06632,
  "volume":        84646504,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-04T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-04",
  "open":          46.07426,
  "high":          46.54656,
  "low":           45.89168,
  "close":         46.24757,
  "volume":        105078336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-05T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-05",
  "open":          46.09542,
  "high":          46.43014,
  "low":           45.78187,
  "close":         45.87449,
  "volume":        90774312,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-06T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-06",
  "open":          46.26873,
  "high":          46.30445,
  "low":           45.80304,
  "close":         45.86258,
  "volume":        75652440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-09T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-09",
  "open":          46.02133,
  "high":          46.19862,
  "low":           45.84538,
  "close":         45.98694,
  "volume":        55272760,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-10T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-10",
  "open":          46.1576,
  "high":          46.26344,
  "low":           45.86258,
  "close":         46.23169,
  "volume":        76149800,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-11T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-11",
  "open":          46.1748,
  "high":          46.30445,
  "low":           45.67471,
  "close":         45.93799,
  "volume":        90723672,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-12T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-12",
  "open":          45.79113,
  "high":          45.92344,
  "low":           45.28178,
  "close":         45.85067,
  "volume":        86642744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-13T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-13",
  "open":          45.71969,
  "high":          45.80833,
  "low":           45.02777,
  "close":         45.04762,
  "volume":        88056216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-16T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-16",
  "open":          44.87563,
  "high":          45.14288,
  "low":           44.00246,
  "close":         44.09507,
  "volume":        121516176,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-17T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-17",
  "open":          43.91382,
  "high":          44.4708,
  "low":           43.75507,
  "close":         44.4708,
  "volume":        122230464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-18T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-18",
  "open":          44.51446,
  "high":          45.12038,
  "low":           44.45227,
  "close":         44.96427,
  "volume":        90372192,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-19T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-19",
  "open":          45.25268,
  "high":          45.30031,
  "low":           44.80552,
  "close":         45.05159,
  "volume":        70501968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-20T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-20",
  "open":          44.95766,
  "high":          45.10715,
  "low":           44.32262,
  "close":         44.34908,
  "volume":        91295104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-23T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-23",
  "open":          43.65452,
  "high":          44.44963,
  "low":           43.58175,
  "close":         44.2406,
  "volume":        103258968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-24T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-24",
  "open":          44.38613,
  "high":          44.43904,
  "low":           43.83577,
  "close":         43.94822,
  "volume":        86946600,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-25T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-25",
  "open":          44.11227,
  "high":          44.79096,
  "low":           44.03554,
  "close":         44.55547,
  "volume":        79425728,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-26T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-26",
  "open":          44.44831,
  "high":          44.57002,
  "low":           44.24457,
  "close":         44.31998,
  "volume":        60116356,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-27T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-27",
  "open":          44.29351,
  "high":          44.66792,
  "low":           44.22869,
  "close":         44.63882,
  "volume":        54975704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-05-31T00:  00:  00-04:  00",
  "tradingDay":    "2011-05-31",
  "open":          45.11509,
  "high":          46.01736,
  "low":           45.11377,
  "close":         46.01736,
  "volume":        112768528,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-01T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-01",
  "open":          46.15496,
  "high":          46.58625,
  "low":           45.59666,
  "close":         45.71043,
  "volume":        149737216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-02T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-02",
  "open":          45.84141,
  "high":          46.03721,
  "low":           45.55035,
  "close":         45.78849,
  "volume":        91482560,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-03T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-03",
  "open":          45.40218,
  "high":          45.68662,
  "low":           45.24739,
  "close":         45.43658,
  "volume":        84614000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-06T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-06",
  "open":          45.73557,
  "high":          45.91417,
  "low":           44.69173,
  "close":         44.72216,
  "volume":        124701392,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-07T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-07",
  "open":          44.73804,
  "high":          44.74598,
  "low":           43.90985,
  "close":         43.92838,
  "volume":        143066688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-08T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-08",
  "open":          43.89397,
  "high":          44.29351,
  "low":           43.74448,
  "close":         43.95483,
  "volume":        90133336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-09T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-09",
  "open":          44.08846,
  "high":          44.14402,
  "low":           43.75771,
  "close":         43.85561,
  "volume":        74334968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-10T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-10",
  "open":          43.73125,
  "high":          43.8781,
  "low":           43.06446,
  "close":         43.11606,
  "volume":        117191104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-13T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-13",
  "open":          43.28805,
  "high":          43.4349,
  "low":           43.00626,
  "close":         43.20867,
  "volume":        88991224,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-14T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-14",
  "open":          43.65849,
  "high":          44.08846,
  "low":           43.5672,
  "close":         43.98129,
  "volume":        90317016,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-15T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-15",
  "open":          43.62541,
  "high":          43.69817,
  "low":           42.98112,
  "close":         43.22852,
  "volume":        107820624,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-16T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-16",
  "open":          43.24836,
  "high":          43.48385,
  "low":           42.11456,
  "close":         43.01816,
  "volume":        137834576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-17T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-17",
  "open":          43.52486,
  "high":          43.55926,
  "low":           42.25083,
  "close":         42.3699,
  "volume":        166153136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-20T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-20",
  "open":          41.98623,
  "high":          42.03122,
  "low":           41.07867,
  "close":         41.71635,
  "volume":        172943088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-21T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-21",
  "open":          41.89627,
  "high":          43.10283,
  "low":           41.70047,
  "close":         43.03668,
  "volume":        133187512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-22T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-22",
  "open":          43.01816,
  "high":          43.51295,
  "low":           42.65037,
  "close":         42.6808,
  "volume":        105438128,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-23T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-23",
  "open":          42.19526,
  "high":          43.88207,
  "low":           42.08678,
  "close":         43.82121,
  "volume":        151107600,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-24T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-24",
  "open":          43.83973,
  "high":          44.07523,
  "low":           43.0089,
  "close":         43.1756,
  "volume":        118758768,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-27T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-27",
  "open":          43.33965,
  "high":          44.17445,
  "low":           43.29467,
  "close":         43.92838,
  "volume":        91772816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-28T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-28",
  "open":          44.14137,
  "high":          44.54489,
  "low":           44.11359,
  "close":         44.35438,
  "volume":        79446896,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-29T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-29",
  "open":          44.45757,
  "high":          44.50122,
  "low":           43.90721,
  "close":         44.19297,
  "volume":        95216544,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-06-30T00:  00:  00-04:  00",
  "tradingDay":    "2011-06-30",
  "open":          44.28029,
  "high":          44.46947,
  "low":           44.03421,
  "close":         44.40862,
  "volume":        87256512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-07-01T00:  00:  00-04:  00",
  "tradingDay":    "2011-07-01",
  "open":          44.44566,
  "high":          45.44452,
  "low":           44.21414,
  "close":         45.41277,
  "volume":        117513104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-07-05T00:  00:  00-04:  00",
  "tradingDay":    "2011-07-05",
  "open":          45.37836,
  "high":          46.28196,
  "low":           45.31221,
  "close":         46.22904,
  "volume":        95906648,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-07-06T00:  00:  00-04:  00",
  "tradingDay":    "2011-07-06",
  "open":          46.16554,
  "high":          46.84688,
  "low":           45.86919,
  "close":         46.5373,
  "volume":        120027112,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-07-07T00:  00:  00-04:  00",
  "tradingDay":    "2011-07-07",
  "open":          46.92229,
  "high":          47.36284,
  "low":           46.83365,
  "close":         47.257,
  "volume":        107889408,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-07-08T00:  00:  00-04:  00",
  "tradingDay":    "2011-07-08",
  "open":          46.75427,
  "high":          47.62744,
  "low":           46.59551,
  "close":         47.58907,
  "volume":        132256280,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-07-11T00:  00:  00-04:  00",
  "tradingDay":    "2011-07-11",
  "open":          47.14323,
  "high":          47.59701,
  "low":           46.67754,
  "close":         46.83365,
  "volume":        119549408,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-07-12T00:  00:  00-04:  00",
  "tradingDay":    "2011-07-12",
  "open":          46.77147,
  "high":          47.3205,
  "low":           46.12188,
  "close":         46.80057,
  "volume":        121912248,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-07-13T00:  00:  00-04:  00",
  "tradingDay":    "2011-07-13",
  "open":          47.4065,
  "high":          47.62744,
  "low":           47.14852,
  "close":         47.36549,
  "volume":        105723088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-07-14T00:  00:  00-04:  00",
  "tradingDay":    "2011-07-14",
  "open":          47.76106,
  "high":          47.84044,
  "low":           47.14323,
  "close":         47.33241,
  "volume":        116222840,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-07-15T00:  00:  00-04:  00",
  "tradingDay":    "2011-07-15",
  "open":          47.78223,
  "high":          48.28893,
  "low":           47.51763,
  "close":         48.27835,
  "volume":        130782344,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-07-18T00:  00:  00-04:  00",
  "tradingDay":    "2011-07-18",
  "open":          48.33391,
  "high":          49.56561,
  "low":           48.32597,
  "close":         49.45316,
  "volume":        154582320,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-07-19T00:  00:  00-04:  00",
  "tradingDay":    "2011-07-19",
  "open":          50.00881,
  "high":          50.0948,
  "low":           49.38966,
  "close":         49.85667,
  "volume":        221129584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-07-20T00:  00:  00-04:  00",
  "tradingDay":    "2011-07-20",
  "open":          52.40606,
  "high":          52.4259,
  "low":           51.0672,
  "close":         51.18626,
  "volume":        254113360,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-07-21T00:  00:  00-04:  00",
  "tradingDay":    "2011-07-21",
  "open":          51.19288,
  "high":          51.60433,
  "low":           50.78937,
  "close":         51.23787,
  "volume":        142133952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-07-22T00:  00:  00-04:  00",
  "tradingDay":    "2011-07-22",
  "open":          51.37413,
  "high":          52.2645,
  "low":           51.29872,
  "close":         52.03297,
  "volume":        139491440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-07-25T00:  00:  00-04:  00",
  "tradingDay":    "2011-07-25",
  "open":          51.6427,
  "high":          52.91938,
  "low":           51.54612,
  "close":         52.72093,
  "volume":        159218816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-07-26T00:  00:  00-04:  00",
  "tradingDay":    "2011-07-26",
  "open":          52.91938,
  "high":          53.51472,
  "low":           52.87704,
  "close":         53.37051,
  "volume":        128653824,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-07-27T00:  00:  00-04:  00",
  "tradingDay":    "2011-07-27",
  "open":          52.99743,
  "high":          53.26865,
  "low":           51.88083,
  "close":         51.93904,
  "volume":        177985472,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-07-28T00:  00:  00-04:  00",
  "tradingDay":    "2011-07-28",
  "open":          51.81071,
  "high":          52.52116,
  "low":           51.349,
  "close":         51.83718,
  "volume":        160355632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-07-29T00:  00:  00-04:  00",
  "tradingDay":    "2011-07-29",
  "open":          51.28417,
  "high":          52.27773,
  "low":           50.8026,
  "close":         51.6599,
  "volume":        170766944,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-01T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-01",
  "open":          52.62567,
  "high":          52.85323,
  "low":           51.90994,
  "close":         52.48941,
  "volume":        165435056,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-02T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-02",
  "open":          52.6045,
  "high":          52.64155,
  "low":           51.3781,
  "close":         51.45219,
  "volume":        172644512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-03T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-03",
  "open":          51.70488,
  "high":          52.06605,
  "low":           50.56976,
  "close":         51.9364,
  "volume":        197740800,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-04T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-04",
  "open":          51.51834,
  "high":          51.77103,
  "low":           49.92282,
  "close":         49.92546,
  "volume":        235237840,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-05T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-05",
  "open":          50.325,
  "high":          50.73645,
  "low":           47.96745,
  "close":         49.42934,
  "volume":        325173888,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-08T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-08",
  "open":          47.85102,
  "high":          48.6554,
  "low":           46.70399,
  "close":         46.72913,
  "volume":        308772352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-09T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-09",
  "open":          47.82192,
  "high":          49.56032,
  "low":           46.96595,
  "close":         49.48094,
  "volume":        292245312,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-10T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-10",
  "open":          49.10257,
  "high":          49.56561,
  "low":           47.95818,
  "close":         48.11562,
  "volume":        237193280,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-11T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-11",
  "open":          49.01921,
  "high":          49.67145,
  "low":           48.25189,
  "close":         49.43993,
  "volume":        200294880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-12T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-12",
  "open":          50.01807,
  "high":          50.22578,
  "low":           49.51005,
  "close":         49.87519,
  "volume":        142797600,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-15T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-15",
  "open":          50.24694,
  "high":          50.93093,
  "low":           50.02072,
  "close":         50.72454,
  "volume":        124324216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-16T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-16",
  "open":          50.4745,
  "high":          50.71925,
  "low":           49.75215,
  "close":         50.33691,
  "volume":        134675808,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-17T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-17",
  "open":          50.58431,
  "high":          50.8714,
  "low":           50.00881,
  "close":         50.33162,
  "volume":        119334744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-18T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-18",
  "open":          49.06155,
  "high":          49.30101,
  "low":           47.80869,
  "close":         48.42784,
  "volume":        229846256,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-19T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-19",
  "open":          47.91453,
  "high":          48.55353,
  "low":           47.09824,
  "close":         47.10221,
  "volume":        209450688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-22T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-22",
  "open":          48.26115,
  "high":          48.27306,
  "low":           46.97785,
  "close":         47.15646,
  "volume":        144506624,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-23T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-23",
  "open":          47.66448,
  "high":          49.43199,
  "low":           47.23054,
  "close":         49.4267,
  "volume":        177313504,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-24T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-24",
  "open":          49.4095,
  "high":          50.13581,
  "low":           49.0298,
  "close":         49.76802,
  "volume":        169062464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-25T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-25",
  "open":          48.29951,
  "high":          49.67145,
  "low":           48.28893,
  "close":         49.44257,
  "volume":        235209120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-26T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-26",
  "open":          49.15548,
  "high":          50.77614,
  "low":           49.05626,
  "close":         50.74703,
  "volume":        173167568,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-29T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-29",
  "open":          51.35561,
  "high":          51.79484,
  "low":           51.33179,
  "close":         51.59242,
  "volume":        109398872,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-30T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-30",
  "open":          51.36487,
  "high":          51.83982,
  "low":           51.09498,
  "close":         51.59507,
  "volume":        112818416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-08-31T00:  00:  00-04:  00",
  "tradingDay":    "2011-08-31",
  "open":          51.6718,
  "high":          51.87157,
  "low":           50.51948,
  "close":         50.91241,
  "volume":        141118816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-01T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-01",
  "open":          51.04338,
  "high":          51.24448,
  "low":           50.36866,
  "close":         50.40968,
  "volume":        92788696,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-02T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-02",
  "open":          49.59736,
  "high":          50.00881,
  "low":           49.19253,
  "close":         49.48623,
  "volume":        118553928,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-06T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-06",
  "open":          48.60248,
  "high":          50.31706,
  "low":           48.48473,
  "close":         50.23901,
  "volume":        137593456,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-07T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-07",
  "open":          51.00899,
  "high":          51.01428,
  "low":           50.53801,
  "close":         50.79334,
  "volume":        94638304,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-08T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-08",
  "open":          50.59092,
  "high":          51.41249,
  "low":           50.57902,
  "close":         50.82113,
  "volume":        112342216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-09T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-09",
  "open":          50.82641,
  "high":          51.0672,
  "low":           49.61456,
  "close":         49.94002,
  "volume":        152543744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-12T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-12",
  "open":          49.34732,
  "high":          50.38983,
  "low":           49.20179,
  "close":         50.26547,
  "volume":        126291736,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-13T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-13",
  "open":          50.55653,
  "high":          51.09498,
  "low":           50.30648,
  "close":         50.88462,
  "volume":        118977968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-14T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-14",
  "open":          51.20214,
  "high":          51.88877,
  "low":           51.03545,
  "close":         51.50378,
  "volume":        144349392,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-15T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-15",
  "open":          51.78558,
  "high":          52.0806,
  "low":           51.58316,
  "close":         51.988,
  "volume":        112790448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-16T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-16",
  "open":          52.32933,
  "high":          52.98552,
  "low":           52.26185,
  "close":         52.98552,
  "volume":        188558528,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-19T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-19",
  "open":          52.52248,
  "high":          54.66969,
  "low":           52.28434,
  "close":         54.45801,
  "volume":        222402464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-20T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-20",
  "open":          54.93031,
  "high":          55.94372,
  "low":           54.3998,
  "close":         54.69879,
  "volume":        209415168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-21T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-21",
  "open":          55.51772,
  "high":          55.7757,
  "low":           54.50696,
  "close":         54.52548,
  "volume":        163583936,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-22T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-22",
  "open":          53.07681,
  "high":          54.21855,
  "low":           52.48279,
  "close":         53.16016,
  "volume":        261442992,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-23T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-23",
  "open":          52.95642,
  "high":          53.81107,
  "low":           52.89953,
  "close":         53.48826,
  "volume":        147524032,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-26T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-26",
  "open":          52.90085,
  "high":          53.44593,
  "low":           51.76838,
  "close":         53.33876,
  "volume":        219437200,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-27T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-27",
  "open":          54.07434,
  "high":          54.14314,
  "low":           52.66272,
  "close":         52.82148,
  "volume":        170743504,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-28T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-28",
  "open":          52.94451,
  "high":          53.41417,
  "low":           52.45766,
  "close":         52.5238,
  "volume":        116039160,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-29T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-29",
  "open":          53.17339,
  "high":          53.21175,
  "low":           51.09498,
  "close":         51.6718,
  "volume":        175837296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-09-30T00:  00:  00-04:  00",
  "tradingDay":    "2011-09-30",
  "open":          51.21537,
  "high":          51.44954,
  "low":           50.42952,
  "close":         50.44804,
  "volume":        147997216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-03T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-03",
  "open":          50.31971,
  "high":          50.62268,
  "low":           49.36981,
  "close":         49.559,
  "volume":        180618160,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-04T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-04",
  "open":          49.55503,
  "high":          50.51154,
  "low":           46.8654,
  "close":         49.28117,
  "volume":        333033408,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-05T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-05",
  "open":          48.66598,
  "high":          50.2496,
  "low":           47.66713,
  "close":         50.04189,
  "volume":        212308624,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-06T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-06",
  "open":          49.36319,
  "high":          50.90579,
  "low":           49.18856,
  "close":         49.92546,
  "volume":        219357840,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-07T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-07",
  "open":          49.70452,
  "high":          49.97441,
  "low":           48.75065,
  "close":         48.92396,
  "volume":        144590512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-10T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-10",
  "open":          50.15302,
  "high":          51.43896,
  "low":           50.03659,
  "close":         51.43896,
  "volume":        119457192,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-11T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-11",
  "open":          51.89274,
  "high":          53.34008,
  "low":           51.79484,
  "close":         52.95774,
  "volume":        163504576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-12T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-12",
  "open":          53.89045,
  "high":          54.14314,
  "low":           52.9379,
  "close":         53.20911,
  "volume":        167986864,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-13T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-13",
  "open":          53.57822,
  "high":          54.03465,
  "low":           53.29643,
  "close":         54.03465,
  "volume":        115048976,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-14T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-14",
  "open":          55.16845,
  "high":          55.82994,
  "low":           54.93957,
  "close":         55.82994,
  "volume":        154781120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-17T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-17",
  "open":          55.78364,
  "high":          56.45174,
  "low":           55.02821,
  "close":         55.56402,
  "volume":        185198704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-18T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-18",
  "open":          55.79819,
  "high":          56.2017,
  "low":           55.03483,
  "close":         55.86169,
  "volume":        237989952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-19T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-19",
  "open":          53.09798,
  "high":          54.03333,
  "low":           52.62832,
  "close":         52.7368,
  "volume":        298024672,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-20T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-20",
  "open":          52.90085,
  "high":          52.96568,
  "low":           52.15337,
  "close":         52.2989,
  "volume":        148346416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-21T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-21",
  "open":          52.66801,
  "high":          52.8056,
  "low":           51.69561,
  "close":         51.97609,
  "volume":        167705680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-24T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-24",
  "open":          52.41399,
  "high":          53.77932,
  "low":           52.3108,
  "close":         53.68274,
  "volume":        135611584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-25T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-25",
  "open":          53.58087,
  "high":          53.78593,
  "low":           52.57275,
  "close":         52.62435,
  "volume":        116241736,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-26T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-26",
  "open":          53.14296,
  "high":          53.25674,
  "low":           52.01313,
  "close":         52.99876,
  "volume":        123179080,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-27T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-27",
  "open":          53.95792,
  "high":          54.11006,
  "low":           53.16942,
  "close":         53.53986,
  "volume":        133586608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-28T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-28",
  "open":          53.31627,
  "high":          53.75947,
  "low":           53.25145,
  "close":         53.57425,
  "volume":        87150688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-10-31T00:  00:  00-04:  00",
  "tradingDay":    "2011-10-31",
  "open":          53.26865,
  "high":          54.15372,
  "low":           53.05829,
  "close":         53.55176,
  "volume":        104100248,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-01T00:  00:  00-04:  00",
  "tradingDay":    "2011-11-01",
  "open":          52.57673,
  "high":          52.85323,
  "low":           52.02239,
  "close":         52.45766,
  "volume":        143557248,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-02T00:  00:  00-04:  00",
  "tradingDay":    "2011-11-02",
  "open":          52.93128,
  "high":          52.97759,
  "low":           52.27243,
  "close":         52.57673,
  "volume":        88612536,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-03T00:  00:  00-04:  00",
  "tradingDay":    "2011-11-03",
  "open":          52.79634,
  "high":          53.36919,
  "low":           52.30551,
  "close":         53.32553,
  "volume":        119190368,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-04T00:  00:  00-04:  00",
  "tradingDay":    "2011-11-04",
  "open":          53.18794,
  "high":          53.37448,
  "low":           52.80825,
  "close":         52.95113,
  "volume":        81586752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-07T00:  00:  00-05:  00",
  "tradingDay":    "2011-11-07",
  "open":          52.90747,
  "high":          52.91938,
  "low":           52.40738,
  "close":         52.88366,
  "volume":        72957776,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-08T00:  00:  00-05:  00",
  "tradingDay":    "2011-11-08",
  "open":          53.20117,
  "high":          53.97776,
  "low":           53.12576,
  "close":         53.7436,
  "volume":        108099536,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-09T00:  00:  00-05:  00",
  "tradingDay":    "2011-11-09",
  "open":          52.51851,
  "high":          53.03712,
  "low":           52.15601,
  "close":         52.29493,
  "volume":        150817344,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-10T00:  00:  00-05:  00",
  "tradingDay":    "2011-11-10",
  "open":          52.54232,
  "high":          52.55026,
  "low":           50.55785,
  "close":         50.964,
  "volume":        201108944,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-11T00:  00:  00-05:  00",
  "tradingDay":    "2011-11-11",
  "open":          51.14658,
  "high":          51.4244,
  "low":           50.3078,
  "close":         50.88462,
  "volume":        176490368,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-14T00:  00:  00-05:  00",
  "tradingDay":    "2011-11-14",
  "open":          50.7391,
  "high":          50.96798,
  "low":           50.03527,
  "close":         50.17551,
  "volume":        116906144,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-15T00:  00:  00-05:  00",
  "tradingDay":    "2011-11-15",
  "open":          50.37925,
  "high":          51.53024,
  "low":           50.20065,
  "close":         51.4416,
  "volume":        116343776,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-16T00:  00:  00-05:  00",
  "tradingDay":    "2011-11-16",
  "open":          51.49717,
  "high":          51.74722,
  "low":           50.84494,
  "close":         50.90447,
  "volume":        94269440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-17T00:  00:  00-05:  00",
  "tradingDay":    "2011-11-17",
  "open":          50.79996,
  "high":          50.87933,
  "low":           49.67806,
  "close":         49.93076,
  "volume":        129609240,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-18T00:  00:  00-05:  00",
  "tradingDay":    "2011-11-18",
  "open":          50.13053,
  "high":          50.27208,
  "low":           49.59604,
  "close":         49.60398,
  "volume":        100441848,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-21T00:  00:  00-05:  00",
  "tradingDay":    "2011-11-21",
  "open":          48.98085,
  "high":          49.17268,
  "low":           48.40932,
  "close":         48.81945,
  "volume":        120993112,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-22T00:  00:  00-05:  00",
  "tradingDay":    "2011-11-22",
  "open":          49.08537,
  "high":          49.99955,
  "low":           49.07478,
  "close":         49.81169,
  "volume":        110490344,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-23T00:  00:  00-05:  00",
  "tradingDay":    "2011-11-23",
  "open":          49.54709,
  "high":          49.72305,
  "low":           48.53765,
  "close":         48.5522,
  "volume":        115712624,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-25T00:  00:  00-05:  00",
  "tradingDay":    "2011-11-25",
  "open":          48.74933,
  "high":          49.10257,
  "low":           48.06667,
  "close":         48.09974,
  "volume":        68772544,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-28T00:  00:  00-05:  00",
  "tradingDay":    "2011-11-28",
  "open":          49.25206,
  "high":          49.83947,
  "low":           48.99408,
  "close":         49.76009,
  "volume":        93577064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-29T00:  00:  00-05:  00",
  "tradingDay":    "2011-11-29",
  "open":          49.67806,
  "high":          50.11862,
  "low":           48.97688,
  "close":         49.37378,
  "volume":        101509888,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-11-30T00:  00:  00-05:  00",
  "tradingDay":    "2011-11-30",
  "open":          50.44275,
  "high":          50.57505,
  "low":           50.0485,
  "close":         50.56446,
  "volume":        109618072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-01T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-01",
  "open":          50.62532,
  "high":          51.46409,
  "low":           50.37263,
  "close":         51.32253,
  "volume":        104519752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-02T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-02",
  "open":          51.57919,
  "high":          52.07664,
  "low":           51.40853,
  "close":         51.55671,
  "volume":        102385184,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-05T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-05",
  "open":          52.05811,
  "high":          52.44442,
  "low":           51.64799,
  "close":         51.99461,
  "volume":        96472040,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-06T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-06",
  "open":          51.92846,
  "high":          52.20893,
  "low":           51.51437,
  "close":         51.72208,
  "volume":        76613152,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-07T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-07",
  "open":          51.58713,
  "high":          51.72075,
  "low":           51.16775,
  "close":         51.476,
  "volume":        82335064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-08T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-08",
  "open":          51.78558,
  "high":          52.32403,
  "low":           51.62682,
  "close":         51.68371,
  "volume":        101597568,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-09T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-09",
  "open":          51.97344,
  "high":          52.13088,
  "low":           51.73266,
  "close":         52.07531,
  "volume":        80214096,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-12T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-12",
  "open":          51.85041,
  "high":          52.11235,
  "low":           51.52363,
  "close":         51.83982,
  "volume":        81273064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-13T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-13",
  "open":          51.99329,
  "high":          52.3108,
  "low":           51.21273,
  "close":         51.43896,
  "volume":        91550592,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-14T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-14",
  "open":          51.18362,
  "high":          51.24977,
  "low":           49.96647,
  "close":         50.29855,
  "volume":        109912104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-15T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-15",
  "open":          50.71396,
  "high":          50.7682,
  "low":           50.04982,
  "close":         50.13317,
  "volume":        69228328,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-16T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-16",
  "open":          50.32103,
  "high":          50.82244,
  "low":           50.21652,
  "close":         50.40835,
  "volume":        113805576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-19T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-19",
  "open":          50.60019,
  "high":          50.91505,
  "low":           50.33691,
  "close":         50.56578,
  "volume":        63581248,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-20T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-20",
  "open":          51.30005,
  "high":          52.40341,
  "low":           51.23389,
  "close":         52.38357,
  "volume":        91082704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-21T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-21",
  "open":          52.48147,
  "high":          52.56217,
  "low":           51.86231,
  "close":         52.44972,
  "volume":        70982696,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-22T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-22",
  "open":          52.52248,
  "high":          52.80428,
  "low":           52.40341,
  "close":         52.72754,
  "volume":        54626496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-23T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-23",
  "open":          52.87969,
  "high":          53.39433,
  "low":           52.8519,
  "close":         53.35993,
  "volume":        72780904,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-27T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-27",
  "open":          53.3295,
  "high":          54.12197,
  "low":           53.31892,
  "close":         53.78328,
  "volume":        71600240,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-28T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-28",
  "open":          53.83091,
  "high":          54.01084,
  "low":           53.09666,
  "close":         53.26865,
  "volume":        61780020,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-29T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-29",
  "open":          53.36919,
  "high":          53.66686,
  "low":           52.98685,
  "close":         53.59674,
  "volume":        58351408,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2011-12-30T00:  00:  00-05:  00",
  "tradingDay":    "2011-12-30",
  "open":          53.38374,
  "high":          53.75021,
  "low":           53.3811,
  "close":         53.58087,
  "volume":        48530428,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-01-03T00:  00:  00-05:  00",
  "tradingDay":    "2012-01-03",
  "open":          54.22383,
  "high":          54.5731,
  "low":           54.11006,
  "close":         54.40509,
  "volume":        81595064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-01-04T00:  00:  00-05:  00",
  "tradingDay":    "2012-01-04",
  "open":          54.25294,
  "high":          54.86152,
  "low":           54.14711,
  "close":         54.69747,
  "volume":        70253288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-01-05T00:  00:  00-05:  00",
  "tradingDay":    "2012-01-05",
  "open":          54.89724,
  "high":          55.37351,
  "low":           54.5956,
  "close":         55.30472,
  "volume":        73229136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-01-06T00:  00:  00-05:  00",
  "tradingDay":    "2012-01-06",
  "open":          55.53492,
  "high":          55.92916,
  "low":           55.46215,
  "close":         55.88286,
  "volume":        85948856,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-01-09T00:  00:  00-05:  00",
  "tradingDay":    "2012-01-09",
  "open":          56.29299,
  "high":          56.59066,
  "low":           55.74395,
  "close":         55.79422,
  "volume":        106367088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-01-10T00:  00:  00-05:  00",
  "tradingDay":    "2012-01-10",
  "open":          56.34723,
  "high":          56.35913,
  "low":           55.76379,
  "close":         55.99399,
  "volume":        69735520,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-01-11T00:  00:  00-05:  00",
  "tradingDay":    "2012-01-11",
  "open":          55.91462,
  "high":          55.94239,
  "low":           55.47406,
  "close":         55.9027,
  "volume":        58091388,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-01-12T00:  00:  00-05:  00",
  "tradingDay":    "2012-01-12",
  "open":          55.86699,
  "high":          55.94901,
  "low":           55.39997,
  "close":         55.74924,
  "volume":        57424712,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-01-13T00:  00:  00-05:  00",
  "tradingDay":    "2012-01-13",
  "open":          55.52566,
  "high":          55.62488,
  "low":           55.38807,
  "close":         55.54021,
  "volume":        61052120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-01-17T00:  00:  00-05:  00",
  "tradingDay":    "2012-01-17",
  "open":          56.121,
  "high":          56.35781,
  "low":           55.95695,
  "close":         56.18715,
  "volume":        65569936,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-01-18T00:  00:  00-05:  00",
  "tradingDay":    "2012-01-18",
  "open":          56.48482,
  "high":          56.81821,
  "low":           56.39882,
  "close":         56.77058,
  "volume":        74719704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-01-19T00:  00:  00-05:  00",
  "tradingDay":    "2012-01-19",
  "open":          56.91214,
  "high":          57.06958,
  "low":           56.42661,
  "close":         56.59066,
  "volume":        70656160,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-01-20T00:  00:  00-05:  00",
  "tradingDay":    "2012-01-20",
  "open":          56.55626,
  "high":          56.55758,
  "low":           55.53227,
  "close":         55.60503,
  "volume":        111751888,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-01-23T00:  00:  00-05:  00",
  "tradingDay":    "2012-01-23",
  "open":          55.91329,
  "high":          56.68327,
  "low":           55.86963,
  "close":         56.54568,
  "volume":        82621536,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-01-24T00:  00:  00-05:  00",
  "tradingDay":    "2012-01-24",
  "open":          56.24007,
  "high":          56.24007,
  "low":           55.50581,
  "close":         55.61959,
  "volume":        147835456,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-01-25T00:  00:  00-05:  00",
  "tradingDay":    "2012-01-25",
  "open":          60.1217,
  "high":          60.12303,
  "low":           58.70479,
  "close":         59.09242,
  "volume":        258684832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-01-26T00:  00:  00-05:  00",
  "tradingDay":    "2012-01-26",
  "open":          59.32791,
  "high":          59.37422,
  "low":           58.62673,
  "close":         58.82386,
  "volume":        87526352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-01-27T00:  00:  00-05:  00",
  "tradingDay":    "2012-01-27",
  "open":          58.77358,
  "high":          59.33321,
  "low":           58.71008,
  "close":         59.17445,
  "volume":        80954848,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-01-30T00:  00:  00-05:  00",
  "tradingDay":    "2012-01-30",
  "open":          58.96674,
  "high":          60.05026,
  "low":           58.9244,
  "close":         59.93252,
  "volume":        102400304,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-01-31T00:  00:  00-05:  00",
  "tradingDay":    "2012-01-31",
  "open":          60.29501,
  "high":          60.62444,
  "low":           59.94046,
  "close":         60.39159,
  "volume":        105797168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-02-01T00:  00:  00-05:  00",
  "tradingDay":    "2012-02-01",
  "open":          60.64693,
  "high":          60.72366,
  "low":           60.26855,
  "close":         60.35323,
  "volume":        72898064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-02-02T00:  00:  00-05:  00",
  "tradingDay":    "2012-02-02",
  "open":          60.31486,
  "high":          60.48288,
  "low":           60.06085,
  "close":         60.21167,
  "volume":        50425388,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-02-03T00:  00:  00-05:  00",
  "tradingDay":    "2012-02-03",
  "open":          60.50008,
  "high":          60.85728,
  "low":           60.26988,
  "close":         60.81495,
  "volume":        77440064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-02-06T00:  00:  00-05:  00",
  "tradingDay":    "2012-02-06",
  "open":          60.64296,
  "high":          61.51613,
  "low":           60.61915,
  "close":         61.38251,
  "volume":        67402152,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-02-07T00:  00:  00-05:  00",
  "tradingDay":    "2012-02-07",
  "open":          61.55317,
  "high":          62.14719,
  "low":           61.46321,
  "close":         62.02547,
  "volume":        85364576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-02-08T00:  00:  00-05:  00",
  "tradingDay":    "2012-02-08",
  "open":          62.24509,
  "high":          63.07858,
  "low":           62.14058,
  "close":         63.06402,
  "volume":        110107120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-02-09T00:  00:  00-05:  00",
  "tradingDay":    "2012-02-09",
  "open":          63.60115,
  "high":          65.71925,
  "low":           63.57734,
  "close":         65.24562,
  "volume":        238695184,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-02-10T00:  00:  00-05:  00",
  "tradingDay":    "2012-02-10",
  "open":          64.96118,
  "high":          65.83435,
  "low":           64.6344,
  "close":         65.27869,
  "volume":        170420752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-02-13T00:  00:  00-05:  00",
  "tradingDay":    "2012-02-13",
  "open":          66.11482,
  "high":          66.65592,
  "low":           65.76423,
  "close":         66.49319,
  "volume":        139620688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-02-14T00:  00:  00-05:  00",
  "tradingDay":    "2012-02-14",
  "open":          66.74456,
  "high":          67.41399,
  "low":           66.41382,
  "close":         67.40076,
  "volume":        124699128,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-02-15T00:  00:  00-05:  00",
  "tradingDay":    "2012-02-15",
  "open":          68.0358,
  "high":          69.62734,
  "low":           65.73777,
  "close":         65.84097,
  "volume":        406578496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-02-16T00:  00:  00-05:  00",
  "tradingDay":    "2012-02-16",
  "open":          65.02336,
  "high":          66.79616,
  "low":           64.38039,
  "close":         66.4416,
  "volume":        254983360,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-02-17T00:  00:  00-05:  00",
  "tradingDay":    "2012-02-17",
  "open":          66.56067,
  "high":          67.17718,
  "low":           66.18891,
  "close":         66.4297,
  "volume":        144637376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-02-21T00:  00:  00-05:  00",
  "tradingDay":    "2012-02-21",
  "open":          67.08457,
  "high":          68.11385,
  "low":           66.69429,
  "close":         68.11385,
  "volume":        163478880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-02-22T00:  00:  00-05:  00",
  "tradingDay":    "2012-02-22",
  "open":          67.87968,
  "high":          68.19852,
  "low":           67.34917,
  "close":         67.87439,
  "volume":        130466392,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-02-23T00:  00:  00-05:  00",
  "tradingDay":    "2012-02-23",
  "open":          68.14428,
  "high":          68.5081,
  "low":           67.40605,
  "close":         68.3176,
  "volume":        153339680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-02-24T00:  00:  00-05:  00",
  "tradingDay":    "2012-02-24",
  "open":          68.72904,
  "high":          69.17886,
  "low":           68.61526,
  "close":         69.11402,
  "volume":        112105632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-02-27T00:  00:  00-05:  00",
  "tradingDay":    "2012-02-27",
  "open":          68.94071,
  "high":          69.91972,
  "low":           68.30304,
  "close":         69.55723,
  "volume":        147820336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-02-28T00:  00:  00-05:  00",
  "tradingDay":    "2012-02-28",
  "open":          69.84829,
  "high":          70.8339,
  "low":           69.56913,
  "close":         70.8339,
  "volume":        162075232,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-02-29T00:  00:  00-05:  00",
  "tradingDay":    "2012-02-29",
  "open":          71.61182,
  "high":          72.44794,
  "low":           70.87228,
  "close":         71.76397,
  "volume":        256994720,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-01T00:  00:  00-05:  00",
  "tradingDay":    "2012-03-01",
  "open":          72.49954,
  "high":          72.52733,
  "low":           71.27843,
  "close":         72.03252,
  "volume":        184445104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-02T00:  00:  00-05:  00",
  "tradingDay":    "2012-03-02",
  "open":          72.03121,
  "high":          72.34078,
  "low":           71.77455,
  "close":         72.12646,
  "volume":        116541056,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-05T00:  00:  00-05:  00",
  "tradingDay":    "2012-03-05",
  "open":          72.16219,
  "high":          72.43075,
  "low":           69.58898,
  "close":         70.53623,
  "volume":        218423584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-06T00:  00:  00-05:  00",
  "tradingDay":    "2012-03-06",
  "open":          69.31777,
  "high":          70.60635,
  "low":           68.2951,
  "close":         70.15257,
  "volume":        218725184,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-07T00:  00:  00-05:  00",
  "tradingDay":    "2012-03-07",
  "open":          70.97944,
  "high":          71.14746,
  "low":           69.23177,
  "close":         70.20946,
  "volume":        215431872,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-08T00:  00:  00-05:  00",
  "tradingDay":    "2012-03-08",
  "open":          70.77305,
  "high":          71.83673,
  "low":           70.39864,
  "close":         71.70443,
  "volume":        139414336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-09T00:  00:  00-05:  00",
  "tradingDay":    "2012-03-09",
  "open":          72.02856,
  "high":          72.46515,
  "low":           71.8526,
  "close":         72.12514,
  "volume":        113082208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-12T00:  00:  00-04:  00",
  "tradingDay":    "2012-03-12",
  "open":          72.63317,
  "high":          73.02874,
  "low":           72.36725,
  "close":         73.02874,
  "volume":        109946120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-13T00:  00:  00-04:  00",
  "tradingDay":    "2012-03-13",
  "open":          73.76167,
  "high":          75.16933,
  "low":           73.52486,
  "close":         75.15874,
  "volume":        186496528,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-14T00:  00:  00-04:  00",
  "tradingDay":    "2012-03-14",
  "open":          76.47247,
  "high":          78.68053,
  "low":           76.12453,
  "close":         78.00052,
  "volume":        383018880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-15T00:  00:  00-04:  00",
  "tradingDay":    "2012-03-15",
  "open":          79.33144,
  "high":          79.38039,
  "low":           76.54126,
  "close":         77.46867,
  "volume":        313055840,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-16T00:  00:  00-04:  00",
  "tradingDay":    "2012-03-16",
  "open":          77.35225,
  "high":          77.95024,
  "low":           76.4685,
  "close":         77.47,
  "volume":        222841632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-19T00:  00:  00-04:  00",
  "tradingDay":    "2012-03-19",
  "open":          79.16077,
  "high":          79.61324,
  "low":           77.9304,
  "close":         79.52459,
  "volume":        243290096,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-20T00:  00:  00-04:  00",
  "tradingDay":    "2012-03-20",
  "open":          79.30498,
  "high":          80.29192,
  "low":           76.99769,
  "close":         80.16756,
  "volume":        220461408,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-21T00:  00:  00-04:  00",
  "tradingDay":    "2012-03-21",
  "open":          79.74156,
  "high":          80.65575,
  "low":           79.5656,
  "close":         79.70981,
  "volume":        173859952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-22T00:  00:  00-04:  00",
  "tradingDay":    "2012-03-22",
  "open":          79.08537,
  "high":          79.9744,
  "low":           78.7877,
  "close":         79.29175,
  "volume":        168485744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-23T00:  00:  00-04:  00",
  "tradingDay":    "2012-03-23",
  "open":          79.44257,
  "high":          79.6172,
  "low":           78.6382,
  "close":         78.85648,
  "volume":        116210744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-26T00:  00:  00-04:  00",
  "tradingDay":    "2012-03-26",
  "open":          79.35128,
  "high":          80.325,
  "low":           78.75197,
  "close":         80.30251,
  "volume":        160820496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-27T00:  00:  00-04:  00",
  "tradingDay":    "2012-03-27",
  "open":          80.19667,
  "high":          81.53288,
  "low":           80.18079,
  "close":         81.29475,
  "volume":        163895360,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-28T00:  00:  00-04:  00",
  "tradingDay":    "2012-03-28",
  "open":          81.81071,
  "high":          82.21687,
  "low":           80.74306,
  "close":         81.71016,
  "volume":        176942368,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-29T00:  00:  00-04:  00",
  "tradingDay":    "2012-03-29",
  "open":          81.06984,
  "high":          81.56992,
  "low":           80.33558,
  "close":         80.68353,
  "volume":        164194688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-03-30T00:  00:  00-04:  00",
  "tradingDay":    "2012-03-30",
  "open":          80.53932,
  "high":          80.77614,
  "low":           79.10653,
  "close":         79.31953,
  "volume":        197344736,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-04-02T00:  00:  00-04:  00",
  "tradingDay":    "2012-04-02",
  "open":          79.61588,
  "high":          81.8623,
  "low":           79.42934,
  "close":         81.84379,
  "volume":        161524208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-04-03T00:  00:  00-04:  00",
  "tradingDay":    "2012-04-03",
  "open":          82.99081,
  "high":          83.6404,
  "low":           82.3571,
  "close":         83.25806,
  "volume":        225289120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-04-04T00:  00:  00-04:  00",
  "tradingDay":    "2012-04-04",
  "open":          82.60052,
  "high":          82.8003,
  "low":           81.62814,
  "close":         82.59524,
  "volume":        154676800,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-04-05T00:  00:  00-04:  00",
  "tradingDay":    "2012-04-05",
  "open":          82.94847,
  "high":          83.96452,
  "low":           82.47485,
  "close":         83.83488,
  "volume":        173113904,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-04-09T00:  00:  00-04:  00",
  "tradingDay":    "2012-04-09",
  "open":          82.83602,
  "high":          84.64983,
  "low":           82.72621,
  "close":         84.17223,
  "volume":        161305760,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-04-10T00:  00:  00-04:  00",
  "tradingDay":    "2012-04-10",
  "open":          84.66174,
  "high":          85.2002,
  "low":           82.81882,
  "close":         83.14163,
  "volume":        240173664,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-04-11T00:  00:  00-04:  00",
  "tradingDay":    "2012-04-11",
  "open":          84.16827,
  "high":          84.2569,
  "low":           82.46691,
  "close":         82.84528,
  "volume":        188051344,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-04-12T00:  00:  00-04:  00",
  "tradingDay":    "2012-04-12",
  "open":          82.68652,
  "high":          83.52398,
  "low":           82.09118,
  "close":         82.3915,
  "volume":        165839440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-04-13T00:  00:  00-04:  00",
  "tradingDay":    "2012-04-13",
  "open":          82.56878,
  "high":          82.64684,
  "low":           79.84343,
  "close":         80.07098,
  "volume":        232062448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-04-16T00:  00:  00-04:  00",
  "tradingDay":    "2012-04-16",
  "open":          80.70998,
  "high":          80.7391,
  "low":           76.50157,
  "close":         76.7503,
  "volume":        283661696,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-04-17T00:  00:  00-04:  00",
  "tradingDay":    "2012-04-17",
  "open":          76.59286,
  "high":          80.70205,
  "low":           75.6628,
  "close":         80.66236,
  "volume":        276831680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-04-18T00:  00:  00-04:  00",
  "tradingDay":    "2012-04-18",
  "open":          81.19419,
  "high":          82.05811,
  "low":           79.73759,
  "close":         80.48244,
  "volume":        257677264,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-04-19T00:  00:  00-04:  00",
  "tradingDay":    "2012-04-19",
  "open":          79.40816,
  "high":          80.00483,
  "low":           77.33109,
  "close":         77.71739,
  "volume":        225332208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-04-20T00:  00:  00-04:  00",
  "tradingDay":    "2012-04-20",
  "open":          78.23866,
  "high":          78.6673,
  "low":           75.46568,
  "close":         75.80436,
  "volume":        278313952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-04-23T00:  00:  00-04:  00",
  "tradingDay":    "2012-04-23",
  "open":          75.49081,
  "high":          76.29254,
  "low":           73.63995,
  "close":         75.63502,
  "volume":        260913136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-04-24T00:  00:  00-04:  00",
  "tradingDay":    "2012-04-24",
  "open":          74.43243,
  "high":          75.1045,
  "low":           73.42564,
  "close":         74.12418,
  "volume":        290576352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-04-25T00:  00:  00-04:  00",
  "tradingDay":    "2012-04-25",
  "open":          81.44821,
  "high":          81.76044,
  "low":           80.17285,
  "close":         80.70205,
  "volume":        244498720,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-04-26T00:  00:  00-04:  00",
  "tradingDay":    "2012-04-26",
  "open":          81.26697,
  "high":          81.32253,
  "low":           79.66086,
  "close":         80.39777,
  "volume":        144711456,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-04-27T00:  00:  00-04:  00",
  "tradingDay":    "2012-04-27",
  "open":          80.04982,
  "high":          80.19667,
  "low":           79.44521,
  "close":         79.77596,
  "volume":        109808552,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-04-30T00:  00:  00-04:  00",
  "tradingDay":    "2012-04-30",
  "open":          79.08801,
  "high":          79.16739,
  "low":           77.12999,
  "close":         77.25964,
  "volume":        136633504,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-01T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-01",
  "open":          77.38136,
  "high":          78.95042,
  "low":           76.89582,
  "close":         77.01489,
  "volume":        164939968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-02T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-02",
  "open":          76.76485,
  "high":          77.7121,
  "low":           76.58228,
  "close":         77.52424,
  "volume":        115437488,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-03T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-03",
  "open":          78.12223,
  "high":          78.2413,
  "low":           76.77278,
  "close":         76.97388,
  "volume":        105429056,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-04T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-04",
  "open":          76.34679,
  "high":          76.51612,
  "low":           74.77111,
  "close":         74.78169,
  "volume":        143527776,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-07T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-07",
  "open":          74.28558,
  "high":          75.77658,
  "low":           74.24985,
  "close":         75.34132,
  "volume":        125109560,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-08T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-08",
  "open":          75.35455,
  "high":          75.60856,
  "low":           73.91911,
  "close":         75.16933,
  "volume":        135610064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-09T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-09",
  "open":          74.57663,
  "high":          75.93665,
  "low":           74.19958,
  "close":         75.30163,
  "volume":        129766456,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-10T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-10",
  "open":          76.01604,
  "high":          76.18803,
  "low":           75.20373,
  "close":         75.4789,
  "volume":        92063824,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-11T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-11",
  "open":          74.74862,
  "high":          76.00148,
  "low":           74.66262,
  "close":         74.97485,
  "volume":        107857656,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-14T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-14",
  "open":          74.42713,
  "high":          75.08069,
  "low":           73.76961,
  "close":         73.85163,
  "volume":        96536288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-15T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-15",
  "open":          74.27896,
  "high":          74.51312,
  "low":           72.99567,
  "close":         73.18353,
  "volume":        128579744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-16T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-16",
  "open":          73.29995,
  "high":          73.67568,
  "low":           71.57874,
  "close":         72.24554,
  "volume":        151414480,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-17T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-17",
  "open":          72.14366,
  "high":          72.4334,
  "low":           70.13405,
  "close":         70.13405,
  "volume":        193614528,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-18T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-18",
  "open":          70.64207,
  "high":          71.89229,
  "low":           69.0836,
  "close":         70.16845,
  "volume":        197682608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-21T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-21",
  "open":          70.71352,
  "high":          74.29086,
  "low":           70.65398,
  "close":         74.25647,
  "volume":        170364064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-22T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-22",
  "open":          75.35058,
  "high":          75.92343,
  "low":           73.10548,
  "close":         73.68626,
  "volume":        187559280,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-23T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-23",
  "open":          73.75638,
  "high":          75.78055,
  "low":           73.19146,
  "close":         75.4842,
  "volume":        157893776,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-24T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-24",
  "open":          76.1867,
  "high":          76.27005,
  "low":           74.24985,
  "close":         74.79095,
  "volume":        133949424,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-25T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-25",
  "open":          74.69438,
  "high":          74.86107,
  "low":           73.8847,
  "close":         74.39008,
  "volume":        88669976,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-29T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-29",
  "open":          75.52918,
  "high":          75.93931,
  "low":           74.78963,
  "close":         75.71043,
  "volume":        102717768,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-30T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-30",
  "open":          75.30428,
  "high":          76.73177,
  "low":           74.955,
  "close":         76.62328,
  "volume":        142917792,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-05-31T00:  00:  00-04:  00",
  "tradingDay":    "2012-05-31",
  "open":          76.83099,
  "high":          76.93154,
  "low":           75.60327,
  "close":         76.43278,
  "volume":        132792192,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-01T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-01",
  "open":          75.29897,
  "high":          75.7607,
  "low":           74.15592,
  "close":         74.2181,
  "volume":        140639600,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-04T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-04",
  "open":          74.28558,
  "high":          75.07936,
  "low":           72.5657,
  "close":         74.65469,
  "volume":        150360048,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-05T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-05",
  "open":          74.25515,
  "high":          74.94309,
  "low":           73.86619,
  "close":         74.46153,
  "volume":        104798664,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-06T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-06",
  "open":          75.11509,
  "high":          75.91946,
  "low":           74.81477,
  "close":         75.60327,
  "volume":        108278680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-07T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-07",
  "open":          76.37457,
  "high":          76.37853,
  "low":           75.47626,
  "close":         75.63766,
  "volume":        102535600,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-08T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-08",
  "open":          75.62179,
  "high":          76.80983,
  "low":           75.27781,
  "close":         76.77543,
  "volume":        93691200,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-11T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-11",
  "open":          77.75443,
  "high":          77.85764,
  "low":           75.49346,
  "close":         75.5649,
  "volume":        159612608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-12T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-12",
  "open":          76.00017,
  "high":          76.28593,
  "low":           74.97353,
  "close":         76.22507,
  "volume":        117585664,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-13T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-13",
  "open":          76.0081,
  "high":          76.532,
  "low":           75.46039,
  "close":         75.69587,
  "volume":        79304032,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-14T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-14",
  "open":          75.57416,
  "high":          75.87315,
  "low":           75.04762,
  "close":         75.61253,
  "volume":        93360136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-15T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-15",
  "open":          75.54241,
  "high":          76.02133,
  "low":           75.35058,
  "close":         75.9565,
  "volume":        90502200,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-18T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-18",
  "open":          75.53712,
  "high":          77.77693,
  "low":           75.45906,
  "close":         77.49779,
  "volume":        118889536,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-19T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-19",
  "open":          77.18291,
  "high":          78.05608,
  "low":           77.14322,
  "close":         77.71342,
  "volume":        97561240,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-20T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-20",
  "open":          77.81927,
  "high":          77.95686,
  "low":           76.83894,
  "close":         77.49249,
  "volume":        96955040,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-21T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-21",
  "open":          77.4528,
  "high":          77.82059,
  "low":           76.39441,
  "close":         76.42484,
  "volume":        88140120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-22T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-22",
  "open":          76.60609,
  "high":          77.02283,
  "low":           76.12717,
  "close":         77.01092,
  "volume":        76867120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-25T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-25",
  "open":          76.37589,
  "high":          76.70663,
  "low":           75.45906,
  "close":         75.51199,
  "volume":        82161208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-26T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-26",
  "open":          75.58607,
  "high":          76.00413,
  "low":           75.05688,
  "close":         75.67868,
  "volume":        74709120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-27T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-27",
  "open":          76.0716,
  "high":          76.3018,
  "low":           75.66412,
  "close":         76.00546,
  "volume":        54816216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-28T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-28",
  "open":          75.63105,
  "high":          75.93931,
  "low":           74.82932,
  "close":         75.28442,
  "volume":        76407552,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-06-29T00:  00:  00-04:  00",
  "tradingDay":    "2012-06-29",
  "open":          76.4685,
  "high":          77.26229,
  "low":           75.97238,
  "close":         77.26229,
  "volume":        113768536,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-02T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-02",
  "open":          77.35886,
  "high":          78.51515,
  "low":           77.20937,
  "close":         78.38947,
  "volume":        108005056,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-03T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-03",
  "open":          78.7017,
  "high":          79.37907,
  "low":           78.58527,
  "close":         79.301,
  "volume":        65250204,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-05T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-05",
  "open":          79.45315,
  "high":          81.27623,
  "low":           79.33276,
  "close":         80.69411,
  "volume":        130759664,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-06T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-06",
  "open":          80.31706,
  "high":          80.49567,
  "low":           79.5881,
  "close":         80.15698,
  "volume":        113117736,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-09T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-09",
  "open":          80.08025,
  "high":          81.21802,
  "low":           79.92281,
  "close":         81.21669,
  "volume":        102419952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-10T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-10",
  "open":          81.75646,
  "high":          82.00784,
  "low":           80.08157,
  "close":         80.46524,
  "volume":        138204192,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-11T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-11",
  "open":          80.18873,
  "high":          80.39246,
  "low":           79.01127,
  "close":         79.96514,
  "volume":        126693104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-12T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-12",
  "open":          79.41081,
  "high":          79.83813,
  "low":           78.41064,
  "close":         79.23354,
  "volume":        115536512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-13T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-13",
  "open":          79.76935,
  "high":          80.33029,
  "low":           79.37907,
  "close":         80.03658,
  "volume":        84121928,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-16T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-16",
  "open":          80.05643,
  "high":          80.91637,
  "low":           80.04321,
  "close":         80.29324,
  "volume":        81338072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-17T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-17",
  "open":          80.80656,
  "high":          80.9005,
  "low":           79.79581,
  "close":         80.29722,
  "volume":        79314616,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-18T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-18",
  "open":          80.25092,
  "high":          80.48244,
  "low":           79.85004,
  "close":         80.20725,
  "volume":        68216224,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-19T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-19",
  "open":          80.87139,
  "high":          81.40984,
  "low":           80.17285,
  "close":         81.27357,
  "volume":        117930344,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-20T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-20",
  "open":          81.10291,
  "high":          81.28945,
  "low":           79.86857,
  "close":         79.94794,
  "volume":        107297560,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-23T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-23",
  "open":          78.6382,
  "high":          80.15963,
  "low":           77.75312,
  "close":         79.88577,
  "volume":        131729448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-24T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-24",
  "open":          80.35543,
  "high":          80.65971,
  "low":           79.18194,
  "close":         79.50078,
  "volume":        152558112,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-25T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-25",
  "open":          76.00017,
  "high":          76.83894,
  "low":           75.41011,
  "close":         76.06763,
  "volume":        236817600,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-26T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-26",
  "open":          76.70135,
  "high":          76.78602,
  "low":           75.45773,
  "close":         76.05573,
  "volume":        109819136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-27T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-27",
  "open":          76.07293,
  "high":          77.50439,
  "low":           75.62047,
  "close":         77.41575,
  "volume":        109074608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-30T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-30",
  "open":          78.1778,
  "high":          79.30498,
  "low":           77.76767,
  "close":         78.72154,
  "volume":        102349656,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-07-31T00:  00:  00-04:  00",
  "tradingDay":    "2012-07-31",
  "open":          79.80639,
  "high":          80.92696,
  "low":           79.73891,
  "close":         80.8026,
  "volume":        124805704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-01T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-01",
  "open":          81.48392,
  "high":          81.54876,
  "low":           79.77596,
  "close":         80.28001,
  "volume":        103796384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-02T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-02",
  "open":          79.75479,
  "high":          80.79333,
  "low":           79.41214,
  "close":         80.40967,
  "volume":        89691160,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-03T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-03",
  "open":          81.18229,
  "high":          81.75779,
  "low":           80.90843,
  "close":         81.45615,
  "volume":        93109944,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-06T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-06",
  "open":          81.6665,
  "high":          82.66933,
  "low":           81.39794,
  "close":         82.36239,
  "volume":        81611696,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-07T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-07",
  "open":          82.3915,
  "high":          82.68652,
  "low":           81.76572,
  "close":         82.14542,
  "volume":        78423448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-08T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-08",
  "open":          81.94434,
  "high":          82.53835,
  "low":           81.64137,
  "close":         82.00651,
  "volume":        66058224,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-09T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-09",
  "open":          82.09154,
  "high":          82.60706,
  "low":           82.08489,
  "close":         82.47419,
  "volume":        59621520,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-10T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-10",
  "open":          82.20581,
  "high":          82.61105,
  "low":           82.20448,
  "close":         82.60308,
  "volume":        52431592,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-13T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-13",
  "open":          82.82762,
  "high":          83.70587,
  "low":           82.80902,
  "close":         83.70587,
  "volume":        74948936,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-14T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-14",
  "open":          83.95433,
  "high":          84.84985,
  "low":           83.73377,
  "close":         83.93041,
  "volume":        91436168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-15T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-15",
  "open":          83.87859,
  "high":          84.23734,
  "low":           83.40692,
  "close":         83.81615,
  "volume":        69217624,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-16T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-16",
  "open":          83.86664,
  "high":          84.60405,
  "low":           83.7723,
  "close":         84.54825,
  "volume":        68483048,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-17T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-17",
  "open":          85.03453,
  "high":          86.12271,
  "low":           84.87642,
  "close":         86.11208,
  "volume":        119012728,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-20T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-20",
  "open":          86.36452,
  "high":          88.37613,
  "low":           86.34991,
  "close":         88.37613,
  "volume":        164876080,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-21T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-21",
  "open":          89.12948,
  "high":          89.66891,
  "low":           86.40704,
  "close":         87.16837,
  "volume":        218453184,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-22T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-22",
  "open":          86.95046,
  "high":          88.88766,
  "low":           86.11208,
  "close":         88.87038,
  "volume":        151957088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-23T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-23",
  "open":          88.50368,
  "high":          89.00724,
  "low":           87.84466,
  "close":         88.0413,
  "volume":        112926920,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-24T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-24",
  "open":          87.62675,
  "high":          88.95143,
  "low":           87.1006,
  "close":         88.11969,
  "volume":        117554880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-27T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-27",
  "open":          90.34786,
  "high":          90.46478,
  "low":           89.49087,
  "close":         89.77521,
  "volume":        114753568,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-28T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-28",
  "open":          89.6822,
  "high":          89.83101,
  "low":           89.10954,
  "close":         89.65829,
  "volume":        71869904,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-29T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-29",
  "open":          89.71808,
  "high":          90.03961,
  "low":           89.36597,
  "close":         89.48157,
  "volume":        54513384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-30T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-30",
  "open":          89.10556,
  "high":          89.22647,
  "low":           88.07053,
  "close":         88.20605,
  "volume":        81362144,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-08-31T00:  00:  00-04:  00",
  "tradingDay":    "2012-08-31",
  "open":          88.65514,
  "high":          88.83451,
  "low":           87.32648,
  "close":         88.38808,
  "volume":        90939432,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-09-04T00:  00:  00-04:  00",
  "tradingDay":    "2012-09-04",
  "open":          88.45718,
  "high":          89.70346,
  "low":           88.28976,
  "close":         89.68087,
  "volume":        98888016,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-09-05T00:  00:  00-04:  00",
  "tradingDay":    "2012-09-05",
  "open":          89.76059,
  "high":          89.86423,
  "low":           88.96738,
  "close":         89.05109,
  "volume":        90413336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-09-06T00:  00:  00-04:  00",
  "tradingDay":    "2012-09-06",
  "open":          89.44171,
  "high":          90.12199,
  "low":           89.12682,
  "close":         89.8536,
  "volume":        105202624,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-09-07T00:  00:  00-04:  00",
  "tradingDay":    "2012-09-07",
  "open":          90.0901,
  "high":          90.6787,
  "low":           89.78717,
  "close":         90.40765,
  "volume":        88613032,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-09-10T00:  00:  00-04:  00",
  "tradingDay":    "2012-09-10",
  "open":          90.40898,
  "high":          90.78632,
  "low":           87.97088,
  "close":         88.05592,
  "volume":        131172312,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-09-11T00:  00:  00-04:  00",
  "tradingDay":    "2012-09-11",
  "open":          88.3708,
  "high":          89.03381,
  "low":           87.22683,
  "close":         87.77026,
  "volume":        135457808,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-09-12T00:  00:  00-04:  00",
  "tradingDay":    "2012-09-12",
  "open":          88.602,
  "high":          89.00724,
  "low":           87.16039,
  "close":         88.99262,
  "volume":        191442592,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-09-13T00:  00:  00-04:  00",
  "tradingDay":    "2012-09-13",
  "open":          89.99975,
  "high":          91.07996,
  "low":           89.6543,
  "close":         90.74513,
  "volume":        160837440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-09-14T00:  00:  00-04:  00",
  "tradingDay":    "2012-09-14",
  "open":          91.67255,
  "high":          92.60526,
  "low":           91.39751,
  "close":         91.84793,
  "volume":        161405680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-09-17T00:  00:  00-04:  00",
  "tradingDay":    "2012-09-17",
  "open":          92.92015,
  "high":          92.97994,
  "low":           92.29037,
  "close":         92.97729,
  "volume":        106989384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-09-18T00:  00:  00-04:  00",
  "tradingDay":    "2012-09-18",
  "open":          92.99058,
  "high":          93.3161,
  "low":           92.53085,
  "close":         93.26029,
  "volume":        100396296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-09-19T00:  00:  00-04:  00",
  "tradingDay":    "2012-09-19",
  "open":          93.04107,
  "high":          93.53666,
  "low":           92.94939,
  "close":         93.28554,
  "volume":        87858896,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-09-20T00:  00:  00-04:  00",
  "tradingDay":    "2012-09-20",
  "open":          92.89491,
  "high":          93.01449,
  "low":           92.15883,
  "close":         92.83379,
  "volume":        90468280,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-09-21T00:  00:  00-04:  00",
  "tradingDay":    "2012-09-21",
  "open":          93.32672,
  "high":          93.68015,
  "low":           92.92149,
  "close":         93.01848,
  "volume":        153641488,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-09-24T00:  00:  00-04:  00",
  "tradingDay":    "2012-09-24",
  "open":          91.26065,
  "high":          92.35813,
  "low":           90.74779,
  "close":         91.78281,
  "volume":        171962144,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-09-25T00:  00:  00-04:  00",
  "tradingDay":    "2012-09-25",
  "open":          91.44667,
  "high":          92.04723,
  "low":           89.41913,
  "close":         89.49087,
  "volume":        139442272,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-09-26T00:  00:  00-04:  00",
  "tradingDay":    "2012-09-26",
  "open":          88.85311,
  "high":          89.37794,
  "low":           87.8513,
  "close":         88.38011,
  "volume":        155016544,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-09-27T00:  00:  00-04:  00",
  "tradingDay":    "2012-09-27",
  "open":          88.26186,
  "high":          90.63751,
  "low":           87.73837,
  "close":         90.52457,
  "volume":        159689664,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-09-28T00:  00:  00-04:  00",
  "tradingDay":    "2012-09-28",
  "open":          90.18311,
  "high":          90.49667,
  "low":           88.58871,
  "close":         88.63521,
  "volume":        143836160,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-01T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-01",
  "open":          89.17464,
  "high":          89.91737,
  "low":           87.22683,
  "close":         87.61082,
  "volume":        146114384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-02T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-02",
  "open":          87.93235,
  "high":          88.53556,
  "low":           86.44956,
  "close":         87.86591,
  "volume":        168802576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-03T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-03",
  "open":          88.33759,
  "high":          89.26765,
  "low":           88.0413,
  "close":         89.21318,
  "volume":        114045336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-04T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-04",
  "open":          89.18661,
  "high":          89.58521,
  "low":           88.42927,
  "close":         88.59535,
  "volume":        99657960,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-05T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-05",
  "open":          88.38277,
  "high":          88.48906,
  "low":           86.53327,
  "close":         86.70732,
  "volume":        159667088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-08T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-08",
  "open":          85.94865,
  "high":          86.039,
  "low":           84.51768,
  "close":         84.79138,
  "volume":        171487232,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-09T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-09",
  "open":          84.85516,
  "high":          85.09963,
  "low":           82.84888,
  "close":         84.48313,
  "volume":        225412800,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-10T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-10",
  "open":          84.99998,
  "high":          85.69621,
  "low":           84.63593,
  "close":         85.15543,
  "volume":        137182096,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-11T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-11",
  "open":          85.89816,
  "high":          85.99117,
  "low":           83.45341,
  "close":         83.45341,
  "volume":        146784976,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-12T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-12",
  "open":          83.64741,
  "high":          84.42069,
  "low":           83.08139,
  "close":         83.66734,
  "volume":        123650472,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-15T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-15",
  "open":          84.0181,
  "high":          84.38747,
  "low":           82.88873,
  "close":         84.33831,
  "volume":        116255072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-16T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-16",
  "open":          84.41936,
  "high":          86.40305,
  "low":           83.83874,
  "close":         86.33529,
  "volume":        147776944,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-17T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-17",
  "open":          86.21306,
  "high":          86.73389,
  "low":           85.566,
  "close":         85.64704,
  "volume":        104645672,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-18T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-18",
  "open":          84.98006,
  "high":          85.30824,
  "low":           83.70587,
  "close":         84.05664,
  "volume":        128115104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-19T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-19",
  "open":          83.84538,
  "high":          83.94105,
  "low":           80.99805,
  "close":         81.02728,
  "volume":        200008336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-22T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-22",
  "open":          81.37007,
  "high":          84.42069,
  "low":           81.14952,
  "close":         84.24133,
  "volume":        146959584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-23T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-23",
  "open":          83.83874,
  "high":          84.22405,
  "low":           81.27441,
  "close":         81.49496,
  "volume":        190078832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-24T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-24",
  "open":          82.56853,
  "high":          83.24747,
  "low":           81.13358,
  "close":         81.95602,
  "volume":        150130448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-25T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-25",
  "open":          82.3772,
  "high":          82.64294,
  "low":           80.45728,
  "close":         80.98742,
  "volume":        176419248,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-26T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-26",
  "open":          80.9728,
  "high":          81.58,
  "low":           78.52408,
  "close":         80.25134,
  "volume":        273750048,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-10-31T00:  00:  00-04:  00",
  "tradingDay":    "2012-10-31",
  "open":          79.0396,
  "high":          79.98029,
  "low":           78.08562,
  "close":         79.09806,
  "volume":        137087264,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-01T00:  00:  00-04:  00",
  "tradingDay":    "2012-11-01",
  "open":          79.48337,
  "high":          80.11848,
  "low":           78.94526,
  "close":         79.26015,
  "volume":        97156200,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-02T00:  00:  00-04:  00",
  "tradingDay":    "2012-11-02",
  "open":          79.1738,
  "high":          79.31464,
  "low":           76.365,
  "close":         76.63737,
  "volume":        161109888,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-05T00:  00:  00-05:  00",
  "tradingDay":    "2012-11-05",
  "open":          77.53024,
  "high":          78.09492,
  "low":           76.74366,
  "close":         77.67638,
  "volume":        142270672,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-06T00:  00:  00-05:  00",
  "tradingDay":    "2012-11-06",
  "open":          78.42177,
  "high":          78.48953,
  "low":           77.07451,
  "close":         77.44121,
  "volume":        100776376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-07T00:  00:  00-05:  00",
  "tradingDay":    "2012-11-07",
  "open":          76.59232,
  "high":          76.68575,
  "low":           74.17779,
  "close":         74.4781,
  "volume":        212360816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-08T00:  00:  00-05:  00",
  "tradingDay":    "2012-11-08",
  "open":          74.82914,
  "high":          75.04269,
  "low":           71.44692,
  "close":         71.77527,
  "volume":        282598816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-09T00:  00:  00-05:  00",
  "tradingDay":    "2012-11-09",
  "open":          72.13165,
  "high":          74.06167,
  "low":           71.23737,
  "close":         73.01791,
  "volume":        248820544,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-12T00:  00:  00-05:  00",
  "tradingDay":    "2012-11-12",
  "open":          73.96423,
  "high":          74.01095,
  "low":           71.8954,
  "close":         72.45332,
  "volume":        138015616,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-13T00:  00:  00-05:  00",
  "tradingDay":    "2012-11-13",
  "open":          71.9301,
  "high":          73.47438,
  "low":           71.58974,
  "close":         72.46266,
  "volume":        142674224,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-14T00:  00:  00-05:  00",
  "tradingDay":    "2012-11-14",
  "open":          72.80969,
  "high":          73.06996,
  "low":           71.56572,
  "close":         71.65915,
  "volume":        127678712,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-15T00:  00:  00-05:  00",
  "tradingDay":    "2012-11-15",
  "open":          71.74591,
  "high":          72.00885,
  "low":           69.75582,
  "close":         70.15624,
  "volume":        211360608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-16T00:  00:  00-05:  00",
  "tradingDay":    "2012-11-16",
  "open":          70.10018,
  "high":          70.74085,
  "low":           67.50413,
  "close":         70.43119,
  "volume":        338989888,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-19T00:  00:  00-05:  00",
  "tradingDay":    "2012-11-19",
  "open":          72.17036,
  "high":          75.7461,
  "low":           72.05957,
  "close":         75.50985,
  "volume":        220260528,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-20T00:  00:  00-05:  00",
  "tradingDay":    "2012-11-20",
  "open":          76.33472,
  "high":          76.34006,
  "low":           74.02163,
  "close":         74.86651,
  "volume":        171984944,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-21T00:  00:  00-05:  00",
  "tradingDay":    "2012-11-21",
  "open":          75.31232,
  "high":          75.72875,
  "low":           74.29124,
  "close":         74.97195,
  "volume":        99866408,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-23T00:  00:  00-05:  00",
  "tradingDay":    "2012-11-23",
  "open":          75.70206,
  "high":          76.34673,
  "low":           75.09208,
  "close":         76.27999,
  "volume":        73001112,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-26T00:  00:  00-05:  00",
  "tradingDay":    "2012-11-26",
  "open":          76.86728,
  "high":          78.74925,
  "low":           76.57497,
  "close":         78.68652,
  "volume":        168727376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-27T00:  00:  00-05:  00",
  "tradingDay":    "2012-11-27",
  "open":          78.68919,
  "high":          78.80531,
  "low":           77.42786,
  "close":         78.05252,
  "volume":        142701936,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-28T00:  00:  00-05:  00",
  "tradingDay":    "2012-11-28",
  "open":          77.05014,
  "high":          78.18866,
  "low":           76.38144,
  "close":         77.80693,
  "volume":        139422640,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-29T00:  00:  00-05:  00",
  "tradingDay":    "2012-11-29",
  "open":          78.77861,
  "high":          79.31651,
  "low":           78.11525,
  "close":         78.66383,
  "volume":        137720416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-11-30T00:  00:  00-05:  00",
  "tradingDay":    "2012-11-30",
  "open":          78.25006,
  "high":          78.5357,
  "low":           77.77222,
  "close":         78.11926,
  "volume":        104701832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-12-03T00:  00:  00-05:  00",
  "tradingDay":    "2012-12-03",
  "open":          79.23643,
  "high":          79.3619,
  "low":           78.14862,
  "close":         78.24072,
  "volume":        97471176,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-12-04T00:  00:  00-05:  00",
  "tradingDay":    "2012-12-04",
  "open":          77.65077,
  "high":          77.65477,
  "low":           76.36408,
  "close":         76.8606,
  "volume":        149288272,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-12-05T00:  00:  00-05:  00",
  "tradingDay":    "2012-12-05",
  "open":          75.9343,
  "high":          75.97968,
  "low":           71.91142,
  "close":         71.91408,
  "volume":        279519552,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-12-06T00:  00:  00-05:  00",
  "tradingDay":    "2012-12-06",
  "open":          70.59937,
  "high":          73.85211,
  "low":           69.22326,
  "close":         73.04193,
  "volume":        314992576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-12-07T00:  00:  00-05:  00",
  "tradingDay":    "2012-12-07",
  "open":          73.86413,
  "high":          74.10439,
  "low":           70.74085,
  "close":         71.17464,
  "volume":        210787472,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-12-10T00:  00:  00-05:  00",
  "tradingDay":    "2012-12-10",
  "open":          70.07349,
  "high":          71.87671,
  "low":           69.61701,
  "close":         70.71683,
  "volume":        168701904,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-12-11T00:  00:  00-05:  00",
  "tradingDay":    "2012-12-11",
  "open":          72.04489,
  "high":          73.35159,
  "low":           71.72455,
  "close":         72.26112,
  "volume":        158496864,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-12-12T00:  00:  00-05:  00",
  "tradingDay":    "2012-12-12",
  "open":          73.11268,
  "high":          73.14337,
  "low":           71.57774,
  "close":         71.94212,
  "volume":        130344416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-12-13T00:  00:  00-05:  00",
  "tradingDay":    "2012-12-13",
  "open":          70.89435,
  "high":          71.76059,
  "low":           70.18026,
  "close":         70.69948,
  "volume":        167303872,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-12-14T00:  00:  00-05:  00",
  "tradingDay":    "2012-12-14",
  "open":          68.70538,
  "high":          69.15652,
  "low":           67.48143,
  "close":         68.04336,
  "volume":        270138656,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-12-17T00:  00:  00-05:  00",
  "tradingDay":    "2012-12-17",
  "open":          67.92857,
  "high":          69.40612,
  "low":           66.90083,
  "close":         69.24996,
  "volume":        203075056,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-12-18T00:  00:  00-05:  00",
  "tradingDay":    "2012-12-18",
  "open":          70.07349,
  "high":          71.39487,
  "low":           69.43949,
  "close":         71.2614,
  "volume":        167413248,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-12-19T00:  00:  00-05:  00",
  "tradingDay":    "2012-12-19",
  "open":          70.93706,
  "high":          71.2347,
  "low":           70.14022,
  "close":         70.24834,
  "volume":        120238272,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-12-20T00:  00:  00-05:  00",
  "tradingDay":    "2012-12-20",
  "open":          70.74085,
  "high":          70.76755,
  "low":           69.25663,
  "close":         69.63702,
  "volume":        128887944,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-12-21T00:  00:  00-05:  00",
  "tradingDay":    "2012-12-21",
  "open":          68.40507,
  "high":          69.36207,
  "low":           68.10342,
  "close":         69.3167,
  "volume":        159613936,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-12-24T00:  00:  00-05:  00",
  "tradingDay":    "2012-12-24",
  "open":          69.45284,
  "high":          69.97338,
  "low":           69.23394,
  "close":         69.42881,
  "volume":        47025884,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-12-26T00:  00:  00-05:  00",
  "tradingDay":    "2012-12-26",
  "open":          69.27264,
  "high":          69.33405,
  "low":           68.22088,
  "close":         68.47181,
  "volume":        80924048,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-12-27T00:  00:  00-05:  00",
  "tradingDay":    "2012-12-27",
  "open":          68.54388,
  "high":          68.90559,
  "low":           67.35864,
  "close":         68.74676,
  "volume":        121778656,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-12-28T00:  00:  00-05:  00",
  "tradingDay":    "2012-12-28",
  "open":          68.11009,
  "high":          68.66934,
  "low":           67.82046,
  "close":         68.01666,
  "volume":        94795736,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2012-12-31T00:  00:  00-05:  00",
  "tradingDay":    "2012-12-31",
  "open":          68.0834,
  "high":          71.46161,
  "low":           67.93791,
  "close":         71.03049,
  "volume":        176463744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-02T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-02",
  "open":          73.92019,
  "high":          74.07768,
  "low":           72.29314,
  "close":         73.28085,
  "volume":        149976048,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-03T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-03",
  "open":          73.08598,
  "high":          73.36627,
  "low":           72.20906,
  "close":         72.35587,
  "volume":        94444352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-04T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-04",
  "open":          71.67116,
  "high":          71.89273,
  "low":           70.18427,
  "close":         70.34043,
  "volume":        159027296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-07T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-07",
  "open":          69.67574,
  "high":          70.64742,
  "low":           68.76545,
  "close":         69.92667,
  "volume":        129547248,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-08T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-08",
  "open":          70.63541,
  "high":          70.99312,
  "low":           69.57296,
  "close":         70.11486,
  "volume":        122738400,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-09T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-09",
  "open":          69.7665,
  "high":          70.07482,
  "low":           68.87089,
  "close":         69.01904,
  "volume":        109063752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-10T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-10",
  "open":          70.54732,
  "high":          70.57,
  "low":           68.80816,
  "close":         69.87461,
  "volume":        160850896,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-11T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-11",
  "open":          69.55828,
  "high":          70.1162,
  "low":           69.27532,
  "close":         69.44616,
  "volume":        93853224,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-14T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-14",
  "open":          67.09436,
  "high":          67.7377,
  "low":           66.53778,
  "close":         66.97023,
  "volume":        196448272,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-15T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-15",
  "open":          66.50975,
  "high":          66.60184,
  "low":           64.51833,
  "close":         64.85735,
  "volume":        234602720,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-16T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-16",
  "open":          66.02124,
  "high":          67.99664,
  "low":           65.7356,
  "close":         67.54951,
  "volume":        184841456,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-17T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-17",
  "open":          68.11276,
  "high":          68.17149,
  "low":           67.00761,
  "close":         67.09436,
  "volume":        121385320,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-18T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-18",
  "open":          66.53911,
  "high":          67.03297,
  "low":           66.25615,
  "close":         66.73666,
  "volume":        126602840,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-22T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-22",
  "open":          67.34529,
  "high":          67.78842,
  "low":           66.28685,
  "close":         67.37332,
  "volume":        123498104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-23T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-23",
  "open":          67.91255,
  "high":          68.73742,
  "low":           67.37332,
  "close":         68.60661,
  "volume":        230517248,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-24T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-24",
  "open":          61.39772,
  "high":          62.16253,
  "low":           60.09636,
  "close":         60.12973,
  "volume":        390888640,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-25T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-25",
  "open":          60.28856,
  "high":          60.89453,
  "low":           58.06089,
  "close":         58.71224,
  "volume":        323224928,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-28T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-28",
  "open":          58.43862,
  "high":          60.49144,
  "low":           58.17567,
  "close":         60.04029,
  "volume":        210185104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-29T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-29",
  "open":          61.19751,
  "high":          61.42442,
  "low":           60.34595,
  "close":         61.16681,
  "volume":        152805088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-30T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-30",
  "open":          60.9973,
  "high":          61.74475,
  "low":           60.66362,
  "close":         60.97461,
  "volume":        111620072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-01-31T00:  00:  00-05:  00",
  "tradingDay":    "2013-01-31",
  "open":          60.99463,
  "high":          61.30162,
  "low":           60.72769,
  "close":         60.79575,
  "volume":        85445552,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-02-01T00:  00:  00-05:  00",
  "tradingDay":    "2013-02-01",
  "open":          61.28027,
  "high":          61.32832,
  "low":           59.84276,
  "close":         60.54616,
  "volume":        144348720,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-02-04T00:  00:  00-05:  00",
  "tradingDay":    "2013-02-04",
  "open":          60.58487,
  "high":          60.85582,
  "low":           58.9952,
  "close":         59.03791,
  "volume":        127664480,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-02-05T00:  00:  00-05:  00",
  "tradingDay":    "2013-02-05",
  "open":          59.26882,
  "high":          61.36302,
  "low":           59.02457,
  "close":         61.10942,
  "volume":        153413440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-02-06T00:  00:  00-05:  00",
  "tradingDay":    "2013-02-06",
  "open":          60.92656,
  "high":          62.2653,
  "low":           60.40735,
  "close":         61.04402,
  "volume":        158860976,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-02-07T00:  00:  00-05:  00",
  "tradingDay":    "2013-02-07",
  "open":          62.19186,
  "high":          63.09806,
  "low":           60.96615,
  "close":         62.85909,
  "volume":        187435952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-02-08T00:  00:  00-05:  00",
  "tradingDay":    "2013-02-08",
  "open":          63.63507,
  "high":          64.28082,
  "low":           62.86312,
  "close":         63.76663,
  "volume":        168435744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-02-11T00:  00:  00-05:  00",
  "tradingDay":    "2013-02-11",
  "open":          63.97069,
  "high":          65.10378,
  "low":           63.53438,
  "close":         64.43118,
  "volume":        137650176,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-02-12T00:  00:  00-05:  00",
  "tradingDay":    "2013-02-12",
  "open":          64.37479,
  "high":          64.76009,
  "low":           62.79465,
  "close":         62.81613,
  "volume":        162023136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-02-13T00:  00:  00-05:  00",
  "tradingDay":    "2013-02-13",
  "open":          62.7235,
  "high":          63.58673,
  "low":           62.18784,
  "close":         62.69665,
  "volume":        126416752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-02-14T00:  00:  00-05:  00",
  "tradingDay":    "2013-02-14",
  "open":          62.36236,
  "high":          63.31823,
  "low":           62.29524,
  "close":         62.64026,
  "volume":        94573464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-02-15T00:  00:  00-05:  00",
  "tradingDay":    "2013-02-15",
  "open":          62.94367,
  "high":          63.11954,
  "low":           61.74481,
  "close":         61.77703,
  "volume":        104262016,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-02-19T00:  00:  00-05:  00",
  "tradingDay":    "2013-02-19",
  "open":          61.90322,
  "high":          62.12206,
  "low":           60.9299,
  "close":         61.7542,
  "volume":        115927464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-02-20T00:  00:  00-05:  00",
  "tradingDay":    "2013-02-20",
  "open":          61.44543,
  "high":          61.44543,
  "low":           60.25193,
  "close":         60.25865,
  "volume":        126707256,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-02-21T00:  00:  00-05:  00",
  "tradingDay":    "2013-02-21",
  "open":          59.87603,
  "high":          60.30161,
  "low":           59.44912,
  "close":         59.88409,
  "volume":        118961328,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-02-22T00:  00:  00-05:  00",
  "tradingDay":    "2013-02-22",
  "open":          60.31235,
  "high":          60.62784,
  "low":           59.95658,
  "close":         60.52178,
  "volume":        87961976,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-02-25T00:  00:  00-05:  00",
  "tradingDay":    "2013-02-25",
  "open":          60.9299,
  "high":          61.1004,
  "low":           59.41555,
  "close":         59.44643,
  "volume":        99114952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-02-26T00:  00:  00-05:  00",
  "tradingDay":    "2013-02-26",
  "open":          59.58337,
  "high":          60.61979,
  "low":           58.75638,
  "close":         60.27476,
  "volume":        133410360,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-02-27T00:  00:  00-05:  00",
  "tradingDay":    "2013-02-27",
  "open":          60.20226,
  "high":          60.74061,
  "low":           59.15779,
  "close":         59.68406,
  "volume":        156249632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-02-28T00:  00:  00-05:  00",
  "tradingDay":    "2013-02-28",
  "open":          59.61424,
  "high":          60.12708,
  "low":           59.25848,
  "close":         59.25848,
  "volume":        85796632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-03-01T00:  00:  00-05:  00",
  "tradingDay":    "2013-03-01",
  "open":          58.80202,
  "high":          58.82619,
  "low":           57.72533,
  "close":         57.79111,
  "volume":        146964800,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-03-04T00:  00:  00-05:  00",
  "tradingDay":    "2013-03-04",
  "open":          57.43266,
  "high":          57.48636,
  "low":           56.25125,
  "close":         56.39221,
  "volume":        155022832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-03-05T00:  00:  00-05:  00",
  "tradingDay":    "2013-03-05",
  "open":          56.58419,
  "high":          58.42478,
  "low":           56.48619,
  "close":         57.88106,
  "volume":        169838336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-03-06T00:  00:  00-05:  00",
  "tradingDay":    "2013-03-06",
  "open":          58.33349,
  "high":          58.43283,
  "low":           56.98023,
  "close":         57.14536,
  "volume":        122437648,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-03-07T00:  00:  00-05:  00",
  "tradingDay":    "2013-03-07",
  "open":          56.98963,
  "high":          57.99786,
  "low":           56.52781,
  "close":         57.80588,
  "volume":        124625336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-03-08T00:  00:  00-05:  00",
  "tradingDay":    "2013-03-08",
  "open":          57.70116,
  "high":          58.457,
  "low":           57.5414,
  "close":         57.95892,
  "volume":        104174120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-03-11T00:  00:  00-04:  00",
  "tradingDay":    "2013-03-11",
  "open":          57.69042,
  "high":          58.93762,
  "low":           57.07555,
  "close":         58.78457,
  "volume":        126158280,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-03-12T00:  00:  00-04:  00",
  "tradingDay":    "2013-03-12",
  "open":          58.47982,
  "high":          58.92016,
  "low":           57.40178,
  "close":         57.51724,
  "volume":        123943776,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-03-13T00:  00:  00-04:  00",
  "tradingDay":    "2013-03-13",
  "open":          57.51992,
  "high":          58.33214,
  "low":           57.10508,
  "close":         57.5065,
  "volume":        107957328,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-03-14T00:  00:  00-04:  00",
  "tradingDay":    "2013-03-14",
  "open":          58.10794,
  "high":          58.35094,
  "low":           57.78843,
  "close":         58.06364,
  "volume":        80838016,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-03-15T00:  00:  00-04:  00",
  "tradingDay":    "2013-03-15",
  "open":          58.79263,
  "high":          59.63841,
  "low":           58.70133,
  "close":         59.56189,
  "volume":        171309472,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-03-18T00:  00:  00-04:  00",
  "tradingDay":    "2013-03-18",
  "open":          59.26519,
  "high":          61.41455,
  "low":           59.23163,
  "close":         61.18095,
  "volume":        161263376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-03-19T00:  00:  00-04:  00",
  "tradingDay":    "2013-03-19",
  "open":          61.68842,
  "high":          61.88577,
  "low":           60.21166,
  "close":         61.01582,
  "volume":        140135072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-03-20T00:  00:  00-04:  00",
  "tradingDay":    "2013-03-20",
  "open":          61.40918,
  "high":          61.43737,
  "low":           60.35799,
  "close":         60.69228,
  "volume":        82111000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-03-21T00:  00:  00-04:  00",
  "tradingDay":    "2013-03-21",
  "open":          60.44257,
  "high":          61.48436,
  "low":           60.42646,
  "close":         60.77954,
  "volume":        101955144,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-03-22T00:  00:  00-04:  00",
  "tradingDay":    "2013-03-22",
  "open":          61.0279,
  "high":          62.03748,
  "low":           60.83055,
  "close":         62.01197,
  "volume":        105107448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-03-25T00:  00:  00-04:  00",
  "tradingDay":    "2013-03-25",
  "open":          62.38519,
  "high":          63.09135,
  "low":           61.99451,
  "close":         62.23616,
  "volume":        133314272,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-03-26T00:  00:  00-04:  00",
  "tradingDay":    "2013-03-26",
  "open":          62.48587,
  "high":          62.53957,
  "low":           61.8267,
  "close":         61.9086,
  "volume":        78289064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-03-27T00:  00:  00-04:  00",
  "tradingDay":    "2013-03-27",
  "open":          61.2803,
  "high":          61.32594,
  "low":           60.51104,
  "close":         60.69228,
  "volume":        88163088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-03-28T00:  00:  00-04:  00",
  "tradingDay":    "2013-03-28",
  "open":          60.38887,
  "high":          60.65738,
  "low":           59.28801,
  "close":         59.42763,
  "volume":        117844016,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-01T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-01",
  "open":          59.35917,
  "high":          59.56726,
  "low":           57.4246,
  "close":         57.58168,
  "volume":        103678040,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-02T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-02",
  "open":          57.40581,
  "high":          58.82082,
  "low":           57.24471,
  "close":         57.69982,
  "volume":        140932832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-03T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-03",
  "open":          57.91194,
  "high":          58.70536,
  "low":           57.76963,
  "close":         57.99517,
  "volume":        96624096,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-04T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-04",
  "open":          58.2328,
  "high":          58.39927,
  "low":           57.09032,
  "close":         57.42192,
  "volume":        95412192,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-05T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-05",
  "open":          56.98963,
  "high":          57.05005,
  "low":           56.34254,
  "close":         56.81511,
  "volume":        101974512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-08T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-08",
  "open":          57.03662,
  "high":          57.39238,
  "low":           56.71978,
  "close":         57.2192,
  "volume":        80089416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-09T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-09",
  "open":          57.23934,
  "high":          57.52663,
  "low":           56.75469,
  "close":         57.32257,
  "volume":        81603744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-10T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-10",
  "open":          57.48636,
  "high":          58.67583,
  "low":           57.19235,
  "close":         58.4919,
  "volume":        100005816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-11T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-11",
  "open":          58.22743,
  "high":          58.80068,
  "low":           57.88911,
  "close":         58.30932,
  "volume":        87419704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-12T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-12",
  "open":          58.28515,
  "high":          58.28515,
  "low":           57.60584,
  "close":         57.70116,
  "volume":        63506328,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-15T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-15",
  "open":          57.32526,
  "high":          57.44474,
  "low":           56.32508,
  "close":         56.36536,
  "volume":        84467776,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-16T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-16",
  "open":          56.59628,
  "high":          57.2729,
  "low":           56.46202,
  "close":         57.22323,
  "volume":        81342296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-17T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-17",
  "open":          56.42175,
  "high":          56.46605,
  "low":           53.44674,
  "close":         54.07638,
  "volume":        251408576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-18T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-18",
  "open":          54.36502,
  "high":          54.47779,
  "low":           52.32306,
  "close":         52.63318,
  "volume":        177252064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-19T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-19",
  "open":          52.08543,
  "high":          53.64678,
  "low":           51.70013,
  "close":         52.42912,
  "volume":        162081984,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-22T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-22",
  "open":          52.76072,
  "high":          53.99583,
  "low":           52.52846,
  "close":         53.52192,
  "volume":        114369192,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-23T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-23",
  "open":          54.23614,
  "high":          54.8255,
  "low":           53.54072,
  "close":         54.52344,
  "volume":        176703824,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-24T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-24",
  "open":          52.83321,
  "high":          55.74781,
  "low":           52.69359,
  "close":         54.43349,
  "volume":        258000688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-25T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-25",
  "open":          55.20812,
  "high":          55.57194,
  "low":           54.64024,
  "close":         54.8255,
  "volume":        102376000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-26T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-26",
  "open":          55.01748,
  "high":          56.22037,
  "low":           54.80805,
  "close":         56.0096,
  "volume":        203316624,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-29T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-29",
  "open":          56.45934,
  "high":          58.214,
  "low":           56.3855,
  "close":         57.74412,
  "volume":        170260688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-04-30T00:  00:  00-04:  00",
  "tradingDay":    "2013-04-30",
  "open":          58.41269,
  "high":          59.77534,
  "low":           58.00591,
  "close":         59.44374,
  "volume":        183966336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-01T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-01",
  "open":          59.66928,
  "high":          59.73238,
  "low":           58.31738,
  "close":         58.97521,
  "volume":        134915744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-02T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-02",
  "open":          59.30949,
  "high":          60.22374,
  "low":           59.1551,
  "close":         59.81159,
  "volume":        112269392,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-03T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-03",
  "open":          60.58891,
  "high":          60.84667,
  "low":           60.29892,
  "close":         60.41035,
  "volume":        96174936,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-06T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-06",
  "open":          61.17961,
  "high":          62.0509,
  "low":           60.99166,
  "close":         61.85086,
  "volume":        132118752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-07T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-07",
  "open":          62.42412,
  "high":          62.52749,
  "low":           60.90977,
  "close":         61.57565,
  "volume":        128690104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-08T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-08",
  "open":          61.62667,
  "high":          62.47647,
  "low":           61.19304,
  "close":         62.27107,
  "volume":        125722536,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-09T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-09",
  "open":          62.13863,
  "high":          62.56973,
  "low":           61.56699,
  "close":         61.72781,
  "volume":        105379760,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-10T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-10",
  "open":          61.88998,
  "high":          62.12512,
  "low":           60.87778,
  "close":         61.21428,
  "volume":        88492800,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-13T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-13",
  "open":          61.01698,
  "high":          61.88052,
  "low":           61.01563,
  "close":         61.45348,
  "volume":        83804320,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-14T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-14",
  "open":          61.3332,
  "high":          61.51564,
  "low":           59.75206,
  "close":         59.98315,
  "volume":        118162072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-15T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-15",
  "open":          59.348,
  "high":          59.59666,
  "low":           57.07765,
  "close":         57.95471,
  "volume":        195924512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-16T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-16",
  "open":          57.19657,
  "high":          59.17097,
  "low":           56.61007,
  "close":         58.72906,
  "volume":        159481504,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-17T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-17",
  "open":          59.33313,
  "high":          59.47368,
  "low":           58.24661,
  "close":         58.55067,
  "volume":        113130984,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-20T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-20",
  "open":          58.36824,
  "high":          60.24532,
  "low":           58.12363,
  "close":         59.85748,
  "volume":        119340856,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-21T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-21",
  "open":          59.21151,
  "high":          60.20208,
  "low":           58.67771,
  "close":         59.41557,
  "volume":        120588448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-22T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-22",
  "open":          60.00883,
  "high":          60.58994,
  "low":           59.22097,
  "close":         59.64396,
  "volume":        117142392,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-23T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-23",
  "open":          58.9142,
  "high":          60.29398,
  "low":           58.89258,
  "close":         59.75072,
  "volume":        93389952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-24T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-24",
  "open":          59.57639,
  "high":          60.22641,
  "low":           59.51016,
  "close":         60.15749,
  "volume":        73052496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-28T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-28",
  "open":          60.7994,
  "high":          60.96292,
  "low":           59.57639,
  "close":         59.65612,
  "volume":        102048392,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-29T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-29",
  "open":          59.46152,
  "high":          60.47506,
  "low":           59.38043,
  "close":         60.13046,
  "volume":        87417616,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-30T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-30",
  "open":          60.22506,
  "high":          61.42104,
  "low":           60.071,
  "close":         61.02643,
  "volume":        93478008,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-05-31T00:  00:  00-04:  00",
  "tradingDay":    "2013-05-31",
  "open":          61.15076,
  "high":          61.77241,
  "low":           60.74534,
  "close":         60.77643,
  "volume":        101561488,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-06-03T00:  00:  00-04:  00",
  "tradingDay":    "2013-06-03",
  "open":          60.91157,
  "high":          61.13184,
  "low":           59.79667,
  "close":         60.91021,
  "volume":        98403280,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-06-04T00:  00:  00-04:  00",
  "tradingDay":    "2013-06-04",
  "open":          61.24806,
  "high":          61.41158,
  "low":           60.4602,
  "close":         60.71967,
  "volume":        77360624,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-06-05T00:  00:  00-04:  00",
  "tradingDay":    "2013-06-05",
  "open":          60.22506,
  "high":          60.91021,
  "low":           59.96288,
  "close":         60.15208,
  "volume":        76795288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-06-06T00:  00:  00-04:  00",
  "tradingDay":    "2013-06-06",
  "open":          60.20073,
  "high":          60.40749,
  "low":           58.65743,
  "close":         59.2534,
  "volume":        110222152,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-06-07T00:  00:  00-04:  00",
  "tradingDay":    "2013-06-07",
  "open":          58.98853,
  "high":          59.89937,
  "low":           58.48446,
  "close":         59.70612,
  "volume":        106963304,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-06-10T00:  00:  00-04:  00",
  "tradingDay":    "2013-06-10",
  "open":          60.10073,
  "high":          60.68858,
  "low":           59.02907,
  "close":         59.31151,
  "volume":        119018224,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-06-11T00:  00:  00-04:  00",
  "tradingDay":    "2013-06-11",
  "open":          58.88582,
  "high":          59.8345,
  "low":           58.55878,
  "close":         59.13718,
  "volume":        75684584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-06-12T00:  00:  00-04:  00",
  "tradingDay":    "2013-06-12",
  "open":          59.39394,
  "high":          59.63044,
  "low":           58.31283,
  "close":         58.40607,
  "volume":        70091856,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-06-13T00:  00:  00-04:  00",
  "tradingDay":    "2013-06-13",
  "open":          58.44797,
  "high":          59.07502,
  "low":           57.94119,
  "close":         58.91555,
  "volume":        75612064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-06-14T00:  00:  00-04:  00",
  "tradingDay":    "2013-06-14",
  "open":          58.83987,
  "high":          58.96015,
  "low":           57.90741,
  "close":         58.11687,
  "volume":        71904792,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-06-17T00:  00:  00-04:  00",
  "tradingDay":    "2013-06-17",
  "open":          58.30472,
  "high":          58.88042,
  "low":           58.15877,
  "close":         58.3804,
  "volume":        68563808,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-06-18T00:  00:  00-04:  00",
  "tradingDay":    "2013-06-18",
  "open":          58.32093,
  "high":          58.7723,
  "low":           58.1385,
  "close":         58.34931,
  "volume":        51576216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-06-19T00:  00:  00-04:  00",
  "tradingDay":    "2013-06-19",
  "open":          58.29931,
  "high":          58.33445,
  "low":           57.16414,
  "close":         57.16414,
  "volume":        82197840,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-06-20T00:  00:  00-04:  00",
  "tradingDay":    "2013-06-20",
  "open":          56.66412,
  "high":          57.56686,
  "low":           56.106,
  "close":         56.33168,
  "volume":        94477712,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-06-21T00:  00:  00-04:  00",
  "tradingDay":    "2013-06-21",
  "open":          56.55466,
  "high":          56.75872,
  "low":           55.15055,
  "close":         55.88031,
  "volume":        127166080,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-06-24T00:  00:  00-04:  00",
  "tradingDay":    "2013-06-24",
  "open":          55.05596,
  "high":          55.22623,
  "low":           53.7924,
  "close":         54.39918,
  "volume":        127049168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-06-25T00:  00:  00-04:  00",
  "tradingDay":    "2013-06-25",
  "open":          54.82622,
  "high":          55.10866,
  "low":           53.89781,
  "close":         54.41134,
  "volume":        83071744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-06-26T00:  00:  00-04:  00",
  "tradingDay":    "2013-06-26",
  "open":          54.58297,
  "high":          54.70324,
  "low":           53.46942,
  "close":         53.7951,
  "volume":        97239296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-06-27T00:  00:  00-04:  00",
  "tradingDay":    "2013-06-27",
  "open":          53.95457,
  "high":          54.24377,
  "low":           53.18292,
  "close":         53.21535,
  "volume":        89166920,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-06-28T00:  00:  00-04:  00",
  "tradingDay":    "2013-06-28",
  "open":          52.88831,
  "high":          54.09241,
  "low":           52.55182,
  "close":         53.58699,
  "volume":        152920160,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-01T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-01",
  "open":          54.41945,
  "high":          55.71409,
  "low":           54.22079,
  "close":         55.30191,
  "volume":        103377384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-02T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-02",
  "open":          55.40191,
  "high":          56.979,
  "low":           55.3357,
  "close":         56.55466,
  "volume":        124232088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-03T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-03",
  "open":          56.87494,
  "high":          57.16144,
  "low":           56.41412,
  "close":         56.86683,
  "volume":        63671100,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-05T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-05",
  "open":          56.81142,
  "high":          57.20333,
  "low":           56.13032,
  "close":         56.41006,
  "volume":        72433136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-08T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-08",
  "open":          56.77358,
  "high":          56.89386,
  "low":           55.49516,
  "close":         56.08978,
  "volume":        78836872,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-09T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-09",
  "open":          55.89383,
  "high":          57.23171,
  "low":           55.45868,
  "close":         57.0763,
  "volume":        93207176,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-10T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-10",
  "open":          56.70467,
  "high":          57.40739,
  "low":           56.52222,
  "close":         56.85737,
  "volume":        74368168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-11T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-11",
  "open":          57.15738,
  "high":          57.87362,
  "low":           56.91684,
  "close":         57.74389,
  "volume":        86279536,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-12T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-12",
  "open":          57.79254,
  "high":          58.08174,
  "low":           57.21955,
  "close":         57.63848,
  "volume":        73902728,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-15T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-15",
  "open":          57.43577,
  "high":          58.30742,
  "low":           57.40739,
  "close":         57.76416,
  "volume":        64000388,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-16T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-16",
  "open":          57.63983,
  "high":          58.20607,
  "low":           57.32225,
  "close":         58.13715,
  "volume":        57225180,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-17T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-17",
  "open":          58.06958,
  "high":          58.41013,
  "low":           57.86957,
  "close":         58.15201,
  "volume":        52605520,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-18T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-18",
  "open":          58.56689,
  "high":          58.76825,
  "low":           58.19255,
  "close":         58.34797,
  "volume":        57887460,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-19T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-19",
  "open":          58.52905,
  "high":          58.64798,
  "low":           57.34658,
  "close":         57.42766,
  "volume":        71031624,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-22T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-22",
  "open":          58.03714,
  "high":          58.07633,
  "low":           57.49793,
  "close":         57.61145,
  "volume":        54966780,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-23T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-23",
  "open":          57.56956,
  "high":          57.69929,
  "low":           56.58439,
  "close":         56.62223,
  "volume":        97621864,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-24T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-24",
  "open":          59.31691,
  "high":          60.08181,
  "low":           58.82095,
  "close":         59.53044,
  "volume":        156466112,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-25T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-25",
  "open":          59.55611,
  "high":          59.65071,
  "low":           58.89528,
  "close":         59.2588,
  "volume":        60699360,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-26T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-26",
  "open":          58.82635,
  "high":          59.60206,
  "low":           58.69662,
  "close":         59.5953,
  "volume":        52927408,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-29T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-29",
  "open":          59.56963,
  "high":          60.81156,
  "low":           59.48854,
  "close":         60.51426,
  "volume":        65657188,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-30T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-30",
  "open":          60.80751,
  "high":          61.77916,
  "low":           60.70886,
  "close":         61.26158,
  "volume":        81772352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-07-31T00:  00:  00-04:  00",
  "tradingDay":    "2013-07-31",
  "open":          61.48726,
  "high":          61.80484,
  "low":           60.73588,
  "close":         61.15482,
  "volume":        85349392,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-01T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-01",
  "open":          61.5724,
  "high":          61.73186,
  "low":           61.25347,
  "close":         61.71564,
  "volume":        54506516,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-02T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-02",
  "open":          61.89539,
  "high":          62.54946,
  "low":           61.71294,
  "close":         62.50757,
  "volume":        72618128,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-05T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-05",
  "open":          62.79812,
  "high":          63.60625,
  "low":           62.45486,
  "close":         63.44138,
  "volume":        84265328,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-06T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-06",
  "open":          63.24813,
  "high":          63.77113,
  "low":           62.45757,
  "close":         62.87379,
  "volume":        88509816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-07T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-07",
  "open":          62.67784,
  "high":          63.11029,
  "low":           62.40351,
  "close":         62.83731,
  "volume":        78980432,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-08T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-08",
  "open":          63.09985,
  "high":          63.1325,
  "low":           62.2959,
  "close":         62.71216,
  "volume":        67210608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-09T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-09",
  "open":          62.38977,
  "high":          62.63734,
  "low":           61.71096,
  "close":         61.81979,
  "volume":        70261352,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-12T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-12",
  "open":          62.14762,
  "high":          63.75145,
  "low":           62.11634,
  "close":         63.57596,
  "volume":        95678888,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-13T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-13",
  "open":          64.06296,
  "high":          67.28964,
  "low":           63.66983,
  "close":         66.59724,
  "volume":        231546816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-14T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-14",
  "open":          67.72766,
  "high":          68.59419,
  "low":           67.11824,
  "close":         67.812,
  "volume":        198579600,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-15T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-15",
  "open":          67.52906,
  "high":          68.34253,
  "low":           66.53058,
  "close":         67.73174,
  "volume":        128723288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-16T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-16",
  "open":          68.03645,
  "high":          68.41599,
  "low":           67.86098,
  "close":         68.33301,
  "volume":        95120192,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-19T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-19",
  "open":          68.60643,
  "high":          69.88513,
  "low":           68.56018,
  "close":         69.06894,
  "volume":        134032328,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-20T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-20",
  "open":          69.33692,
  "high":          69.45391,
  "low":           68.1276,
  "close":         68.16161,
  "volume":        94170416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-21T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-21",
  "open":          68.50441,
  "high":          68.98868,
  "low":           68.17929,
  "close":         68.33709,
  "volume":        88182128,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-22T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-22",
  "open":          68.6935,
  "high":          68.77647,
  "low":           67.77119,
  "close":         68.41871,
  "volume":        64114276,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-23T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-23",
  "open":          68.46496,
  "high":          68.47176,
  "low":           67.92764,
  "close":         68.1548,
  "volume":        58475900,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-26T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-26",
  "open":          68.11807,
  "high":          69.40358,
  "low":           68.08407,
  "close":         68.42007,
  "volume":        86891992,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-27T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-27",
  "open":          67.74399,
  "high":          68.35749,
  "low":           66.15241,
  "close":         66.46393,
  "volume":        111367096,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-28T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-28",
  "open":          66.1116,
  "high":          67.44472,
  "low":           66.1116,
  "close":         66.77816,
  "volume":        80759616,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-29T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-29",
  "open":          66.88018,
  "high":          67.53994,
  "low":           66.80945,
  "close":         66.88699,
  "volume":        62919704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-08-30T00:  00:  00-04:  00",
  "tradingDay":    "2013-08-30",
  "open":          66.9278,
  "high":          67.05703,
  "low":           66.17962,
  "close":         66.27757,
  "volume":        71516944,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-09-03T00:  00:  00-04:  00",
  "tradingDay":    "2013-09-03",
  "open":          67.07743,
  "high":          68.09767,
  "low":           66.29525,
  "close":         66.46256,
  "volume":        87190448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-09-04T00:  00:  00-04:  00",
  "tradingDay":    "2013-09-04",
  "open":          67.9562,
  "high":          68.32076,
  "low":           67.51001,
  "close":         67.83785,
  "volume":        90585232,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-09-05T00:  00:  00-04:  00",
  "tradingDay":    "2013-09-05",
  "open":          68.05006,
  "high":          68.10855,
  "low":           67.15089,
  "close":         67.37262,
  "volume":        62055940,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-09-06T00:  00:  00-04:  00",
  "tradingDay":    "2013-09-06",
  "open":          67.80384,
  "high":          67.93171,
  "low":           66.64893,
  "close":         67.77392,
  "volume":        94390216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-09-09T00:  00:  00-04:  00",
  "tradingDay":    "2013-09-09",
  "open":          68.69621,
  "high":          69.09343,
  "low":           68.48945,
  "close":         68.85537,
  "volume":        89444328,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-09-10T00:  00:  00-04:  00",
  "tradingDay":    "2013-09-10",
  "open":          68.85945,
  "high":          69.0295,
  "low":           66.58772,
  "close":         67.28692,
  "volume":        195120112,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-09-11T00:  00:  00-04:  00",
  "tradingDay":    "2013-09-11",
  "open":          63.52835,
  "high":          64.43705,
  "low":           63.22908,
  "close":         63.62357,
  "volume":        235945776,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-09-12T00:  00:  00-04:  00",
  "tradingDay":    "2013-09-12",
  "open":          63.73104,
  "high":          64.66966,
  "low":           63.39232,
  "close":         64.30102,
  "volume":        106080104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-09-13T00:  00:  00-04:  00",
  "tradingDay":    "2013-09-13",
  "open":          63.84531,
  "high":          64.18403,
  "low":           63.21412,
  "close":         63.24133,
  "volume":        78456480,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-09-16T00:  00:  00-04:  00",
  "tradingDay":    "2013-09-16",
  "open":          62.7108,
  "high":          62.79378,
  "low":           60.83628,
  "close":         61.23077,
  "volume":        144017472,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-09-17T00:  00:  00-04:  00",
  "tradingDay":    "2013-09-17",
  "open":          60.93694,
  "high":          62.53532,
  "low":           60.87437,
  "close":         61.93814,
  "volume":        104853928,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-09-18T00:  00:  00-04:  00",
  "tradingDay":    "2013-09-18",
  "open":          63.00735,
  "high":          63.43857,
  "low":           62.66455,
  "close":         63.2114,
  "volume":        119945216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-09-19T00:  00:  00-04:  00",
  "tradingDay":    "2013-09-19",
  "open":          64.03031,
  "high":          64.72816,
  "low":           63.83307,
  "close":         64.24796,
  "volume":        106208752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-09-20T00:  00:  00-04:  00",
  "tradingDay":    "2013-09-20",
  "open":          65.02335,
  "high":          65.09816,
  "low":           63.39096,
  "close":         63.58277,
  "volume":        183596368,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-09-23T00:  00:  00-04:  00",
  "tradingDay":    "2013-09-23",
  "open":          67.48553,
  "high":          67.59571,
  "low":           65.64909,
  "close":         66.74279,
  "volume":        200085120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-09-24T00:  00:  00-04:  00",
  "tradingDay":    "2013-09-24",
  "open":          67.31956,
  "high":          67.39983,
  "low":           66.35918,
  "close":         66.5333,
  "volume":        95655360,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-09-25T00:  00:  00-04:  00",
  "tradingDay":    "2013-09-25",
  "open":          66.58092,
  "high":          66.60676,
  "low":           65.48994,
  "close":         65.50354,
  "volume":        83214176,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-09-26T00:  00:  00-04:  00",
  "tradingDay":    "2013-09-26",
  "open":          66.1116,
  "high":          66.45985,
  "low":           65.82594,
  "close":         66.14153,
  "volume":        62280152,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-09-27T00:  00:  00-04:  00",
  "tradingDay":    "2013-09-27",
  "open":          65.80961,
  "high":          65.93068,
  "low":           65.39336,
  "close":         65.66949,
  "volume":        59869692,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-09-30T00:  00:  00-04:  00",
  "tradingDay":    "2013-09-30",
  "open":          64.92133,
  "high":          65.52122,
  "low":           64.53499,
  "close":         64.8533,
  "volume":        68302256,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-01T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-01",
  "open":          65.08456,
  "high":          66.53874,
  "low":           65.07504,
  "close":         66.37823,
  "volume":        92908952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-02T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-02",
  "open":          66.06127,
  "high":          66.90059,
  "low":           65.80553,
  "close":         66.59588,
  "volume":        75922520,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-03T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-03",
  "open":          66.72511,
  "high":          66.97541,
  "low":           65.39607,
  "close":         65.75928,
  "volume":        84736616,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-04T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-04",
  "open":          65.8205,
  "high":          65.92116,
  "low":           65.10497,
  "close":         65.70759,
  "volume":        67964104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-07T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-07",
  "open":          66.18778,
  "high":          67.01621,
  "low":           66.02319,
  "close":         66.34966,
  "volume":        81990208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-08T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-08",
  "open":          66.64757,
  "high":          66.74279,
  "low":           65.36887,
  "close":         65.42328,
  "volume":        76377560,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-09T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-09",
  "open":          65.9266,
  "high":          66.3551,
  "low":           65.06143,
  "close":         66.19186,
  "volume":        79215120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-10T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-10",
  "open":          66.8353,
  "high":          66.97948,
  "low":           66.25307,
  "close":         66.60676,
  "volume":        73144496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-11T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-11",
  "open":          66.24628,
  "high":          67.17809,
  "low":           65.99734,
  "close":         67.03798,
  "volume":        70292968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-14T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-14",
  "open":          66.63261,
  "high":          67.68685,
  "low":           66.56731,
  "close":         67.47736,
  "volume":        68759504,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-15T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-15",
  "open":          67.67733,
  "high":          68.28812,
  "low":           67.40662,
  "close":         67.83649,
  "volume":        84033104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-16T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-16",
  "open":          68.12352,
  "high":          68.36021,
  "low":           67.91131,
  "close":         68.16705,
  "volume":        65924144,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-17T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-17",
  "open":          68.01334,
  "high":          68.66628,
  "low":           67.97252,
  "close":         68.6282,
  "volume":        66579136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-18T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-18",
  "open":          68.83089,
  "high":          69.27571,
  "low":           68.79279,
  "close":         69.22538,
  "volume":        76279784,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-21T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-21",
  "open":          69.61715,
  "high":          71.32163,
  "low":           69.58314,
  "close":         70.9217,
  "volume":        104520176,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-22T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-22",
  "open":          71.60866,
  "high":          71.88617,
  "low":           69.10839,
  "close":         70.71901,
  "volume":        140213952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-23T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-23",
  "open":          70.60066,
  "high":          71.508,
  "low":           70.60066,
  "close":         71.41142,
  "volume":        82365848,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-24T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-24",
  "open":          71.41685,
  "high":          72.43301,
  "low":           71.06998,
  "close":         72.35683,
  "volume":        101016592,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-25T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-25",
  "open":          72.27658,
  "high":          72.5364,
  "low":           71.43182,
  "close":         71.54745,
  "volume":        88684944,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-28T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-28",
  "open":          71.96642,
  "high":          72.23305,
  "low":           71.17336,
  "close":         72.08069,
  "volume":        144513680,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-29T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-29",
  "open":          72.94994,
  "high":          73.35532,
  "low":           69.99396,
  "close":         70.28506,
  "volume":        166926768,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-30T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-30",
  "open":          70.65508,
  "high":          71.75966,
  "low":           70.33132,
  "close":         71.40325,
  "volume":        92982464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-10-31T00:  00:  00-04:  00",
  "tradingDay":    "2013-10-31",
  "open":          71.41685,
  "high":          71.75558,
  "low":           70.90945,
  "close":         71.10398,
  "volume":        72381440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-11-01T00:  00:  00-04:  00",
  "tradingDay":    "2013-11-01",
  "open":          71.33524,
  "high":          71.38965,
  "low":           70.17081,
  "close":         70.74078,
  "volume":        72169728,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-11-04T00:  00:  00-05:  00",
  "tradingDay":    "2013-11-04",
  "open":          70.88633,
  "high":          71.66444,
  "low":           70.57481,
  "close":         71.65491,
  "volume":        64225280,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-11-05T00:  00:  00-05:  00",
  "tradingDay":    "2013-11-05",
  "open":          71.35292,
  "high":          71.94602,
  "low":           71.14479,
  "close":         71.47807,
  "volume":        69697520,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-11-06T00:  00:  00-05:  00",
  "tradingDay":    "2013-11-06",
  "open":          71.71752,
  "high":          71.81466,
  "low":           70.9034,
  "close":         71.27557,
  "volume":        58305304,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-11-07T00:  00:  00-05:  00",
  "tradingDay":    "2013-11-07",
  "open":          71.09222,
  "high":          71.58617,
  "low":           70.10707,
  "close":         70.12212,
  "volume":        68548944,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-11-08T00:  00:  00-05:  00",
  "tradingDay":    "2013-11-08",
  "open":          70.40809,
  "high":          71.3043,
  "low":           70.13581,
  "close":         71.22631,
  "volume":        72907024,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-11-11T00:  00:  00-05:  00",
  "tradingDay":    "2013-11-11",
  "open":          71.14832,
  "high":          71.37818,
  "low":           70.38483,
  "close":         71.0197,
  "volume":        59369424,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-11-12T00:  00:  00-05:  00",
  "tradingDay":    "2013-11-12",
  "open":          70.83088,
  "high":          71.68604,
  "low":           70.7392,
  "close":         71.15105,
  "volume":        53366924,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-11-13T00:  00:  00-05:  00",
  "tradingDay":    "2013-11-13",
  "open":          70.89382,
  "high":          71.45755,
  "low":           70.73374,
  "close":         71.23589,
  "volume":        51477668,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-11-14T00:  00:  00-05:  00",
  "tradingDay":    "2013-11-14",
  "open":          71.53417,
  "high":          72.41944,
  "low":           71.40555,
  "close":         72.26619,
  "volume":        73716808,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-11-15T00:  00:  00-05:  00",
  "tradingDay":    "2013-11-15",
  "open":          72.05,
  "high":          72.39344,
  "low":           71.76404,
  "close":         71.83245,
  "volume":        82982568,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-11-18T00:  00:  00-05:  00",
  "tradingDay":    "2013-11-18",
  "open":          71.83245,
  "high":          72.13347,
  "low":           70.9034,
  "close":         70.96223,
  "volume":        63935068,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-11-19T00:  00:  00-05:  00",
  "tradingDay":    "2013-11-19",
  "open":          71.01697,
  "high":          71.61216,
  "low":           70.87193,
  "close":         71.08811,
  "volume":        54537020,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-11-20T00:  00:  00-05:  00",
  "tradingDay":    "2013-11-20",
  "open":          71.04433,
  "high":          71.20715,
  "low":           70.37389,
  "close":         70.46555,
  "volume":        50685424,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-11-21T00:  00:  00-05:  00",
  "tradingDay":    "2013-11-21",
  "open":          70.8213,
  "high":          71.31525,
  "low":           70.28358,
  "close":         71.30567,
  "volume":        68394008,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-11-22T00:  00:  00-05:  00",
  "tradingDay":    "2013-11-22",
  "open":          71.08401,
  "high":          71.44523,
  "low":           70.94855,
  "close":         71.12232,
  "volume":        58395928,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-11-25T00:  00:  00-05:  00",
  "tradingDay":    "2013-11-25",
  "open":          71.28925,
  "high":          71.95286,
  "low":           71.28651,
  "close":         71.66142,
  "volume":        59875908,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-11-26T00:  00:  00-05:  00",
  "tradingDay":    "2013-11-26",
  "open":          71.70657,
  "high":          73.35806,
  "low":           71.69699,
  "close":         72.98316,
  "volume":        104768584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-11-27T00:  00:  00-05:  00",
  "tradingDay":    "2013-11-27",
  "open":          73.38132,
  "high":          74.70717,
  "low":           72.98316,
  "close":         74.7017,
  "volume":        94866248,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-11-29T00:  00:  00-05:  00",
  "tradingDay":    "2013-11-29",
  "open":          75.18333,
  "high":          76.39424,
  "low":           74.95483,
  "close":         76.08501,
  "volume":        83037384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-02T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-02",
  "open":          76.34909,
  "high":          77.2152,
  "low":           75.36668,
  "close":         75.42277,
  "volume":        123342496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-03T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-03",
  "open":          76.3915,
  "high":          77.49569,
  "low":           76.30531,
  "close":         77.48748,
  "volume":        117710536,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-04T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-04",
  "open":          77.37528,
  "high":          77.88017,
  "low":           76.73494,
  "close":         77.30687,
  "volume":        98615528,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-05T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-05",
  "open":          78.3536,
  "high":          78.69429,
  "low":           77.49979,
  "close":         77.70367,
  "volume":        116826936,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-06T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-06",
  "open":          77.41496,
  "high":          77.54632,
  "low":           76.5639,
  "close":         76.62548,
  "volume":        89882560,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-09T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-09",
  "open":          76.74589,
  "high":          77.93354,
  "low":           76.74589,
  "close":         77.50253,
  "volume":        83654960,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-10T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-10",
  "open":          77.11121,
  "high":          77.70094,
  "low":           76.78693,
  "close":         77.38213,
  "volume":        72633688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-11T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-11",
  "open":          77.58052,
  "high":          78.12372,
  "low":           76.58032,
  "close":         76.80882,
  "volume":        93893480,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-12T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-12",
  "open":          76.92649,
  "high":          77.35339,
  "low":           76.62685,
  "close":         76.69662,
  "volume":        68461976,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-13T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-13",
  "open":          77.0127,
  "high":          77.0168,
  "low":           75.75663,
  "close":         75.86062,
  "volume":        86872176,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-16T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-16",
  "open":          75.94135,
  "high":          76.98396,
  "low":           75.93998,
  "close":         76.28068,
  "volume":        73762120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-17T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-17",
  "open":          76.04944,
  "high":          76.54612,
  "low":           75.71695,
  "close":         75.93724,
  "volume":        60008920,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-18T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-18",
  "open":          75.21343,
  "high":          75.45287,
  "low":           73.72202,
  "close":         75.35983,
  "volume":        147701120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-19T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-19",
  "open":          75.16827,
  "high":          75.25448,
  "low":           74.39658,
  "close":         74.49646,
  "volume":        83775544,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-20T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-20",
  "open":          74.62918,
  "high":          75.47476,
  "low":           74.54572,
  "close":         75.12039,
  "volume":        113912296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-23T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-23",
  "open":          77.71735,
  "high":          78.08952,
  "low":           77.00038,
  "close":         78.00332,
  "volume":        130850552,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-24T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-24",
  "open":          77.97595,
  "high":          78.24824,
  "low":           77.44781,
  "close":         77.6722,
  "volume":        43734276,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-26T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-26",
  "open":          77.73103,
  "high":          77.92259,
  "low":           77.08521,
  "close":         77.15636,
  "volume":        53249988,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-27T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-27",
  "open":          77.14542,
  "high":          77.22614,
  "low":           76.55433,
  "close":         76.63506,
  "volume":        58960148,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-30T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-30",
  "open":          76.27521,
  "high":          76.63506,
  "low":           75.57191,
  "close":         75.87293,
  "volume":        66202176,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2013-12-31T00:  00:  00-05:  00",
  "tradingDay":    "2013-12-31",
  "open":          75.82504,
  "high":          76.79788,
  "low":           75.80178,
  "close":         76.76231,
  "volume":        58278992,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-02T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-02",
  "open":          76.03165,
  "high":          76.21637,
  "low":           75.53087,
  "close":         75.68275,
  "volume":        61382928,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-03T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-03",
  "open":          75.6458,
  "high":          75.76073,
  "low":           73.94505,
  "close":         74.0203,
  "volume":        102583336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-06T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-06",
  "open":          73.53731,
  "high":          74.81663,
  "low":           73.01052,
  "close":         74.42394,
  "volume":        107914176,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-07T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-07",
  "open":          74.4773,
  "high":          74.7017,
  "low":           73.60162,
  "close":         73.89169,
  "volume":        82933608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-08T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-08",
  "open":          73.72339,
  "high":          74.64697,
  "low":           73.70697,
  "close":         74.35963,
  "volume":        67537440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-09T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-09",
  "open":          74.81663,
  "high":          74.82484,
  "low":           73.24997,
  "close":         73.41006,
  "volume":        72985960,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-10T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-10",
  "open":          73.86295,
  "high":          73.99567,
  "low":           72.66982,
  "close":         72.92022,
  "volume":        79684232,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-13T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-13",
  "open":          72.50563,
  "high":          74.22828,
  "low":           72.50153,
  "close":         73.30196,
  "volume":        99041616,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-14T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-14",
  "open":          73.64266,
  "high":          74.80705,
  "low":           73.56604,
  "close":         74.76054,
  "volume":        87424696,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-15T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-15",
  "open":          75.73611,
  "high":          76.65011,
  "low":           75.48161,
  "close":         76.26151,
  "volume":        102812824,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-16T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-16",
  "open":          75.92493,
  "high":          76.19173,
  "low":           75.48434,
  "close":         75.83599,
  "volume":        60003804,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-17T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-17",
  "open":          75.45698,
  "high":          75.53771,
  "low":           73.87254,
  "close":         73.97788,
  "volume":        113205560,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-21T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-21",
  "open":          74.01072,
  "high":          75.26405,
  "low":           73.94368,
  "close":         75.12723,
  "volume":        85880408,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-22T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-22",
  "open":          75.37762,
  "high":          76.25194,
  "low":           74.95483,
  "close":         75.46108,
  "volume":        99415816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-23T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-23",
  "open":          75.24627,
  "high":          76.14384,
  "low":           74.54435,
  "close":         76.10006,
  "volume":        105428544,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-24T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-24",
  "open":          75.80178,
  "high":          76.02344,
  "low":           74.53614,
  "close":         74.71675,
  "volume":        113160976,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-27T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-27",
  "open":          75.31878,
  "high":          75.91124,
  "low":           74.67297,
  "close":         75.32289,
  "volume":        150575568,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-28T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-28",
  "open":          69.61176,
  "high":          70.46555,
  "low":           68.6964,
  "close":         69.30253,
  "volume":        278594048,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-29T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-29",
  "open":          68.96457,
  "high":          69.42157,
  "low":           68.22434,
  "close":         68.51578,
  "volume":        131493704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-30T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-30",
  "open":          68.7607,
  "high":          69.30253,
  "low":           67.96163,
  "close":         68.38306,
  "volume":        177245136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-01-31T00:  00:  00-05:  00",
  "tradingDay":    "2014-01-31",
  "open":          67.76324,
  "high":          68.62251,
  "low":           67.53063,
  "close":         68.49525,
  "volume":        121463472,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-02-03T00:  00:  00-05:  00",
  "tradingDay":    "2014-02-03",
  "open":          68.77028,
  "high":          69.47083,
  "low":           68.31738,
  "close":         68.62251,
  "volume":        105055080,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-02-04T00:  00:  00-05:  00",
  "tradingDay":    "2014-02-04",
  "open":          69.21359,
  "high":          69.70753,
  "low":           68.7908,
  "close":         69.61587,
  "volume":        98428432,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-02-05T00:  00:  00-05:  00",
  "tradingDay":    "2014-02-05",
  "open":          69.30663,
  "high":          70.50387,
  "low":           69.26833,
  "close":         70.13581,
  "volume":        85950568,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-02-06T00:  00:  00-05:  00",
  "tradingDay":    "2014-02-06",
  "open":          70.20738,
  "high":          70.68088,
  "low":           69.89767,
  "close":         70.54461,
  "volume":        66938704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-02-07T00:  00:  00-05:  00",
  "tradingDay":    "2014-02-07",
  "open":          71.72698,
  "high":          71.97887,
  "low":           71.21494,
  "close":         71.53152,
  "volume":        97183824,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-02-10T00:  00:  00-05:  00",
  "tradingDay":    "2014-02-10",
  "open":          71.30579,
  "high":          73.22594,
  "low":           71.30029,
  "close":         72.813,
  "volume":        89724072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-02-11T00:  00:  00-05:  00",
  "tradingDay":    "2014-02-11",
  "open":          73.03599,
  "high":          74.01878,
  "low":           72.8832,
  "close":         73.7724,
  "volume":        73347928,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-02-12T00:  00:  00-05:  00",
  "tradingDay":    "2014-02-12",
  "open":          73.90866,
  "high":          74.26791,
  "low":           73.39799,
  "close":         73.76688,
  "volume":        80047032,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-02-13T00:  00:  00-05:  00",
  "tradingDay":    "2014-02-13",
  "open":          73.59345,
  "high":          74.99606,
  "low":           73.53014,
  "close":         74.93825,
  "volume":        79874120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-02-14T00:  00:  00-05:  00",
  "tradingDay":    "2014-02-14",
  "open":          74.66846,
  "high":          75.1516,
  "low":           74.49503,
  "close":         74.87769,
  "volume":        71060160,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-02-18T00:  00:  00-05:  00",
  "tradingDay":    "2014-02-18",
  "open":          75.13646,
  "high":          75.86873,
  "low":           75.10067,
  "close":         75.15298,
  "volume":        67778544,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-02-19T00:  00:  00-05:  00",
  "tradingDay":    "2014-02-19",
  "open":          74.98229,
  "high":          75.27686,
  "low":           73.55078,
  "close":         73.96647,
  "volume":        81528376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-02-20T00:  00:  00-05:  00",
  "tradingDay":    "2014-02-20",
  "open":          73.33743,
  "high":          73.91554,
  "low":           72.81438,
  "close":         73.11032,
  "volume":        79426592,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-02-21T00:  00:  00-05:  00",
  "tradingDay":    "2014-02-21",
  "open":          73.31128,
  "high":          73.58107,
  "low":           72.20874,
  "close":         72.29821,
  "volume":        72398384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-02-24T00:  00:  00-05:  00",
  "tradingDay":    "2014-02-24",
  "open":          72.00916,
  "high":          72.94101,
  "low":           71.90868,
  "close":         72.61479,
  "volume":        75104616,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-02-25T00:  00:  00-05:  00",
  "tradingDay":    "2014-02-25",
  "open":          72.87219,
  "high":          72.89284,
  "low":           71.71322,
  "close":         71.85912,
  "volume":        60452468,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-02-26T00:  00:  00-05:  00",
  "tradingDay":    "2014-02-26",
  "open":          72.18121,
  "high":          72.2638,
  "low":           70.96993,
  "close":         71.21081,
  "volume":        71748168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-02-27T00:  00:  00-05:  00",
  "tradingDay":    "2014-02-27",
  "open":          71.18191,
  "high":          72.7841,
  "low":           71.03188,
  "close":         72.63131,
  "volume":        78418208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-02-28T00:  00:  00-05:  00",
  "tradingDay":    "2014-02-28",
  "open":          72.82539,
  "high":          73.33055,
  "low":           71.86738,
  "close":         72.43448,
  "volume":        96598264,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-03T00:  00:  00-05:  00",
  "tradingDay":    "2014-03-03",
  "open":          71.99126,
  "high":          73.0415,
  "low":           71.96236,
  "close":         72.6437,
  "volume":        62047872,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-04T00:  00:  00-05:  00",
  "tradingDay":    "2014-03-04",
  "open":          73.08968,
  "high":          73.31541,
  "low":           72.64508,
  "close":         73.1227,
  "volume":        67341184,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-05T00:  00:  00-05:  00",
  "tradingDay":    "2014-03-05",
  "open":          73.07866,
  "high":          73.60584,
  "low":           72.83228,
  "close":         73.27687,
  "volume":        51961080,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-06T00:  00:  00-05:  00",
  "tradingDay":    "2014-03-06",
  "open":          73.33469,
  "high":          73.56317,
  "low":           72.6905,
  "close":         73.05526,
  "volume":        48180348,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-07T00:  00:  00-05:  00",
  "tradingDay":    "2014-03-07",
  "open":          73.18465,
  "high":          73.22456,
  "low":           72.40833,
  "close":         73.01259,
  "volume":        57513028,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-10T00:  00:  00-04:  00",
  "tradingDay":    "2014-03-10",
  "open":          72.72629,
  "high":          73.41039,
  "low":           72.72354,
  "close":         73.07866,
  "volume":        46382976,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-11T00:  00:  00-04:  00",
  "tradingDay":    "2014-03-11",
  "open":          73.70219,
  "high":          74.15504,
  "low":           73.30853,
  "close":         73.79029,
  "volume":        72856808,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-12T00:  00:  00-04:  00",
  "tradingDay":    "2014-03-12",
  "open":          73.57281,
  "high":          73.96371,
  "low":           73.22732,
  "close":         73.86186,
  "volume":        52095480,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-13T00:  00:  00-04:  00",
  "tradingDay":    "2014-03-13",
  "open":          73.97611,
  "high":          74.28168,
  "low":           72.8364,
  "close":         73.0415,
  "volume":        66874768,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-14T00:  00:  00-04:  00",
  "tradingDay":    "2014-03-14",
  "open":          72.78548,
  "high":          73.07453,
  "low":           71.98851,
  "close":         72.22113,
  "volume":        61544404,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-17T00:  00:  00-04:  00",
  "tradingDay":    "2014-03-17",
  "open":          72.5735,
  "high":          72.94789,
  "low":           72.3808,
  "close":         72.5033,
  "volume":        51774368,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-18T00:  00:  00-04:  00",
  "tradingDay":    "2014-03-18",
  "open":          72.38769,
  "high":          73.22318,
  "low":           72.29133,
  "close":         73.14474,
  "volume":        54396324,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-19T00:  00:  00-04:  00",
  "tradingDay":    "2014-03-19",
  "open":          73.26311,
  "high":          73.81094,
  "low":           72.81438,
  "close":         73.12546,
  "volume":        58315816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-20T00:  00:  00-04:  00",
  "tradingDay":    "2014-03-20",
  "open":          72.95203,
  "high":          73.31953,
  "low":           72.58727,
  "close":         72.77309,
  "volume":        54071576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-21T00:  00:  00-04:  00",
  "tradingDay":    "2014-03-21",
  "open":          73.21768,
  "high":          73.4682,
  "low":           72.44687,
  "close":         73.34707,
  "volume":        97156216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-24T00:  00:  00-04:  00",
  "tradingDay":    "2014-03-24",
  "open":          74.11651,
  "high":          74.3973,
  "low":           73.64851,
  "close":         74.21699,
  "volume":        92291544,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-25T00:  00:  00-04:  00",
  "tradingDay":    "2014-03-25",
  "open":          74.53495,
  "high":          75.11994,
  "low":           74.27205,
  "close":         75.01533,
  "volume":        73245488,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-26T00:  00:  00-04:  00",
  "tradingDay":    "2014-03-26",
  "open":          75.22593,
  "high":          75.56729,
  "low":           74.17156,
  "close":         74.2982,
  "volume":        77779608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-27T00:  00:  00-04:  00",
  "tradingDay":    "2014-03-27",
  "open":          74.31472,
  "high":          74.53495,
  "low":           73.65677,
  "close":         73.97887,
  "volume":        57608928,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-28T00:  00:  00-04:  00",
  "tradingDay":    "2014-03-28",
  "open":          74.09724,
  "high":          74.18258,
  "low":           73.53702,
  "close":         73.89627,
  "volume":        52039540,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-03-31T00:  00:  00-04:  00",
  "tradingDay":    "2014-03-31",
  "open":          74.22249,
  "high":          74.43997,
  "low":           73.76826,
  "close":         73.87975,
  "volume":        43763196,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-01T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-01",
  "open":          74.02016,
  "high":          74.58588,
  "low":           73.88389,
  "close":         74.5556,
  "volume":        52089668,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-02T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-02",
  "open":          74.65607,
  "high":          74.80748,
  "low":           74.36427,
  "close":         74.67947,
  "volume":        46812340,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-03T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-03",
  "open":          74.51981,
  "high":          74.67259,
  "low":           74.00364,
  "close":         74.16193,
  "volume":        42186684,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-04T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-04",
  "open":          74.32848,
  "high":          74.32848,
  "low":           73.03186,
  "close":         73.20255,
  "volume":        71417608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-07T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-07",
  "open":          72.71115,
  "high":          73.07591,
  "low":           71.83572,
  "close":         72.0532,
  "volume":        75205600,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-08T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-08",
  "open":          72.28996,
  "high":          72.41796,
  "low":           71.39664,
  "close":         72.04907,
  "volume":        63280024,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-09T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-09",
  "open":          71.92107,
  "high":          73.01947,
  "low":           71.85362,
  "close":         72.99607,
  "volume":        53494004,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-10T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-10",
  "open":          73.04562,
  "high":          73.26035,
  "low":           72.01191,
  "close":         72.05457,
  "volume":        62180820,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-11T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-11",
  "open":          71.48198,
  "high":          71.96511,
  "low":           71.18191,
  "close":         71.52189,
  "volume":        70548704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-14T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-14",
  "open":          71.82195,
  "high":          71.87289,
  "low":           71.19154,
  "close":         71.80682,
  "volume":        53393020,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-15T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-15",
  "open":          71.61274,
  "high":          71.80132,
  "low":           70.38219,
  "close":         71.29478,
  "volume":        69145096,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-16T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-16",
  "open":          71.30717,
  "high":          71.72561,
  "low":           70.76897,
  "close":         71.43931,
  "volume":        55767236,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-17T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-17",
  "open":          71.59622,
  "high":          72.6437,
  "low":           71.46546,
  "close":         72.25554,
  "volume":        73799088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-21T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-21",
  "open":          72.3106,
  "high":          73.24659,
  "low":           72.12065,
  "close":         73.11307,
  "volume":        47397900,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-22T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-22",
  "open":          72.71941,
  "high":          73.20392,
  "low":           72.47027,
  "close":         73.18603,
  "volume":        52582240,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-23T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-23",
  "open":          72.82677,
  "high":          73.10757,
  "low":           72.1881,
  "close":         72.22939,
  "volume":        102473504,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-24T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-24",
  "open":          78.21146,
  "high":          78.45784,
  "low":           77.18186,
  "close":         78.15089,
  "volume":        197171232,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-25T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-25",
  "open":          77.70493,
  "high":          78.73175,
  "low":           77.62646,
  "close":         78.72488,
  "volume":        101263152,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-28T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-28",
  "open":          78.84325,
  "high":          82.0022,
  "low":           78.80884,
  "close":         81.77372,
  "volume":        173708752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-29T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-29",
  "open":          81.72554,
  "high":          82.03387,
  "low":           81.1433,
  "close":         81.53146,
  "volume":        87538016,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-04-30T00:  00:  00-04:  00",
  "tradingDay":    "2014-04-30",
  "open":          81.57413,
  "high":          82.50874,
  "low":           81.18322,
  "close":         81.22314,
  "volume":        118545248,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-01T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-01",
  "open":          81.48604,
  "high":          81.87144,
  "low":           80.70972,
  "close":         81.41446,
  "volume":        63363572,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-02T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-02",
  "open":          81.55624,
  "high":          81.78886,
  "low":           81.17083,
  "close":         81.56587,
  "volume":        49690752,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-05T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-05",
  "open":          81.23002,
  "high":          82.72485,
  "low":           81.21075,
  "close":         82.71935,
  "volume":        74483456,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-06T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-06",
  "open":          82.83496,
  "high":          83.19421,
  "low":           81.81776,
  "close":         81.81776,
  "volume":        97186728,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-07T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-07",
  "open":          81.93201,
  "high":          82.21418,
  "low":           80.89829,
  "close":         81.53146,
  "volume":        73392968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-08T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-08",
  "open":          81.42211,
  "high":          82.27474,
  "low":           81.16605,
  "close":         81.38612,
  "volume":        59422400,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-09T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-09",
  "open":          80.90859,
  "high":          81.14529,
  "low":           80.32587,
  "close":         81.047,
  "volume":        75239432,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-12T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-12",
  "open":          81.31692,
  "high":          82.17093,
  "low":           81.30447,
  "close":         82.05605,
  "volume":        55036288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-13T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-13",
  "open":          81.94116,
  "high":          82.29273,
  "low":           81.76123,
  "close":         82.18478,
  "volume":        41216168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-14T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-14",
  "open":          82.00068,
  "high":          82.68861,
  "low":           81.90517,
  "close":         82.2,
  "volume":        42935644,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-15T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-15",
  "open":          82.31489,
  "high":          82.57787,
  "low":           81.39304,
  "close":         81.50101,
  "volume":        59564004,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-16T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-16",
  "open":          81.47471,
  "high":          82.7066,
  "low":           81.02763,
  "close":         82.70383,
  "volume":        71309192,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-19T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-19",
  "open":          82.75089,
  "high":          84.06306,
  "low":           82.67892,
  "close":         83.6838,
  "volume":        81988736,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-20T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-20",
  "open":          83.67411,
  "high":          83.93433,
  "low":           83.14952,
  "close":         83.70041,
  "volume":        60592804,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-21T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-21",
  "open":          83.57861,
  "high":          83.97585,
  "low":           83.33361,
  "close":         83.92187,
  "volume":        50830792,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-22T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-22",
  "open":          83.96201,
  "high":          84.41185,
  "low":           83.61597,
  "close":         84.05475,
  "volume":        51830692,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-23T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-23",
  "open":          84.05198,
  "high":          85.08732,
  "low":           83.94402,
  "close":         85.00427,
  "volume":        59915848,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-27T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-27",
  "open":          85.21604,
  "high":          86.62787,
  "low":           85.21189,
  "close":         86.59603,
  "volume":        90016096,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-28T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-28",
  "open":          86.65002,
  "high":          87.17738,
  "low":           86.33997,
  "close":         86.3718,
  "volume":        81454832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-29T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-29",
  "open":          86.90331,
  "high":          88.15181,
  "low":           86.89224,
  "close":         87.94557,
  "volume":        97139648,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-05-30T00:  00:  00-04:  00",
  "tradingDay":    "2014-05-30",
  "open":          88.30544,
  "high":          89.16222,
  "low":           87.04865,
  "close":         87.61614,
  "volume":        145530656,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-02T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-02",
  "open":          87.74902,
  "high":          87.86945,
  "low":           86.1628,
  "close":         87.01405,
  "volume":        95301688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-03T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-03",
  "open":          86.98775,
  "high":          88.41064,
  "low":           86.95868,
  "close":         88.24454,
  "volume":        75581880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-04T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-04",
  "open":          88.23071,
  "high":          89.67713,
  "low":           88.04661,
  "close":         89.2522,
  "volume":        86562696,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-05T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-05",
  "open":          89.44321,
  "high":          89.88198,
  "low":           88.9463,
  "close":         89.60239,
  "volume":        78388672,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-06T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-06",
  "open":          89.95535,
  "high":          90.14359,
  "low":           89.20375,
  "close":         89.35601,
  "volume":        90432960,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-09T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-09",
  "open":          89.81693,
  "high":          90.96023,
  "low":           88.89648,
  "close":         90.78582,
  "volume":        77835576,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-10T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-10",
  "open":          91.7838,
  "high":          92.09384,
  "low":           90.65987,
  "close":         91.31872,
  "volume":        64792108,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-11T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-11",
  "open":          91.20245,
  "high":          91.81286,
  "low":           90.56298,
  "close":         90.94085,
  "volume":        47147436,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-12T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-12",
  "open":          91.10556,
  "high":          91.19276,
  "low":           89.04181,
  "close":         89.41968,
  "volume":        56506104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-13T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-13",
  "open":          89.33247,
  "high":          89.56502,
  "low":           88.05353,
  "close":         88.44109,
  "volume":        56275428,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-16T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-16",
  "open":          88.66394,
  "high":          89.86537,
  "low":           88.6058,
  "close":         89.33247,
  "volume":        36702696,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-17T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-17",
  "open":          89.43906,
  "high":          89.81693,
  "low":           88.94492,
  "close":         89.21621,
  "volume":        30680498,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-18T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-18",
  "open":          89.4003,
  "high":          89.41968,
  "low":           88.50891,
  "close":         89.3131,
  "volume":        34589884,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-19T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-19",
  "open":          89.41968,
  "high":          89.42937,
  "low":           88.49922,
  "close":         89.00305,
  "volume":        36668016,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-20T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-20",
  "open":          88.99336,
  "high":          89.67159,
  "low":           88.07291,
  "close":         88.0826,
  "volume":        104136768,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-23T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-23",
  "open":          88.47984,
  "high":          88.77052,
  "low":           87.78224,
  "close":         88.00509,
  "volume":        45096864,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-24T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-24",
  "open":          87.92757,
  "high":          88.88678,
  "low":           87.38499,
  "close":         87.47219,
  "volume":        40289032,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-25T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-25",
  "open":          87.40437,
  "high":          87.87913,
  "low":           86.86179,
  "close":         87.54971,
  "volume":        38051960,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-26T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-26",
  "open":          87.55939,
  "high":          88.21825,
  "low":           87.00713,
  "close":         88.07291,
  "volume":        33676684,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-27T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-27",
  "open":          87.9954,
  "high":          89.13869,
  "low":           87.94695,
  "close":         89.11932,
  "volume":        66084088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-06-30T00:  00:  00-04:  00",
  "tradingDay":    "2014-06-30",
  "open":          89.23559,
  "high":          90.80521,
  "low":           89.2259,
  "close":         90.03977,
  "volume":        51180780,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-01T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-01",
  "open":          90.61142,
  "high":          91.14432,
  "low":           90.23355,
  "close":         90.61142,
  "volume":        39450348,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-02T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-02",
  "open":          90.94085,
  "high":          91.13463,
  "low":           90.19479,
  "close":         90.57267,
  "volume":        29378710,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-03T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-03",
  "open":          90.75676,
  "high":          91.17339,
  "low":           90.30138,
  "close":         91.10556,
  "volume":        23626510,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-07T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-07",
  "open":          91.21214,
  "high":          93.0046,
  "low":           91.17339,
  "close":         92.98523,
  "volume":        58280488,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-08T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-08",
  "open":          93.27589,
  "high":          93.78941,
  "low":           90.99899,
  "close":         92.38451,
  "volume":        67315176,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-09T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-09",
  "open":          92.47171,
  "high":          92.96584,
  "low":           91.81286,
  "close":         92.42326,
  "volume":        37605988,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-10T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-10",
  "open":          90.84396,
  "high":          92.57829,
  "low":           90.61142,
  "close":         92.08415,
  "volume":        40959384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-11T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-11",
  "open":          92.3942,
  "high":          92.90771,
  "low":           91.90975,
  "close":         92.25855,
  "volume":        35110164,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-14T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-14",
  "open":          92.87865,
  "high":          93.87661,
  "low":           92.67518,
  "close":         93.45029,
  "volume":        44184280,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-15T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-15",
  "open":          93.78941,
  "high":          93.83785,
  "low":           92.07446,
  "close":         92.35545,
  "volume":        47162920,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-16T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-16",
  "open":          93.94444,
  "high":          94.08008,
  "low":           91.79348,
  "close":         91.83224,
  "volume":        55219796,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-17T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-17",
  "open":          92.07446,
  "high":          92.31669,
  "low":           89.69097,
  "close":         90.19479,
  "volume":        59137440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-18T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-18",
  "open":          90.70832,
  "high":          91.79348,
  "low":           90.12697,
  "close":         91.49313,
  "volume":        51592072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-21T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-21",
  "open":          92.03571,
  "high":          92.04539,
  "low":           90.80521,
  "close":         91.01836,
  "volume":        40333412,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-22T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-22",
  "open":          91.73534,
  "high":          91.93881,
  "low":           91.19276,
  "close":         91.7741,
  "volume":        56968276,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-23T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-23",
  "open":          92.45233,
  "high":          94.83582,
  "low":           92.21011,
  "close":         94.16728,
  "volume":        95900304,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-24T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-24",
  "open":          94.02195,
  "high":          94.29324,
  "low":           93.42123,
  "close":         94.01226,
  "volume":        47196668,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-25T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-25",
  "open":          93.83785,
  "high":          94.79707,
  "low":           93.63439,
  "close":         94.63235,
  "volume":        44864432,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-28T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-28",
  "open":          94.77769,
  "high":          96.15353,
  "low":           94.51609,
  "close":         95.94037,
  "volume":        57093264,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-29T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-29",
  "open":          96.24073,
  "high":          96.34731,
  "low":           95.19431,
  "close":         95.32027,
  "volume":        44527864,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-30T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-30",
  "open":          95.37841,
  "high":          95.63032,
  "low":           94.63235,
  "close":         95.09743,
  "volume":        34069604,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-07-31T00:  00:  00-04:  00",
  "tradingDay":    "2014-07-31",
  "open":          94.13822,
  "high":          94.4192,
  "low":           92.36514,
  "close":         92.62673,
  "volume":        58667216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-01T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-01",
  "open":          91.94851,
  "high":          93.61501,
  "low":           91.86131,
  "close":         93.14025,
  "volume":        50068384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-04T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-04",
  "open":          93.37279,
  "high":          93.57626,
  "low":           92.21011,
  "close":         92.61704,
  "volume":        41240732,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-05T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-05",
  "open":          92.3942,
  "high":          92.70425,
  "low":           91.4253,
  "close":         92.16167,
  "volume":        57728004,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-06T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-06",
  "open":          91.80317,
  "high":          92.51047,
  "low":           91.76441,
  "close":         92.00664,
  "volume":        39796000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-07T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-07",
  "open":          92.43507,
  "high":          93.42827,
  "low":           91.62689,
  "close":         91.99691,
  "volume":        47971884,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-08T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-08",
  "open":          91.78268,
  "high":          92.32796,
  "low":           90.82844,
  "close":         92.25007,
  "volume":        42995084,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-11T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-11",
  "open":          92.76614,
  "high":          93.55486,
  "low":           92.34743,
  "close":         93.46722,
  "volume":        37572264,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-12T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-12",
  "open":          93.51591,
  "high":          94.33382,
  "low":           93.09721,
  "close":         93.44775,
  "volume":        34707472,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-13T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-13",
  "open":          93.62302,
  "high":          94.68436,
  "low":           93.51591,
  "close":         94.68436,
  "volume":        32777858,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-14T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-14",
  "open":          94.772,
  "high":          95.00569,
  "low":           94.25594,
  "close":         94.93753,
  "volume":        28874368,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-15T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-15",
  "open":          95.32702,
  "high":          95.6094,
  "low":           94.31435,
  "close":         95.40492,
  "volume":        50272548,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-18T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-18",
  "open":          95.90151,
  "high":          96.75839,
  "low":           95.40492,
  "close":         96.55391,
  "volume":        48856432,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-19T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-19",
  "open":          96.79734,
  "high":          98.03396,
  "low":           96.7097,
  "close":         97.8879,
  "volume":        71272360,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-20T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-20",
  "open":          97.81,
  "high":          98.43317,
  "low":           97.32314,
  "close":         97.92685,
  "volume":        54121504,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-21T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-21",
  "open":          97.92685,
  "high":          98.28712,
  "low":           97.47894,
  "close":         97.93658,
  "volume":        34381712,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-22T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-22",
  "open":          97.65421,
  "high":          98.80319,
  "low":           97.55684,
  "close":         98.65714,
  "volume":        45376368,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-25T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-25",
  "open":          99.13425,
  "high":          99.48479,
  "low":           98.61819,
  "close":         98.87135,
  "volume":        41357032,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-26T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-26",
  "open":          98.75451,
  "high":          98.83241,
  "low":           98.20923,
  "close":         98.23843,
  "volume":        34046704,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-27T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-27",
  "open":          98.36502,
  "high":          99.87428,
  "low":           98.05343,
  "close":         99.44585,
  "volume":        53782496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-28T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-28",
  "open":          98.92004,
  "high":          100.0788,
  "low":           98.89082,
  "close":         99.5627,
  "volume":        70307608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-08-29T00:  00:  00-04:  00",
  "tradingDay":    "2014-08-29",
  "open":          100.108,
  "high":          100.1956,
  "low":           99.51401,
  "close":         99.80612,
  "volume":        45798872,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-02T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-02",
  "open":          100.3514,
  "high":          101.0135,
  "low":           100.0203,
  "close":         100.5851,
  "volume":        55009956,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-03T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-03",
  "open":          100.3904,
  "high":          100.4877,
  "low":           95.98915,
  "close":         96.33969,
  "volume":        128805728,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-04T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-04",
  "open":          96.25205,
  "high":          97.45946,
  "low":           95.21991,
  "close":         95.54124,
  "volume":        88031824,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-05T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-05",
  "open":          96.20337,
  "high":          96.77786,
  "low":           95.72624,
  "close":         96.3689,
  "volume":        60034816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-08T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-08",
  "open":          96.69023,
  "high":          96.69996,
  "low":           95.47308,
  "close":         95.77493,
  "volume":        47607916,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-09T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-09",
  "open":          96.47601,
  "high":          100.3709,
  "low":           93.61327,
  "close":         95.41465,
  "volume":        194970368,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-10T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-10",
  "open":          95.43413,
  "high":          98.45266,
  "low":           95.1907,
  "close":         98.34555,
  "volume":        103592080,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-11T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-11",
  "open":          97.77106,
  "high":          98.77399,
  "low":           97.00182,
  "close":         98.76424,
  "volume":        64083936,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-12T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-12",
  "open":          98.55003,
  "high":          99.50427,
  "low":           98.42345,
  "close":         98.9882,
  "volume":        65826944,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-15T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-15",
  "open":          100.108,
  "high":          100.3417,
  "low":           98.77399,
  "close":         98.95898,
  "volume":        62971500,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-16T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-16",
  "open":          97.17709,
  "high":          98.59872,
  "low":           96.291,
  "close":         98.20923,
  "volume":        68714024,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-17T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-17",
  "open":          98.60844,
  "high":          99.12453,
  "low":           97.94632,
  "close":         98.91031,
  "volume":        62570972,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-18T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-18",
  "open":          99.30952,
  "high":          99.66006,
  "low":           98.89082,
  "close":         99.11478,
  "volume":        38306152,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-19T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-19",
  "open":          99.54322,
  "high":          99.66006,
  "low":           98.2287,
  "close":         98.30659,
  "volume":        72816136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-22T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-22",
  "open":          99.12453,
  "high":          99.45558,
  "low":           97.93658,
  "close":         98.40397,
  "volume":        54213216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-23T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-23",
  "open":          97.95605,
  "high":          100.2346,
  "low":           97.89764,
  "close":         99.94244,
  "volume":        65113496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-24T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-24",
  "open":          99.47507,
  "high":          100.1469,
  "low":           98.54029,
  "close":         99.07584,
  "volume":        61795904,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-25T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-25",
  "open":          97.86842,
  "high":          98.06317,
  "low":           95.15175,
  "close":         95.29781,
  "volume":        102793496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-26T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-26",
  "open":          95.94046,
  "high":          98.10212,
  "low":           95.81388,
  "close":         98.10212,
  "volume":        64053948,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-29T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-29",
  "open":          96.05731,
  "high":          97.79053,
  "low":           96.03783,
  "close":         97.47894,
  "volume":        51109548,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-09-30T00:  00:  00-04:  00",
  "tradingDay":    "2014-09-30",
  "open":          98.16054,
  "high":          98.87135,
  "low":           97.8879,
  "close":         98.10212,
  "volume":        56755740,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-01T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-01",
  "open":          97.94632,
  "high":          98.04369,
  "low":           96.106,
  "close":         96.57338,
  "volume":        52881004,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-02T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-02",
  "open":          96.66101,
  "high":          97.58604,
  "low":           95.46334,
  "close":         97.27446,
  "volume":        49046836,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-03T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-03",
  "open":          96.82655,
  "high":          97.57631,
  "low":           96.43706,
  "close":         97.00182,
  "volume":        44642788,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-06T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-06",
  "open":          97.32314,
  "high":          98.00475,
  "low":           96.80707,
  "close":         97.00182,
  "volume":        38051148,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-07T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-07",
  "open":          96.81681,
  "high":          97.48868,
  "low":           96.13521,
  "close":         96.15468,
  "volume":        43230264,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-08T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-08",
  "open":          96.16442,
  "high":          98.45266,
  "low":           95.72624,
  "close":         98.1508,
  "volume":        58954012,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-09T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-09",
  "open":          98.87135,
  "high":          99.68928,
  "low":           97.9658,
  "close":         98.36502,
  "volume":        79464968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-10T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-10",
  "open":          98.04369,
  "high":          99.34847,
  "low":           97.66395,
  "close":         98.08265,
  "volume":        68121856,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-13T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-13",
  "open":          98.66688,
  "high":          99.10505,
  "low":           97.18682,
  "close":         97.18682,
  "volume":        55029572,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-14T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-14",
  "open":          97.75158,
  "high":          97.87816,
  "low":           95.97941,
  "close":         96.15468,
  "volume":        65407520,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-15T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-15",
  "open":          95.39518,
  "high":          96.54417,
  "low":           92.6785,
  "close":         94.97649,
  "volume":        103657904,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-16T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-16",
  "open":          93.03879,
  "high":          95.15175,
  "low":           92.90247,
  "close":         93.73013,
  "volume":        74102024,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-17T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-17",
  "open":          94.93753,
  "high":          96.39811,
  "low":           94.26566,
  "close":         95.10307,
  "volume":        70019840,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-20T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-20",
  "open":          95.78467,
  "high":          97.33288,
  "low":           95.63861,
  "close":         97.13814,
  "volume":        79609472,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-21T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-21",
  "open":          100.3125,
  "high":          100.3125,
  "low":           98.60844,
  "close":         99.77692,
  "volume":        97177904,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-22T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-22",
  "open":          100.1372,
  "high":          101.3738,
  "low":           99.9035,
  "close":         100.2832,
  "volume":        70105600,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-23T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-23",
  "open":          101.3446,
  "high":          102.2891,
  "low":           100.9064,
  "close":         102.0749,
  "volume":        72992984,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-24T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-24",
  "open":          102.4157,
  "high":          102.7175,
  "low":           101.7828,
  "close":         102.4546,
  "volume":        48323936,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-27T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-27",
  "open":          102.0944,
  "high":          102.7078,
  "low":           101.9483,
  "close":         102.3475,
  "volume":        35110464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-28T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-28",
  "open":          102.6299,
  "high":          103.9347,
  "low":           102.5812,
  "close":         103.9347,
  "volume":        49358116,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-29T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-29",
  "open":          103.8471,
  "high":          104.5481,
  "low":           103.5647,
  "close":         104.5189,
  "volume":        54109900,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-30T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-30",
  "open":          104.1489,
  "high":          104.5287,
  "low":           103.1168,
  "close":         104.1684,
  "volume":        41752016,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-10-31T00:  00:  00-04:  00",
  "tradingDay":    "2014-10-31",
  "open":          105.1713,
  "high":          105.2005,
  "low":           104.3923,
  "close":         105.1616,
  "volume":        45844060,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-11-03T00:  00:  00-05:  00",
  "tradingDay":    "2014-11-03",
  "open":          105.3758,
  "high":          107.4011,
  "low":           105.1713,
  "close":         106.5248,
  "volume":        53693660,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-11-04T00:  00:  00-05:  00",
  "tradingDay":    "2014-11-04",
  "open":          106.4858,
  "high":          106.6124,
  "low":           104.8889,
  "close":         105.7458,
  "volume":        42696436,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-11-05T00:  00:  00-05:  00",
  "tradingDay":    "2014-11-05",
  "open":          106.2327,
  "high":          106.4274,
  "low":           105.2882,
  "close":         105.999,
  "volume":        38446336,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-11-06T00:  00:  00-05:  00",
  "tradingDay":    "2014-11-06",
  "open":          106.1261,
  "high":          106.3901,
  "low":           105.422,
  "close":         106.3021,
  "volume":        35757184,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-11-07T00:  00:  00-05:  00",
  "tradingDay":    "2014-11-07",
  "open":          106.351,
  "high":          106.9085,
  "low":           106.1554,
  "close":         106.6053,
  "volume":        34451484,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-11-10T00:  00:  00-05:  00",
  "tradingDay":    "2014-11-10",
  "open":          106.6151,
  "high":          106.9182,
  "low":           106.2728,
  "close":         106.4293,
  "volume":        27808952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-11-11T00:  00:  00-05:  00",
  "tradingDay":    "2014-11-11",
  "open":          106.3119,
  "high":          107.329,
  "low":           106.0088,
  "close":         107.2801,
  "volume":        28061216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-11-12T00:  00:  00-05:  00",
  "tradingDay":    "2014-11-12",
  "open":          106.9671,
  "high":          108.9719,
  "low":           106.9574,
  "close":         108.7959,
  "volume":        48001284,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-11-13T00:  00:  00-05:  00",
  "tradingDay":    "2014-11-13",
  "open":          109.3337,
  "high":          110.9473,
  "low":           109.1382,
  "close":         110.3312,
  "volume":        60865460,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-11-14T00:  00:  00-05:  00",
  "tradingDay":    "2014-11-14",
  "open":          110.654,
  "high":          111.671,
  "low":           110.5562,
  "close":         111.6612,
  "volume":        45057444,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-11-17T00:  00:  00-05:  00",
  "tradingDay":    "2014-11-17",
  "open":          111.7493,
  "high":          114.6929,
  "low":           110.8007,
  "close":         111.4754,
  "volume":        47801168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-11-18T00:  00:  00-05:  00",
  "tradingDay":    "2014-11-18",
  "open":          111.4265,
  "high":          113.1379,
  "low":           111.3776,
  "close":         112.9228,
  "volume":        45221464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-11-19T00:  00:  00-05:  00",
  "tradingDay":    "2014-11-19",
  "open":          112.8935,
  "high":          113.1868,
  "low":           111.2896,
  "close":         112.1404,
  "volume":        42813544,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-11-20T00:  00:  00-05:  00",
  "tradingDay":    "2014-11-20",
  "open":          112.3751,
  "high":          114.2821,
  "low":           112.3165,
  "close":         113.7443,
  "volume":        44374376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-11-21T00:  00:  00-05:  00",
  "tradingDay":    "2014-11-21",
  "open":          114.9178,
  "high":          114.9765,
  "low":           113.4704,
  "close":         113.9007,
  "volume":        58469100,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-11-24T00:  00:  00-05:  00",
  "tradingDay":    "2014-11-24",
  "open":          114.2919,
  "high":          116.15,
  "low":           114.0474,
  "close":         116.0131,
  "volume":        48521152,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-11-25T00:  00:  00-05:  00",
  "tradingDay":    "2014-11-25",
  "open":          116.4434,
  "high":          117.1084,
  "low":           114.8591,
  "close":         115.0058,
  "volume":        70393240,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-11-26T00:  00:  00-05:  00",
  "tradingDay":    "2014-11-26",
  "open":          115.3383,
  "high":          116.4727,
  "low":           115.2307,
  "close":         116.3749,
  "volume":        41752848,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-11-28T00:  00:  00-05:  00",
  "tradingDay":    "2014-11-28",
  "open":          116.639,
  "high":          116.7661,
  "low":           115.4459,
  "close":         116.3065,
  "volume":        25374140,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-01T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-01",
  "open":          116.1793,
  "high":          116.6194,
  "low":           108.8154,
  "close":         112.5316,
  "volume":        85704600,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-02T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-02",
  "open":          110.9962,
  "high":          113.1966,
  "low":           110.2628,
  "close":         112.1013,
  "volume":        60687640,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-03T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-03",
  "open":          113.1966,
  "high":          113.7834,
  "low":           112.5707,
  "close":         113.3726,
  "volume":        44034784,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-04T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-04",
  "open":          113.2162,
  "high":          114.6146,
  "low":           112.7468,
  "close":         112.9423,
  "volume":        43106612,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-05T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-05",
  "open":          113.4313,
  "high":          113.5193,
  "low":           112.1111,
  "close":         112.4632,
  "volume":        39183160,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-08T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-08",
  "open":          111.583,
  "high":          112.1209,
  "low":           109.1577,
  "close":         109.9205,
  "volume":        58965552,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-09T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-09",
  "open":          107.7593,
  "high":          111.7786,
  "low":           106.9378,
  "close":         111.6026,
  "volume":        61566116,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-10T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-10",
  "open":          111.8862,
  "high":          112.3165,
  "low":           109.0795,
  "close":         109.4804,
  "volume":        45570564,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-11T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-11",
  "open":          109.7836,
  "high":          111.2896,
  "low":           108.8839,
  "close":         109.1577,
  "volume":        42406976,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-12T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-12",
  "open":          108.0233,
  "high":          109.4022,
  "low":           107.1627,
  "close":         107.3094,
  "volume":        57291932,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-15T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-15",
  "open":          108.258,
  "high":          109.1382,
  "low":           104.004,
  "close":         105.8327,
  "volume":        68734240,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-16T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-16",
  "open":          104.0235,
  "high":          107.7299,
  "low":           103.916,
  "close":         104.3951,
  "volume":        62047948,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-17T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-17",
  "open":          104.757,
  "high":          107.417,
  "low":           104.4636,
  "close":         106.9965,
  "volume":        54616512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-18T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-18",
  "open":          109.4022,
  "high":          110.165,
  "low":           108.2189,
  "close":         110.165,
  "volume":        60337208,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-19T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-19",
  "open":          109.7836,
  "high":          110.742,
  "low":           109.1968,
  "close":         109.3142,
  "volume":        90424416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-22T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-22",
  "open":          109.6858,
  "high":          110.9865,
  "low":           109.5,
  "close":         110.4486,
  "volume":        46186348,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-23T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-23",
  "open":          110.7322,
  "high":          110.83,
  "low":           109.9792,
  "close":         110.0574,
  "volume":        26615526,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-24T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-24",
  "open":          110.0965,
  "high":          110.2237,
  "low":           109.5391,
  "close":         109.5391,
  "volume":        14806218,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-26T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-26",
  "open":          109.6271,
  "high":          111.9937,
  "low":           109.5391,
  "close":         111.4754,
  "volume":        34481544,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-29T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-29",
  "open":          111.2799,
  "high":          112.2382,
  "low":           111.1918,
  "close":         111.3972,
  "volume":        28221452,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-30T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-30",
  "open":          111.1332,
  "high":          111.407,
  "low":           109.6369,
  "close":         110.0379,
  "volume":        30555438,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2014-12-31T00:  00:  00-05:  00",
  "tradingDay":    "2014-12-31",
  "open":          110.3312,
  "high":          110.6344,
  "low":           107.7788,
  "close":         107.9451,
  "volume":        42337240,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-01-02T00:  00:  00-05:  00",
  "tradingDay":    "2015-01-02",
  "open":          108.9328,
  "high":          108.9817,
  "low":           104.9819,
  "close":         106.9182,
  "volume":        54404740,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-01-05T00:  00:  00-05:  00",
  "tradingDay":    "2015-01-05",
  "open":          105.9012,
  "high":          106.2532,
  "low":           103.0847,
  "close":         103.9062,
  "volume":        65735492,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-01-06T00:  00:  00-05:  00",
  "tradingDay":    "2015-01-06",
  "open":          104.3169,
  "high":          105.0602,
  "low":           102.3219,
  "close":         103.916,
  "volume":        67281288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-01-07T00:  00:  00-05:  00",
  "tradingDay":    "2015-01-07",
  "open":          104.8352,
  "high":          105.8132,
  "low":           104.3365,
  "close":         105.3731,
  "volume":        41010572,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-01-08T00:  00:  00-05:  00",
  "tradingDay":    "2015-01-08",
  "open":          106.8204,
  "high":          109.676,
  "low":           106.3021,
  "close":         109.4218,
  "volume":        60703592,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-01-09T00:  00:  00-05:  00",
  "tradingDay":    "2015-01-09",
  "open":          110.1846,
  "high":          110.7518,
  "low":           107.7788,
  "close":         109.5391,
  "volume":        54910804,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-01-12T00:  00:  00-05:  00",
  "tradingDay":    "2015-01-12",
  "open":          110.1161,
  "high":          110.1454,
  "low":           106.3999,
  "close":         106.84,
  "volume":        50770676,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-01-13T00:  00:  00-05:  00",
  "tradingDay":    "2015-01-13",
  "open":          108.9719,
  "high":          110.3117,
  "low":           106.5075,
  "close":         107.7886,
  "volume":        68605296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-01-14T00:  00:  00-05:  00",
  "tradingDay":    "2015-01-14",
  "open":          106.6346,
  "high":          108.0526,
  "low":           106.1065,
  "close":         107.3779,
  "volume":        50060816,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-01-15T00:  00:  00-05:  00",
  "tradingDay":    "2015-01-15",
  "open":          107.5735,
  "high":          107.6321,
  "low":           104.3071,
  "close":         104.4636,
  "volume":        61367640,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-01-16T00:  00:  00-05:  00",
  "tradingDay":    "2015-01-16",
  "open":          104.669,
  "high":          105.2068,
  "low":           102.8793,
  "close":         103.6519,
  "volume":        80284328,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-01-20T00:  00:  00-05:  00",
  "tradingDay":    "2015-01-20",
  "open":          105.4611,
  "high":          106.5662,
  "low":           104.1507,
  "close":         106.3217,
  "volume":        51025496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-01-21T00:  00:  00-05:  00",
  "tradingDay":    "2015-01-21",
  "open":          106.5466,
  "high":          108.6101,
  "low":           105.8816,
  "close":         107.1334,
  "volume":        49671632,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-01-22T00:  00:  00-05:  00",
  "tradingDay":    "2015-01-22",
  "open":          107.8277,
  "high":          109.989,
  "low":           107.2996,
  "close":         109.9205,
  "volume":        55009892,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-01-23T00:  00:  00-05:  00",
  "tradingDay":    "2015-01-23",
  "open":          109.8423,
  "high":          111.2407,
  "low":           109.0697,
  "close":         110.4877,
  "volume":        47512912,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-01-26T00:  00:  00-05:  00",
  "tradingDay":    "2015-01-26",
  "open":          111.2309,
  "high":          111.8373,
  "low":           110.3117,
  "close":         110.6051,
  "volume":        56869412,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-01-27T00:  00:  00-05:  00",
  "tradingDay":    "2015-01-27",
  "open":          109.9401,
  "high":          109.9987,
  "low":           106.6249,
  "close":         106.7324,
  "volume":        97724456,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-01-28T00:  00:  00-05:  00",
  "tradingDay":    "2015-01-28",
  "open":          115.0351,
  "high":          115.5143,
  "low":           112.7663,
  "close":         112.7663,
  "volume":        149781088,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-01-29T00:  00:  00-05:  00",
  "tradingDay":    "2015-01-29",
  "open":          113.754,
  "high":          116.5607,
  "low":           113.0108,
  "close":         116.2771,
  "volume":        86341040,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-01-30T00:  00:  00-05:  00",
  "tradingDay":    "2015-01-30",
  "open":          115.7882,
  "high":          117.3529,
  "low":           114.2723,
  "close":         114.5755,
  "volume":        85634456,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-02-02T00:  00:  00-05:  00",
  "tradingDay":    "2015-02-02",
  "open":          115.4459,
  "high":          116.5412,
  "low":           113.5193,
  "close":         116.0131,
  "volume":        64154312,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-02-03T00:  00:  00-05:  00",
  "tradingDay":    "2015-02-03",
  "open":          115.8859,
  "high":          116.4629,
  "low":           115.0156,
  "close":         116.0326,
  "volume":        53086768,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-02-04T00:  00:  00-05:  00",
  "tradingDay":    "2015-02-04",
  "open":          115.8859,
  "high":          117.8516,
  "low":           115.7001,
  "close":         116.9226,
  "volume":        71732072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-02-05T00:  00:  00-05:  00",
  "tradingDay":    "2015-02-05",
  "open":          117.8356,
  "high":          118.0418,
  "low":           117.0797,
  "close":         117.7571,
  "volume":        43029332,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-02-06T00:  00:  00-05:  00",
  "tradingDay":    "2015-02-06",
  "open":          117.8356,
  "high":          118.0615,
  "low":           116.2942,
  "close":         116.7655,
  "volume":        44516700,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-02-09T00:  00:  00-05:  00",
  "tradingDay":    "2015-02-09",
  "open":          116.3924,
  "high":          117.6589,
  "low":           116.2746,
  "close":         117.5411,
  "volume":        39610608,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-02-10T00:  00:  00-05:  00",
  "tradingDay":    "2015-02-10",
  "open":          117.9829,
  "high":          119.9269,
  "low":           117.9731,
  "close":         119.7992,
  "volume":        63157972,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-02-11T00:  00:  00-05:  00",
  "tradingDay":    "2015-02-11",
  "open":          120.5356,
  "high":          122.6465,
  "low":           120.2705,
  "close":         122.6072,
  "volume":        74925440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-02-12T00:  00:  00-05:  00",
  "tradingDay":    "2015-02-12",
  "open":          123.7657,
  "high":          125.1599,
  "low":           123.2846,
  "close":         124.1584,
  "volume":        75854952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-02-13T00:  00:  00-05:  00",
  "tradingDay":    "2015-02-13",
  "open":          124.9635,
  "high":          124.9635,
  "low":           123.3632,
  "close":         124.7672,
  "volume":        55278260,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-02-17T00:  00:  00-05:  00",
  "tradingDay":    "2015-02-17",
  "open":          125.1697,
  "high":          126.5344,
  "low":           124.6101,
  "close":         125.5035,
  "volume":        64323076,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-02-18T00:  00:  00-05:  00",
  "tradingDay":    "2015-02-18",
  "open":          125.3071,
  "high":          126.4362,
  "low":           125.1304,
  "close":         126.3675,
  "volume":        45723872,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-02-19T00:  00:  00-05:  00",
  "tradingDay":    "2015-02-19",
  "open":          126.1417,
  "high":          126.6817,
  "low":           125.9944,
  "close":         126.1122,
  "volume":        38054896,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-02-20T00:  00:  00-05:  00",
  "tradingDay":    "2015-02-20",
  "open":          126.2791,
  "high":          127.1431,
  "low":           125.7195,
  "close":         127.1333,
  "volume":        49855772,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-02-23T00:  00:  00-05:  00",
  "tradingDay":    "2015-02-23",
  "open":          127.6536,
  "high":          130.5794,
  "low":           127.3002,
  "close":         130.5794,
  "volume":        72289760,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-02-24T00:  00:  00-05:  00",
  "tradingDay":    "2015-02-24",
  "open":          130.5205,
  "high":          131.1685,
  "low":           128.7827,
  "close":         129.7645,
  "volume":        70511400,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-02-25T00:  00:  00-05:  00",
  "tradingDay":    "2015-02-25",
  "open":          129.1656,
  "high":          129.2049,
  "low":           125.8177,
  "close":         126.446,
  "volume":        76096648,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-02-26T00:  00:  00-05:  00",
  "tradingDay":    "2015-02-26",
  "open":          126.446,
  "high":          128.4882,
  "low":           124.3057,
  "close":         128.0365,
  "volume":        92979728,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-02-27T00:  00:  00-05:  00",
  "tradingDay":    "2015-02-27",
  "open":          127.634,
  "high":          128.1936,
  "low":           125.906,
  "close":         126.122,
  "volume":        63164388,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-02T00:  00:  00-05:  00",
  "tradingDay":    "2015-03-02",
  "open":          126.8977,
  "high":          127.9089,
  "low":           125.965,
  "close":         126.7406,
  "volume":        48988180,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-03T00:  00:  00-05:  00",
  "tradingDay":    "2015-03-03",
  "open":          126.6129,
  "high":          127.1628,
  "low":           125.7588,
  "close":         127.0057,
  "volume":        38517212,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-04T00:  00:  00-05:  00",
  "tradingDay":    "2015-03-04",
  "open":          126.7504,
  "high":          127.202,
  "low":           125.9846,
  "close":         126.2006,
  "volume":        32253308,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-05T00:  00:  00-05:  00",
  "tradingDay":    "2015-03-05",
  "open":          126.2399,
  "high":          126.4068,
  "low":           123.4712,
  "close":         124.1094,
  "volume":        57564776,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-06T00:  00:  00-05:  00",
  "tradingDay":    "2015-03-06",
  "open":          126.0631,
  "high":          127.0155,
  "low":           123.9621,
  "close":         124.2959,
  "volume":        74192296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-09T00:  00:  00-04:  00",
  "tradingDay":    "2015-03-09",
  "open":          125.6311,
  "high":          127.2118,
  "low":           122.7839,
  "close":         124.8261,
  "volume":        90169480,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-10T00:  00:  00-04:  00",
  "tradingDay":    "2015-03-10",
  "open":          124.1094,
  "high":          124.9046,
  "low":           121.5469,
  "close":         122.2439,
  "volume":        70132912,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-11T00:  00:  00-04:  00",
  "tradingDay":    "2015-03-11",
  "open":          122.4599,
  "high":          122.4992,
  "low":           119.8876,
  "close":         120.0152,
  "volume":        70216840,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-12T00:  00:  00-04:  00",
  "tradingDay":    "2015-03-12",
  "open":          120.084,
  "high":          122.6268,
  "low":           119.4163,
  "close":         122.185,
  "volume":        49259212,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-13T00:  00:  00-04:  00",
  "tradingDay":    "2015-03-13",
  "open":          122.1359,
  "high":          123.1177,
  "low":           120.3491,
  "close":         121.3407,
  "volume":        52787936,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-16T00:  00:  00-04:  00",
  "tradingDay":    "2015-03-16",
  "open":          121.6254,
  "high":          122.6759,
  "low":           120.6338,
  "close":         122.6759,
  "volume":        36539312,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-17T00:  00:  00-04:  00",
  "tradingDay":    "2015-03-17",
  "open":          123.6086,
  "high":          125.0028,
  "low":           123.3632,
  "close":         124.7279,
  "volume":        51968932,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-18T00:  00:  00-04:  00",
  "tradingDay":    "2015-03-18",
  "open":          124.6886,
  "high":          126.8093,
  "low":           124.0701,
  "close":         126.1319,
  "volume":        66480848,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-19T00:  00:  00-04:  00",
  "tradingDay":    "2015-03-19",
  "open":          126.4068,
  "high":          126.8977,
  "low":           125.0813,
  "close":         125.1795,
  "volume":        46658584,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-20T00:  00:  00-04:  00",
  "tradingDay":    "2015-03-20",
  "open":          125.9355,
  "high":          126.0631,
  "low":           122.8821,
  "close":         123.6086,
  "volume":        69968528,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-23T00:  00:  00-04:  00",
  "tradingDay":    "2015-03-23",
  "open":          124.8064,
  "high":          125.5231,
  "low":           124.2173,
  "close":         124.8948,
  "volume":        38408636,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-24T00:  00:  00-04:  00",
  "tradingDay":    "2015-03-24",
  "open":          124.9144,
  "high":          125.7097,
  "low":           124.2566,
  "close":         124.3843,
  "volume":        33451108,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-25T00:  00:  00-04:  00",
  "tradingDay":    "2015-03-25",
  "open":          124.237,
  "high":          124.5119,
  "low":           121.1345,
  "close":         121.1345,
  "volume":        52612644,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-26T00:  00:  00-04:  00",
  "tradingDay":    "2015-03-26",
  "open":          120.5258,
  "high":          122.6072,
  "low":           120.3687,
  "close":         121.9788,
  "volume":        48454672,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-27T00:  00:  00-04:  00",
  "tradingDay":    "2015-03-27",
  "open":          122.3421,
  "high":          122.4305,
  "low":           120.673,
  "close":         121.0069,
  "volume":        40279176,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-30T00:  00:  00-04:  00",
  "tradingDay":    "2015-03-30",
  "open":          121.7923,
  "high":          124.0995,
  "low":           121.7432,
  "close":         124.0701,
  "volume":        47972700,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-03-31T00:  00:  00-04:  00",
  "tradingDay":    "2015-03-31",
  "open":          123.7952,
  "high":          124.1879,
  "low":           122.0967,
  "close":         122.1654,
  "volume":        42870744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-01T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-01",
  "open":          122.5483,
  "high":          122.8428,
  "low":           120.8596,
  "close":         121.9887,
  "volume":        41374412,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-02T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-02",
  "open":          122.7545,
  "high":          123.2748,
  "low":           121.9298,
  "close":         123.0392,
  "volume":        32817374,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-06T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-06",
  "open":          122.2047,
  "high":          125.1893,
  "low":           122.0672,
  "close":         125.0322,
  "volume":        37883376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-07T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-07",
  "open":          125.317,
  "high":          125.7882,
  "low":           123.6872,
  "close":         123.7166,
  "volume":        35661232,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-08T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-08",
  "open":          123.5595,
  "high":          124.0995,
  "low":           122.6956,
  "close":         123.3141,
  "volume":        38021184,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-09T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-09",
  "open":          123.5595,
  "high":          124.2763,
  "low":           122.3912,
  "close":         124.2566,
  "volume":        33086064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-10T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-10",
  "open":          123.6577,
  "high":          124.8948,
  "low":           122.9803,
  "close":         124.7868,
  "volume":        40932876,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-13T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-13",
  "open":          126.0337,
  "high":          126.23,
  "low":           124.3057,
  "close":         124.5413,
  "volume":        37039212,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-14T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-14",
  "open":          124.6886,
  "high":          124.9733,
  "low":           123.6185,
  "close":         124.0014,
  "volume":        25997656,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-15T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-15",
  "open":          124.1094,
  "high":          124.8162,
  "low":           123.7166,
  "close":         124.4726,
  "volume":        29507432,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-16T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-16",
  "open":          123.9817,
  "high":          124.7868,
  "low":           123.8148,
  "close":         123.8737,
  "volume":        28894782,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-17T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-17",
  "open":          123.265,
  "high":          123.8443,
  "low":           122.1948,
  "close":         122.4796,
  "volume":        52920144,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-20T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-20",
  "open":          123.265,
  "high":          125.7882,
  "low":           122.8919,
  "close":         125.2777,
  "volume":        47926560,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-21T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-21",
  "open":          125.7686,
  "high":          125.8668,
  "low":           124.3646,
  "close":         124.6003,
  "volume":        33036258,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-22T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-22",
  "open":          124.6886,
  "high":          126.5246,
  "low":           124.021,
  "close":         126.2791,
  "volume":        38352512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-23T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-23",
  "open":          125.965,
  "high":          128.0464,
  "low":           125.8079,
  "close":         127.31,
  "volume":        46619368,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-24T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-24",
  "open":          128.1151,
  "high":          128.2525,
  "low":           126.878,
  "close":         127.9089,
  "volume":        45351288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-27T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-27",
  "open":          129.902,
  "high":          130.707,
  "low":           128.7631,
  "close":         130.2358,
  "volume":        98751472,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-28T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-28",
  "open":          132.0128,
  "high":          132.0914,
  "low":           127.2118,
  "close":         128.1838,
  "volume":        121128432,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-29T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-29",
  "open":          127.7911,
  "high":          129.1951,
  "low":           125.965,
  "close":         126.2988,
  "volume":        64561004,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-04-30T00:  00:  00-04:  00",
  "tradingDay":    "2015-04-30",
  "open":          125.1795,
  "high":          125.5526,
  "low":           122.3127,
  "close":         122.8723,
  "volume":        84737616,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-05-01T00:  00:  00-04:  00",
  "tradingDay":    "2015-05-01",
  "open":          123.805,
  "high":          127.7617,
  "low":           123.0196,
  "close":         126.6031,
  "volume":        59597264,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-05-04T00:  00:  00-04:  00",
  "tradingDay":    "2015-05-04",
  "open":          127.1431,
  "high":          128.1936,
  "low":           125.9257,
  "close":         126.3577,
  "volume":        51933384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-05-05T00:  00:  00-04:  00",
  "tradingDay":    "2015-05-05",
  "open":          125.8177,
  "high":          126.1122,
  "low":           123.4908,
  "close":         123.5105,
  "volume":        50184760,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-05-06T00:  00:  00-04:  00",
  "tradingDay":    "2015-05-06",
  "open":          124.2566,
  "high":          124.4432,
  "low":           121.1149,
  "close":         122.7348,
  "volume":        73478296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-05-07T00:  00:  00-04:  00",
  "tradingDay":    "2015-05-07",
  "open":          123.0109,
  "high":          124.3024,
  "low":           122.2715,
  "close":         123.494,
  "volume":        44569176,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-05-08T00:  00:  00-04:  00",
  "tradingDay":    "2015-05-08",
  "open":          124.894,
  "high":          125.8207,
  "low":           124.332,
  "close":         125.8207,
  "volume":        56344696,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-05-11T00:  00:  00-04:  00",
  "tradingDay":    "2015-05-11",
  "open":          125.5939,
  "high":          125.7615,
  "low":           123.8588,
  "close":         124.539,
  "volume":        42636832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-05-12T00:  00:  00-04:  00",
  "tradingDay":    "2015-05-12",
  "open":          123.8292,
  "high":          125.0911,
  "low":           123.0602,
  "close":         124.0855,
  "volume":        48848712,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-05-13T00:  00:  00-04:  00",
  "tradingDay":    "2015-05-13",
  "open":          124.3714,
  "high":          125.3968,
  "low":           124.0954,
  "close":         124.2334,
  "volume":        35190344,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-05-14T00:  00:  00-04:  00",
  "tradingDay":    "2015-05-14",
  "open":          125.6137,
  "high":          127.1319,
  "low":           125.3672,
  "close":         127.1319,
  "volume":        45849832,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-05-15T00:  00:  00-04:  00",
  "tradingDay":    "2015-05-15",
  "open":          127.2503,
  "high":          127.6643,
  "low":           126.4024,
  "close":         126.9545,
  "volume":        38754392,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-05-18T00:  00:  00-04:  00",
  "tradingDay":    "2015-05-18",
  "open":          126.57,
  "high":          128.877,
  "low":           126.5503,
  "close":         128.3545,
  "volume":        51610552,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-05-19T00:  00:  00-04:  00",
  "tradingDay":    "2015-05-19",
  "open":          128.8474,
  "high":          129.0347,
  "low":           127.8122,
  "close":         128.2362,
  "volume":        45271476,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-05-20T00:  00:  00-04:  00",
  "tradingDay":    "2015-05-20",
  "open":          128.1671,
  "high":          129.1333,
  "low":           127.5164,
  "close":         128.2263,
  "volume":        36976224,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-05-21T00:  00:  00-04:  00",
  "tradingDay":    "2015-05-21",
  "open":          128.2362,
  "high":          129.7742,
  "low":           127.9995,
  "close":         129.5376,
  "volume":        40298464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-05-22T00:  00:  00-04:  00",
  "tradingDay":    "2015-05-22",
  "open":          129.7446,
  "high":          131.0953,
  "low":           129.5474,
  "close":         130.6713,
  "volume":        46247944,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-05-26T00:  00:  00-04:  00",
  "tradingDay":    "2015-05-26",
  "open":          130.7305,
  "high":          131.0361,
  "low":           127.2995,
  "close":         127.7925,
  "volume":        71708512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-05-27T00:  00:  00-04:  00",
  "tradingDay":    "2015-05-27",
  "open":          128.5023,
  "high":          130.3953,
  "low":           128.2164,
  "close":         130.1784,
  "volume":        46488636,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-05-28T00:  00:  00-04:  00",
  "tradingDay":    "2015-05-28",
  "open":          130.0009,
  "high":          130.0896,
  "low":           129.2516,
  "close":         129.922,
  "volume":        31172802,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-05-29T00:  00:  00-04:  00",
  "tradingDay":    "2015-05-29",
  "open":          129.3798,
  "high":          129.5967,
  "low":           128.0685,
  "close":         128.4432,
  "volume":        51612072,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-01T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-01",
  "open":          129.3502,
  "high":          129.5376,
  "low":           128.2164,
  "close":         128.6995,
  "volume":        32571928,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-02T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-02",
  "open":          128.0291,
  "high":          128.808,
  "low":           127.4967,
  "close":         128.1277,
  "volume":        34149064,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-03T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-03",
  "open":          128.8178,
  "high":          129.0939,
  "low":           128.0685,
  "close":         128.2854,
  "volume":        31426580,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-04T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-04",
  "open":          127.7531,
  "high":          128.739,
  "low":           127.0925,
  "close":         127.5362,
  "volume":        38999956,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-05T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-05",
  "open":          127.6939,
  "high":          127.8615,
  "low":           126.5503,
  "close":         126.8362,
  "volume":        36136280,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-08T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-08",
  "open":          127.0826,
  "high":          127.3883,
  "low":           125.0418,
  "close":         125.9982,
  "volume":        53427976,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-09T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-09",
  "open":          124.9137,
  "high":          126.2742,
  "low":           123.8489,
  "close":         125.6235,
  "volume":        56877308,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-10T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-10",
  "open":          126.1362,
  "high":          127.5164,
  "low":           126.0475,
  "close":         127.0629,
  "volume":        39646168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-11T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-11",
  "open":          127.3587,
  "high":          128.3446,
  "low":           126.6686,
  "close":         126.777,
  "volume":        35896908,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-12T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-12",
  "open":          126.3728,
  "high":          126.5207,
  "low":           125.3179,
  "close":         125.377,
  "volume":        37344516,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-15T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-15",
  "open":          124.3221,
  "high":          125.4461,
  "low":           123.9376,
  "close":         125.1306,
  "volume":        44617964,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-16T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-16",
  "open":          125.2292,
  "high":          126.0475,
  "low":           124.5883,
  "close":         125.801,
  "volume":        31944482,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-17T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-17",
  "open":          125.9193,
  "high":          126.077,
  "low":           124.9531,
  "close":         125.5052,
  "volume":        33388744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-18T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-18",
  "open":          125.4362,
  "high":          126.501,
  "low":           125.4263,
  "close":         126.077,
  "volume":        35913540,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-19T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-19",
  "open":          125.9489,
  "high":          126.0179,
  "low":           124.6179,
  "close":         124.8151,
  "volume":        55499276,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-22T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-22",
  "open":          125.6925,
  "high":          126.2545,
  "low":           125.2883,
  "close":         125.8108,
  "volume":        34526080,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-23T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-23",
  "open":          125.6827,
  "high":          125.8108,
  "low":           125.0911,
  "close":         125.239,
  "volume":        30701660,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-24T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-24",
  "open":          125.4165,
  "high":          127.97,
  "low":           125.3278,
  "close":         126.3038,
  "volume":        56071344,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-25T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-25",
  "open":          127.0432,
  "high":          127.3784,
  "low":           125.7024,
  "close":         125.7024,
  "volume":        32394830,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-26T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-26",
  "open":          125.87,
  "high":          126.1855,
  "low":           124.7263,
  "close":         124.963,
  "volume":        44696976,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-29T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-29",
  "open":          123.6912,
  "high":          124.6869,
  "low":           122.725,
  "close":         122.7743,
  "volume":        49864432,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-06-30T00:  00:  00-04:  00",
  "tradingDay":    "2015-06-30",
  "open":          123.7996,
  "high":          124.3419,
  "low":           123.0996,
  "close":         123.6616,
  "volume":        45005120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-01T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-01",
  "open":          125.1109,
  "high":          125.1503,
  "low":           124.2137,
  "close":         124.8151,
  "volume":        30671230,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-02T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-02",
  "open":          124.6475,
  "high":          124.9038,
  "low":           123.9968,
  "close":         124.6573,
  "volume":        27600030,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-06T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-06",
  "open":          123.1785,
  "high":          124.4503,
  "low":           123.0898,
  "close":         124.2235,
  "volume":        28461678,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-07T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-07",
  "open":          124.1151,
  "high":          124.3714,
  "low":           122.025,
  "close":         123.9179,
  "volume":        47618164,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-08T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-08",
  "open":          122.725,
  "high":          122.8827,
  "low":           120.8123,
  "close":         120.8419,
  "volume":        61630520,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-09T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-09",
  "open":          122.1039,
  "high":          122.3109,
  "low":           117.5391,
  "close":         118.3771,
  "volume":        79718952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-10T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-10",
  "open":          120.2208,
  "high":          122.1039,
  "low":           119.5011,
  "close":         121.5419,
  "volume":        62231800,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-13T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-13",
  "open":          123.2672,
  "high":          123.9771,
  "low":           122.5672,
  "close":         123.8883,
  "volume":        42033120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-14T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-14",
  "open":          124.263,
  "high":          124.5883,
  "low":           123.2771,
  "close":         123.839,
  "volume":        32222400,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-15T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-15",
  "open":          123.9475,
  "high":          125.3573,
  "low":           123.8095,
  "close":         125.032,
  "volume":        34130400,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-16T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-16",
  "open":          125.9686,
  "high":          126.7573,
  "low":           125.5545,
  "close":         126.6982,
  "volume":        36740400,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-17T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-17",
  "open":          127.2601,
  "high":          127.7925,
  "low":           126.501,
  "close":         127.7925,
  "volume":        46824880,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-20T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-20",
  "open":          129.1235,
  "high":          131.0953,
  "low":           128.8573,
  "close":         130.208,
  "volume":        59742504,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-21T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-21",
  "open":          130.977,
  "high":          131.046,
  "low":           128.4826,
  "close":         128.9066,
  "volume":        77854056,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-22T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-22",
  "open":          120.2701,
  "high":          123.7306,
  "low":           120.2701,
  "close":         123.4545,
  "volume":        117101600,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-23T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-23",
  "open":          124.4207,
  "high":          125.2982,
  "low":           123.2968,
  "close":         123.3954,
  "volume":        51728716,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-24T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-24",
  "open":          123.5531,
  "high":          123.9672,
  "low":           122.1532,
  "close":         122.7447,
  "volume":        42765244,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-27T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-27",
  "open":          121.3546,
  "high":          121.8672,
  "low":           120.3982,
  "close":         121.0391,
  "volume":        45091236,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-28T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-28",
  "open":          121.6405,
  "high":          122.163,
  "low":           120.8222,
  "close":         121.6405,
  "volume":        34098756,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-29T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-29",
  "open":          121.4137,
  "high":          121.7588,
  "low":           120.5461,
  "close":         121.256,
  "volume":        37540884,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-30T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-30",
  "open":          120.5954,
  "high":          120.8419,
  "low":           119.994,
  "close":         120.6447,
  "volume":        34109100,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-07-31T00:  00:  00-04:  00",
  "tradingDay":    "2015-07-31",
  "open":          120.8715,
  "high":          120.9109,
  "low":           119.2053,
  "close":         119.5898,
  "volume":        43498176,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-03T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-03",
  "open":          119.787,
  "high":          120.8419,
  "low":           115.8631,
  "close":         116.7701,
  "volume":        70976592,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-04T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-04",
  "open":          115.7645,
  "high":          116.0406,
  "low":           111.6533,
  "close":         113.0237,
  "volume":        125913848,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-05T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-05",
  "open":          111.3575,
  "high":          115.7842,
  "low":           110.5195,
  "close":         113.773,
  "volume":        100732824,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-06T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-06",
  "open":          114.8525,
  "high":          115.3774,
  "low":           113.0104,
  "close":         114.0206,
  "volume":        53417744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-07T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-07",
  "open":          113.4759,
  "high":          115.1298,
  "low":           113.3967,
  "close":         114.4068,
  "volume":        39046664,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-10T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-10",
  "open":          115.4071,
  "high":          118.8337,
  "low":           115.4071,
  "close":         118.5664,
  "volume":        55486176,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-11T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-11",
  "open":          116.6748,
  "high":          117.0412,
  "low":           112.2379,
  "close":         112.3964,
  "volume":        98027416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-12T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-12",
  "open":          111.4456,
  "high":          114.3078,
  "low":           108.5736,
  "close":         114.1295,
  "volume":        102675000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-13T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-13",
  "open":          114.9218,
  "high":          115.2783,
  "low":           113.4363,
  "close":         114.0404,
  "volume":        49007952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-14T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-14",
  "open":          113.2184,
  "high":          115.1892,
  "low":           112.9114,
  "close":         114.8426,
  "volume":        43347204,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-17T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-17",
  "open":          114.9218,
  "high":          116.5163,
  "low":           114.387,
  "close":         116.031,
  "volume":        41282508,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-18T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-18",
  "open":          115.3081,
  "high":          116.3083,
  "low":           114.8921,
  "close":         115.3774,
  "volume":        34896976,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-19T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-19",
  "open":          114.9812,
  "high":          115.3972,
  "low":           113.5749,
  "close":         113.9017,
  "volume":        48756328,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-20T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-20",
  "open":          112.9807,
  "high":          113.2481,
  "low":           110.5543,
  "close":         111.5645,
  "volume":        69168120,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-21T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-21",
  "open":          109.3659,
  "high":          110.8217,
  "low":           104.622,
  "close":         104.7409,
  "volume":        129523520,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-24T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-24",
  "open":          93.95581,
  "high":          107.7516,
  "low":           91.11346,
  "close":         102.1263,
  "volume":        163784464,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-25T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-25",
  "open":          110.0393,
  "high":          110.0393,
  "low":           102.5026,
  "close":         102.7403,
  "volume":        104609640,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-26T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-26",
  "open":          106.0482,
  "high":          108.8311,
  "low":           104.0377,
  "close":         108.633,
  "volume":        97716216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-27T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-27",
  "open":          111.1485,
  "high":          112.1488,
  "low":           108.9598,
  "close":         111.8319,
  "volume":        85439312,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-28T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-28",
  "open":          111.0891,
  "high":          112.2181,
  "low":           110.4652,
  "close":         112.1983,
  "volume":        53681688,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-08-31T00:  00:  00-04:  00",
  "tradingDay":    "2015-08-31",
  "open":          110.9505,
  "high":          113.4264,
  "low":           110.9207,
  "close":         111.6734,
  "volume":        56776308,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-01T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-01",
  "open":          109.0886,
  "high":          110.8019,
  "low":           106.3255,
  "close":         106.682,
  "volume":        77593512,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-02T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-02",
  "open":          109.1678,
  "high":          111.2575,
  "low":           108.0784,
  "close":         111.2575,
  "volume":        62490976,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-03T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-03",
  "open":          111.406,
  "high":          111.6932,
  "low":           108.9796,
  "close":         109.3064,
  "volume":        53751864,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-04T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-04",
  "open":          107.9199,
  "high":          109.3857,
  "low":           107.4644,
  "close":         108.217,
  "volume":        50482764,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-08T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-08",
  "open":          110.5741,
  "high":          111.4753,
  "low":           109.2569,
  "close":         111.2278,
  "volume":        55377228,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-09T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-09",
  "open":          112.6638,
  "high":          112.9213,
  "low":           108.7122,
  "close":         109.0886,
  "volume":        85837952,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-10T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-10",
  "open":          109.2074,
  "high":          112.1884,
  "low":           108.841,
  "close":         111.4853,
  "volume":        63504744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-11T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-11",
  "open":          110.7128,
  "high":          113.1094,
  "low":           110.6831,
  "close":         113.1094,
  "volume":        50401076,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-14T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-14",
  "open":          115.4566,
  "high":          115.7636,
  "low":           113.7532,
  "close":         114.1988,
  "volume":        58931276,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-15T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-15",
  "open":          114.8129,
  "high":          115.4071,
  "low":           113.3174,
  "close":         115.1595,
  "volume":        43762808,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-16T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-16",
  "open":          115.1298,
  "high":          115.417,
  "low":           114.3276,
  "close":         115.2883,
  "volume":        37535096,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-17T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-17",
  "open":          114.5455,
  "high":          115.3675,
  "low":           112.6143,
  "close":         112.8222,
  "volume":        64736416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-18T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-18",
  "open":          111.1287,
  "high":          113.1986,
  "low":           110.792,
  "close":         112.3568,
  "volume":        75007992,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-21T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-21",
  "open":          112.5746,
  "high":          114.2583,
  "low":           112.5648,
  "close":         114.0998,
  "volume":        50710560,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-22T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-22",
  "open":          112.2874,
  "high":          113.0797,
  "low":           111.4357,
  "close":         112.3073,
  "volume":        50835968,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-23T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-23",
  "open":          112.535,
  "high":          113.6145,
  "low":           112.2082,
  "close":         113.2184,
  "volume":        36104612,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-24T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-24",
  "open":          112.1587,
  "high":          114.387,
  "low":           111.2872,
  "close":         113.8918,
  "volume":        50708032,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-25T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-25",
  "open":          115.318,
  "high":          115.5656,
  "low":           112.9213,
  "close":         113.6046,
  "volume":        56698256,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-28T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-28",
  "open":          112.7529,
  "high":          113.466,
  "low":           111.3565,
  "close":         111.3565,
  "volume":        52616020,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-29T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-29",
  "open":          111.7427,
  "high":          112.4162,
  "low":           106.8206,
  "close":         108.0091,
  "volume":        74079136,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-09-30T00:  00:  00-04:  00",
  "tradingDay":    "2015-09-30",
  "open":          109.1084,
  "high":          110.4652,
  "low":           107.6823,
  "close":         109.2371,
  "volume":        67119784,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-01T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-01",
  "open":          108.019,
  "high":          108.5637,
  "low":           106.2759,
  "close":         108.5241,
  "volume":        64551128,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-02T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-02",
  "open":          106.9692,
  "high":          109.9403,
  "low":           106.5136,
  "close":         109.3164,
  "volume":        58584232,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-05T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-05",
  "open":          108.8212,
  "high":          110.2968,
  "low":           108.019,
  "close":         109.7125,
  "volume":        52571288,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-06T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-06",
  "open":          109.5639,
  "high":          110.6632,
  "low":           108.7023,
  "close":         110.2374,
  "volume":        49332276,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-07T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-07",
  "open":          110.6632,
  "high":          110.693,
  "low":           108.3557,
  "close":         109.7125,
  "volume":        47220528,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-08T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-08",
  "open":          109.1282,
  "high":          109.1282,
  "low":           107.1673,
  "close":         108.4448,
  "volume":        62582560,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-09T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-09",
  "open":          108.94,
  "high":          111.198,
  "low":           108.4349,
  "close":         111.0396,
  "volume":        53246596,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-12T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-12",
  "open":          111.6437,
  "high":          111.6635,
  "low":           110.3661,
  "close":         110.5246,
  "volume":        30763646,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-13T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-13",
  "open":          109.7521,
  "high":          111.3664,
  "low":           109.6135,
  "close":         110.7128,
  "volume":        33370768,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-14T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-14",
  "open":          110.2176,
  "high":          110.4454,
  "low":           108.5042,
  "close":         109.148,
  "volume":        44895020,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-15T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-15",
  "open":          109.8611,
  "high":          111.0198,
  "low":           109.4253,
  "close":         110.7821,
  "volume":        38039960,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-16T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-16",
  "open":          110.7029,
  "high":          110.9207,
  "low":           109.4649,
  "close":         109.97,
  "volume":        39614332,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-19T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-19",
  "open":          109.7323,
  "high":          110.6731,
  "low":           109.049,
  "close":         110.6534,
  "volume":        30048656,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-20T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-20",
  "open":          110.2671,
  "high":          113.0698,
  "low":           109.7521,
  "close":         112.6737,
  "volume":        49444156,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-21T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-21",
  "open":          112.9015,
  "high":          114.4662,
  "low":           112.6044,
  "close":         112.6638,
  "volume":        42738740,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-22T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-22",
  "open":          113.2283,
  "high":          114.387,
  "low":           113.0005,
  "close":         114.387,
  "volume":        42059292,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-23T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-23",
  "open":          115.5754,
  "high":          118.0811,
  "low":           115.209,
  "close":         117.9325,
  "volume":        59944540,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-26T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-26",
  "open":          116.9422,
  "high":          116.9917,
  "low":           113.8126,
  "close":         114.1691,
  "volume":        66979124,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-27T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-27",
  "open":          114.288,
  "high":          115.417,
  "low":           112.8916,
  "close":         113.4462,
  "volume":        70564376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-28T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-28",
  "open":          115.8032,
  "high":          118.1504,
  "low":           114.9416,
  "close":         118.1207,
  "volume":        86383712,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-29T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-29",
  "open":          117.5562,
  "high":          119.527,
  "low":           117.1303,
  "close":         119.3685,
  "volume":        51725740,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-10-30T00:  00:  00-04:  00",
  "tradingDay":    "2015-10-30",
  "open":          119.8241,
  "high":          120.0519,
  "low":           118.299,
  "close":         118.3485,
  "volume":        49845524,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-11-02T00:  00:  00-05:  00",
  "tradingDay":    "2015-11-02",
  "open":          118.7149,
  "high":          120.1906,
  "low":           118.4574,
  "close":         120.0123,
  "volume":        32516536,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-11-03T00:  00:  00-05:  00",
  "tradingDay":    "2015-11-03",
  "open":          119.626,
  "high":          122.3,
  "low":           119.5369,
  "close":         121.3889,
  "volume":        45961800,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-11-04T00:  00:  00-05:  00",
  "tradingDay":    "2015-11-04",
  "open":          121.9435,
  "high":          122.6268,
  "low":           120.448,
  "close":         120.8244,
  "volume":        45322740,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-11-05T00:  00:  00-05:  00",
  "tradingDay":    "2015-11-05",
  "open":          121.1924,
  "high":          122.0278,
  "low":           119.5314,
  "close":         120.2674,
  "volume":        39767224,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-11-06T00:  00:  00-05:  00",
  "tradingDay":    "2015-11-06",
  "open":          120.4564,
  "high":          121.1526,
  "low":           119.969,
  "close":         120.4066,
  "volume":        33221496,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-11-09T00:  00:  00-05:  00",
  "tradingDay":    "2015-11-09",
  "open":          120.3072,
  "high":          121.1526,
  "low":           119.4021,
  "close":         119.9193,
  "volume":        34055196,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-11-10T00:  00:  00-05:  00",
  "tradingDay":    "2015-11-10",
  "open":          116.2691,
  "high":          117.4328,
  "low":           115.4336,
  "close":         116.1398,
  "volume":        59448744,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-11-11T00:  00:  00-05:  00",
  "tradingDay":    "2015-11-11",
  "open":          115.742,
  "high":          116.7863,
  "low":           114.5882,
  "close":         115.4834,
  "volume":        45463264,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-11-12T00:  00:  00-05:  00",
  "tradingDay":    "2015-11-12",
  "open":          115.6326,
  "high":          116.1895,
  "low":           115.0258,
  "close":         115.0955,
  "volume":        32701992,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-11-13T00:  00:  00-05:  00",
  "tradingDay":    "2015-11-13",
  "open":          114.5783,
  "high":          114.9463,
  "low":           111.6641,
  "close":         111.7337,
  "volume":        46060988,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-11-16T00:  00:  00-05:  00",
  "tradingDay":    "2015-11-16",
  "open":          110.7789,
  "high":          113.6235,
  "low":           110.4009,
  "close":         113.5638,
  "volume":        38313476,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-11-17T00:  00:  00-05:  00",
  "tradingDay":    "2015-11-17",
  "open":          114.2998,
  "high":          114.4291,
  "low":           112.7084,
  "close":         113.0764,
  "volume":        27766756,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-11-18T00:  00:  00-05:  00",
  "tradingDay":    "2015-11-18",
  "open":          115.1352,
  "high":          116.8559,
  "low":           114.8766,
  "close":         116.657,
  "volume":        46927868,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-11-19T00:  00:  00-05:  00",
  "tradingDay":    "2015-11-19",
  "open":          117.0051,
  "high":          119.1037,
  "low":           116.1299,
  "close":         118.1389,
  "volume":        43530732,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-11-20T00:  00:  00-05:  00",
  "tradingDay":    "2015-11-20",
  "open":          118.5567,
  "high":          119.2728,
  "low":           118.2086,
  "close":         118.6561,
  "volume":        34473048,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-11-23T00:  00:  00-05:  00",
  "tradingDay":    "2015-11-23",
  "open":          118.6263,
  "high":          119.0838,
  "low":           116.7067,
  "close":         117.1145,
  "volume":        32658758,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-11-24T00:  00:  00-05:  00",
  "tradingDay":    "2015-11-24",
  "open":          116.6968,
  "high":          118.7059,
  "low":           116.4879,
  "close":         118.2384,
  "volume":        43027920,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-11-25T00:  00:  00-05:  00",
  "tradingDay":    "2015-11-25",
  "open":          118.5666,
  "high":          118.5865,
  "low":           117.2836,
  "close":         117.393,
  "volume":        21504358,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-11-27T00:  00:  00-05:  00",
  "tradingDay":    "2015-11-27",
  "open":          117.6516,
  "high":          117.771,
  "low":           116.9653,
  "close":         117.1742,
  "volume":        13117193,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-11-30T00:  00:  00-05:  00",
  "tradingDay":    "2015-11-30",
  "open":          117.3532,
  "high":          118.7655,
  "low":           117.1145,
  "close":         117.6615,
  "volume":        39392904,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-01T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-01",
  "open":          118.1091,
  "high":          118.1688,
  "low":           116.2293,
  "close":         116.7067,
  "volume":        35041416,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-02T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-02",
  "open":          116.4183,
  "high":          117.4726,
  "low":           115.4535,
  "close":         115.6524,
  "volume":        33567664,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-03T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-03",
  "open":          115.921,
  "high":          116.1597,
  "low":           113.6036,
  "close":         114.5783,
  "volume":        41795068,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-04T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-04",
  "open":          114.6678,
  "high":          118.6064,
  "low":           114.4888,
  "close":         118.3876,
  "volume":        58090412,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-07T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-07",
  "open":          118.3379,
  "high":          119.2131,
  "low":           117.1742,
  "close":         117.6416,
  "volume":        32258296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-08T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-08",
  "open":          116.8857,
  "high":          117.9599,
  "low":           116.2293,
  "close":         117.5919,
  "volume":        34495572,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-09T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-09",
  "open":          117.0051,
  "high":          117.0548,
  "low":           114.4589,
  "close":         114.996,
  "volume":        46612868,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-10T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-10",
  "open":          115.4137,
  "high":          116.3089,
  "low":           114.8866,
  "close":         115.543,
  "volume":        29371216,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-11T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-11",
  "open":          114.5683,
  "high":          114.7672,
  "low":           112.241,
  "close":         112.5692,
  "volume":        47140516,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-14T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-14",
  "open":          111.5746,
  "high":          112.0719,
  "low":           109.1975,
  "close":         111.873,
  "volume":        65356324,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-15T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-15",
  "open":          111.3359,
  "high":          112.1912,
  "low":           109.7544,
  "close":         109.8937,
  "volume":        53612444,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-16T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-16",
  "open":          110.4706,
  "high":          111.3856,
  "low":           108.2128,
  "close":         110.7391,
  "volume":        56543564,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-17T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-17",
  "open":          111.4154,
  "high":          111.6442,
  "low":           108.3918,
  "close":         108.3918,
  "volume":        45015748,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-18T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-18",
  "open":          108.3222,
  "high":          108.9289,
  "low":           105.2389,
  "close":         105.4578,
  "volume":        96976672,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-21T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-21",
  "open":          106.701,
  "high":          106.7905,
  "low":           105.0002,
  "close":         106.7507,
  "volume":        47848840,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-22T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-22",
  "open":          106.8204,
  "high":          107.1386,
  "low":           105.8755,
  "close":         106.6513,
  "volume":        32967222,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-23T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-23",
  "open":          106.6911,
  "high":          108.2625,
  "low":           106.6214,
  "close":         108.0238,
  "volume":        32834506,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-24T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-24",
  "open":          108.4117,
  "high":          108.4117,
  "low":           107.3674,
  "close":         107.447,
  "volume":        13670379,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-28T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-28",
  "open":          107.0093,
  "high":          107.1088,
  "low":           105.6069,
  "close":         106.2435,
  "volume":        26849104,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-29T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-29",
  "open":          106.3827,
  "high":          108.8394,
  "low":           106.2833,
  "close":         108.1531,
  "volume":        31099040,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-30T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-30",
  "open":          107.994,
  "high":          108.1133,
  "low":           106.6016,
  "close":         106.7408,
  "volume":        25350516,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2015-12-31T00:  00:  00-05:  00",
  "tradingDay":    "2015-12-31",
  "open":          106.4325,
  "high":          106.4524,
  "low":           104.2543,
  "close":         104.6919,
  "volume":        41134300,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-01-04T00:  00:  00-05:  00",
  "tradingDay":    "2016-01-04",
  "open":          102.0562,
  "high":          104.8013,
  "low":           101.4495,
  "close":         104.7814,
  "volume":        68016376,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-01-05T00:  00:  00-05:  00",
  "tradingDay":    "2016-01-05",
  "open":          105.1793,
  "high":          105.2787,
  "low":           101.8573,
  "close":         102.1557,
  "volume":        56093636,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-01-06T00:  00:  00-05:  00",
  "tradingDay":    "2016-01-06",
  "open":          100.0173,
  "high":          101.8175,
  "low":           99.33101,
  "close":         100.1565,
  "volume":        68828760,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-01-07T00:  00:  00-05:  00",
  "tradingDay":    "2016-01-07",
  "open":          98.14743,
  "high":          99.5896,
  "low":           95.90957,
  "close":         95.92946,
  "volume":        81534440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-01-08T00:  00:  00-05:  00",
  "tradingDay":    "2016-01-08",
  "open":          98.01814,
  "high":          98.57511,
  "low":           96.23779,
  "close":         96.43671,
  "volume":        71182168,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-01-11T00:  00:  00-05:  00",
  "tradingDay":    "2016-01-11",
  "open":          98.43587,
  "high":          98.52538,
  "low":           96.81466,
  "close":         97.99824,
  "volume":        50009196,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-01-12T00:  00:  00-05:  00",
  "tradingDay":    "2016-01-12",
  "open":          100.0073,
  "high":          100.1466,
  "low":           98.30656,
  "close":         99.42052,
  "volume":        49420924,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-01-13T00:  00:  00-05:  00",
  "tradingDay":    "2016-01-13",
  "open":          99.77858,
  "high":          100.6439,
  "low":           96.77488,
  "close":         96.86439,
  "volume":        62778412,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-01-14T00:  00:  00-05:  00",
  "tradingDay":    "2016-01-14",
  "open":          97.43131,
  "high":          99.93771,
  "low":           95.22329,
  "close":         98.98289,
  "volume":        63512876,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-01-15T00:  00:  00-05:  00",
  "tradingDay":    "2016-01-15",
  "open":          95.68081,
  "high":          97.18266,
  "low":           94.84534,
  "close":         96.60579,
  "volume":        80267000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-01-19T00:  00:  00-05:  00",
  "tradingDay":    "2016-01-19",
  "open":          97.87889,
  "high":          98.11759,
  "low":           94.98459,
  "close":         96.13834,
  "volume":        53375768,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-01-20T00:  00:  00-05:  00",
  "tradingDay":    "2016-01-20",
  "open":          94.58675,
  "high":          97.66007,
  "low":           92.91582,
  "close":         96.26763,
  "volume":        72726904,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-01-21T00:  00:  00-05:  00",
  "tradingDay":    "2016-01-21",
  "open":          96.53617,
  "high":          97.35175,
  "low":           94.42761,
  "close":         95.78027,
  "volume":        52444440,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-01-22T00:  00:  00-05:  00",
  "tradingDay":    "2016-01-22",
  "open":          98.09769,
  "high":          100.9124,
  "low":           97.8391,
  "close":         100.8726,
  "volume":        66157448,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-01-25T00:  00:  00-05:  00",
  "tradingDay":    "2016-01-25",
  "open":          100.9721,
  "high":          100.982,
  "low":           98.67457,
  "close":         98.90333,
  "volume":        52075548,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-01-26T00:  00:  00-05:  00",
  "tradingDay":    "2016-01-26",
  "open":          99.39068,
  "high":          100.3355,
  "low":           97.54072,
  "close":         99.45036,
  "volume":        75484384,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-01-27T00:  00:  00-05:  00",
  "tradingDay":    "2016-01-27",
  "open":          95.52168,
  "high":          96.10849,
  "low":           92.83624,
  "close":         92.91582,
  "volume":        134093296,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-01-28T00:  00:  00-05:  00",
  "tradingDay":    "2016-01-28",
  "open":          93.28382,
  "high":          94.00987,
  "low":           91.89137,
  "close":         93.5822,
  "volume":        55980928,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-01-29T00:  00:  00-05:  00",
  "tradingDay":    "2016-01-29",
  "open":          94.27842,
  "high":          96.81466,
  "low":           93.8408,
  "close":         96.81466,
  "volume":        64766040,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-02-01T00:  00:  00-05:  00",
  "tradingDay":    "2016-02-01",
  "open":          95.94936,
  "high":          96.18806,
  "low":           94.88513,
  "close":         95.90957,
  "volume":        41165668,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-02-02T00:  00:  00-05:  00",
  "tradingDay":    "2016-02-02",
  "open":          94.90502,
  "high":          95.52168,
  "low":           93.77117,
  "close":         93.9701,
  "volume":        37559908,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-02-03T00:  00:  00-05:  00",
  "tradingDay":    "2016-02-03",
  "open":          94.48729,
  "high":          96.31735,
  "low":           93.57226,
  "close":         95.83,
  "volume":        46213612,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-02-04T00:  00:  00-05:  00",
  "tradingDay":    "2016-02-04",
  "open":          95.86,
  "high":          97.33,
  "low":           95.19,
  "close":         96.6,
  "volume":        46471600,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-02-05T00:  00:  00-05:  00",
  "tradingDay":    "2016-02-05",
  "open":          96.52,
  "high":          96.92,
  "low":           93.69,
  "close":         94.02,
  "volume":        46418000,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-02-08T00:  00:  00-05:  00",
  "tradingDay":    "2016-02-08",
  "open":          93.13,
  "high":          95.7,
  "low":           93.04,
  "close":         95.01,
  "volume":        54021300,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-02-09T00:  00:  00-05:  00",
  "tradingDay":    "2016-02-09",
  "open":          94.29,
  "high":          95.94,
  "low":           93.93,
  "close":         94.99,
  "volume":        44331100,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-02-10T00:  00:  00-05:  00",
  "tradingDay":    "2016-02-10",
  "open":          95.92,
  "high":          96.35,
  "low":           94.1,
  "close":         94.27,
  "volume":        42343600,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-02-11T00:  00:  00-05:  00",
  "tradingDay":    "2016-02-11",
  "open":          93.79,
  "high":          94.72,
  "low":           92.59,
  "close":         93.7,
  "volume":        50074700,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-02-12T00:  00:  00-05:  00",
  "tradingDay":    "2016-02-12",
  "open":          94.19,
  "high":          94.5,
  "low":           93.01,
  "close":         93.99,
  "volume":        40351300,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-02-16T00:  00:  00-05:  00",
  "tradingDay":    "2016-02-16",
  "open":          95.02,
  "high":          96.85,
  "low":           94.61,
  "close":         96.64,
  "volume":        49057900,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-02-17T00:  00:  00-05:  00",
  "tradingDay":    "2016-02-17",
  "open":          96.67,
  "high":          98.21,
  "low":           96.15,
  "close":         98.12,
  "volume":        44863200,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-02-18T00:  00:  00-05:  00",
  "tradingDay":    "2016-02-18",
  "open":          98.84,
  "high":          98.89,
  "low":           96.09,
  "close":         96.26,
  "volume":        39020900,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-02-19T00:  00:  00-05:  00",
  "tradingDay":    "2016-02-19",
  "open":          96,
  "high":          96.76,
  "low":           95.8,
  "close":         96.04,
  "volume":        35374100,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-02-22T00:  00:  00-05:  00",
  "tradingDay":    "2016-02-22",
  "open":          96.31,
  "high":          96.9,
  "low":           95.92,
  "close":         96.88,
  "volume":        34280700,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-02-23T00:  00:  00-05:  00",
  "tradingDay":    "2016-02-23",
  "open":          96.4,
  "high":          96.5,
  "low":           94.55,
  "close":         94.69,
  "volume":        31942600,
  "openInterest":  null
}, {
  "symbol":        "AAPL",
  "timestamp":     "2016-02-24T00:  00:  00-05:  00",
  "tradingDay":    "2016-02-24",
  "open":          93.98,
  "high":          96.38,
  "low":           93.32,
  "close":         96.1,
  "volume":        36237787,
  "openInterest":  null
}];
