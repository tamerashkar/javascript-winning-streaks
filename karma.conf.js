module.exports = function(config) {
  config.set({
    frameworks:  ['jasmine'],
    reporters:   ['spec'],
    browsers:    ['PhantomJS'],
    files:       [
      'node_modules/lodash/lodash.min.js',
      'src/collection.js',
      'src/calculator.js',
      'src/streaker.js',
      'mocks/*',
      'spec/*.spec.js',
    ]
  });
};
