# Streaker

## Prerequisite

 - lodash ^4.5.1

## Installation

Include `build/streaker.min.js` as a script tag.

```html
<script src="./build/streaker.js"></script>
```

Or, include it as a require.

```javascript
var Streaker = require('./build/streaker.js');
```

## Usage

Calculate the streaks of a collection based on a model value.

```javascript
var data = require('./mocks/raiders.json');
var streaks = Streaker(data);
var results = streaks.get(); // returns an array of the streaks in the data.
```

The `Streaker` object will expect your data to have columns of `date` and `score`.
If you're column names are different, you can override the defaults.

```javascript
var options = {
	columns: {
		date: 'timestamp',
		score: 'price'
	}
};

var streaks = Streaker(data, options);
```

**Build Your Own Compare Method**

The streaks comes equipped with a compare method, but you can override
it with your own method.

```javascript
var options = {
	compare: function(obj, key) {
		return obj.total_points - obj.points_against;
	}
}

var streaks = Streaker(data, options);
```

**Build Your Own Streak Object**

The `streak` object that gets created for every streak can also be modified.
By default, it will create an object like this

```javascript
{
	startDate: 'date',
	startPrice: 12,
	endDate: 'date',
	endPrice: 14,
	change: 2,
	streakLength: 2,
	pending: false,
	positive: true
}
```

You can modify what gets added to a streak on the `start` and `end` of the streak.

```javascript
var options = {
	streaks: {
		start: function(streak, today) {
			streak.startedOn = today.scheduled;
			return streak;
		},
		end: function(streak, today, pending) {
			streak.endedOn = today.scheduled;
			return streak;
		}
	}
}

var streaks = Streaker(data, options);

// The streak object will now look something like this
{
	startedOn: 'date',
	endedOn: 'date',
	streakLength: -1,
	pending: false,
	positive: false
}
```

**Note:** The calculator will automatically generate the `streakLength`, `pending`, and `positive` columns for you.

### Streak Collection

### Get Streaks
Returns an array of streaks.

```javascript
var result = streaker.get();
```

### Averages
Taking the average streak length.

```javascript
var avg = streaks.avg('streakLength');
```

### Winning/Losing Streaks
You can also add whether you want just winning or losing streaks

```javascript
var avg = streaks.winning().avg('streakLength');
```

**Note:** By using the `winning()` or `losing()` method,
the inner data will be altered to be ready for the next method.
So if you are attempting to run multiple tasks on the streaks,
you might want to store in a variable like so.

```javascript
var streaks = Streaker(data);

var winning = streaks.winning();
console.log(winning.avg('streakLength'));
console.log(winning.lists('streakLength'));

var losing = streaks.losing();
console.log(losing.avg('streakLength'));
console.log(losing.lists('streakLength'));
```

### Counting

You can count the streaks. In this next example, we'll
count the winning streaks.

```javascript
var count = streaks.winning().count();
```

A more complicated version of the `count` method is `countWhere`.

```javascript
// Return the count of the winning streaks
// where the streak length was for two days.
var count = streaks.winning().countWhere(function(streak) {
 return streak.streakLength === 2;
});
```
### Grouping

You can group the streaks by a column.

```javascript
var groups = streaks.groupBy('streakLength');
```

### Lists
List a column in the objects array.

```javascript
var list = streaks.lists('streakLength');
```

### Longest
Returns the longest streak.

```javascript
var streak = streaks.longest();
```

## Examples
Let's take some data for the `Oakland Raiders`, and let's find their best winning and losing streaks. The raider's data can be found in `mocks/raiders.json` file.

```javascript
var options = {
	columns: {
		date: 'scheduled'
	},
	compare: function(today, key, streak) {
		return (today.total_points-today.points_against);
	},
	streaks: {
		start: function(streak, today) {
			streak.startOn = today.scheduled;
			return streak;
		},
		end: function(streak, today, pending) {
			streak.endedOn = today.scheduled;
			return streak;
		}
	}
};

var streaks = Streaker(data, options);

// The longest, winning streak
console.log(streaks.winning().longest().streakLength); // 15

// The longest, losing streak
console.log(streaks.losing().longest(true).streakLength); // -19
```

Now let's alter our compare method, so that we only look at streaks in which they won or lost by 14 or more points.

```javascript
options.compare = function (today, key, streak) {
	var change = today.total_points - today.points_against;
	if (Math.abs(change) >= 14) {
		return change;
	}
	return 0;
};

var streaks = Streaker(data, options);

// The longest, winning streak of at least 14 points
console.log(streaks.winning().longest().streakLength); // 4

// The longest, losing streak of at least 14 points
console.log(streaks.losing().longest(true).streakLength); // -3

// The total number streaks where the streak was at least three
var count = streaks.countWhere(function(streak) {
	return streak.streakLength > 3;
});

console.log(count); // 3
```

You can find this example in `example.js` files and test it out yourself

```bash
$ npm install
$ npm run build
$ node example.js
```



