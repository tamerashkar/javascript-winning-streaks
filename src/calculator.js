/**
 * Merge defaults with user options
 * @source https://gist.github.com/cferdinandi/4f8a0e17921c5b46e6c4
 * @private
 * @param {Object} defaults Default settings
 * @param {Object} options User options
 * @returns {Object} Merged values of defaults and options
 */
var extend = function ( defaults, options ) {
    var extended = {};
    var prop;
    for (prop in defaults) {
        if (Object.prototype.hasOwnProperty.call(defaults, prop)) {
            extended[prop] = defaults[prop];
        }
    }
    for (prop in options) {
        if (Object.prototype.hasOwnProperty.call(options, prop)) {
            extended[prop] = options[prop];
        }
    }
    return extended;
};

/**
 * Based on some value in a model,
 * calculate the streaks of the collection.
 *
 * @param {array} data The data that will be analyzed for streaks.
 * @param {object} options The options that will override the defaults.
 * @return {array} streaks
 */
function Calculator(data, options) {
  if ( ! Array.isArray(data)) {
    throw new TypeError('The `data` parameter must be an array.');
  }

  var streaks = [],
      endedStreak = {},
      currentStreak = { streakLength: 0 },
      numberOfDays = data.length - 1;

  setOptions();
  options.sort(data);

  return calculate();

  /**
   * Calculate the winning/losing streaks.
   *
   * @return {array} streaks
   */
  function calculate() {
    var lastDay, streak;
    for (var i = 0; i < numberOfDays; i++) {
      if ( hasAStreakEnded(data[i], i) && endedStreak ) {
        streaks.push(endedStreak);
        endedStreak = {};
      }
    }

    // Check if we have an ongoing streak that hasn't ended yet.
    if (currentStreak.streakLength !== 0) {
      lastDay = data[numberOfDays];
      streak = onStreakEnd(lastDay, true);
      if (streak) streaks.push(endedStreak);
    }

    return streaks;
  }

  /**
   * Compare today and tomorrow to see if a current
   * streak has ended.
   *
   * @param {object} today
   * @param {integer} key
   * @return {boolean} didStreakEnd
   */
  function hasAStreakEnded(today, key) {
    var didStreakEnd = false;
    var comparison   = options.compare(today, key, currentStreak);

    if (comparison > 0) {
      // Check if we have ended a negative streak.
      didStreakEnd = analyzeCurrentStreak(today, currentStreak.streakLength < 0, true);
      currentStreak.streakLength++;
    } else if (comparison < 0) {
      // Check if we have a ended a positive streak.
      didStreakEnd = analyzeCurrentStreak(today, currentStreak.streakLength > 0, false);
      currentStreak.streakLength--;
    } else {
      // Since the comparison between the days neutral,
      // we can safely say that any existing streak has come to an end.
      if (currentStreak.streakLength !== 0) {
        onStreakEnd(today);
        didStreakEnd = true;
      }
      currentStreak.streakLength = 0;
    }

    return didStreakEnd;
  }

  /**
   * Determine's whether a current streak has ended.
   *
   * @param {object} today
   * @param {boolean} predicate
   * @param {boolean} isPositive
   * @return {boolean} didStreakEnd
   */
  function analyzeCurrentStreak(today, predicate, isPositive) {
      var didStreakEnd = false;
      // When we had a streak going in the other direction,
      // we'll want to end that streak, and begin a new one.
      // If we didn't have a streak in the current direction,
      // we will begin a new streak.
      if (predicate) {
        didStreakEnd = onStreakEnd(today);
        onStreakStart(today, isPositive);
      } else if (currentStreak.streakLength === 0) {
        onStreakStart(today, isPositive);
      }

      return didStreakEnd;
  }

  /**
   * On a streak's start, we'll verify a streak is actually
   * created and if not, we'll reset our current streak.
   *
   * @param {object} today
   * @param {boolean} isPositive
   */
  function onStreakStart(today, isPositive) {
    // Send our streak attributes to our streak constructer.
    currentStreak = options.streaks.start(currentStreak, today);
    // Check if a streak was passed back, if it wasn't,
    // then we'll assume the streak was rejected.
    // Instead we'll reset the current streak back to 0.
    if ( ! currentStreak) {
      currentStreak = {
        streakLength: 0
      };
    } else {
      // @TODO: Is this really necc, we shoudn't assume
      // that they want this variable. And since they
      // have the ability to decide, why force them?
      currentStreak.positive = isPositive;
    }
  }

  /**
   * On a streak's end, we'll end the current streak
   * and reset the clock.
   *
   * @param {object} today
   * @param {boolean} [pending]
   * @return {object} endedStreak
   */
  function onStreakEnd(today, pending) {
    // We'll point the current streak to the ended streak,
    // and reset the current streak.
    endedStreak                = currentStreak;
    currentStreak              = {};
    currentStreak.streakLength = 0;
    endedStreak                = options.streaks.end(endedStreak, today, pending);

    return endedStreak ? true : false;
  }

  /**
   * Extend the default calculator options with the user options.
   *
   * @return {object}
   */
  function setOptions() {
    var defaultOptions = {
      sort:     _sort,
      compare:  _compare,
      streaks:  {
        start:  _start,
        end:    _end
      },
      columns:  {
        score:  'score',
        date:   'date'
      }
    };

    options = extend(defaultOptions, options);

    return options;
  }

  /**
   * Sort the array by the datetime. This can overriden by setting
   * a `sort` object to the `options` object.
   *
   * @param {array} data
   */
  function _sort(data) {
    data.sort(function(a, b) {
      var dateMapping  = _getMapping('date');
      var aVal         = new Date(a[dateMapping]);
      var bVal         = new Date(b[dateMapping]);
      if (aVal>bVal) {
        return 1;
      }
      if (aVal<bVal) {
        return -1;
      }
      return 0;
    });
  }

  /**
   * Our default start streak method that creates the
   * streak object. This can overriden by setting
   * a `streaks` object with an `start` method on the `options` object.
   *
   * @param {object} today
   * @param {object} streak
   * @return {object|boolean} streak
   */
  function _start(streak, today) {
    streak.startDate  = today[_getMapping('date')];
    streak.startPrice = today[_getMapping('score')];

    return streak;
  }

  /**
   * Our default end streak method that ends the streak, and
   * touches up the streak object. This can overriden by setting
   * a streaks object with an `end` method on the `options` object.
   *
   * @param {object} today
   * @param {object} streak
   * @param {boolean} [pending]
   * @return {object|boolean} streak
   */
  function _end(streak, today, pending) {
    var dateColumn    = _getMapping('date'),
        scoreColumn   = _getMapping('score'),
        pendingStreak = pending || false;

    // We might want to "temporarily" end a pending streak
    // so it still shows up in the streaks  with it's pending
    // attibute set to true.
    streak.pending    = pending || false;
    streak.endDate    = today[dateColumn];
    streak.endPrice   = today[scoreColumn];
    streak.change     = streak.endPrice - streak.startPrice;

    return streak;
  }

  /**
   * Default calculator compare method.
   * Can be overridden by setting the
   * the compare attribute on the options object.
   *
   * @param {object} today
   * @param {integer} tomorrow
   * @param {object} streak
   * @return {integer}
   */
  function _compare(today, key, streak) {
    var scoreColumn = _getMapping('score');
    var tomorrow = data[key+1];

    return tomorrow[scoreColumn] - today[scoreColumn];
  }

  /**
   * Return the mapping of the columns on the data object.
   *
   * @param {string} column
   * @return {string}
   */
  function _getMapping(column) {
    return options.columns ? options.columns[column] : false;
  }
}

