/**
 * Construct the Streaker object.
 *
 * @param {array} data The data that will be analyzed for streaks.
 * @param {object} options The options that will override the defaults.
 * @return {Collection}
 */
function Streaker(data, options) {
  var streaker       = {};
  streaker.data      = data;
  streaker.options   = options;
  streaker.calculate = Calculator;

  // Calculate and set the the streaks.
  streaker.streaks   = streaker.calculate(
      streaker.data,
      streaker.options
  );

  return new Collection(streaker.streaks);
}

