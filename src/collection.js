/**
 * Construct the streaks collection module.
 *
 * @constructor
 * @param {array} streaks The array of streaks
 */
function Collection(streaks) {
  var collection = this;
  collection.streaks = streaks;
}

/**
 * Return the average of a given column in the streak objects.
 *
 * @param {string} column The column to average
 * @return float
 */
Collection.prototype.avg = function avg(column) {
  var array = this.lists(column);
  var length = array.length;
  if ( ! length) return false;
  var count = array.reduce(function(a, b) { return a + b; }, 0);

  return count / length;
};

/**
 * Count the streaks in a collection.
 *
 * @return integer
 */
Collection.prototype.count = function count() {
  return this.get().length;
};

/**
 * Count the streaks in a collection given the
 * predicate returns truthy.
 *
 * @param {string|array|object|function} [predicate]
 * @return integer
 */
Collection.prototype.countWhere = function countWhere(predicate) {
  var c = new Collection(_filter.call(this, predicate));

  return c.count();
};

/**
 * Returns a collection filtered by a predicate.
 *
 * @param {string|array|object|function} [predicate]
 * @return object
 */
Collection.prototype.filter = function(predicate) {
  return new Collection(_filter.call(this, predicate));
};

/**
 * Return the collection of streaks.
 *
 * @return array
 */
Collection.prototype.get = function getStreaks() {
  return this.streaks;
};

/**
 * Returns an array of the values of an object key from the collection
 *
 * @param {string} column The column to list
 * @return array
 */
 Collection.prototype.lists = function lists(column) {
  return _.map(this.get(), function(a) {
    return a[column];
  });
};

/**
 * Return the longest streak in the collection.
 *
 * @param {boolean} [isReversed=false] Pass `true` to get the longest negative streak.
 * @param {string} [column = streakLength]
 * @return object
 */
Collection.prototype.longest = function longest(isReversed, column) {
  isReversed = isReversed || false;
  column = column || 'streakLength';

  if (isReversed) {
    return _.minBy(this.get(), function(s) {
      return s[column];
    });
  }

  return _.maxBy(this.get(), function(s) {
    return s[column];
  });
};

/**
 * Returns a collection of losing streaks.
 *
 * @return object
 */
Collection.prototype.losing = function losing() {
  return new Collection(_filter.call(this, ['positive', false]));
};

/**
 * Returns a collection grouped by the column.
 *
 * @param {string} column The column to group by
 * @return object
 */
Collection.prototype.groupBy = function groupBy(column) {
  return new Collection(_group.call(this, column));
};

/**
 * Returns a collection of winning streaks.
 *
 * @return object
 */
Collection.prototype.winning = function winning() {
  return new Collection(_filter.call(this, 'positive'));
};

/**
 * Filter the streaks by the given predicate.
 *
 * @private
 * @param {string|array|object|function} [predicate]
 * @return array
 */
function _filter(predicate) {
  return _.filter(this.get(), predicate);
}

/**
 * Group the streaks by a given predicate.
 *
 * @private
 * @param {string|array|object|function} [predicate]
 * @return array
 */
function _group(predicate) {
  return _.groupBy(this.get(), predicate);
}

/**
 * Set the streaks of the collection.
 *
 * @private
 * @param array
 */
function _set(streaks) {
  collection.streaks = streaks;
}

