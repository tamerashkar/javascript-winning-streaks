var data     = require('./mocks/raiders.json');
var _        = require('lodash');
var Streaker = require('./build/streaker.js');
var options  = {
  columns: {
    date: 'scheduled'
  },
  compare: function(now, next) {
    return (now.total_points-now.points_against);
  },
  streaks: {
    start: function(streak, today) {
      streak.startOn = today.scheduled;
      return streak;
    },
    end: function(streak, today, pending) {
      streak.endedOn = today.scheduled;
      return streak;
    }
  }
};

var streaks = Streaker(data, options);

// The longest, winning streak
console.log(streaks.winning().longest());

// The longest, losing streak
console.log(streaks.losing().longest(true));


options.compare = function (a, obj) {
  var change = a.total_points - a.points_against;
  if (Math.abs(change) >= 14) {
    return change;
  }
  return 0;
};

var streaks = Streaker(data, options);

// The longest, winning streak of at least 14 points
console.log(streaks.winning().longest());

// The longest, losing streak of at least 14 points
console.log(streaks.losing().longest(true));

// The total number streaks where the streak was at least three
var count = streaks.countWhere(function(streak) {
  return streak.streakLength > 3;
});

console.log(count);
